<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('front/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('front/css/font-awesome-all.min.css')}}" rel="stylesheet">
    <link href="{{ asset('front/css/custom.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('front/css/owl.carousel.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('front/css/owl.theme.default.min.css')}}" rel="stylesheet" type="text/css"> 
    <link href="{{ asset('front/css/animate.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('admin/plugins/select2/select2.min.css')}}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.datetimepicker.min.css')}}">
    @yield('styles')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="{{ asset('front/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('front/js/font-awesome-all.min.js')}}"></script>
    <script src="{{ asset('front/js/custom.js')}}"></script>
    <script src="{{ asset('front/js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('front/js/wow.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.0/jquery.waypoints.min.js"></script>
    <script src="{{ asset('front/js/jquery.counterup.js')}}"></script>
    <script src="{{asset('admin/plugins/select2/select2.full.min.js')}}"></script>      
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
    <script src="{{asset('js/jquery.datetimepicker.full.min.js')}}"></script>
<style>
    .rate {
    float: left;
    height: 46px;
    padding: 0 10px;
}
.rate:not(:checked) > input {
    position:absolute;
    top:-9999px;
}
.rate:not(:checked) > label {
    float:right;
    width:1em;
    overflow:hidden;
    white-space:nowrap;
    cursor:pointer;
    font-size:30px;
    color:#ccc;
}
.rate:not(:checked) > label:before {
    content: '★ ';
}
.rate > input:checked ~ label {
    color: #ffc700;    
}
.rate:not(:checked) > label:hover,
.rate:not(:checked) > label:hover ~ label {
    color: #deb217;  
}
.rate > input:checked + label:hover,
.rate > input:checked + label:hover ~ label,
.rate > input:checked ~ label:hover,
.rate > input:checked ~ label:hover ~ label,
.rate > label:hover ~ input:checked ~ label {
    color: #c59b08;
}
    </style>
</head>
<body>
    <input type="hidden" value="{{auth()->id()}}" id="loggedIn_id">
    @yield('content')
 <div class="modal" id="web-notification">
  <div class="modal-dialog">
      <div class="modal-content web-push-modal">
          <!-- Modal body -->
          <div class="modal-body">
             <div class="notification_box">
              <div class="nti-inner">
                <p class="nti-title">Allow Web Notifications ?</p>
                <a class="nti-btn btn-accept" onclick="initFirebaseMessagingRegistration();" href="javascript:void(0)"><i class="fa fa-check" aria-hidden="true"></i></a>
                <a class="nti-btn btn-decline" href="javascript:void(0)" onclick="closeNotification()" ><i class="fa fa-times" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>
      </div>
  </div>
</div>  
    <!-- <script src="https://cdn.socket.io/4.5.0/socket.io.min.js" integrity="sha384-7EyYLQZgWBi67fBtVxw60/OWl1kjsfrPFcaU0pp0nAh+i8FD068QogUvg85Ewy1k" crossorigin="anonymous"></script> -->
    <!-- <script src="https://www.gstatic.com/firebasejs/7.23.0/firebase.js"></script> -->
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-database.js"></script>
    <script src="{{asset('js/firebase-chat.js')}}"></script>
    @yield('scripts')
    @if(empty(auth()->user()->fcm_token))
      <script type="text/javascript">
        $(document).ready(function() { 
          //$('#web-notification').modal('show');
        });
      </script>      
    @endif
    <script>
        function closeNotification(){
            jQuery('#web-notification').modal('hide');
        }
        //active-menu---------->
        var pgurl = window.location.href.substr(window.location.href.indexOf("/") + 1);
        var myarr = pgurl.split("/");
        var activepart = myarr[myarr.length - 1];

        $(".activelink").each(function() {
          var title = $(this).attr("data-title");
          //console.log(activepart,title)
          if (activepart == title)
            $(this).parent().addClass("current-menu-item");
        });

       $('.select2').select2(); 

       new WOW().init();
    // check slected lesoon option
        function myChangeFunction(status) {
            //console.log(status.name)
            var idname = status.name;
            $("#"+idname).html("Attached").css('color','#00A300');
            lesson.value=1;
            unitlesson.value=1;
         }
         $('.wrap-menu').on('click',function(){
            $('.wrap-menu').toggleClass('wrap-menu-show','');
            $('.sub-menu').toggleClass('sub-menu-show','');
         })

        $('.fileUpload').on('click', function() {
            var inputFile = $(this).closest('.custom-input-file').find("input[type=file]");
            var inputTxt = $(this).closest('.custom-input-file').find("input[type=text]");
            inputFile.trigger('click');

            inputFile.on('change', function() {
                var fileName = $(this)[0].files[0].name;    
                inputTxt.val(fileName);
            });
        });

        function preventBack() {
            window.history.forward(); 
        }
          
        setTimeout("preventBack()", 0);
          
        window.onunload = function () { null };

        $('.favorite').on('click', function() {
            var element = this;
            var lesson_id = $(element).attr("data-id");
            var document_type = $(element).attr("data-doctype");
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{route('toggle.favorite')}}",
                type: "POST",
                data: {
                    lesson_id: lesson_id,
                    document_type: document_type
                },
                cache: false,
                success: function(result){
                  if (result == 1) {
                    toastr.success('Add to favorite successfully.');
                    $(element).toggleClass('white-box yellow-box');
                  } else {
                    toastr.error('Removed from favorite successfully.');
                    $(element).toggleClass('yellow-box white-box');
                  }
                }
            });       
        
        });


        $('.follower').on('click', function() {
            var element = this;
            var user_id = $(element).attr("data-id");
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{route('toggle.follower')}}",
                type: "POST",
                data: {
                    follower_id: user_id,
                    
                },
                cache: false,
                success: function(result){
                  if (result == 1) {
                    toastr.success('Following successfully.');
                    $('#followerimg').attr('src', '{{ asset("front/images/binaclr2.png")}}');
                    location.reload();
                  } else {
                    toastr.error('Removed from Following successfully.');
                    $('#followerimg').attr('src', '{{ asset("front/images/binaclr.png")}}');
                    location.reload();
                  }
                }
            });       
        
        });

        
        $(document).ready(function() {
            toastr.options.timeOut = 5000;
            @if(count($errors) > 0)
                @foreach($errors->all() as $error)
                    toastr.error("{{ $error }}");
                @endforeach
            @endif

            @if (Session::has('success'))
                toastr.success('{{ Session::get('success') }}');
            @endif
        });

        $('#search-form').on('click', function() {
    
        var number = $('input[name=search]');
        //console.log(number);
        });
    </script>
</body>
</html>
