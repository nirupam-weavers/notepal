@extends('admin.layouts.admin-app')

@section('content')
 <!-- Content Wrapper. Contains page content -->

<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Specific Expectation <small>Specific Expectation List</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Specific Expectation</a></li>
      <li class="active">Specific Expectation List</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Specific Expectation List</h3>
            <a href="{{route('admin.specificexpectation.create')}}" class="btn btn-primary a-btn-slide-text" style="float:right"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <span><strong>Add</strong></span> </a> 
            </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="category_table" class="table table-bordered table-hover">
              <thead>
                <tr>
                   <th><b>ID</b></th>
                   <th><b>Grade</b></th>
                   <th><b>Subject</b></th>
                   <th><b>Strand</b></th>
                   <th><b>Specific Expectation</b></th>
                   <th align="center">Action</th>
                </tr>
              </thead>
              <tbody>
               @if(!empty($expectations))
               @php $i=1; @endphp
                 @foreach($expectations as $expectation)                 
                  <tr>
                   <td>{{ $i }}</td>
                   <td>
                    @if($expectation->grade)
                    {{$expectation->grade->grade}}
                    @endif
                   </td>
                   <td>
                    @if($expectation->subject)
                    {{$expectation->subject->subject}}
                    @endif
                   </td>
                   <td>
                    @if($expectation->strand)
                    {{$expectation->strand->strand}}
                    @endif
                   </td>
                   <td>{{$expectation->expectation}}</td>
                  <td><a class="btn btn-primary" href="{{ url('admin/specificexpectation-edit/'.$expectation->id) }}">Edit</a>
                    <a class="btn btn-danger" href="{{ url('admin/specificexpectation-delete/'.$expectation->id) }}" onclick="return confirm('Do you want to remove?')">Delete</a>
                  </td>
                 </tr>
                 @php $i++; @endphp
                 @endforeach
               @endif
             
              </tbody>
              
            </table>
          </div>
          <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
      <!-- /.col --> 
    </div>
    <!-- /.row --> 
  </section>
  <!-- /.content --> 
</div>
<!-- /.content-wrapper -->
@endsection

 