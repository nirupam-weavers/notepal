<div class="dashbord-common-left-menu dashbord-left-menu-teacher">
  <div class="dashbord-left-menu main-menu">
    <div class="nav_close" onclick="menu_close()">
      <i class="far fa-times-circle"></i>
    </div>
    <div class="dashbord-left-menu-wraper">
      <div class="logo">
      <a href="{{ route('teacher.home') }}"> <img src="{{ asset('front/images/logo.png')}}"></a>
      </div>
      <div class="dashbord-menu-mail">
        <div class="dashbord-menu-list">
          <ul>
            <li>
              <a href="{{ route('teacher.mylesson') }}" data-title="" class="activelink"><span><i class="far fa-list-alt"></i></span>My Lessons</a>
            </li>
            <li>
              <a href="{{ route('teacher.units') }}" data-title="" class="activelink"><span><i class="far fa-list-alt"></i></span>My Units</a>
            </li>
            <li>
              <a href="{{ route('teacher.lesson') }}" data-title="lesson" class="activelink">
                <span><i class="far fa-lightbulb"></i></span> Lessons</a>
            </li>
            <li>
              <a href="{{ route('teacher.independentpractice') }}" data-title="independentpractice" class="activelink">
                <span><i class="far fa-file-archive"></i></span> Independent Practice</a>
            </li>
            <li>
              <a href="{{ route('teacher.assessment') }}" data-title="assessment" class="activelink"><span><i class="far fa-file-archive"></i></span> Assessments </a>
            </li>
            <li>
              <a href="{{ route('teacher.favorite') }}" data-title="favorite" class="activelink"><span><i class="far fa-star"></i></span> Favourites</a>
            </li>
            <li>
              <a href="{{ route('teacher.calendar') }}" data-title="calendar" class="activelink"><span><i class="far fa-calendar"></i></span> Calendar  </a>
            </li>
            <li><a href="{{route('teacher.notes')}}"><span><i class="far fa-clone"></i></span> Notes</a></li>
            <!-- <li><a href=""><span><i class="far fa-sticky-note"></i></span> Report Card</a></li> -->
            <li class="wrap-menu">
              <a href="javascript:void(0);" data-title="search-users" class="activelink"><span><i class="fas fa-search"></i></span> Search Users</a>
              <ul class="sub-menu">
                <li><a href="{{ route('teacher.teacher.search') }}">Teachers</a></li>
                <li><a href="{{ route('teacher.parent.search') }}">Parents</a></li>
                <li><a href="{{ route('teacher.student.search') }}">Students</a></li>
              </ul>
            </li>
            <!-- <li><a href="{{ route('teacher.teacher.search') }}"><span><i class="fas fa-search"></i></span> Search Teachers</a></li>
            <li><a href="{{ route('teacher.parent.search') }}"><span><i class="fas fa-search"></i></span> Search Parents</a></li>
            <li><a href="{{ route('teacher.student.search') }}"><span><i class="fas fa-search"></i></span> Search Students</a></li> -->
            <li><a href="{{ route('teacher.chat') }}"><span><i class="far fa-comments"></i></span> Chat</a></li>
            <li>
              <a href="{{ route('teacher.updateprofile') }}" data-title="updateprofile" class="activelink"><span><i class="far fa-edit"></i></span> Update profile</a>
            </li>
            <li><a href="{{ route('teacher.logout') }}" onclick="event.preventDefault();
                                                       document.getElementById('logout-form').submit();"><span><i class="fas fa-lock"></i></span> Log Out</a>
              <form id="logout-form" action="{{ route('teacher.logout') }}" method="POST" class="d-none">
                @csrf
              </form>
            </li>
          </ul>
        </div>
        <div class="dashbord-last-btn">
          <a href="{{ route('lesson.create') }}" class="btn create-btn activelink" data-title=""><span><i class="fas fa-plus"></i></span> Create Lesson</a>
        </div>
      </div>
    </div>
  </div>
  <div onclick="menu_open()" class="nav_btn">
    <i class="fas fa-bars"></i>
  </div>
</div>
<div class="dashbors-right-content dashbors-right-content-teacher">
  <div class="dashbord-right-top-head">
    <div class="dashbord-right-top-head-wraper">
      <p>Welcome to the teachers portal!</p>
      <div class="dashbord-right-user">
        <ul>
          <li><a href="{{ route('teacher.notification') }}"><span><i class="far fa-bell"></i></span> Notifications</a></li>
          <li><a href="{{ route('teacher.updateprofile') }}">
              <div class="name-user">
                @if(auth()->user()->user_image)
                <img class="name-user" src="{{ asset('storage/user/'.auth()->user()->user_image) }}" />
                @else
                <?php $name = auth()->user()->name;
                echo $name[0] ?>
                @endif
              </div>
            </a></li>
        </ul>
      </div>
    </div>
  </div>