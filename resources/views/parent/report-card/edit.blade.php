
@extends('parent.layouts.app')
@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('parent.layouts.sidebar')       
        <div class="dashbord-content-common ash-bg dashbord-parent-children">
          <div class="dashbord-content-common-pad">
            <form action="{{route('parent.report.card.update')}}" method="POST">
              @csrf
              <div class="my-children-header">
                <div class="my-children-text">
                  <h2>Edit Report Card</h2>
                </div>
                <div class="my-children-add">
                    <div class="form-group">
                      <select class="form-control" name="user_id">
                        <option value="">Select Child</option>
                        @foreach($children as $child)
                        <option value="{{$child->id}}" @if(app('request')->input('user_id')==$child->id) selected @endif>{{$child->name}} <span>( Current Grade: Grade {{$child->grade}} )</span></option>
                        @endforeach
                      </select>
                    </div>
                    <!-- <a href="" class="btn yellow-btn">Add Child</a> -->
                </div>
              </div>
              <div class="my-children-view-area">
                <div class="my-children-search-box">
                    <div class="row">
                      <div class="col-lg-3">
                          <div class="form-group">
                            <label for="exampleFormControlSelect1">Year <span>*</span></label>
                            <select class="form-control" name="year">
                                @for($i=2015;$i<=2025;$i++)
                                <option value="{{$i}}">{{$i}}-{{$i+1}}</option>
                                @endfor
                            </select>
                          </div>
                      </div>
                      <div class="col-lg-3">
                          <div class="form-group">
                            <label for="exampleFormControlSelect1">Grade <span>*</span></label>
                            <select class="form-control" name="grade" id="grade">
                            @foreach($grades as $grade)
                            <option value="{{$grade->grade}}" @if(old('grade') !== null && ($grade->grade == old('grade'))) selected="selected" @endif>Grade {{$grade->grade}}</option>
                            @endforeach
                            </select>
                          </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Report <span>*</span></label>
                            <select class="form-control" name="type">
                              <option value="progress_report">Progress Report</option>
                              <option value="term_1">Term 1</option>
                              <option value="term_2">Term 2</option>
                            </select>
                          </div>
                      </div>
                    </div>
                </div>
              </div>
              <div class="lnguage-btn">
                <ul>
                  <li><button type="submit" class="btn">Submit</button></li>
                </ul>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection