@extends('layouts.app')

@section('content')
<header class="main-header">
    <div class="container-fluid header-row">
      <div class="logo">
        <a href="#"><img src="images/logo.png" alt=""></a>
      </div>
      <div class="login-btn">
        <a href="" class="btn">Sign Up</a>
      </div>
    </div>
</header>

<div class="parents-signup-page">
  <div class="container-fluid">
    <div class="row full-hight">
      <div class="col-lg-6">
        <div class="parent-signup-frm">
            <h3>Forgot Password?</h3>
            <p>Please enter your mail id to reset the password</p>
            <form method="POST" action="{{ route('parent.password.update') }}">
             @csrf
             <input type="hidden" name="token" value="{{ $token }}">
             <input type="hidden" name="email" value="{{ $email }}">
                <div class="form-group">
                    <label for="password">{{ __('Password') }}</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="password-confirm">{{ __('Confirm Password') }}</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                </div>
                  <div class="login-btn">
                    <!-- <a href="" class="btn">{{ __('Send Password Reset Link') }}</a> -->
                    <button type="submit" class="btn">
                        {{ __('Reset Password') }}
                    </button>
                  </div>
            </form>
        </div>
      </div>
      <div class="col-lg-6 red-bg reletive-div">
        <div class="parent-signup-text">
          <h3>Platform for parents</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, </p>
          <p>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
        </div>
        <div class="overlap-img">
            <img src="images/img1.png" alt="">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
