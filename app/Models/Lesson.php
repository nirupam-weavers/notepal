<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    use HasFactory;

    public function documents()
    {
        return $this->hasMany(Document::class);
    }
    public function rating()
    {
        return $this->hasMany(Rating::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function expectation()
    {
        return $this->belongsTo(Expectation::class);
    }

    public function strand()
    {
        return $this->belongsTo(Strand::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function grade_lesson()
    {
        return $this->belongsToMany(Grade::class, 'grade_lessons','lesson_id','grade_id');
    }

    public function expectation_lesson()
    {
        return $this->belongsToMany(Expectation::class, 'expectation_lessons','lesson_id','expectation_id');
    }

    public function userFavorites()
    {
         return $this->belongsToMany(User::class, 'favorites');
    }

    public function getAvgRatingAttribute($avg_rating)
    {
      return round($avg_rating,1);
    }

    public function unit_lessons()
    {
        return $this->hasMany(UnitLesson::class);
    }
}
