<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Availability extends Model
{
    use HasFactory;
   protected $fillable = [
        'user_id',
        'day',
        'slot_id',
        'start_time',
        'end_time',
    ];
}
