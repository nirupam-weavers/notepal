@extends('admin.layouts.admin-app')

@section('content')

<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Marks <small>Entry Form</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Marks</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row"> 
      <!-- left column -->
      <div class="col-md-12"> 
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add</h3>
            @if ($errors->any())
                  <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              @if (session()->has('message'))
                  <div class="alert alert-info alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      {{ session('message') }}
                  </div>
              @endif
              @if (session()->has('error_message'))
                  <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      {{ session('error_message') }}
                  </div>
              @endif
          </div>
          <!-- /.box-header --> 
          <!-- form start -->
          <form name="entry-form" method="post" action="{{route('admin.marks.store')}} " enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="box-body"> 
              <div class="form-group">
                <label for="name">Marks Name<a class='lnkred'>*</a></label>
                <input type="text" class="form-control" name="name" placeholder="Marks Name *">
              </div>
              <div class="form-group">
                <label for="from">Marks From<a class='lnkred'>*</a></label>
                <input type="number" class="form-control" name="from" placeholder="Marks From *">
              </div>
              <div class="form-group">
                <label for="to">Marks To<a class='lnkred'>*</a></label>
                <input type="number" class="form-control" name="to" placeholder="Marks To *">
              </div>
              <div class="form-group">
                <label for="percentage">Marks Percentage<a class='lnkred'>*</a></label>
                <input type="number" class="form-control" name="percentage" placeholder="Percentage *">
              </div>
            </div>
            <!-- /.box-body -->            
            <div class="box-footer">
              <input type="submit" class="btn btn-primary" value="Submit">
            </div>
          </form>
        </div>
        <!-- /.box --> 
        
      </div>
      <!--/.col (left) --> 
    </div>
    <!-- /.row --> 
  </section>
  <!-- /.content --> 
</div>

@endsection