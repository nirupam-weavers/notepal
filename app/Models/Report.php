<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use HasFactory;
    protected $fillable = [
       'progress_report','term_1','term_2'
    ];

    public function category()
    {
    	return $this->belongsTo(Category::class);
    }
}
