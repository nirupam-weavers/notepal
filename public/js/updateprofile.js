imgInp.onchange = evt => {
  const [file] = imgInp.files
  if (file) {
    blah.src = URL.createObjectURL(file)
  }
}

$(function() {
    $("form[name='updateprofile']").validate({
      // Specify validation rules
      rules: {
        name: "required",
        age: "required",
        bio: "required",
      },
      // Specify validation error messages
      messages: {
      
        name: "Please enter your name",
        bio: "Please enter your bio",
        age: "Please enter your age",
      },
      
      submitHandler: function(form) {
        form.submit();
      }
    });
  });
  