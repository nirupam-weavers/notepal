@extends('student.layouts.app')

@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('student.layouts.sidebar')
      <div class="dashbord-content-common ash-bg dashbord-parent-children">
          <div class="dashbord-content-common-pad">
            <div class="student-independnt-practic-sec">
              <h2>Lessons:</h2>
              @foreach($favorites as $favorite)
              @if($favorite->document_type == "lesson")
              <div class="student-independnt-practic-sngl-box">
                <div class="roted-box">
                  <h3>Grade: {{implode(',',$favorite->lesson->grade_lesson()->pluck('grade_id')->all())}}</h3>
                </div>
                <ul class="top-box">
                  <li><span>Subject:</span> {{$favorite->lesson->subject->subject}}</li>
                  <li><span>Strand:</span> {{$favorite->lesson->strand->strand}}</li>
                  <li><span>BY:</span> {{auth()->user()->name}} / {{ date('d-m-Y', strtotime($favorite->lesson->created_at))}}</li>
                </ul>
                <p>{{$favorite->lesson->expectation->expectation}}</p>
                <div class="student-independnt-btm">
                  <div class="row align-items-center">
                    <div class="col-lg-6">
                      <ul class="dnld-sec">
                        <li>
                          <ul class="star-rating">
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li>4.9</li>
                          </ul>
                        </li>
                        <li><span><i class="fas fa-download"></i></span> 100</li>
                        <li><span><i class="far fa-eye"></i></span> 100</li>
                      </ul>
                    </div>
                    <div class="col-lg-6">
                      <ul class="indepnd-action-box">
                        <li class="blue-box"><a href=""><i class="fas fa-share-alt"></i></a></li>
                        <li class="yellow-box"><a href=""><i class="fas fa-star"></i></a></li>
                        <li class="white-box"><a href="{{ url('student/single-lesson/'.$favorite->lesson->id) }}"><i class="fas fa-chevron-right"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              @endif
              @endforeach
            </div>
            <div class="student-independnt-practic-sec">
              <h2>Independent Practice:</h2>
              @foreach($favorites as $favorite)
              @if($favorite->document_type == "practice")
              <div class="student-independnt-practic-sngl-box">
                <div class="roted-box">
                  <h3>grade: {{implode(',',$favorite->lesson->grade_lesson()->pluck('grade_id')->all())}}</h3>
                </div>
                <ul class="top-box">
                  <li><span>Subject:</span> {{$favorite->lesson->subject->subject}}</li>
                  <li><span>Strand:</span> {{$favorite->lesson->strand->strand}}</li>
                  <li><span>BY:</span> {{auth()->user()->name}} / {{ date('d-m-Y', strtotime($favorite->lesson->created_at))}}</li>
                </ul>
                @if($favorite->lesson->expectation)
                <p>{{$favorite->lesson->expectation->expectation}}</p>
                @endif
                <div class="student-independnt-btm">
                  <div class="row align-items-center">
                    <div class="col-lg-6">
                      <ul class="dnld-sec">
                        <li>
                          <ul class="star-rating">
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li>4.9</li>
                          </ul>
                        </li>
                        <li><span><i class="fas fa-download"></i></span> 100</li>
                        <li><span><i class="far fa-eye"></i></span> 100</li>
                      </ul>
                    </div>
                    <div class="col-lg-6">
                      <ul class="indepnd-action-box">
                        <li class="blue-box"><a href=""><i class="fas fa-share-alt"></i></a></li>
                        <li class="yellow-box"><a href=""><i class="fas fa-star"></i></a></li>
                        <li class="white-box"><a href="{{ url('student/single-independentpractice/'.$favorite->lesson->id) }}"><i class="fas fa-chevron-right"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              @endif
              @endforeach
            </div>
            <div class="student-independnt-practic-sec">
              <h2>Assessments:</h2>
              @foreach($favorites as $favorite)
              @if($favorite->document_type == "assessment")
              <div class="student-independnt-practic-sngl-box">
                <div class="roted-box">
                  <h3>grade: {{implode(',',$favorite->lesson->grade_lesson()->pluck('grade_id')->all())}}</h3>
                </div>
                <ul class="top-box">
                  <li><span>Subject:</span> {{$favorite->lesson->subject->subject}}</li>
                  <li><span>Strand:</span> {{$favorite->lesson->strand->strand}}</li>
                  <li><span>BY:</span> {{auth()->user()->name}} / {{ date('d-m-Y', strtotime($favorite->lesson->created_at))}}</li>
                </ul>
                <p>{{$favorite->lesson->expectation->expectation}}</p>
                <div class="student-independnt-btm">
                  <div class="row align-items-center">
                    <div class="col-lg-6">
                      <ul class="dnld-sec">
                        <li>
                          <ul class="star-rating">
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li>4.9</li>
                          </ul>
                        </li>
                        <li><span><i class="fas fa-download"></i></span> 100</li>
                        <li><span><i class="far fa-eye"></i></span> 100</li>
                      </ul>
                    </div>
                    <div class="col-lg-6">
                      <ul class="indepnd-action-box">
                        <li class="blue-box"><a href=""><i class="fas fa-share-alt"></i></a></li>
                        <li class="yellow-box"><a href=""><i class="fas fa-star"></i></a></li>
                        <li class="white-box"><a href="{{ url('student/single-assessment/'.$favorite->lesson->id) }}"><i class="fas fa-chevron-right"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              @endif
              @endforeach
            </div>
          </div>
        </div>
      @endsection