<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Comment;
use DB;

class CategoryController extends Controller
{
    public function category_list(Request $request)
    {
    	$category_list=Category::orderBy('updated_at', 'desc')->get();
        
        return view('admin.category.category_list',compact('category_list'));
    }
    public function category_add(){
    	$category=DB::table('categories')->get();
    	$comments=DB::table('comments')->get();
        return view('admin.category.add_category',compact('category','comments'));
    }
    public function category_create(Request $request){
        $this->validate($request,[
            'name' => ['required', 'string', 'max:255'],
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        
        $category = new Category;
        $category->name = $request->name;
        $category->parent_id = $request->parent_id;
        $category->status = $request->status;        
        if($category->save()){
            if(!empty($request->comments)){
                foreach($request->comments as $comment_id){
                    $category->category_comments()->attach($comment_id);
                }
            }
        }
        
        \Session::flash('message','Successfully added.'); 
        return redirect()->back();
    }
    public function category_edit($id)
    {
        $category=Category::findOrFail($id);
        $cat_comments = [];
        if($category->category_comments()){
            $cat_comments = $category->category_comments()->pluck('comments.id')->all();
        }
        //dd(in_array(0,$cat_comments));
        $comments=DB::table('comments')->get();
        $all_category=DB::table('categories')->where('id','!=',$id)->where('status','=',1)->get();
        
        return view('admin.category.edit_category',compact('category','all_category','comments','cat_comments'));
    }
    public function update_category(Request $request){
        $params = $request->all();
        $this->validate($request,[
            'category_id' => 'required'
        ]);
        
        $category = Category::where('id', $params['category_id'])->first();
        $category->name = $params['name'];
        $category->parent_id = $request->parent_id;
        $category->status = $request->status;
        if($category->save()){
            if(!empty($request->comments)){
                DB::table('category_comments')->where('category_id',$params['category_id'])->delete();
                foreach($request->comments as $comment_id){
                    $category->category_comments()->attach($comment_id);
                }
            }
        }

        
        \Session::flash('message','Successfully updated.'); 
        return redirect()->back();
    }
}
