<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AdminLoginController;
use App\Http\Controllers\Auth\AdminPasswordController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\AjaxController;
use App\Http\Controllers\Admin\AdminSettingsController;
use App\Http\Controllers\Admin\ContactEmailsController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CommentController;
use App\Http\Controllers\Admin\MarksController;
use App\Http\Controllers\Admin\NewsletterController;
use App\Http\Controllers\Admin\ProfileController as AdminProfileController;

Route::get('/login', [AdminLoginController::class, 'showLoginForm'])->name('admin.login');
Route::post('/login/post', [AdminLoginController::class, 'login'])->name('admin.login.post');

Route::get('forgot-password', [AdminPasswordController::class, 'showLinkForm'])->name('admin.password.form');
Route::post('email-password', [AdminPasswordController::class, 'passwordEmail'])->name('admin.password.email'); 
Route::get('reset-password/{token}/{email}', [AdminPasswordController::class, 'resetForm'])->name('admin.reset.password');
Route::post('update-password', [AdminPasswordController::class, 'passwordUpdate'])->name('admin.password.update');


Route::group(['middleware' => ['role:admin','auth']], function(){
	Route::get('home', [AdminController::class,'admin_dashboard'])->name('admin.home');
	Route::post('logout', [AdminController::class,'logout'])->name('admin.logout');

	// admin user routes
	Route::get('user/list-user', [UserController::class,'user_list'])->name('list.user');
	Route::get('/get-dasboard-counts', [AjaxController::class,'getDifferentDashboardCounts'])->name('admin.ajax.get-dashboard-counts');

	// admin user activate routes
	Route::get('/is_active/{user_id?}', [UserController::class,'is_active'])->name('is_active');
	Route::get('/is_inactive/{user_id?}', [UserController::class,'is_inactive'])->name('is_inactive');

	// admin settings routes
    Route::get('settings', [AdminSettingsController::class,'settingsOption']);
    Route::post('settings/update', [AdminSettingsController::class,'saveOrUpdateSettings']);

    //admin contact routes
    Route::get('contact-emails', [ContactEmailsController::class, 'index'])->name('contact-emails');
    Route::get('contact-emails/delete/{id}', [ContactEmailsController::class, 'destroy'])->name('contact-emails.destroy');

	// admin profile routes
	Route::prefix('profile')->group(function () {
        Route::get('/profile/{profile_id?}', [AdminProfileController::class,'profile'])->name('profile.profile');
        Route::patch('/profile/{profile_id?}', [AdminProfileController::class,'profileUpdate'])->name('profile.profile.update');
        Route::get('/change-password', [AdminProfileController::class,'changePassword'])->name('profile.password');
        Route::patch('/change-password', [AdminProfileController::class,'changePasswordStore'])->name('profile.password.store');
    });

	Route::get('category/list-category', [CategoryController::class,'category_list'])->name('list.category');
	Route::get('category/add-category', [CategoryController::class,'category_add'])->name('add.category');
	Route::post('category/create-category', [CategoryController::class,'category_create'])->name('create.category');
	Route::get('category/edit-category/{id}', [CategoryController::class,'category_edit'])->name('edit.category');
	Route::post('category/update-category', [CategoryController::class,'update_category'])->name('update.category');

	Route::get('comment/list-comment', [CommentController::class,'comment_list'])->name('list.comment');
	Route::get('comment/add-comment', [CommentController::class,'comment_add'])->name('add.comment');
	Route::post('comment/create-comment', [CommentController::class,'comment_create'])->name('create.comment');
	Route::get('comment/edit-comment/{id}', [CommentController::class,'comment_edit'])->name('edit.comment');
	Route::post('comment/update-comment', [CommentController::class,'update_comment'])->name('update.comment');

	//admin grade
	Route::get('grade-list', [AdminController::class, 'showGradeList'])->name('admin.grade.list');
	Route::get('grade-create', [AdminController::class, 'gradeCreate'])->name('admin.grade.create');
	Route::post('grade-store', [AdminController::class, 'gradeStore'])->name('admin.grade.store');
	Route::get('grade-edit/{id}', [AdminController::class, 'showGradeEdit'])->name('admin.grade.edit');
	Route::get('grade-delete/{id}', [AdminController::class, 'deleteGrade'])->name('admin.grade.delete');
	Route::post('grade-update', [AdminController::class, 'gradeUpdate'])->name('admin.grade.update');
	//admin subject
	Route::get('subject/list', [AdminController::class, 'subjectList'])->name('admin.subject.list');
	Route::get('subject/create', [AdminController::class, 'SubjectCreate'])->name('admin.subject.create');
	Route::post('subject-store', [AdminController::class, 'subjectStore'])->name('admin.subject.store');
	Route::get('subject/edit/{id}', [AdminController::class, 'subjectEdit'])->name('admin.subject.edit');
	Route::post('subject-update', [AdminController::class, 'subjectUpdate'])->name('admin.subject.update');
	Route::get('subject-delete/{id}', [AdminController::class, 'deleteSubject'])->name('admin.subject.delete');

	Route::get('subject/grade/list', [AdminController::class, 'subject_grade_list'])->name('grade.subject.list');
	Route::get('subject/grade/create', [AdminController::class, 'subject_grade'])->name('assign.subject.grade');
	Route::post('subject/grade/store', [AdminController::class, 'assign_subject_grade'])->name('store.subject.grade');
	Route::get('subject/grade/edit/{id}', [AdminController::class, 'subjectGradeEdit'])->name('subject.grade.edit');
	Route::get('subject/grade/delete/{id}', [AdminController::class, 'subjectGradeDelete'])->name('subject.grade.delete');
	//admin strand
	Route::get('strand/list', [AdminController::class, 'strandList'])->name('admin.strand.list');
	Route::get('strand/create', [AdminController::class, 'strandCreate'])->name('admin.strand.create');
	Route::post('strand-store', [AdminController::class, 'strandStore'])->name('admin.strand.store');
	Route::get('strand/edit/{id}', [AdminController::class, 'strandEdit'])->name('admin.strand.edit');
	Route::get('strand-delete/{id}', [AdminController::class, 'deleteStrand'])->name('admin.strand.delete');
	Route::post('strand-update', [AdminController::class, 'strandUpdate'])->name('admin.strand.update');
	
	Route::get('strand/subject/create', [AdminController::class, 'subjectStrandCreate'])->name('subject.strand.create');
	Route::get('strand/subject/edit/{id}', [AdminController::class, 'subjectStrandEdit'])->name('subject.strand.edit');
	Route::get('strand/subject/delete/{id}', [AdminController::class, 'deleteSubjectStrand'])->name('subject.strand.delete');
	Route::get('strand/subject/list', [AdminController::class, 'subject_strand_list'])->name('subject.strand.list');
	Route::post('subject-strand-store', [AdminController::class, 'subject_strand_store'])->name('subject.strand.store');
	Route::post('subject-strand-update', [AdminController::class, 'subject_strand_update'])->name('subject.strand.update');

	//admin  specificexpectation
	Route::post('grade/subjects', [AdminController::class, 'grade_subjects'])->name('admin.grade.subjects');
	Route::post('subject/strands', [AdminController::class, 'subject_strands'])->name('admin.subject.strands');
	Route::get('specificexpectation-list', [AdminController::class, 'showSpecificexpectationList'])->name('admin.specificexpectation.list');
	Route::get('specificexpectation-create', [AdminController::class, 'showSpecificexpectationCreate'])->name('admin.specificexpectation.create');
	Route::post('specificexpectation-store', [AdminController::class, 'specificexpectationStore'])->name('admin.specificexpectation.store');
	Route::get('specificexpectation-edit/{id}', [AdminController::class, 'showSpecificexpectationEdit'])->name('admin.specificexpectation.edit');
	Route::get('specificexpectation-delete/{id}', [AdminController::class, 'deleteSpecificExpectation'])->name('admin.specificexpectation.delete');
	Route::post('specificexpectation-update', [AdminController::class, 'specificexpectationUpdate'])->name('admin.specificexpectation.update');

	Route::get('marks/list', [MarksController::class, 'index'])->name('admin.marks.list');
	Route::get('marks/create', [MarksController::class, 'create'])->name('admin.marks.create');
	Route::post('marks-store', [MarksController::class, 'store'])->name('admin.marks.store');
	Route::get('marks/edit/{id}', [MarksController::class, 'edit'])->name('admin.marks.edit');
	Route::get('marks-delete/{id}', [MarksController::class, 'delete'])->name('admin.marks.delete');
	Route::post('marks-update', [MarksController::class, 'update'])->name('admin.marks.update');

	// News Letter
	Route::get('newsletter', [NewsletterController::class, 'index'])->name('admin.newsletter');
	Route::post('newsletter-store', [NewsletterController::class, 'newsletterStore'])->name('admin.newsletter.store');
	Route::post('breakingnews-store', [NewsletterController::class, 'breakingnewsStore'])->name('admin.breakingnews.store');
	Route::get('breakingnews', [NewsletterController::class, 'breakingnewsShow'])->name('admin.breakingnews');
});

