<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\StoreUnit;
use App\Http\Requests\StoreLesson;
use App\Models\Grade;
use App\Models\Subject;
use App\Models\Strand;
use App\Models\Lesson;
use App\Models\Document;
use App\Models\Expectation;
use App\Models\Favorite;
use App\Models\Follower;
use App\Models\Notification;
use App\Models\User;
use App\Models\Rating;
use App\Models\UnitLesson;
use App\Models\Unit;
use Mail; 
use App\Models\SubjectStrand;
use Response;
use DB;

class LessonController extends Controller
{ 
    public function showLesson(Request $request)
    {   
        //dd($request);
        $user = auth()->user();
        $lessons = Lesson::with('expectation_lesson','documents','subject','strand')
        ->withCount(['grade_lesson as grade' => function($query) {
               $query->select('grade_id');
              }])
        ->whereHas('documents', function($query){
            $query->where('document_type','lesson');
        }) ;

        if(!empty($request->search)){    
            $keyword = $request->search; 
            $lessons = $lessons->where(function($query) use ($keyword){      
                $query->whereHas('subject', function($query) use ($keyword){
                    $query->where('subject', 'LIKE', "%".$keyword."%");
                });
                $query->orWhereHas('strand',function($query) use($keyword){
                    $query->where('strand', 'LIKE', "%".$keyword."%");
                });
                $query->orWhereHas('expectation_lesson',function($query) use($keyword){
                    $query->where('expectations.expectation', 'LIKE', "%".$keyword."%");
                });
            });
        }
        if(!empty($request->filter)){
            $filter = $request->filter;
            $grade = explode(' ',$filter);
            $lessons = $lessons->where(function($query) use ($filter,$grade){
                //$query->where('subject', 'LIKE', "%".$filter."%");
                $query->whereHas('subject', function($query) use ($filter){
                    $query->where('subject', 'LIKE', "%".$filter."%");
                });
                $query->orWhereHas('strand',function($query) use($filter){
                    $query->where('strand', 'LIKE', "%".$filter."%");
                });
                $query->orWhereHas('documents',function($query) use($filter){
                    $query->where('file_type', 'LIKE', "%".$filter."%");
                });
                $query->orWhereHas('rating',function($query) use($filter){
                    $query->where('rate', 'LIKE', "%".$filter."%");
                });
                if(isset($grade[1])){
                    $query->orWhereHas('grade_lesson',function($query) use($grade){
                        $query->where('grades.grade', 'LIKE', "%".$grade[1]."%");
                    });
                }
            });
        }
        if(!empty($request->sort)){
            $sort = $request->sort;
            switch ($sort) {
                case 'date':
                    $lessons = $lessons->orderBy('created_at', 'DESC');
                    break;
                case 'views':
                    $lessons = $lessons->orderBy('view_count', 'DESC');
                    break;
                case 'downloads':
                    $lessons = $lessons->orderBy('download_count', 'DESC');
                    break;
                case 'ratings':
                    $lessons = $lessons->whereHas('rating',function($query){
                        $query->orderBy('rate', 'DESC');
                    });
                    break;
            }
        }

        $lessons = $lessons->paginate(10);
        $subjects = Subject::all();
        $grades = Grade::all();
        $strands = Strand::all();

        if ($request->ajax()) {
        $view = view('teacher.layouts.lessons',compact('lessons'))->render();
            return response()->json(['html'=>$view]);
        }
      return view('teacher.lesson',compact('lessons','subjects','grades','strands'));
    }

    public function showMyLesson(Request $request)
    {   
        //dd($request);
        $user = auth()->user();
        $lessons = Lesson::with('expectation_lesson','documents','subject','strand')
        ->where('user_id',$user->id)
        ->withCount(['grade_lesson as grade' => function($query) {
               $query->select('grade_id');
              }])
        ->whereHas('documents', function($query){
            $query->where('document_type','lesson');
        }) ;

        if(!empty($request->search)){    
            $keyword = $request->search; 
            $lessons = $lessons->where(function($query) use ($keyword){      
                $query->whereHas('subject', function($query) use ($keyword){
                    $query->where('subject', 'LIKE', "%".$keyword."%");
                });
                $query->orWhereHas('strand',function($query) use($keyword){
                    $query->where('strand', 'LIKE', "%".$keyword."%");
                });
                $query->orWhereHas('expectation_lesson',function($query) use($keyword){
                    $query->where('expectations.expectation', 'LIKE', "%".$keyword."%");
                });
            });
        }
        if(!empty($request->filter)){
            $filter = $request->filter;
            $grade = explode(' ',$filter);
            $lessons = $lessons->where(function($query) use ($filter,$grade){
                //$query->where('subject', 'LIKE', "%".$filter."%");
                $query->whereHas('subject', function($query) use ($filter){
                    $query->where('subject', 'LIKE', "%".$filter."%");
                });
                $query->orWhereHas('strand',function($query) use($filter){
                    $query->where('strand', 'LIKE', "%".$filter."%");
                });
                $query->orWhereHas('documents',function($query) use($filter){
                    $query->where('file_type', 'LIKE', "%".$filter."%");
                });
                $query->orWhereHas('rating',function($query) use($filter){
                    $query->where('rate', 'LIKE', "%".$filter."%");
                });
                if(isset($grade[1])){
                    $query->orWhereHas('grade_lesson',function($query) use($grade){
                        $query->where('grades.grade', 'LIKE', "%".$grade[1]."%");
                    });
                }
            });
        }
        if(!empty($request->sort)){
            $sort = $request->sort;
            switch ($sort) {
                case 'date':
                    $lessons = $lessons->orderBy('created_at', 'DESC');
                    break;
                case 'views':
                    $lessons = $lessons->orderBy('view_count', 'DESC');
                    break;
                case 'downloads':
                    $lessons = $lessons->orderBy('download_count', 'DESC');
                    break;
                case 'ratings':
                    $lessons = $lessons->whereHas('rating',function($query){
                        $query->orderBy('rate', 'DESC');
                    });
                    break;
            }
        }

        $lessons = $lessons->paginate(10);
        $subjects = Subject::all();
        $grades = Grade::all();
        $strands = Strand::all();

        if ($request->ajax()) {
        $view = view('teacher.layouts.mylessons',compact('lessons'))->render();
            return response()->json(['html'=>$view]);
        }
      return view('teacher.mylesson',compact('lessons','subjects','grades','strands'));
    }

    public function showSingleLesson($id, Request $request)
    {   
        $lesson = Lesson::findOrFail($id);
        $lesson->increment( 'view_count', 1 );

        $review_group = DB::table('ratings')->select(DB::raw('count(id) as total_counts,avg(rate) as avg_rating,rate'))
        ->where('lesson_id',$id)
        ->groupBy('rate')
        ->orderBy('rate','desc')
        ->get();
        $total_users = Rating::where('lesson_id',$id)->count();
        $reviews = Rating::where('lesson_id',$id)->paginate(5);
        if ($request->ajax()) {
        $view = view('teacher.layouts.reviews',compact('reviews'))->render();
            return response()->json(['html'=>$view]);
        }
         
        return view('teacher.single-lesson',compact('lesson','review_group','total_users','reviews'));
    }

    public function save_download_count(Request $request){
        $lesson = Lesson::findOrFail($request->lesson_id);
        $lesson->increment( 'download_count', 1 );
        return 1;
    }

    public function showFavorite()
    {   
        $favorites = Favorite::where('user_id',auth()->user()->id)->get();
        return view('teacher.favorite',compact('favorites'));
    }

    public function showProfile()
    {   
        $lessons = Lesson::where('user_id',auth()->user()->id)->get();
        
        return view('teacher.teacher-public-profile',compact('lessons'));
    }

    public function showMyProfile()
    {   
        $lessons = Lesson::where('user_id',auth()->user()->id)->get();
        
        return view('teacher.teacher-my-profile',compact('lessons'));
    }

    public function showCreateLesson()
    {     
        $units = Unit::get();
        return view('teacher.createlesson.create-lesson',compact('units'));
    }

    public function showCopyLesson($id)
    {     
        $user = auth()->user();
        $lesson = Lesson::findOrFail($id); 
        $units = Unit::get();
      return view('teacher.createlesson.copy-lesson',compact('lesson','units'));
    }

    public function showEditLesson($id)
    {     
        $user = auth()->user();
        $lesson =  Lesson::findOrFail($id);
        $units = Unit::get();
      return view('teacher.createlesson.edit-lesson',compact('lesson','units'));
    }

    public function units(Request $request)
    {     
        $user = auth()->user();
        $units = UnitLesson::where('user_id',$user->id);
        if(!empty($request->search)){
            $keyword = $request->search;
            $units = $units->whereHas('unit', function($query) use ($keyword){
                        $query->where('name', 'LIKE', "%".$keyword."%");
                    });
        }
        $units = $units->paginate(10);
        return view('teacher.createunit.units',compact('units'));
    }

    public function unitLessons($id,Request $request)
    {   
        //dd($id);
        $user = auth()->user();
        $lessons = Lesson::with('expectation_lesson','documents','subject','strand')
        ->withCount(['grade_lesson as grade' => function($query) {
               $query->select('grade_id');
              }])
        ->whereHas('documents', function($query){
            $query->where('document_type','lesson');
        }) 
        ->whereHas('unit_lessons', function($query) use($id) {
            $query->where('unit_id',$id);
        });

        if(!empty($request->search)){    
            $keyword = $request->search; 
            $lessons = $lessons->where(function($query) use ($keyword){      
                $query->whereHas('subject', function($query) use ($keyword){
                    $query->where('subject', 'LIKE', "%".$keyword."%");
                });
                $query->orWhereHas('strand',function($query) use($keyword){
                    $query->where('strand', 'LIKE', "%".$keyword."%");
                });
                $query->orWhereHas('expectation_lesson',function($query) use($keyword){
                    $query->where('expectations.expectation', 'LIKE', "%".$keyword."%");
                });
            });
        }
        if(!empty($request->filter)){
            $filter = $request->filter;
            $grade = explode(' ',$filter);
            $lessons = $lessons->where(function($query) use ($filter,$grade){
                //$query->where('subject', 'LIKE', "%".$filter."%");
                $query->whereHas('subject', function($query) use ($filter){
                    $query->where('subject', 'LIKE', "%".$filter."%");
                });
                $query->orWhereHas('strand',function($query) use($filter){
                    $query->where('strand', 'LIKE', "%".$filter."%");
                });
                $query->orWhereHas('documents',function($query) use($filter){
                    $query->where('file_type', 'LIKE', "%".$filter."%");
                });
                $query->orWhereHas('rating',function($query) use($filter){
                    $query->where('rate', 'LIKE', "%".$filter."%");
                });
                if(isset($grade[1])){
                    $query->orWhereHas('grade_lesson',function($query) use($grade){
                        $query->where('grades.grade', 'LIKE', "%".$grade[1]."%");
                    });
                }
            });
        }
        if(!empty($request->sort)){
            $sort = $request->sort;
            switch ($sort) {
                case 'date':
                    $lessons = $lessons->orderBy('created_at', 'DESC');
                    break;
                case 'views':
                    $lessons = $lessons->orderBy('view_count', 'DESC');
                    break;
                case 'downloads':
                    $lessons = $lessons->orderBy('download_count', 'DESC');
                    break;
                case 'ratings':
                    $lessons = $lessons->whereHas('rating',function($query){
                        $query->orderBy('rate', 'DESC');
                    });
                    break;
            }
        }

        $lessons = $lessons->paginate(10);
        $subjects = Subject::all();
        $grades = Grade::all();
        $strands = Strand::all();

        if ($request->ajax()) {
        $view = view('teacher.layouts.lessons',compact('lessons'))->render();
            return response()->json(['html'=>$view]);
        }
      return view('teacher.createunit.unit-lesson',compact('lessons','subjects','grades','strands','id'));
    }

    public function unit_store(StoreUnit $request)
    {        
        $data = $request->all();
        //dd($data);
        $user = auth()->user();
        if($data['unit_id'] == 'create-new-unit'){
             $unit = Unit::firstOrNew(
                ['name' =>  $data['unit']]
            );             
            $unit->save();

            // $unit = new Unit;
            // $unit->name = $data['unit'];
            // $unit->save();
        }else{
            $unit = Unit::findOrFail($data['unit_id']);
        }
        $lesson = new Lesson;
        $lesson->user_id = $user->id;
        $lesson->subject_id = $request->subject;
        $lesson->strand_id = $request->strand;
        //$lesson->expectation_id = $request->expectation;        
        if($lesson->save()){
            //dd($lesson->id);
            $unitLesson = UnitLesson::updateOrCreate(
                ['user_id' =>  $user->id,'unit_id' => $unit->id,'lesson_id' => $lesson->id],
                ['user_id' =>  $user->id,'unit_id' => $unit->id,'lesson_id' => $lesson->id]
            );             
            $unitLesson->save();

            $lesson->grade_lesson()->attach(['grade_id'=>$request->grade]);
            foreach ($request->expectation as $key => $expectation) {
                $lesson->expectation_lesson()->attach(['expectation_id'=>$expectation]);
            }
            
        	if($request->hasFile('unit_lesson_ms_file'))
            {
                $file = $request->file('unit_lesson_ms_file');
                $extension = $file->getClientOriginalExtension();  //getting image extension
                $filename = $file->getClientOriginalName();
                //$filename = $name.'.' .$extension;
                $file->storeAs('document', $filename, 'public');
                $document = new Document(['document' => $filename,'file_type' => 'msword','document_type' => 'lesson']); 
                $lesson->documents()->save($document);          
            }
            if($request->hasFile('unit_lesson_pdf_file'))
            {
                $file = $request->file('unit_lesson_pdf_file');
                $extension = $file->getClientOriginalExtension();  //getting image extension
                $filename = $file->getClientOriginalName();
                //$filename = $name.'.' .$extension;
                $file->storeAs('document', $filename, 'public');
                $document = new Document(['document' => $filename,'file_type' => 'pdf','document_type' => 'lesson']); 
                $lesson->documents()->save($document);          
            }
            if($request->unit_lesson_google_doc_link)
            {
            	$document = new Document(['document' => $request->unit_lesson_google_doc_link,'file_type' => 'google_doc_link','document_type' => 'lesson']);
            	$lesson->documents()->save($document);
            }
            if($request->unit_lesson_google_form_link)
            {
            	$document = new Document(['document' => $request->unit_lesson_google_form_link,'file_type' => 'google_form_link','document_type' => 'lesson']);
            	$lesson->documents()->save($document);
            }
            if($request->unit_lesson_google_slide_link)
            {
            	$document = new Document(['document' => $request->unit_lesson_google_slide_link,'file_type' => 'google_slide_link','document_type' => 'lesson']);
            	$lesson->documents()->save($document);
            }
            if($request->unit_lesson_video_link)
            {
            	$document = new Document(['document' => $request->unit_lesson_video_link,'file_type' => 'video_link','document_type' => 'lesson']);
            	$lesson->documents()->save($document);
            }

            if($request->hasFile('unit_practice_ms_file'))
            {
                $file = $request->file('unit_practice_ms_file');
                $extension = $file->getClientOriginalExtension();  //getting image extension
                $filename = $file->getClientOriginalName();
                //$filename = $name.'.' .$extension;
                $file->storeAs('document', $filename, 'public');
                $document = new Document(['document' => $filename,'file_type' => 'msword','document_type' => 'practice']);
                $lesson->documents()->save($document);           
            }
            if($request->hasFile('unit_practice_pdf_file'))
            {
                $file = $request->file('unit_practice_pdf_file');
                $extension = $file->getClientOriginalExtension();  //getting image extension
                $filename = $file->getClientOriginalName();
                //$filename = $name.'.' .$extension;
                $file->storeAs('document', $filename, 'public');
                $document = new Document(['document' => $filename,'file_type' => 'pdf','document_type' => 'practice']); 
                $lesson->documents()->save($document);          
            }
            if($request->unit_practice_google_doc_link)
            {
            	$document = new Document(['document' => $request->unit_practice_google_doc_link,'file_type' => 'google_doc_link','document_type' => 'practice']);
            	$lesson->documents()->save($document);
            }
            if($request->unit_practice_google_form_link)
            {
            	$document = new Document(['document' => $request->unit_practice_google_form_link,'file_type' => 'google_form_link','document_type' => 'practice']);
            	$lesson->documents()->save($document);
            }
            if($request->unit_practice_google_slide_link)
            {
            	$document = new Document(['document' => $request->unit_practice_google_slide_link,'file_type' => 'google_slide_link','document_type' => 'practice']);
            	$lesson->documents()->save($document);
            }
            if($request->unit_practice_video_link)
            {
            	$document = new Document(['document' => $request->unit_practice_video_link,'file_type' => 'video_link','document_type' => 'practice']);
            	$lesson->documents()->save($document);
            }

            if($request->hasFile('unit_ass_ms_file'))
            {
                $file = $request->file('unit_ass_ms_file');
                $extension = $file->getClientOriginalExtension();  //getting image extension
                $filename = $file->getClientOriginalName();
                //$filename = $name.'.' .$extension;
                $file->storeAs('document', $filename, 'public');
                $document = new Document(['document' => $filename,'file_type' => 'msword','document_type' => 'assessment']);
                $lesson->documents()->save($document);           
            }
            if($request->hasFile('unit_ass_pdf_file'))
            {
                $file = $request->file('unit_ass_pdf_file');
                $extension = $file->getClientOriginalExtension();  //getting image extension
                $filename = $file->getClientOriginalName();
                //$filename = $name.'.' .$extension;
                $file->storeAs('document', $filename, 'public');
                $document = new Document(['document' => $filename,'file_type' => 'pdf','document_type' => 'assessment']); 
                $lesson->documents()->save($document);          
            }
            if($request->unit_ass_google_doc_link)
            {
            	$document = new Document(['document' => $request->unit_ass_google_doc_link,'file_type' => 'google_doc_link','document_type' => 'assessment']);
            	$lesson->documents()->save($document);
            }
            if($request->unit_ass_google_form_link)
            {
            	$document = new Document(['document' => $request->unit_ass_google_form_link,'file_type' => 'google_form_link','document_type' => 'assessment']);
            	$lesson->documents()->save($document);
            }
            if($request->unit_ass_google_slide_link)
            {
            	$document = new Document(['document' => $request->unit_ass_google_slide_link,'file_type' => 'google_slide_link','document_type' => 'assessment']);
            	$lesson->documents()->save($document);
            }
            if($request->unit_ass_video_link)
            {
            	$document = new Document(['document' => $request->unit_ass_video_link,'file_type' => 'video_link','document_type' => 'assessment']);
            	$lesson->documents()->save($document);
            }            
        }
        $followers = Follower::where('follower_id',auth()->id())->get();
        if(isset($follower)){
            $emails=[];
            foreach($followers as $follower){
                $emails[] = $follower->user->email;
                if($follower->user->age < 13){
                    if( $follower->user->parent->email){
                    $emails[] =  $follower->user->parent->email;
                    }
                }
                
                $subject = "Lesson created";
                send_follower_mail($emails,$subject);

                $notification = new Notification;
                $notification->sender_id = $user->id;
                $notification->receiver_id = $follower->user->id;
                $notification->notification_type = "Lesson creation";
                $notification->object_id = $lesson->id;
                $notification->message = $subject;
                $notification->read = 0;
                $notification->save();
            }
        }
        return redirect()->back()->with('success', 'Success! Lesson created');
    }

    public function lesson_store(StoreLesson $request)
    {        
        $user = auth()->user();
        $lesson = new Lesson;
        $lesson->user_id = $user->id;
        $lesson->subject_id = $request->subject;
        $lesson->strand_id = $request->strand;
        //$lesson->expectation_id = $request->expectation;        
        if($lesson->save()){
            $lesson->grade_lesson()->attach(['grade_id'=>$request->grade]);
            foreach ($request->expectation as $key => $expectation) {
                $lesson->expectation_lesson()->attach(['expectation_id'=>$expectation]);
            }
            
            if($request->hasFile('lesson_ms_file'))
            {
                $file = $request->file('lesson_ms_file');
                $extension = $file->getClientOriginalExtension();  //getting image extension
                $filename = $file->getClientOriginalName();
                //$filename = $name.'.' .$extension;
                $file->storeAs('document', $filename, 'public');
                $document = new Document(['document' => $filename,'file_type' => 'msword','document_type' => 'lesson']); 
                $lesson->documents()->save($document);          
            }
            if($request->hasFile('lesson_pdf_file'))
            {
                $file = $request->file('lesson_pdf_file');
                $extension = $file->getClientOriginalExtension();  //getting image extension
                $filename = $file->getClientOriginalName();
                //$filename = $name.'.' .$extension;
                $file->storeAs('document', $filename, 'public');
                $document = new Document(['document' => $filename,'file_type' => 'pdf','document_type' => 'lesson']); 
                $lesson->documents()->save($document);          
            }
            if($request->lesson_google_doc_link)
            {
                $document = new Document(['document' => $request->lesson_google_doc_link,'file_type' => 'google_doc_link','document_type' => 'lesson']);
                $lesson->documents()->save($document);
            }
            if($request->lesson_google_form_link)
            {
                $document = new Document(['document' => $request->lesson_google_form_link,'file_type' => 'google_form_link','document_type' => 'lesson']);
                $lesson->documents()->save($document);
            }
            if($request->lesson_google_slide_link)
            {
                $document = new Document(['document' => $request->lesson_google_slide_link,'file_type' => 'google_slide_link','document_type' => 'lesson']);
                $lesson->documents()->save($document);
            }
            if($request->lesson_video_link)
            {
                $document = new Document(['document' => $request->lesson_video_link,'file_type' => 'video_link','document_type' => 'lesson']);
                $lesson->documents()->save($document);
            }

            if($request->hasFile('practice_ms_file'))
            {
                $file = $request->file('practice_ms_file');
                $extension = $file->getClientOriginalExtension();  //getting image extension
                $filename = $file->getClientOriginalName();
                //$filename = $name.'.' .$extension;
                $file->storeAs('document', $filename, 'public');
                $document = new Document(['document' => $filename,'file_type' => 'msword','document_type' => 'practice']);
                $lesson->documents()->save($document);           
            }
            if($request->hasFile('practice_pdf_file'))
            {
                $file = $request->file('practice_pdf_file');
                $extension = $file->getClientOriginalExtension();  //getting image extension
                $filename = $file->getClientOriginalName();
                //$filename = $name.'.' .$extension;
                $file->storeAs('document', $filename, 'public');
                $document = new Document(['document' => $filename,'file_type' => 'pdf','document_type' => 'practice']); 
                $lesson->documents()->save($document);          
            }
            if($request->practice_google_doc_link)
            {
                $document = new Document(['document' => $request->practice_google_doc_link,'file_type' => 'google_doc_link','document_type' => 'practice']);
                $lesson->documents()->save($document);
            }
            if($request->practice_google_form_link)
            {
                $document = new Document(['document' => $request->practice_google_form_link,'file_type' => 'google_form_link','document_type' => 'practice']);
                $lesson->documents()->save($document);
            }
            if($request->practice_google_slide_link)
            {
                $document = new Document(['document' => $request->practice_google_slide_link,'file_type' => 'google_slide_link','document_type' => 'practice']);
                $lesson->documents()->save($document);
            }
            if($request->practice_video_link)
            {
                $document = new Document(['document' => $request->practice_video_link,'file_type' => 'video_link','document_type' => 'practice']);
                $lesson->documents()->save($document);
            }

            if($request->hasFile('ass_ms_file'))
            {
                $file = $request->file('ass_ms_file');
                $extension = $file->getClientOriginalExtension();  //getting image extension
                $filename = $file->getClientOriginalName();
                //$filename = $name.'.' .$extension;
                $file->storeAs('document', $filename, 'public');
                $document = new Document(['document' => $filename,'file_type' => 'msword','document_type' => 'assessment']);
                $lesson->documents()->save($document);           
            }
            if($request->hasFile('ass_pdf_file'))
            {
                $file = $request->file('ass_pdf_file');
                $extension = $file->getClientOriginalExtension();  //getting image extension
                $filename = $file->getClientOriginalName();
                //$filename = $name.'.' .$extension;
                $file->storeAs('document', $filename, 'public');
                $document = new Document(['document' => $filename,'file_type' => 'pdf','document_type' => 'assessment']); 
                $lesson->documents()->save($document);          
            }
            if($request->ass_google_doc_link)
            {
                $document = new Document(['document' => $request->ass_google_doc_link,'file_type' => 'google_doc_link','document_type' => 'assessment']);
                $lesson->documents()->save($document);
            }
            if($request->ass_google_form_link)
            {
                $document = new Document(['document' => $request->ass_google_form_link,'file_type' => 'google_form_link','document_type' => 'assessment']);
                $lesson->documents()->save($document);
            }
            if($request->ass_google_slide_link)
            {
                $document = new Document(['document' => $request->ass_google_slide_link,'file_type' => 'google_slide_link','document_type' => 'assessment']);
                $lesson->documents()->save($document);
            }
            if($request->ass_video_link)
            {
                $document = new Document(['document' => $request->ass_video_link,'file_type' => 'video_link','document_type' => 'assessment']);
                $lesson->documents()->save($document);
            }            
        }
        $followers = Follower::where('follower_id',auth()->id())->get();
        if(isset($follower)){
        $emails=[];
        foreach($followers as $follower){
            $emails[] = $follower->user->email;
            if($follower->user->age < 13){
                if( $follower->user->parent->email){
                $emails[] =  $follower->user->parent->email;
                }
            }
            
            $subject = "Lesson created";
            send_follower_mail($emails,$subject);

        $notification = new Notification;
        $notification->sender_id = $user->id;
        $notification->receiver_id = $follower->user->id;
        $notification->notification_type = "Lesson creation";
        $notification->object_id = $lesson->id;
        $notification->message = $subject;
        $notification->read = 0;
        $notification->save();
        }
        }
        return redirect()->back()->with('success', 'Success! Lesson created');
    }

    public function lesson_update(StoreLesson $request)
    {        
        $user = auth()->user();
        $lesson = Lesson::find($request->lesson_id) ;
        $lesson->user_id = $user->id;
        $lesson->subject_id = $request->subject;
        $lesson->strand_id = $request->strand;
        //$lesson->expectation_id = $request->expectation;        
        if($lesson->save()){
            $lesson->grade_lesson()->attach(['grade_id'=>$request->grade]);
            foreach ($request->expectation as $key => $expectation) {
                $lesson->expectation_lesson()->attach(['expectation_id'=>$expectation]);
            }
            
        	if($request->hasFile('lesson_ms_file'))
            {
                $file = $request->file('lesson_ms_file');
                $extension = $file->getClientOriginalExtension();  //getting image extension
                $filename = $file->getClientOriginalName();
                //$filename = $name.'.' .$extension;
                $file->storeAs('document', $filename, 'public');
                $document = Document::where('lesson_id',$request->lesson_id) ;
                $document = ['document' => $filename,'file_type' => 'msword','document_type' => 'lesson']; 
                $lesson->documents()->save($document);          
            }
            if($request->hasFile('lesson_pdf_file'))
            {
                $file = $request->file('lesson_pdf_file');
                $extension = $file->getClientOriginalExtension();  //getting image extension
                $filename = $file->getClientOriginalName();
                //$filename = $name.'.' .$extension;
                $file->storeAs('document', $filename, 'public');
                $document = Document::where('lesson_id',$request->lesson_id) ;
                $document = ['document' => $filename,'file_type' => 'pdf','document_type' => 'lesson']; 
                $lesson->documents()->save($document);          
            }
            if($request->lesson_google_doc_link)
            {
                $document = Document::where('lesson_id',$request->lesson_id) ;
            	$document = ['document' => $request->lesson_google_doc_link,'file_type' => 'google_doc_link','document_type' => 'lesson'];
            	$lesson->documents()->save($document);
            }
            if($request->lesson_google_form_link)
            {
                $document = Document::where('lesson_id',$request->lesson_id)->orWhere('document_type','lesson') ;
            	$document = ['document' => $request->lesson_google_form_link,'file_type' => 'google_form_link','document_type' => 'lesson'];
            	$lesson->documents()->save($document);
            }
            if($request->lesson_google_slide_link)
            {
                $document = Document::where('lesson_id',$request->lesson_id)->orWhere('document_type','lesson') ;
            	$document = ['document' => $request->lesson_google_slide_link,'file_type' => 'google_slide_link','document_type' => 'lesson'];
            	$lesson->documents()->save($document);
            }
            if($request->lesson_video_link)
            {
                $document = Document::where('lesson_id',$request->lesson_id)->orWhere('document_type','lesson') ;
            	$document = ['document' => $request->lesson_video_link,'file_type' => 'video_link','document_type' => 'lesson'];
            	$lesson->documents()->save($document);
            }

            if($request->hasFile('practice_ms_file'))
            {
                $file = $request->file('practice_ms_file');
                $extension = $file->getClientOriginalExtension();  //getting image extension
                $filename = $file->getClientOriginalName();
                //$filename = $name.'.' .$extension;
                $file->storeAs('document', $filename, 'public');
                $document = Document::where('lesson_id',$request->lesson_id)->orWhere('document_type','practice') ;
                $document = ['document' => $filename,'file_type' => 'msword','document_type' => 'practice'];
                $lesson->documents()->save($document);           
            }
            if($request->hasFile('practice_pdf_file'))
            {
                $file = $request->file('practice_pdf_file');
                $extension = $file->getClientOriginalExtension();  //getting image extension
                $filename = $file->getClientOriginalName();
                //$filename = $name.'.' .$extension;
                $file->storeAs('document', $filename, 'public');
                $document = Document::where('lesson_id',$request->lesson_id)->orWhere('document_type','practice') ;
                $document = ['document' => $filename,'file_type' => 'pdf','document_type' => 'practice']; 
                $lesson->documents()->save($document);          
            }
            if($request->practice_google_doc_link)
            {
                $document = Document::where('lesson_id',$request->lesson_id)->orWhere('document_type','practice') ;
            	$document =  ['document' => $request->practice_google_doc_link,'file_type' => 'google_doc_link','document_type' => 'practice'];
            	$lesson->documents()->save($document);
            }
            if($request->practice_google_form_link)
            {
                $document = Document::where('lesson_id',$request->lesson_id)->orWhere('document_type','practice') ;
            	$document = ['document' => $request->practice_google_form_link,'file_type' => 'google_form_link','document_type' => 'practice'];
            	$lesson->documents()->save($document);
            }
            if($request->practice_google_slide_link)
            {
                $document = Document::where('lesson_id',$request->lesson_id)->orWhere('document_type','practice') ;
            	$document = ['document' => $request->practice_google_slide_link,'file_type' => 'google_slide_link','document_type' => 'practice'];
            	$lesson->documents()->save($document);
            }
            if($request->practice_video_link)
            {
                $document = Document::where('lesson_id',$request->lesson_id)->orWhere('document_type','practice') ;
            	$document =  ['document' => $request->practice_video_link,'file_type' => 'video_link','document_type' => 'practice'];
            	$lesson->documents()->save($document);
            }

            if($request->hasFile('ass_ms_file'))
            {
                $file = $request->file('ass_ms_file');
                $extension = $file->getClientOriginalExtension();  //getting image extension
                $filename = 'assessment'.$lesson->id. '.' .$extension;
                $file->storeAs('document', $filename, 'public');
                $document = Document::where('lesson_id',$request->lesson_id)->orWhere('document_type','assessment');
                $document = ['document' => $filename,'file_type' => 'msword','document_type' => 'assessment'];
                $lesson->documents()->save($document);           
            }
            if($request->hasFile('ass_pdf_file'))
            {
                $file = $request->file('ass_pdf_file');
                $extension = $file->getClientOriginalExtension();  //getting image extension
                $filename = 'assessment'.$lesson->id. '.' .$extension;
                $file->storeAs('document', $filename, 'public');
                $document = Document::where('lesson_id',$request->lesson_id)->orWhere('document_type','assessment');
                $document = ['document' => $filename,'file_type' => 'pdf','document_type' => 'assessment']; 
                $lesson->documents()->save($document);          
            }
            if($request->ass_google_doc_link)
            {
                $document = Document::where('lesson_id',$request->lesson_id)->orWhere('document_type','assessment');
            	$document = ['document' => $request->ass_google_doc_link,'file_type' => 'google_doc_link','document_type' => 'assessment'];
            	$lesson->documents()->save($document);
            }
            if($request->ass_google_form_link)
            {
                $document = Document::where('lesson_id',$request->lesson_id)->orWhere('document_type','assessment');
            	$document = ['document' => $request->ass_google_form_link,'file_type' => 'google_form_link','document_type' => 'assessment'];
            	$lesson->documents()->save($document);
            }
            if($request->ass_google_slide_link)
            {
                $document = Document::where('lesson_id',$request->lesson_id)->orWhere('document_type','assessment');
            	$document = ['document' => $request->ass_google_slide_link,'file_type' => 'google_slide_link','document_type' => 'assessment'];
            	$lesson->documents()->save($document);
            }
            if($request->ass_video_link)
            {
                $document = Document::where('lesson_id',$request->lesson_id)->orWhere('document_type','assessment');
            	$document = ['document' => $request->ass_video_link,'file_type' => 'video_link','document_type' => 'assessment'];
            	$lesson->documents()->save($document);
            }            
        }

        return redirect()->back()->with('success', 'Success! Lesson Modified');
    }
    public function grade_subjects(Request $request)
    {
        $data = [];
       if(!empty($request->grade_id)){ 
             $data['subjects'] = Subject::wherehas("grade_subjects",function($query) use ($request){
            	$query->where('grade_id',$request->grade_id);
            })
            ->get(["subject","id"]);
        }
        
        return response()->json($data);
    }

    public function subject_strands(Request $request)
    {
        $data['strands'] = Strand::wherehas('subject_strands',function($query) use ($request){
            $query->where("subject_id",$request->subject_id);
            $query->where("grade_id",$request->grade_id);
        })        
        ->get(["strand","id"]);
        return response()->json($data);
    }

    public function strand_expectation(Request $request)
    {
        // $data['expectations'] = Expectation::wherehas("strand_expectation",function($query) use ($request){
        //         $query->where('strand_id',$request->strand_id);
        //     })->get(["expectation","id"]);
        $data['expectations'] = Expectation::where('grade_id',$request->grade_id)
        ->where('subject_id',$request->subject_id)
        ->where('strand_id',$request->strand_id)
        ->get(["expectation","id"]);
        return response()->json($data);
    }
}
