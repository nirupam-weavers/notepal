@extends('teacher.layouts.app')

@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('teacher.layouts.sidebar')
        <div class="dashbord-content-common ash-bg dashbord-parent-children">
          <div class="dashbord-content-common-pad">
            <div class="student-independnt-practic-sec teacher-notification-wraper">
              <h2>Notifications:</h2>             
              @if(!empty($notificationsstatus))
              <div class="toggle-notification">
                <div class="toggle-group">
                  <input type="checkbox" name="booking" id="booking"   tabindex="1" @if($notificationsstatus->booking_status == 1) checked="checked" @endif>
                  <label for="booking">
                     Booking 
                  </label>
                  <div class="onoffswitch pull-right" aria-hidden="true">
                      <div class="onoffswitch-label">
                          <div class="onoffswitch-inner"></div>
                          <div class="onoffswitch-switch"></div>
                      </div>
                  </div>
                </div>
                <div class="toggle-group">
                  <input type="checkbox" name="lesson" id="lesson" tabindex="1"  @if($notificationsstatus->lesson_status == 1) checked="checked" @endif>
                  <label for="lesson">
                     Lesson 
                  </label>
                  <div class="onoffswitch pull-right" aria-hidden="true">
                      <div class="onoffswitch-label">
                          <div class="onoffswitch-inner"></div>
                          <div class="onoffswitch-switch"></div>
                      </div>
                  </div>
                </div>
                <div class="toggle-group">
                  <input type="checkbox" name="newsletter" id="newsletter"  tabindex="1"  @if($notificationsstatus->newsletter_status == 1) checked="checked" @endif>
                  <label for="newsletter">
                     News Letter 
                  </label>
                  <div class="onoffswitch pull-right" aria-hidden="true">
                      <div class="onoffswitch-label">
                          <div class="onoffswitch-inner"></div>
                          <div class="onoffswitch-switch"></div>
                      </div>
                  </div>
                </div>
                <div class="toggle-group">
                  <input type="checkbox" name="breakingnews" id="breakingnews"   tabindex="1" @if($notificationsstatus->breaking_status == 1) checked="checked" @endif  >
                  <label for="breakingnews">
                     Breaking News 
                  </label>
                  <div class="onoffswitch pull-right" aria-hidden="true">
                      <div class="onoffswitch-label">
                          <div class="onoffswitch-inner"></div>
                          <div class="onoffswitch-switch"></div>
                      </div>
                  </div>
                </div>
              </div>
              @endif
              @foreach($notifications as $notification)
              <div class="student-independnt-practic-sngl-box teacher-notification-box">
                <div class="notification-head">
                  <div class="row align-items-center">
                      <div class="col-lg-9">
                        <h4>{{$notification->message}} </h4>
                      </div>
                      <div class="col-lg-3">
                        <h6>{{$notification->created_at->format('m/d/Y')}} </h6>
                      </div>
                  </div>
                </div>
                <div class="notification-body">
                  <div class="row">
                    <div class="col-lg-9">
                      <p>{{$notification->message}} </p>
                    </div>
                    <div class="col-lg-3">
                      <div class="box-icon">
                        @if($notification->notification_type == "Lesson creation")
                        <a href="{{ url('teacher/single-lesson/'.$notification->object_id) }}"><i class="fas fa-chevron-right"></i></a>
                        @elseif($notification->notification_type == "Booking creation")
                        <a href="{{ route('teacher.calendar') }}"><i class="fas fa-chevron-right"></i></a>
                        @else
                        <a href=""><i class="fas fa-chevron-right"></i></a>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
              </div>
               @endforeach
               @if(!empty($notifications)) 
              <div class="notification-but">
                <a href="" class="btn red-text-btn">Load More <span><i class="fas fa-plus"></i></span></a>
              </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
  $("#booking").change(function(){
      if($(this).prop("checked") == true){
        var booking_status = 1;
      }else{
        var booking_status = 0;
      }

      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: "{{route('toggle.booking_status')}}",
        type: "POST",
        data: {
          booking_status: booking_status,
            
        },
        cache: false,
        success: function(result){
          if (result == 1) {
            toastr.success('Booking News Activeted successfully.');
             
          } else {
            toastr.success('Booking News Status change successfully.');
             
          }
        }
    });  
  });

  $("#lesson").change(function(){
      if($(this).prop("checked") == true){
        var lesson_status = 1;
      }else{
        var lesson_status = 0;
      }

      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: "{{route('toggle.lesson_status')}}",
        type: "POST",
        data: {
          lesson_status: lesson_status,
            
        },
        cache: false,
        success: function(result){
          if (result == 1) {
            toastr.success('lesson News Activeted successfully.');
             
          } else {
            toastr.success('lesson News Status change successfully.');
             
          }
        }
    });  
  });

  $("#breakingnews").change(function(){
      if($(this).prop("checked") == true){
        var breakingnews_status = 1;
      }else{
        var breakingnews_status = 0;
      }

      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: "{{route('toggle.breakingnews_status')}}",
        type: "POST",
        data: {
          breakingnews_status: breakingnews_status,
            
        },
        cache: false,
        success: function(result){
          if (result == 1) {
            toastr.success('breakingnews News Activeted successfully.');
             
          } else {
            toastr.success('breakingnews News Status change successfully.');
             
          }
        }
    });  
  });

  $("#newsletter").change(function(){
      if($(this).prop("checked") == true){
        var newsletter_status = 1;
      }else{
        var newsletter_status = 0;
      }

      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: "{{route('toggle.newsletter_status')}}",
        type: "POST",
        data: {
          newsletter_status: newsletter_status,
            
        },
        cache: false,
        success: function(result){
          if (result == 1) {
            toastr.success('newsletter  Activeted successfully.');
             
          } else {
            toastr.success('Newsletter  Status change successfully.');
             
          }
        }
    });  
  });

</script>
@endsection