<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Marks;
use Validator;

class MarksController extends Controller
{
    public function index()
    {
        $marks=Marks::orderBy('created_at')->get();        
        return view('admin.marks.marks-list',compact('marks'));
    }

    public function create()
    {       
        return view('admin.marks.marks-add');
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'from' => ['required'],
            'to' => ['required'],
            'percentage' => ['required'],
        ];
        $messages = array(
          'name.required' => 'Marks name is required',
          'from.required' => 'Marks from range is required',
          'to.required' => 'Marks to range is required',
          'percentage.required' => 'Marks percentage is required',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } 
        //$mid = round(($request->from + $request->to) / 2);

        $marks = new Marks;
        $marks->name = $request->name;
        $marks->from = $request->from;
        $marks->to = $request->to;
        $marks->mid = $request->percentage;
        //$marks->mid = $mid;
        $marks->save();       
        
        \Session::flash('message','Successfully created.');
        return redirect()->route('admin.marks.list');
    }

    public function edit($id)
    {
        $marks=Marks::findOrFail($id);       
        return view('admin.marks.marks-edit',compact('marks'));
    }

    public function update(Request $request)
    {
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'from' => ['required'],
            'to' => ['required'],
            'percentage' => ['required'],
        ];
        $messages = array(
          'name.required' => 'Marks name is required',
          'from.required' => 'Marks from range is required',
          'to.required' => 'Marks to range is required',
          'percentage.required' => 'Marks percentage is required',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } 
        //$mid = round(($request->from + $request->to) / 2);

        $marks = Marks::findOrFail($request->id);
        $marks->name = $request->name;
        $marks->from = $request->from;
        $marks->to = $request->to;
        $marks->mid = $request->percentage;
        //$marks->mid = $mid;
        $marks->save();       
        
        \Session::flash('message','Successfully updated.');
        return redirect()->route('admin.marks.list');
    }

    public function delete($id)
    {
        $marks=Marks::findOrFail($id);  
        $marks->delete();     
        \Session::flash('message','Successfully deleted.');
        return redirect()->route('admin.marks.list');
    }
}
