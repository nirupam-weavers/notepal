@extends('teacher.layouts.app')

@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('teacher.layouts.sidebar')       
        <div class="dashbord-content-common ash-bg dashbord-parent-children">
          <div class="dashbord-content-common-pad">
            <div class="lession-complt-frm-page">
              <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab"><span><i class="far fa-circle"></i></span> Create new lesson</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab"><span><i class="far fa-circle"></i></span> Create a unit</a>
                </li>
              </ul><!-- Tab panes -->

              <div class="tab-content">
                <div class="tab-pane active" id="tabs-1" role="tabpanel">
                  <div class="lession-complt-frm-body">
                    <form action="{{route('lesson.store')}}" method="POST" enctype="multipart/form-data">
                      <div class="lession-complt-frm-top">
                          @csrf
                          <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                  <label for="">Grade <span>*</span></label>
                                  <select class="form-control" id="grade-dropdown" name="grade">
                                    <option value="">Select Grade</option>
                                    @foreach(\App\Models\Grade::all() as $grade)
                                    <option value="{{$grade->id}}" @if(old('grade') !== null && ($grade->grade == old('grade'))) selected="selected" @endif>Grade {{$grade->grade}}</option>
                                    @endforeach
                                  </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                  <label for="">Subject <span>*</span></label>
                                  <select class="form-control" id="subject-dropdown" name="subject">
                                    <option value="">Select Subject</option>
                                  </select>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                  <label for="">Strand <span>*</span></label>
                                  <select class="form-control" id="strand-dropdown" name="strand">
                                    <option value="">Select Strand</option>
                                  </select>
                                </div>
                            </div>
                          </div>
                      </div>
                      <div class="specific-exp">
                        <h3>Specific Expectation(s) <span>*</span></h3>
                        <div class="box-text">
                          <select class="form-control select2" id="expectation-dropdown" name="expectation[]" multiple="multiple" data-placeholder="Select Expectation" required>
                          </select>
                        </div>
                      </div>
                      <div class="single-box-file-wraper">
                        <div class="single-box-file-wraper-head">
                        <h3>Lesson Plan <span>*</span></h3>
                        <p>Upload your lesson plan in any of the following format. Please note, your lesson plan should be divided into three sections- Minds On, Lesson, and Consolidation in order for it to be accepted. </p>
                        </div> 
                        <input type="hidden" name="lesson" value="" id="lesson">
                        @include('teacher.createlesson.lessonplan')                      
                      </div>
                      <div class="single-box-file-wraper">
                        <div class="single-box-file-wraper-head">
                        <h3>Independent Practice <span>(Optional)</span></h3>
                        <p>Upload your Independent Practice in any of the following format. </p>
                        </div>
                        @include('teacher.createlesson.independentpractice')
                      </div>
                      <div class="single-box-file-wraper">
                        <div class="single-box-file-wraper-head">
                        <h3>Assessment <span>(Optional)</span></h3>
                        <p>Upload your Assessment in any of the following format. </p>
                        </div>
                        @include('teacher.createlesson.assessment')
                      </div>
                      <div class="create-lsn-btn">
                        <ul>
                          <li><a href="{{route('lesson.create')}}" class="btn inactive-btn">Discard</a></li>
                          <li><button type="submit" class="btn yellow-btn">Create Lesson Plan</button></li>
                        </ul>
                      </div>
                    </form>
                  </div>
                </div>

                <div class="tab-pane" id="tabs-2" role="tabpanel">
                  @include('teacher.createunit.create-unit')
                </div>
              </div>
            </div>
          </div>
        </div>
       </div>  
    </div>
    </div>
</hiv>
 
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
    $('#grade-dropdown').on('change', function() {
      var grade_id = $('#grade-dropdown').val();
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
          }
      });
      $.ajax({
          url: "{{route('grade.subjects')}}",
          type: "POST",
          data: {
              grade_id: grade_id
          },
          cache: false,
          success: function(result){
              $('#subject-dropdown').html('<option value="">Select Subject</option>'); 
              $.each(result.subjects,function(key,value){
              $("#subject-dropdown").append('<option value="'+value.id+'">'+value.subject+'</option>');
              });
              $('#strand-dropdown').html('<option value="">Select Strand</option>'); 
          }
      });       
        
    });    

    $('#subject-dropdown').on('change', function() {
            var subject_id = this.value;
            var grade_id = $('#grade-dropdown').val();;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{route('subject.strands')}}",
                type: "POST",
                data: {
                    subject_id: subject_id,
                    grade_id: grade_id
                },
                cache: false,
                success: function(result){
                  $('#strand-dropdown').html('<option value="">Select Strand</option>'); 
                  $.each(result.strands,function(key,value){
                  $("#strand-dropdown").append('<option value="'+value.id+'">'+value.strand+'</option>');
                  });
                  $('#expectation-dropdown').html('<option value="">Select Expectation</option>'); 
                }
            });        
        
    });

    $('#strand-dropdown').on('change', function() {
            var strand_id = this.value;
            var grade_id = $('#grade-dropdown').val();
            var subject_id = $('#subject-dropdown').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{route('strand.expectation')}}",
                type: "POST",
                data: {
                    strand_id: strand_id,
                    grade_id: grade_id,
                    subject_id: subject_id,
                },
                cache: false,
                success: function(result){
                  $('#expectation-dropdown').html('<option value="">Select Expectation</option>'); 
                  $.each(result.expectations,function(key,value){
                  $("#expectation-dropdown").append('<option value="'+value.id+'">'+value.expectation+'</option>');
                  }); 
                }
            });        
        
    });

    //unit
    $('#unit-grade-dropdown').on('change', function() {
      var grade_id = $('#unit-grade-dropdown').val();
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
          }
      });
      $.ajax({
          url: "{{route('grade.subjects')}}",
          type: "POST",
          data: {
              grade_id: grade_id
          },
          cache: false,
          success: function(result){
              $('#unit-subject-dropdown').html('<option value="">Select Subject</option>'); 
              $.each(result.subjects,function(key,value){
              $("#unit-subject-dropdown").append('<option value="'+value.id+'">'+value.subject+'</option>');
              });
              $('#unit-strand-dropdown').html('<option value="">Select Strand</option>'); 
          }
      });       
        
    });    

    $('#unit-subject-dropdown').on('change', function() {
            var subject_id = this.value;
            var grade_id = $('#unit-grade-dropdown').val();;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{route('subject.strands')}}",
                type: "POST",
                data: {
                    subject_id: subject_id,
                    grade_id: grade_id
                },
                cache: false,
                success: function(result){
                  $('#unit-strand-dropdown').html('<option value="">Select Strand</option>'); 
                  $.each(result.strands,function(key,value){
                  $("#unit-strand-dropdown").append('<option value="'+value.id+'">'+value.strand+'</option>');
                  });
                  $('#unit-expectation-dropdown').html('<option value="">Select Expectation</option>'); 
                }
            });        
        
    });

    $('#unit-strand-dropdown').on('change', function() {
            var strand_id = this.value;
            var grade_id = $('#unit-grade-dropdown').val();
            var subject_id = $('#unit-subject-dropdown').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{route('strand.expectation')}}",
                type: "POST",
                data: {
                    strand_id: strand_id,
                    grade_id: grade_id,
                    subject_id: subject_id,
                },
                cache: false,
                success: function(result){
                  $('#unit-expectation-dropdown').html('<option value="">Select Expectation</option>'); 
                  $.each(result.expectations,function(key,value){
                  $("#unit-expectation-dropdown").append('<option value="'+value.id+'">'+value.expectation+'</option>');
                  }); 
                }
            });        
        
    });

    $('#unit-select').on('change', function() {
      var unit = this.value;
      if(unit == 'create-new-unit'){
        $('#unit-new').show();
      }else{
        $('#unit-new').hide();
      }
                   
        
    });

});
</script>
@endsection