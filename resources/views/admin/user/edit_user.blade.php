@extends('admin.layouts.admin-app')

@section('content')

<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Category <small>Entry Form</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">User</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row"> 
      <!-- left column -->
      <div class="col-md-12"> 
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Edit</h3>
            @if ($errors->any())
                  <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              @if (session()->has('message'))
                  <div class="alert alert-info alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      {{ session('message') }}
                  </div>
              @endif
              @if (session()->has('error_message'))
                  <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      {{ session('error_message') }}
                  </div>
              @endif
          </div>
          <!-- /.box-header --> 
          <!-- form start -->
          <form name="entry-form" method="post" action="{{route('update.user')}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="user_id" value="{{$user->id}}">
            <div class="box-body">
              <!-- <div class="form-group">
                <label for="inputStatus">Roles</label>
                <select class="form-control custom-select" name="role_id">
                  <option >Select one</option>
                  <option value="1" {{$user->role_id=='1'?"selected":""}}>Super Admin</option>
                  <option value="2" {{$user->role_id=='2'?"selected":""}}>Mentor</option>
                  <option value="3" {{$user->role_id=='3'?"selected":""}}>Mentee</option>
                </select>
              </div> -->
              <div class="form-group">
                <label for="first_name">First Name<a class='lnkred'>*</a></label>
                <input type="text" class="form-control" name="first_name" value="{{$user->first_name}}">
              </div>
              <div class="form-group">
                <label for="last_name">Last Name<a class='lnkred'>*</a></label>
                <input type="text" class="form-control" name="last_name" value="{{$user->last_name}}">
              </div>
              <div class="form-group">
                <label for="zip_code">Zip Code<a class='lnkred'>*</a></label>
                <input type="text" class="form-control" name="zip_code" value="{{$user->zip_code}}">
              </div>
              <div class="form-group">
                <label for="image">Image</label>
                <input type="file" class="form-control" name="user_image" id="imgInp">
                <p class="help-block">
                  @if(!empty($user->user_image))
                  <img id="blah" src="{{asset('user/'.$user->user_image)}}" height="50" width="50">
                  @else
                  <img id="blah" alt="Avatar" class="table-avatar" src="{{ asset('/storage/images/no-image.jpg')}}" height="50" width="50">
                  @endif                
                </p>
              </div>
              <div class="form-group">
                <label for="is_verified">Is Verified<a class='lnkred'>*</a></label>
              </div>
              <div class="checkbox">
                  <input type="radio" name="is_verified" value="1" <?php if($user->is_verified==1){echo" checked";}?> >
                  Yes
                  <input type="radio" name="is_verified" value="0" <?php if($user->is_verified==0){echo" checked";}?>>
                  No
              </div>
            </div>
            <!-- /.box-body -->
            
            <div class="box-footer">
              <input type="submit" class="btn btn-primary" value="Submit">
            </div>
          </form>
        </div>
        <!-- /.box --> 
        
      </div>
      <!--/.col (left) --> 
    </div>
    <!-- /.row --> 
  </section>
  <!-- /.content --> 
</div>

<script type="text/javascript">
  imgInp.onchange = evt => {
    const [file] = imgInp.files
    if (file) {
      blah.src = URL.createObjectURL(file)
    }
  }
</script>
@endsection