
  $(function() {
  $("form[name='register']").validate({
    // Specify validation rules
    rules: {
      name: {required: true},
      exampleCheck1:{
        required: true,
       },
      email: {
          required: true,
          email: true
        },
        password: {
          required: true,
          minlength: 6
        }
      },
    // Specify validation error messages
    messages: {
      name: "Please enter your name",
      exampleCheck1: "Please Check the  Checkbox",
      password: {
          required: "Please provide a password",
          minlength: "Your password must be at least 6 characters long"
        },
        email: "Please enter a valid email address"
      },
    
    submitHandler: function(form) {
      form.submit();
    }
  });
});
