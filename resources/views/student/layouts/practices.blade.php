@foreach($practices as $practice)               
<div   class="student-independnt-practic-sngl-box">
  <div class="roted-box">
    <h3> Grade : {{implode(',',$practice->grade_lesson()->pluck('grade_id')->all())}} 
  </h3>
  </div>
  <ul class="top-box">
  <li><span>Subject:</span> {{$practice->subject->subject}}
  </li>
    <li><span>Strand:</span> {{$practice->strand->strand}}</li>
    <li><span>Date:</span>  {{ date('d-m-Y', strtotime($practice->created_at))}}</li>
  </ul>
  <ul>
  @foreach($practice->expectation_lesson as $exp)
  <li><p> {{$exp->expectation}}  </p></li>
  @endforeach
  </ul>
  <div class="student-independnt-btm">
    <div class="row align-items-center">
      <div class="col-lg-6">
        <ul class="dnld-sec">
          <li>
            <ul class="star-rating">
              <?php for($i=1;$i< $practice->avg_rating;$i++){ ?>
              <li><i class="fas fa-star"></i></li>
               <?php } ?>
              <li>{{$practice->avg_rating}}</li>
            </ul>
          </li>
          <li><span><i class="fas fa-download"></i></span> {{$practice->download_count}}</li>
          <li><span><i class="far fa-eye"></i></span> {{$practice->view_count}}</li>
        </ul>
      </div>
      <div class="col-lg-6">
        <ul class="indepnd-action-box">
          <li class="blue-box"><a href=""><i class="fas fa-share-alt"></i></a></li>
          <?php
        $favoriteClass = "white-box";
        if ($practice->userFavorites()->where(['document_type' => 'practice','user_id' => auth()->user()->id])->exists()) {
          $favoriteClass = "yellow-box";
        }
        ?>
        <li class="{{$favoriteClass}} favorite" data-id="{{$practice->id}}" data-doctype="practice"><i class="fas fa-star"></i></li>
          <li class="white-box"><a href="{{ url('student/single-independentpractice/'.$practice->id) }}"><i class="fas fa-chevron-right"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
@endforeach