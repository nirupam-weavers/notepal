<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GradeSubject extends Model
{
    use HasFactory;

    protected $fillable = [
        'grade_id',
        'subject_id',
     ];

    public function subject()
    {
    	return $this->belongsTo(Subject::class);
    }

    public function grade()
    {
    	return $this->belongsTo(Grade::class);
    }
}
