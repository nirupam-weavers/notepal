<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('front/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('front/css/font-awesome-all.min.css')}}" rel="stylesheet">
    <link href="{{ asset('front/css/custom.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('front/css/owl.carousel.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('front/css/owl.theme.default.min.css')}}" rel="stylesheet" type="text/css"> 
    <link href="{{ asset('front/css/animate.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.datetimepicker.min.css')}}">
    @yield('styles')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="{{ asset('front/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('front/js/font-awesome-all.min.js')}}"></script>
    <script src="{{ asset('front/js/custom.js')}}"></script>
    <script src="{{ asset('front/js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('front/js/wow.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.0/jquery.waypoints.min.js"></script>
    <script src="{{ asset('front/js/jquery.counterup.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
    <script src="{{asset('js/jquery.datetimepicker.full.min.js')}}"></script>
    <script src="{{ asset('admin/plugins/chartjs/Chart.js')}}"></script>
</head>
<body>
    <input type="hidden" value="{{auth()->id()}}" id="loggedIn_id">
    @yield('content')
    <div class="modal" id="web-notification">
      <div class="modal-dialog">
          <div class="modal-content web-push-modal">
              <!-- Modal body -->
              <div class="modal-body">
                 <div class="notification_box">
                  <div class="nti-inner">
                    <p class="nti-title">Allow Web Notifications ?</p>
                    <a class="nti-btn btn-accept" onclick="initFirebaseMessagingRegistration();" href="javascript:void(0)"><i class="fa fa-check" aria-hidden="true"></i></a>
                    <a class="nti-btn btn-decline" href="javascript:void(0)" onclick="closeNotification()" ><i class="fa fa-times" aria-hidden="true"></i></a>
                  </div>
                </div>
              </div>
          </div>
      </div>
    </div> 
    <!-- <script src="https://cdn.socket.io/4.5.0/socket.io.min.js" integrity="sha384-7EyYLQZgWBi67fBtVxw60/OWl1kjsfrPFcaU0pp0nAh+i8FD068QogUvg85Ewy1k" crossorigin="anonymous"></script> -->
    <!-- <script src="{{asset('js/chat.js')}}"></script> -->
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <!-- <script src="https://www.gstatic.com/firebasejs/7.23.0/firebase.js"></script> -->
    <script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-database.js"></script>
    <script src="{{asset('js/firebase-chat.js')}}"></script>
    @yield('scripts')
    @if(empty(auth()->user()->fcm_token))
      <script type="text/javascript">
        $(document).ready(function() { 
          //$('#web-notification').modal('show');
        });
      </script>      
    @endif
    <script>
       new WOW().init();
    </script>
    <script>
        function closeNotification(){
            jQuery('#web-notification').modal('hide');
        }
        $('.wrap-menu').on('click',function(){
            $('.wrap-menu').toggleClass('wrap-menu-show','');
            $('.sub-menu').toggleClass('sub-menu-show','');
        });

        //active-menu---------->
        var pgurl = window.location.href.substr(window.location.href.indexOf("/") + 1);
        var myarr = pgurl.split("/");
        var activepart = myarr[myarr.length - 1];

        $(".activelink").each(function() {
          var title = $(this).attr("data-title");
          //console.log(activepart,title)
          if (activepart == title)
            $(this).parent().addClass("current-menu-item");
        });

       function preventBack() {
            window.history.forward(); 
        }
          
        setTimeout("preventBack()", 0);
          
        window.onunload = function () { null };

        $(document).ready(function() {
            toastr.options.timeOut = 5000;
            @if(count($errors) > 0)
                @foreach($errors->all() as $error)
                    toastr.error("{{ $error }}");
                @endforeach
            @endif

            @if (Session::has('success'))
                toastr.success('{{ Session::get('success') }}');
            @endif
        });

        $('.follower').on('click', function() {
            var element = this;
            var user_id = $(element).attr("data-id");
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{route('toggle.follower')}}",
                type: "POST",
                data: {
                    follower_id: user_id,
                    
                },
                cache: false,
                success: function(result){
                  if (result == 1) {
                    toastr.success('Following successfully.');
                    $(element).toggleClass('white-box yellow-box');
                  } else {
                    toastr.error('Removed from Following successfully.');
                    $(element).toggleClass('yellow-box white-box');
                  }
                }
            });       
        
        });
    </script>
</body>
</html>
