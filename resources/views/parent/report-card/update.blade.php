@extends('parent.layouts.app')
@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('parent.layouts.sidebar')


      <div class="dashbord-content-common ash-bg dashbord-parent-children">
        <div class="dashbord-content-common-pad">
          <form action="{{route('report.update')}}" method="POST">
            @csrf
            <div class="my-children-header">
              <div class="my-children-text">
                <h2>Edit Report Card</h2>
              </div>
              <div class="my-children-add">
                <div class="form-group">
                  <select class="form-control" name="user_id">
                    <option value="">Select Child</option>
                    @foreach($children as $child)
                    <option value="{{$child->id}}" @if(app('request')->input('user_id')==$child->id) selected @endif>{{$child->name}} <span>( Current Grade: Grade {{$child->grade}} )</span></option>
                    @endforeach
                  </select>
                </div>
                <a href="{{route('parent.report.card.edit')}}" class="btn yellow-btn">Back</a>
              </div>
            </div>
            <div class="my-children-view-area">
              <div class="my-children-search-box">
                <div class="row">
                  <div class="col-lg-3">
                    <div class="form-group">
                      <label for="exampleFormControlSelect1">Year <span>*</span></label>
                      <select class="form-control" name="year">
                        @for($i=2015;$i<=2025;$i++) 
                         <option value="{{$i}}" @if(app('request')->input('year')==$i) selected @endif>{{$i}}-{{$i+1}}</option>
                        @endfor
                      </select>
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="form-group">
                      <label for="exampleFormControlSelect1">Grade <span>*</span></label>
                      <select class="form-control" name="grade" id="grade">
                        @foreach($grades as $grade)
                        <option value="{{$grade->grade}}" @if(app('request')->input('grade')==$grade->grade) selected @endif>Grade {{$grade->grade}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <?php
                  $nse = array(0 => 'NA', 1 => 'N', 2 => 'S', 3 => 'G', 4 => 'E');
                  $type = app('request')->input('type');
                  ?>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="exampleFormControlSelect1">Report <span>*</span></label>
                      <select class="form-control" name="type">
                        <option value="progress_report" @if($type=='progress_report' ) selected @endif>Progress Report</option>
                        <option value="term_1" @if($type=='term_1' ) selected @endif>Term 1</option>
                        <option value="term_2" @if($type=='term_2' ) selected @endif>Term 2</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="parent-report-card-language">
              <div class="parent-report-card-language-box-sec">
                <div class="parent-report-card-language-heading">
                  <h2>{{$learning_skills->name}} <span>*</span></h2>
                </div>
                <div class="parent-report-card-language-wraper dashbord-inner-right-padd">
                  <div class="row">
                    @foreach($learning_skills->get_children as $learning_skill)
                    <div class="col-lg-6">
                      <div class="parent-report-card-language-sngl">
                        <h3>{{$learning_skill->name}}</h3>
                        <div class="form-group">
                          <select class="form-control" name="comment[{{$learning_skill->id}}]">
                            @if($learning_skill->category_comments())
                            @foreach($learning_skill->category_comments()->get(['comments.id','name','value']) as $comment)
                            <?php $sel = '';
                            if (isset($report[$learning_skill->id]) && $report[$learning_skill->id] == $comment->value) {
                              $sel = 'selected';
                            }
                            ?>
                            <option value="{{$comment->value}}" {{$sel}}>{{$comment->name}}</option>
                            @endforeach
                            @endif
                          </select>
                        </div>
                      </div>
                    </div>
                    @endforeach
                  </div>
                </div>
              </div>
              <div class="parent-report-card-language-box-sec">
                <div class="parent-report-card-language-heading">
                  <h2>{{$academics->name}} <span>*</span></h2>
                </div>
                @foreach($academics->get_children as $academic)
                <div class="parent-report-card-language-wraper dashbord-inner-right-padd">
                  <h5>{{$academic->name}}</h5>
                  <div class="row">
                    @if($academic->get_children)
                    @foreach($academic->get_children as $acdemic)
                    <?php $val = '';
                    if (isset($report[$acdemic->id])) {
                      $val = $report[$acdemic->id];
                    }
                    ?>
                    <div class="col-lg-6" id="{{$acdemic->name}}" @if($acdemic->name=='Geography' || $acdemic->name=='History') style="display: none;" @endif>
                      <div class="parent-report-card-language-sngl">
                        <h3>{{$acdemic->name}}</h3>
                        <div class="form-group">

                          <input type="number" min="0" value="{{$val}}" class="form-control mark-num" name="comment[{{$acdemic->id}}]" @if($acdemic->name=='Geography' || $acdemic->name=='History') disabled @endif>
                          <select class="form-control marks-select" name="comment[{{$acdemic->id}}]">
                            @foreach($marks as $mark)
                            <option value="{{$mark->mid}}" @if($mark->mid==$val) selected @endif>{{$mark->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    @endforeach

                    @endif
                  </div>
                </div>
                @endforeach
              </div>
              <div class="lnguage-btn">
                <ul>
                  <li><a href="" class="btn ash-btn">Discard</a></li>
                  <li><button type="submit" class="btn">Save Details</button></li>
                </ul>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
  var grade = $('#grade').val();;
  //console.log(typeof grade)
  if (grade == '7' || grade == '8') {
    $('#History').show();
    $('#Geography').show();
    $('.marks-select').hide();
    $('.marks-select').attr('disabled', true);
    $('.marks-input').attr('type', 'text');
    $('.marks-input').attr('disabled', false);
    //console.log(1)
  } else {
    $('#History').hide();
    $('#Geography').hide();
    $('.mark-num').hide();
    $('.marks-select').show();
    $('.marks-input').attr('type', 'hidden');
    $('.marks-input').attr('disabled', true);
    $('.marks-select').attr('disabled', false);
    // console.log(2)
  };
</script>


@endsection