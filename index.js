const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server, {
    cors: { origin : '*'},
});
let users = [];

const addUser = (socket_id, user_id) => {
    //console.log('addUser', users)
    if(socket_id != null){
        users.some(user => user.user_id === user_id) ? 
            ( 
                index = users.findIndex((obj => obj.user_id == user_id)),
                users[index].socket_id = socket_id
            )
            : 
            (users.push({user_id, socket_id}))
    }
    //console.log('addUser over', users)
}

const getUser = (user_id) => {
    index = users.findIndex((obj => obj.user_id == user_id))
    if(index >= 0){
        return {
            return:true,
            socket_id:users[index].socket_id
        };
    }else{
        return {
            return:false,
        };
    }
}

const removeUser = (socket_id) => {
    index = users.findIndex((obj => obj.socket_id == socket_id))
    users.splice(index)[0];
}



io.on('connection', (socket) => {
    //console.log(socket.id)
    // it add the users socket id and user id to users table
    socket.on('addUser',(socket_id, user_id) => {
        console.log('addUser-',socket_id,user_id)
        addUser(socket_id, user_id)
    })
    socket.on('sendMessage', (message) => {
        const receiver = getUser(message.receiver_id)
       // console.log(message)
        if(receiver.return){
            io.to(receiver.socket_id).emit('getMessage', message)
        }else{
            console.log('User not found..')
        }
         
    });
    socket.on('messageTyping', (id) => {
        const receiver = getUser(id)
        if(receiver.return){
            io.to(receiver.socket_id).emit('showMessageTyping')
        }else{
            console.log('User not found..')
        }
         
    });

    socket.on('disconnect', () => {
        //console.log('disconnect',socket.id)
        //removeUser(socket.id)
    })
});

server.listen(3000, () => {
    console.log('Running Server!!!');
})

