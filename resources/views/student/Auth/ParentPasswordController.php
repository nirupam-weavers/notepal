<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request; 
use DB; 
use Carbon\Carbon; 
use App\Models\User; 
use Mail; 
use Hash;
use Illuminate\Support\Str;

class ParentPasswordController extends Controller
{
    /**
       * Write code on Method
       *
       * @return response()
       */
      public function showLinkForm()
      {
         return view('auth.parent.passwords.email');
      }
  
      /**
       * Write code on Method
       *
       * @return response()
       */
      public function passwordEmail(Request $request)
      {
          $request->validate([
              'email' => 'required|email|exists:users',
          ]);

          $parent = DB::table('users')
                              ->where([
                                'email' => $request->email, 
                                'role' => 'parent'
                              ])
                              ->first();
  
          if(!$parent){
              return back()->withInput()->with('error', 'This email not belongs to parent!');
          }
  
          $token = Str::random(64);
  
          DB::table('password_resets')->insert([
              'email' => $request->email, 
              'token' => $token, 
              'created_at' => Carbon::now()
            ]);
  
          Mail::send('mail.parent.forgot-password', ['token' => $token, 'email' => $request->email], function($message) use($request){
              $message->to($request->email);
              $message->subject('Reset Password');
          });
  
          return back()->with('message', 'We have e-mailed your password reset link!');
      }
      /**
       * Write code on Method
       *
       * @return response()
       */
      public function resetForm($token,$email) { 
         return view('auth.parent.passwords.reset', ['token' => $token, 'email' => $email]);
      }
  
      /**
       * Write code on Method
       *
       * @return response()
       */
      public function passwordUpdate(Request $request)
      {
          $request->validate([
              'email' => 'required|email|exists:users',
              'password' => 'required|string|min:6|confirmed',
              'password_confirmation' => 'required'
          ]);
  
          $updatePassword = DB::table('password_resets')
                              ->where([
                                'email' => $request->email, 
                                'token' => $request->token
                              ])
                              ->first();
  
          if(!$updatePassword){
              return back()->withInput()->with('error', 'Invalid token!');
          }
  
          $user = User::where('email', $request->email)
                      ->update(['password' => Hash::make($request->password)]);
 
          DB::table('password_resets')->where(['email'=> $request->email])->delete();
  
          return redirect('parent/login')->with('password-reset', 'Your password has been changed!');
      }
}
