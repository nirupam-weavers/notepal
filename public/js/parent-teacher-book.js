const wdays = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
var base_url = window.location.origin;
var teachertimes = base_url+"/"+"parent/get-teacher-times";
var teacherbook = base_url+"/"+"parent/slot-booking";
var showUser = base_url+"/"+"parent/show-user";
 $('#slot-datepicker').datetimepicker({    
   inline:true,
   format:'Y-m-d',
   timepicker:false,
   minDate:new Date(),
 });

 function addUserId(id){
  $("#teacher-book").attr('data-userid',id);
   $.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
       }
   });
   $.ajax({
       url: showUser,
       type: "POST",
       data: {
           user_id: id,
       },
       cache: false,
       success: function(result){
         $('#showuser').html(result);
       }
   });
 }

 $('#slot-datepicker').on('change', function(e){
    e.preventDefault();
     var date = this.value;
     var dt = new Date(date);
     // var day = wdays[dt.getDay()];
     // var month = months[dt.getMonth()];
     // var day_dt = dt.getDate();
     let day_dt = new Intl.DateTimeFormat('en', {timeZone: 'UTC', day: 'numeric' }).format(dt);
     let month = new Intl.DateTimeFormat('en', {timeZone: 'UTC', month: 'long' }).format(dt);
     let day = new Intl.DateTimeFormat('en', {timeZone: 'UTC', weekday: 'long' }).format(dt);
     $('#show_dt').html(day+', '+month+' '+day_dt);
     var duration = $('#duration').val();
     var user_id = $('#teacher-book').attr('data-userid');
     if(typeof user_id === "undefined"){
      toastr.error('Pleade select teacher.');
     }else{      
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: teachertimes,
        type: "POST",
        data: {
            user_id: user_id,
            day:day,
            day_date:date,
            duration:duration
        },
        cache: false,
        success: function(result){
          $('#booking-time-wrp').html(result);
        }
      });
     }       
  });

  $('#duration').on('change', function(e){
    e.preventDefault();
    var user_id = $('#teacher-book').attr('data-userid');
    var date = $('#slot-datepicker').val();
    if(typeof user_id === "undefined"){
      toastr.error('Pleade select teacher.');
     }else if(date == ""){
      toastr.error('Pleade select date.');
     }
      else{
        var dt = new Date(date);
        // var day = wdays[dt.getDay()];
        // var month = months[dt.getMonth()];
        // var day_dt = dt.getDate();
        let day_dt = new Intl.DateTimeFormat('en', {timeZone: 'UTC', day: 'numeric' }).format(dt);
        let month = new Intl.DateTimeFormat('en', {timeZone: 'UTC', month: 'long' }).format(dt);
        let day = new Intl.DateTimeFormat('en', {timeZone: 'UTC', weekday: 'long' }).format(dt);
        $('#show_dt').html(day+', '+month+' '+day_dt);
        var duration = $('#duration').val();
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url: teachertimes,
          type: "POST",
          data: {
              user_id: user_id,
              day:day,
              day_date:date,
              duration:duration
          },
          cache: false,
          success: function(result){
            $('#booking-time-wrp').html(result);
          }
        }); 
      }
  });

  $('.booking-time-wrp').on('click', '.booking-time-each', function (e) {
    e.preventDefault();
    var ele = $(this);  
    var time = ele.attr('data-time');
    var time_txt = ele.text();
    $('.booking-time-wrp').find('.booking-time-confrm').remove();
    var t_row = '<div class="booking-time-confrm"><span> '+time_txt+'</span><button type="button" data-time="'+time+'" id="confirm" class="booking-confirm">Confirm</button></div>' 
    ele.after(t_row);
  });

  $('.booking-time-wrp').on('click', '#confirm', function (e) {
    e.preventDefault();
    var user_id = $('#teacher-book').attr('data-userid');
    var date = $('#slot-datepicker').val();
    var dt = new Date(date);
    //var day_for = wdays[dt.getDay()];
    let day_for = new Intl.DateTimeFormat('en', {timeZone: 'UTC', weekday: 'long' }).format(dt);
    var duration = $('#duration').val();
    var purpose = $('#purpose').val();
    var subject = $('#subject').val();
    var start_time = $(this).attr('data-time');
    var flag = true;
    if(purpose == ""){
      flag = false;
      toastr.error('Pleade select purpose.');
    }
    if(date == ""){
      flag = false;
      toastr.error('Pleade select date.');
    }

    if(flag){
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: teacherbook,
        type: "POST",
        data: {
            user_id:user_id,
            day_for:day_for,
            date_for:date,
            start_time:start_time,
            duration:duration,
            purpose:purpose,
            //subject:subject
        },
        cache: false,
        success: function(result){
          switch (result['msg']) {
            case 'success':
                toastr.success('Booked successfully.');
                $('#teacherBooking').modal('hide');
                break;
            case 'error':
                toastr.error('Fields are required.');
                break;
          }
        }
      });
    }
  });