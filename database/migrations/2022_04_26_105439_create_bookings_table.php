<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->text('booking_id');
            $table->integer('mentor_id');
            $table->integer('user_id');
            $table->integer('subject_id')->nullable();
            $table->string('purpose')->nullable();
            $table->decimal('session_length');
            $table->string('day_for');
            $table->date('date_for');
            $table->time('start_time');
            $table->time('end_time');
            $table->string('transaction_id')->nullable();
            $table->decimal('amount')->nullable();
            $table->decimal('admin_amount')->nullable();
            $table->decimal('coupon_amount')->nullable();
            $table->integer('coupon_id')->nullable();
            $table->boolean('payment_status')->default(0);
            $table->string('status')->nullable();
            $table->text('cancel_reason');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
