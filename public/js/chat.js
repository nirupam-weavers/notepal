let ip_address = 'http://localhost:3000';
let socket = io(ip_address);

let loggedIn_id = $('#loggedIn_id').val();
let loggedIn_role = $('#loggedIn_role').val();
let receiver_id = $('#receiver_id').val();
socket.on('connect',() => {
    socket.emit('addUser',socket.id,loggedIn_id);
})

function sendMessage(type,data){
    receiver_id = $('#receiver_id').val();
    let message = {
        type: type,
        data: data,
        sender_id:loggedIn_id,
        receiver_id:receiver_id
    }
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: '/api/save-message',
        type: "POST",
        data: message,
        cache: false,
        success: function(res){
            //console.log(res.result.sender_image)
            message.receiver_image = res.result.receiver_image;
            message.sent = res.result.sent;
            socket.emit('sendMessage',message)
            let html = `<div class="chating-content-other-wraper chating-content-you">
            <div class="chating-content-text">
            <p>${message.data}</p>
            </div>
            <div class="chating-contentimg-box">
            <div class="chat-per-img">
                <img src="${res.result.sender_image}">
            </div>
            <div class="time-chat">
                <p>${res.result.sent}</p>
            </div>
            </div>
            </div>`;
            $('#chat-box').append(html);
            fixScroll();
        }
    });
    
}

socket.on('getMessage',(message) => {
    //console.log(message)
    let html = `<div class="chating-content-other-wraper">
    <div class="chating-contentimg-box">
    <div class="chat-per-img">
        <img src="${message.receiver_image}">
    </div>
    <div class="time-chat">
    <p>${message.sent}</p>
    </div>
    </div>
    <div class="chating-content-text">
    <p>${message.data}</p>
    </div>
    </div>`;
    $('#chat-box').append(html);
    fixScroll();
})
$(document).on('click', '.set-receiver', function () {
    let element = $(this);
    let id = element.attr('data-id')
    if (typeof id !== 'undefined' && id !== false) {

    } else {
        element = $(this).parents('.set-receiver');
        id = element.attr('data-id')
    }
    $('#receiver_id').val(id);
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: '/api/get-messages',
        type: "POST",
        data: {
            sender_id:loggedIn_id,
            receiver_id:id,
        },
        cache: false,
        success: function(result){
            $('#chat-box').html(result);
            $('.chat-box-body').show();  
            $('.chating-per-profile-dtls').show();  
            fixScroll();          
        }
    });

    $.ajax({
        url: '/api/get-user',
        type: "POST",
        data: {
            id:id,
        },
        cache: false,
        success: function(res){
            //console.log(res.user_image)
            $('#name').html(res.name);      
            $('#user-mame').html(res.name);      
            $('#category').html(res.role);      
            $('#email').html(res.email);   
            $('#experience').html(''); 
            if(res.experience != null) {  
                $('#experience').html('Experience: <span>'+res.experience+'+ Years </span>');
            }
            let usr_img = `<img src="/front/images/img3.png">`;    
            if(res.user_image != null) {
                usr_img = `<img src="/storage/user/${res.user_image}">`;  
            }
            $('#user-image').html(usr_img);    
        }
    });
})

$('#send-message').on('click',function(){
    let input = $('#chat-input').val();
    if(input != ''){
        sendMessage('text',input);
    }
    $('#chat-input').val('');
})

$('#chat-input').on('keypress',function(e){
    receiver_id = $('#receiver_id').val();
    socket.emit('messageTyping',receiver_id);
    if(e.keyCode == 13){
        e.preventDefault();        
        let input = $('#chat-input').val();
        if(input != ''){
            sendMessage('text',input);
        }
        $('#chat-input').val('');
    }
    //console.log(e.keyCode)
})

const fixScroll = () => {
    chatWindow = document.querySelector(`.chating-content`);
    var xH = chatWindow.scrollHeight;
    chatWindow.scrollTo(0, xH);
}

let typing = null;

socket.on('showMessageTyping', () => {
    $(`#typing`).html('is typing..')
    clearInterval(typing);
    typing = setInterval(function(){
        $(`#typing`).html('')
    },2000)
});


//Search user

var  timerId;
var  searchBoxDom  =  document.getElementById('search');

 // This represents a very heavy method. Which takes a lot of time to execute
 function  makeAjaxCall() {
   var search = searchBoxDom.value;
     $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
         }
     });
     $.ajax({
         url: '/api/search-users',
         type: "POST",
         data: {
             search: search,
             role: loggedIn_role,
             loggedIn_id: loggedIn_id,
         },
         cache: false,
         success: function(res){
           $('#users').html(res);
         }
     });       
 }

 // Debounce function: Input as function which needs to be debounced and delay is the debounced time in milliseconds
 var  debounceFunction  =  function (func, delay) {
   // Cancels the setTimeout method execution
   clearTimeout(timerId)
   // Executes the func after delay time.
   timerId  =  setTimeout(func, delay)
 }

 // Event listener on the input box
 searchBoxDom.addEventListener('input', function () {
   // Debounces makeAjaxCall method
   debounceFunction(makeAjaxCall, 500)
 }); 