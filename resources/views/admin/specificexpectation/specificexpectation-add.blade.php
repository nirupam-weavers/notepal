@extends('admin.layouts.admin-app')

@section('content')

<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>  Specific Expectation <small>Entry Form</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> Specific Expectation</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row"> 
      <!-- left column -->
      <div class="col-md-12"> 
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add</h3>
            @if ($errors->any())
                  <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              @if (session()->has('message'))
                  <div class="alert alert-info alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      {{ session('message') }}
                  </div>
              @endif
              @if (session()->has('error_message'))
                  <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      {{ session('error_message') }}
                  </div>
              @endif
          </div>
          <!-- /.box-header --> 
          <!-- form start -->
          <form name="entry-form" method="post" action="{{route('admin.specificexpectation.store')}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="box-body"> 
              <div class="form-group">
                <label for="parent_id">Select Grade<span>*</span></label>
                  <select class="form-control" id="grade-dropdown" name="grade_id" required>
                    <option value="0">Select Grade</option>
                    @if(!empty($grades))
                    @foreach($grades as $grade)
                    <option value="{{$grade->id}}" >Grade {{$grade->grade}}</option>
                    @endforeach
                    @endif
                  </select>
              </div>  
              <div class="form-group">
                <label for="">Select Subject<span>*</span></label>
                <select class="form-control" id="subject-dropdown" name="subject_id">
                  <option value="">Select Subject</option>
                </select>
              </div>
              <div class="form-group">
                <label for="">Select Strand<span>*</span></label>
                <select class="form-control" id="strand-dropdown" name="strand_id">
                  <option value="">Select Strand</option>
                </select>
              </div>          
              
              <div class="form-group">
                <label for="name">Specific Expectation Name<a class='lnkred'>*</a></label>
                <input type="text" class="form-control" name="specificexpectation" value="" placeholder="specificexpectations Name *" id="specificexpectation_name">
              </div>
            </div>
            <!-- /.box-body -->
            
            <div class="box-footer">
              <input type="submit" class="btn btn-primary" value="Submit">
            </div>
          </form>
        </div>
        <!-- /.box --> 
        
      </div>
      <!--/.col (left) --> 
    </div>
    <!-- /.row --> 
  </section>
  <!-- /.content --> 
</div>

@endsection

@section('script')
<script type="text/javascript">
$(document).ready(function() {
    $('#grade-dropdown').on('change', function() {
      var grade_id = $('#grade-dropdown').val();
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
          }
      });
      $.ajax({
          url: "{{route('admin.grade.subjects')}}",
          type: "POST",
          data: {
              grade_id: grade_id
          },
          cache: false,
          success: function(result){
              $('#subject-dropdown').html('<option value="">Select Subject</option>'); 
              $.each(result.subjects,function(key,value){
              $("#subject-dropdown").append('<option value="'+value.id+'">'+value.subject+'</option>');
              });
              $('#strand-dropdown').html('<option value="">Select Strand</option>'); 
          }
      });       
        
    });    

    $('#subject-dropdown').on('change', function() {
            var subject_id = this.value;
            var grade_id = $('#grade-dropdown').val();;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{route('admin.subject.strands')}}",
                type: "POST",
                data: {
                    subject_id: subject_id,
                    grade_id: grade_id
                },
                cache: false,
                success: function(result){
                  $('#strand-dropdown').html('<option value="">Select Strand</option>'); 
                  $.each(result.strands,function(key,value){
                  $("#strand-dropdown").append('<option value="'+value.id+'">'+value.strand+'</option>');
                  });
                }
            });        
        
    });

});
</script>
@endsection