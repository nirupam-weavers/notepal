@extends('teacher.layouts.app')

@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('teacher.layouts.sidebar')
      <div class="dashbord-content-common ash-bg dashbord-parent-children">
        <div class="parent-search-area">
          <form method="post" action="{{route('teacher.unit.lessons',$id)}}" id="filter-lesson">
            @csrf
            <div class="row">
              <div class="col-lg-2">
                  <div class="form-group">
                    <select class="form-control" id="sort" name="sort">
                      <option value="date">Sort</option>
                      <option value="date" @if(app('request')->input('sort') == "date") selected @endif>Date</option>
                      <option value="views" @if(app('request')->input('sort') == "views") selected @endif>Views</option>
                      <option value="downloads" @if(app('request')->input('sort') == "downloads") selected @endif>Downloads</option>
                      <option value="ratings" @if(app('request')->input('sort') == "ratings") selected @endif>Ratings</option>
                    </select>
                  </div>
              </div>
              <div class="col-lg-2">
                  <div class="form-group">
                    <input type="hidden" name="filter" id="filter" value="{{app('request')->input('filter')}}">
                    <input type="hidden" name="filter_show" id="filter-show">
                    <ul class="filter-wrp">
                      <li class="init">{{app('request')->input('filter_show') ?? "Filter"}}</li>
                      <li  class="filter-each level_one_li">Grade
                          <ul class="filter-each-sub level_one_ul">
                            @foreach($grades as $grade)
                              <li  class="level_two_li" data-show="Grade {{$grade->grade}}" data-value="Grade {{$grade->grade}}">Grade {{$grade->grade}}</li>
                            @endforeach
                          </ul>
                      </li>
                      <li  class="filter-each level_one_li">Subject
                          <ul class="filter-each-sub level_one_ul">
                            @foreach($subjects as $subject)
                            <li  class="level_two_li" data-show="{{$subject->subject}}" data-value="{{$subject->subject}}">{{$subject->subject}}</li>
                            @endforeach
                          </ul>
                      </li>
                      <li class="filter-each level_one_li">Strand
                          <ul class="filter-each-sub level_one_ul">
                          @foreach($strands as $strand)
                            <li  class="level_two_li" data-show="{{$strand->strand}}" data-value="{{$strand->strand}}">{{$strand->strand}}</li>
                          @endforeach
                          </ul>
                      </li>
                      <li class="filter-each level_one_li">Format
                          <ul class="filter-each-sub level_one_ul">
                              <li class="level_two_li" data-value="msword" data-show="MS Word">MS Word</li>
                              <li class="level_two_li" data-value="pdf" data-show="PDF">PDF</li>
                              <li class="level_two_li" data-value="google_doc_link" data-show="Google doc link">Google doc link</li>
                              <li class="level_two_li" data-value="google_form_link" data-show="Google form link">Google form link</li>
                              <li class="level_two_li" data-value="google_slide_link" data-show="Google slide link">Google slide link</li>
                              <li class="level_two_li" data-value="video_link" data-show="Video link">Video link</li>
                          </ul>
                      </li>
                      <li class="filter-each level_one_li">Ratings
                          <ul class="filter-each-sub level_one_ul">
                              <li class="level_two_li" data-show="Ratings 1" data-value="1">Ratings 1</li>
                              <li class="level_two_li" data-show="Ratings 2" data-value="2">Ratings 2</li>
                              <li class="level_two_li" data-show="Ratings 3" data-value="3">Ratings 3</li>
                              <li class="level_two_li" data-show="Ratings 4" data-value="4">Ratings 4</li>
                              <li class="level_two_li" data-show="Ratings 5" data-value="5">Ratings 5</li>
                          </ul>
                      </li>
                    </ul>
                  </div>
              </div>
              <div class="col-lg-4">
                  <div class="form-group">
                    <input type="text" class="form-control" id="search" name="search" aria-describedby="" placeholder="Search" value="{{app('request')->input('search')}}">
                  </div>
              </div>
              <div class="col-lg-2">
                <button type="submit" id="search-form" class="btn">Search Now</button>
              </div>
              <div class="col-lg-2">
                <a href="{{route('teacher.lesson')}}" class="btn">Clear</a>
              </div>
            </div>
          </form>
        </div>


        <div class="dashbord-content-common-pad">
          <div class="student-independnt-practic-sec">
            <h2> Lessons:</h2>
            <div id="post-data">
                @include('teacher.layouts.lessons')
            </div>
            
            @if($lessons->lastPage() > 1)
            <div class="lod-more-btn see-more">
              <a href="javascript:void(0);" class="btn yellow-btn">Load More</a>
            </div>
            @endif
            <div class="ajax-load text-center" style="display:none; width: 50px; background: transparent; margin: 0px auto;">
                <img src="{{asset('front/images/blue-loader.gif')}}">
            </div>
          </div>
        </div>
      </div>
      @endsection
      @section('scripts')

      <script>
        var page = 1;
        var lastPage = "{{$lessons->lastPage()}}";
        jQuery(document).on('click', ".see-more", function() {
            page++;
            loadMoreData(page);
            if (lastPage <= page) {
                jQuery(this).hide();
                return false;
            }
        });
        jQuery(document).on('change', '#sort', function() {
            page = 1;
            var sortingOpt = jQuery(this).val();
            $.ajax({
                url: '?sort=' + sortingOpt + '&page=' + page,
                type: "get",
                beforeSend: function() {
                    $('.ajax-load').show();
                }
            }).done(function(data) {
                if (data.html == " ") {
                    $('.ajax-load').html("No more records found");
                    return;
                }
            //console.log(lastPage);
                if (lastPage > 1) {
                    jQuery(".see-more").show();
                }

                $('.ajax-load').hide();
                $("#post-data").html('');
                $("#post-data").append(data.html);

            }).fail(function(jqXHR, ajaxOptions, thrownError) {
                alert('server not responding...');
            });

        })

        function loadMoreData(page) {
            var sortingOpt = jQuery('#sort').val();
            $.ajax({
                url: '?sort=' + sortingOpt + '&page=' + page,
                type: "get",
                beforeSend: function() {
                    $('.ajax-load').show();
                }
            }).done(function(data) {
                if (data.html == " ") {
                    $('.ajax-load').html("No more records found");
                    return;
                }
                $('.ajax-load').hide();
                $("#post-data").append(data.html);

            }).fail(function(jqXHR, ajaxOptions, thrownError) {
                alert('server not responding...');
            });
        }

        $("ul").on("click", ".init", function () {
          $(".level_one_li").removeClass('open_level_one_li');
            $(".level_one_ul").removeClass('open_level_one_ul');
            $(this).parent("ul").children('li:not(.init)').slideToggle();            
        });

        $(".level_one_li").on("click", function () {
          
          var old = $(this).siblings('.open_level_one_li');
          if(old.length > 0){
            old.removeClass('open_level_one_li');
            old.children('ul').removeClass('open_level_one_ul');
          }

          $(this).addClass('open_level_one_li');
          $(this).children('ul').addClass('open_level_one_ul');
        });

        // $(".level_one_li").on("click", function () {
        //     $('.level_one_ul').removeClass('open_level_one_ul');
        //     $(this).children("ul").toggleClass('open_level_one_ul');
        // });

        $(".level_two_li").on("click", function (e) {
            $('#filter').val($(e.target).attr('data-value'));
            $('#filter-show').val($(e.target).attr('data-show'));
            $('.init').html($(e.target).attr('data-show'));
            $('.filter-wrp').children('li:not(.init)').slideToggle();
            $('#filter-lesson').submit();
        });
      </script>
    @endsection