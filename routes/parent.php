<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ChildController;
use App\Http\Controllers\Parent\ParentController;
use App\Http\Controllers\Parent\ReportCardController;
use App\Http\Controllers\Auth\ParentLoginController;
use App\Http\Controllers\Auth\ParentRegisterController;
use App\Http\Controllers\Auth\ParentPasswordController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\Teacher\TeacherController;
use App\Http\Controllers\Parent\CalendarController;
use App\Http\Controllers\Parent\ChatController;
use App\Http\Controllers\Parent\NotificationController;
use App\Http\Controllers\HomeController;

Route::post('/send/otp', [ChildController::class, 'send_otp'])->name('send.otp');
Route::post('/verify/otp', [ChildController::class, 'otp_verify'])->name('otp.verify');
Route::get('/signup', [ParentRegisterController::class, 'showRegistrationForm'])->name('parent.signup');
Route::get('/login', [ParentLoginController::class, 'showLoginForm'])->name('parent.login');
Route::post('/login/post', [ParentLoginController::class, 'login'])->name('parent.login.post');
Route::post('/register', [ParentRegisterController::class, 'register'])->name('parent.register');

Route::get('forgot-password', [ParentPasswordController::class, 'showLinkForm'])->name('parent.password.form');
Route::post('email-password', [ParentPasswordController::class, 'passwordEmail'])->name('parent.password.email'); 
Route::get('reset-password/{token}/{email}', [ParentPasswordController::class, 'resetForm'])->name('parent.reset.password');
Route::post('update-password', [ParentPasswordController::class, 'passwordUpdate'])->name('parent.password.update');

Route::post('add/child', [ChildController::class, 'add_child'])->name('add.child');

Route::group(['middleware' => ['role:parent','auth']], function(){
	Route::get('/home', [ParentController::class, 'index'])->name('parent.home');
	Route::get('/childedit/{id}', [ParentController::class, 'showChildedit'])->name('parent.childedit');
	Route::post('/updatechildprofile', [ParentController::class, 'updateChildProfile'])->name('parent.updatechildprofile');
	Route::get('/profile', [ParentController::class, 'showUpdateProfile'])->name('parent.updateprofile');
	Route::post('/updateprofile/post', [ParentController::class, 'updateProfile'])->name('parent.updateprofile.post');
	Route::post('/logout', [ParentController::class, 'logout'])->name('parent.logout');

	//search
	Route::post('/show-user', [HomeController::class,'showUser']);
	Route::get('search-users', [ParentController::class, 'search_user'])->name('parent.search.users');
	Route::any('search-users/teacher', [ParentController::class, 'showTeacherSearch'])->name('parent.teacher.search');
	Route::any('search-users/parent', [ParentController::class, 'showParentSearch'])->name('parent.parent.search');
	Route::get('/add-report-card', [ReportCardController::class, 'add_report_card'])->name('parent.report.card.add');
	Route::post('/slot-booking', [BookingController::class, 'slot_booking']);
	Route::post('/get-teacher-times', [TeacherController::class, 'get_availability']);

	Route::any('/report-card', [ReportCardController::class, 'index'])->name('parent.report.card');
	Route::post('/report-card-post', [ReportCardController::class, 'report_post'])->name('report.post');
	Route::post('/report-card-update', [ReportCardController::class, 'report_update'])->name('report.update');
	Route::any('/edit-report-card', [ReportCardController::class, 'edit_report_card'])->name('parent.report.card.edit');
	Route::any('/update-report-card', [ReportCardController::class, 'update_report_card'])->name('parent.report.card.update');

	Route::get('calendar', [CalendarController::class, 'index'])->name('parent.calendar');
	Route::post('/accept-booking', [BookingController::class, 'accept_booking'])->name('parent.accept.booking');
	Route::post('/cancel-booking', [BookingController::class, 'cancel_booking'])->name('parent.cancel.booking');
	Route::post('/selfcancel-booking', [BookingController::class, 'selfcancel_booking'])->name('parent.selfcancel.booking');

	//Chat
	Route::get('chat', [ChatController::class, 'index'])->name('parent.chat');
	Route::get('notification', [NotificationController::class, 'index'])->name('parent.notification');
});