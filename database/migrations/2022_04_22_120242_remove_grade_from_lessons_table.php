<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveGradeFromLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('lessons', 'grade')){
  
            Schema::table('lessons', function (Blueprint $table) {
                $table->dropColumn('grade');
            });
        }
        if (Schema::hasColumn('lessons', 'expectation_id')){
  
            Schema::table('lessons', function (Blueprint $table) {
                $table->dropColumn('expectation_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lessons', function (Blueprint $table) {
            //
        });
    }
}
