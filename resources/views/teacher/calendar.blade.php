@extends('teacher.layouts.app')

@section('content')
<div class="dashbord-common cal-page">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('teacher.layouts.sidebar')
      @include('teacher.popup.slot-book')
      @include('teacher.popup.availability')
      @include('teacher.popup.cancel-booking')
      @include('popup.common-book')
      
      <div class="dashbord-content-common ash-bg dashbord-parent-children dashbord-parent-calender">
          <div class="dashbord-content-common-pad">
            <div class="teacher-calender-page">
              <div class="teacher-calender-page-wraper">
                <div class="teacher-calender-lft">
                  <div class="calender-ability">
                    <div class="row align-items-center">
                      <div class="col-lg-6">
                        <a href="javascript:void(0);" class="btn blue-btn" data-toggle="modal" data-target="#scheduleBooking">Schedule Booking</a>
                        <a href="javascript:void(0);" class="btn blue-btn" data-toggle="modal" data-target="#manageAvailability">Manage Availability</a>
                      </div>
                      <div class="col-lg-6">
                      </div>
                    </div>
                  </div>
                  
                  
                    @if(count($bookings)>0)
                    <div class="partner-calender-Scheduled">
                    <h2>Booking Requests</h2>
                    <div class="partner-calender-Scheduled-wraper">
                      @php $i = 1; @endphp
                      @foreach($bookings as $booking)
                      <?php
                      $bg='light-yellow-bg';
                      if($i%2 == 0){
                        $bg='light-red-bg';
                      }
                      ?>
                      <div class="partner-calender-Scheduled-sngl {{$bg}}">
                        <div class="partner-calender-Scheduled-icon-text">
                          <div class="partner-calender-icon">
                            <i class="fas fa-desktop"></i>
                          </div>
                          <div class="partner-calender-text">
                          @if($booking->subject)
                          <p>{{$booking->subject->subject}} Lesson</p>
                          @else
                          <h3>{{$booking->purpose}}</h3>
                          @endif                            
                            <ul>
                              <li>{{date('H:i',strtotime($booking->start_time))}} - {{date('H:i',strtotime($booking->end_time))}} </li>
                              <li>{{date('d F, Y', strtotime($booking->date_for))}}</li>
                            </ul>
                          </div>
                        </div>
                        <div class="partner-cal-user-star">
                          <div class="user-image">
                            <img src="{{ asset('front/images/img3.png')}} ">
                          </div>
                          <div class="user-image-text">
                            <h3>{{$booking->mentor->name}}</h3>
                            <ul>
                              <li><img src="{{ asset('front/images/star.png')}}"></li>
                              <li>4.9</li>
                            </ul>
                          </div>
                        </div>
                        <div class="partner-calender-subject">
                          <h3>Subject:</h3>
                          @if($booking->subject)
                          <p>{{$booking->subject->subject}}</p>
                          @endif
                        </div>
                        <div class="partner-calender-btnd">
                          <ul>
                            @if($booking->status == "")
                            <li><a href="javascript:void(0);" class="btn ash-btn" data-toggle="modal" data-target="#cancelModal" onclick="cancelBooking({{$booking->id}});">Cancel</a>
                            </li>
                            <li>
                              <form method="post" action="{{route('accept.booking')}}">
                                  @csrf
                                  <input type="hidden" name="id" value="{{$booking->id}}">
                                  <button type="submit" class="btn blue-btn">Accept</button>
                              </form>
                            </li>
                            @else
                            <li></li>
                            <li><button type="button" class="btn blue-btn">{{$booking->status}}</button></li>
                            @endif
                          </ul>
                        </div>
                      </div>
                      
                      @php $i++; @endphp
                      @endforeach
                    </div>
                    </div>
                    @endif
                   @if(count($pendding_statuses)>0)
                    <div class="partner-calender-Scheduled">
                      <h2>My Booking Requests </h2>
                      <div class="partner-calender-Scheduled-wraper">
                        @php $i = 1; @endphp
                        @foreach($pendding_statuses as $pendding_status)
                        <?php
                        $bg='light-yellow-bg';
                        if($i%2 == 0){
                          $bg='light-red-bg';
                        }
                        ?>
                        <div class="partner-calender-Scheduled-sngl {{$bg}}">
                          <div class="partner-calender-Scheduled-icon-text">
                            <div class="partner-calender-icon">
                              <i class="fas fa-desktop"></i>
                            </div>
                            <div class="partner-calender-text">
                              @if($pendding_status->subject)
                              <p>{{$pendding_status->subject->subject}} Lesson</p>
                              @else
                              <h3>{{$pendding_status->purpose}}</h3>
                              @endif
                              <ul>
                              <li>{{date('H:i',strtotime($pendding_status->start_time))}} - {{date('H:i',strtotime($pendding_status->end_time))}} </li>
                              <li>{{date('d F, Y', strtotime($pendding_status->date_for))}}</li>
                              </ul>
                            </div>
                          </div>
                          <div class="partner-cal-user-star">
                            <div class="user-image">
                              <img src="{{ asset('front/images/img3.png')}}">
                            </div>
                            <div class="user-image-text">
                              <h3>{{$pendding_status->mentor->name}}</h3>
                              <ul>
                                <li><img src="{{ asset('front/images/star.png')}}"></li>
                                <li>4.9</li>
                              </ul>
                            </div>
                          </div>
                          <div class="partner-calender-subject">
                            <h3>Subject:</h3>                            
                          </div>
                          <div class="partner-calender-status">
                            <ul>
                            <form method="post" action="{{route('teacher.selfcancel.booking')}}">
                              <li><button type="button"  data-toggle="modal" id="edit_id" data-userid="{{$pendding_status->user_id}}" data-mentorid="{{$pendding_status->mentor_id}}" data-id="{{$pendding_status->id}}" data-date="{{$pendding_status->date_for}}" data-time="{{$pendding_status->start_time}}" data-target="#commonBooking"  onclick="addUserId({{$pendding_status->mentor_id}});"><i class="far fa-edit"></i></button></li>
                              @if($pendding_status->user_status != "Cancelled")
                              @csrf
                                  <input type="hidden" name="id" value="{{$pendding_status->id}}">
                                  <li><button type="submit" class="del_status"><i class="fa fa-times" aria-hidden="true"></i></button></li>
                              </form>
                              @endif
                             <li>Status:</li>
                              @if($pendding_status->user_status == "")
                              <li class="pending-statis">Pending</li>
                              @elseif($pendding_status->user_status == "Accepted")
                              <li class="pending-statis">{{$pendding_status->user_status}}</li>
                              @else
                              <li class="cancel-status">{{$pendding_status->user_status}}</li>
                              @endif
                            </ul>
                          </div>
                        </div>
                        @php $i++; @endphp
                        @endforeach
                      </div>
                    </div>
                   @endif

                   @if(count($pendding_statuses)>0)
                  <div class="parent-calender-history">
                      <h2>History</h2>
                      <div class="parent-calender-history-wraper">
                      @php $i = 1; @endphp
                      @foreach($pendding_statuses as $pendding_status)
                      <?php
                      $bg='light-red-bg';
                      if($i%2 == 0){
                        $bg='light-yellow-bg';
                      }
                      ?>
                        <div class="parent-calender-history-sngl">
                          <div class="parent-calender-history-top">
                            <div class="parent-calender-his-img">
                              <img src="{{ asset('front/images/img3.png')}}">
                            </div>
                            <div class="parent-calender-his-name">
                              <h4>{{$pendding_status->user->name}}</h4>
                              <p>{{$pendding_status->user->role}}</p>
                            </div>
                          </div>
                          <div class="parent-calender-history-dtls {{$bg}}">
                            <h5>Meeting Attended</h5>
                            <ul>
                              <li>{{date('H:i',strtotime($pendding_status->start_time))}} - {{date('H:i',strtotime($pendding_status->end_time))}} </li>
                              <li>{{date('d F, Y', strtotime($pendding_status->date_for))}}</li>
                            </ul>
                          </div>
                        </div>
                        
                        <!-- <div class="parent-calender-history-sngl">
                          <div class="parent-calender-history-top">
                            <div class="parent-calender-his-img">
                              <img src="{{ asset('front/images/img3.png')}}">
                            </div>
                            <div class="parent-calender-his-name">
                              <h4>Eddy Teller</h4>
                              <p>Teacher</p>
                            </div>
                          </div>
                          <div class="parent-calender-history-dtls dark-ash-bg">
                            <h5>Meeting Cancelled</h5>
                            <ul>
                              <li>16.30 - 17.30</li>
                              <li>2nd September,2021</li>
                            </ul>
                          </div>
                        </div> -->
                        @php $i++; @endphp
                        @endforeach
                       </div>
                    </div>
                </div>
                @endif
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
@section('scripts')
    <script src="{{asset('js/common-book.js')}}"></script>
    <script src="{{asset('js/slot-book.js')}}"></script>
    <script src="{{asset('js/availability.js')}}"></script>
    <script src="{{asset('js/times.js')}}"></script>
   
    <script type="text/javascript">      
      function cancelBooking(id){
        $('#cancel_booking_id').val(id);
      }
    </script>
@stop