<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'role',
        'password',
        'google_id',
        'age',
       
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function generateOtpToken()
    {
        do {
            $code = mt_rand(100000,999999);
            $codeStatus = Self::where("otp", $code)->first();
        }while($codeStatus);

        return $code;
    }

    public function favorites()
    {
        return $this->hasMany(Favorite::class);
    }

    public function subjects()
    {
        return $this->belongsToMany(Subject::class,'expertises','user_id','subject_id');
    }

    public function expertise()
    {
         return $this->belongsTo(Expertise::class);
    }

    public function expertises()
    {
        return $this->hasMany(Expertise::class);
    }

    public function rating()
    {
         return $this->belongsTo(Rating::class);
    }
    public function children()
    {
    	return $this->hasMany(User::class, 'parent_id');
    }
    public function parent()
    {
    	return $this->belongsTo(User::class, 'parent_id');
    }

    public function notificationSetting()
    {
         return $this->belongsTo(NotificationSetting::class);
    }

    public function followers()
    {
         return $this->belongsTo(Follower::class);
    }
}
