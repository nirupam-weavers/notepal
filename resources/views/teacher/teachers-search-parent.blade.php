@extends('teacher.layouts.app')

@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('teacher.layouts.sidebar')
      @include('popup.parent-book')
      <div class="dashbord-content-common ash-bg dashbord-parent-children">
        <div class="dashbord-content-common-pad">
          <div class="dashbord-inner-right-padd parent-search-area-wrap">
            <div class="parent-search-area">
              <form method="post" action="{{route('teacher.parent.search')}}" id="filter-person">
                @csrf
                <div class="row">
                  <div class="col-lg-4">
                    <div class="form-group">
                      <input type="text" class="form-control" id="search" name="search" aria-describedby="" placeholder="Search here by name of the user" value="{{app('request')->input('search')}}">
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <button type="submit" class="btn">Search Now</button>
                  </div>
                  <div class="col-lg-2">
                    <a href="{{route('teacher.parent.search')}}" class="btn">Clear</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="parent-teacher-srch-reslt only-teacher-srch-reslt">
            <h2>Parents</h2>
            <div class="parent-teacher-srch-reslt-wrap">
              <table class="table" id="parent-book" data-userid="">
                <thead>
                  <tr>
                    <th scope="col">Profile Image</th>
                    <th scope="col">Parent's Name</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($users as $user)
                  <tr>
                    @if(isset($user->user_image))
                    <td>
                      <div class="user-img"><a href="{{ url('public/profile/'.$user->id) }}"><img src="{{ asset('storage/user/'.$user->user_image) }}" alt=""></a></div>
                    </td>
                    @else
                    <td>
                      <div class="user-img"><a href="{{ url('public/profile/'.$user->id) }}"><img src="{{ asset('front/images/img3.png')}}" alt=""></a></div>
                    </td>
                    @endif
                    <td>{{$user->name}}</td>
                    <td><a href="javascript:void(0);" class="btn yellow-btn" data-bs-toggle="modal" data-bs-target="#parentBooking" onclick="addUserId({{$user->id}});">Book A Slot</a></td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="loder-btn">
                {{$users->links()}}
              </div>
            </div>
          </div>



          <div class="seperator"></div>

        </div>
      </div>

      @endsection

      @section('scripts')
      <script src="{{asset('js/parent-book.js')}}"></script>
      <script src="{{asset('js/times.js')}}"></script>
      @endsection