<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    public function children()
    {
    	return $this->hasMany(Category::class, 'parent_id');
    }
    public function get_children()
    {
    	return $this->children()->where('status', 1);
    }
    public function parent()
    {
    	return $this->belongsTo(Category::class, 'parent_id');
    }
    public function category_comments()
    {
    	return $this->belongsToMany(Comment::class, 'category_comments','category_id','comment_id');
    }
    public function reports()
    {
        return $this->hasMany(Report::class);
    }
}
