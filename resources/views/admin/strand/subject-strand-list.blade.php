@extends('admin.layouts.admin-app')

@section('content')
 <!-- Content Wrapper. Contains page content -->

<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Subject Strand <small>Subject Strand List</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Subject Strand</a></li>
      <li class="active">Subject Strand List</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Subject Strand List</h3>
            @if ($errors->any())
                  <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              @if (session()->has('message'))
                  <div class="alert alert-info alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      {{ session('message') }}
                  </div>
              @endif
              @if (session()->has('error_message'))
                  <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      {{ session('error_message') }}
                  </div>
              @endif
            <a href="{{route('subject.strand.create')}}" class="btn btn-primary a-btn-slide-text" style="float:right"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <span><strong>Add</strong></span> </a> 
            </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="category_table" class="table table-bordered table-hover">
              <thead>
                <tr>
                   <th><b>Sl No</b></th>
                   <th><b>Grade</b></th>
                   <th><b>Subject</b></th>
                   <th><b>strand</b></th>
                   <th align="center">Action</th>
                </tr>
              </thead>
              <tbody>
               @if(!empty($strands))
               @php $i=1; @endphp
                 @foreach($strands as $strand)                 
                  <tr>
                    <td>{{$i}}</td>
                    <td>Grade {{$strand->grade->grade}}</td>
                    <td>{{$strand->subject->subject}}</td>
                    <td>{{$strand->strand->strand}}</td>
                    <td><a class="btn btn-primary" href="{{ url('admin/strand/subject/edit/'.$strand->id) }}">Edit</a>
                    <a class="btn btn-danger" href="{{ url('admin/strand/subject/delete/'.$strand->id) }}" onclick="return confirm('Do you want to remove?')">Delete</a>
                    </td>
                 </tr>
                 @php $i++; @endphp
                 @endforeach
               @endif
             
              </tbody>
              
            </table>
          </div>
          <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
      <!-- /.col --> 
    </div>
    <!-- /.row --> 
  </section>
  <!-- /.content --> 
</div>
<!-- /.content-wrapper -->
@endsection

 