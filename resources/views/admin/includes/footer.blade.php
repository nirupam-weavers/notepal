 <footer class="main-footer">
    <div class="pull-right hidden-xs">
    </div>
    <strong>Copyright &copy; {{date('Y')}} <a href="#">{{config('app.name')}}</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('admin/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="{{asset('admin/dist/js/bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{asset('admin/plugins/morris/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('admin/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('admin/plugins/knob/jquery.knob.js')}}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{asset('admin/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('admin/plugins/select2/select2.full.min.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('admin/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- <script src="{{asset('admin/plugins/datepicker/bootstrap-datepicker.min.js')}}"></script> -->
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('admin/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('admin/plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('admin/dist/js/app.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('admin/dist/js/demo.js')}}"></script>
<!-- page script -->
<!--<script src="dist/js/pages/dashboard.js"></script>-->
<!-- page script -->
<script src="{{asset('admin/plugins/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('admin/dist/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('admin/dist/js/jszip.min.js')}}"></script>
<script src="{{asset('admin/dist/js/pdfmake.min.js')}}"></script>
<script src="{{asset('admin/dist/js/vfs_fonts.js')}}"></script>
<script src="{{asset('admin/dist/js/buttons.print.min.js')}}"></script>
<script src="{{asset('admin/dist/js/buttons.html5.min.js')}}"></script>
@yield('script')
<script>
  $('#discount').on('input', function(e){     
   if (isNaN(this.value)) {
        $(this).val('');
   }
   if(this.value > 100){
       $(this).val(100);
    }
  });
  
  function confirm_delete() {
      return confirm('Are you sure to delete???');
  } 

   $(function () {
    $("#example1").DataTable({
		"ordering": false,
		});
    $('#example2').DataTable({
      "aLengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]],
      "pageLength": 25,
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
  
</script>
<script type="text/javascript">
  $(document).ready(function(){      

    //Initialize Select2 Elements
    $('.select2').select2(); 
       
    $('#from_date').on('changeDate', function(ev){
        $('#hidden_from_date').val(ev.target.value)
        $(this).datepicker('hide');
    });
    $('#to_date').on('changeDate', function(ev){
        $('#hidden_to_date').val(ev.target.value)
        $(this).datepicker('hide');
    });   
    $("#from_date").datepicker({          
    format: 'mm-dd-yyyy', //can also use format: 'dd-mm-yyyy' 
    // startDate: new Date()  
    // endDate: new Date()  
    });
    $("#to_date").datepicker({          
    format: 'mm-dd-yyyy', //can also use format: 'dd-mm-yyyy' 
    //startDate: new Date()
    // endDate: new Date()    
    });
  });

  $('#filter').on('click',function(e){
    e.preventDefault();
    var from = $("#from_date").val();
    var to = $("#to_date").val();

    if(Date.parse(from) > Date.parse(to)){
       alert("Invalid Date Range");
    }
    else{
       $("#filter_form").submit();
    }
  });

  jQuery('#user-list').select2({
  placeholder: "User",
  minimumInputLength: 2,
  ajax: {
    url: "<?php echo url('api/users/list')?>",
    dataType: 'json',
    data: function (params) {
      return {
        q: $.trim(params.term)
      };
    },
    processResults: function (data) {
    console.log(data);
      return {
        results: data
      };
    },
    cache: true
  }
});

  $('#number_of_users').on('change',function(){    
    if (this.value == 'custom') {
      $("#custom_number").show();
    }else{
      $("#custom_number").hide();
      $("#custom_number").val('');
    }
  });
</script>
</body>
</html>
