<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ContactEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ContactEmailsController extends Controller
{

    public function index()
    {
        $contactEmails = ContactEmail::orderBy('created_at', 'DESC')->get();
        return view('admin.contact-emails.index', compact('contactEmails'));
    }

    public function destroy($id)
    {
        $data = ContactEmail::find($id)->delete();
        
        \Session::flash('message','Successfully deleted.'); 
        return redirect()->back();
    }


}
