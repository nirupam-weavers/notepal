<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubjectStrand extends Model
{
    use HasFactory;

    protected $fillable = [
        'grade_id',
        'subject_id',
        'strand_id',
    ];

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }
    public function grade()
    {
        return $this->belongsTo(Grade::class);
    }
    public function strand()
    {
        return $this->belongsTo(Strand::class);
    }
}
