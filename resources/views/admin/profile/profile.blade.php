@extends('admin.layouts.admin-app')

@section('content')

<div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> Edit Profile </h1>
        <ol class="breadcrumb">
        <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Edit Profile</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row"> 
            <!-- left column -->
            <div class="col-md-12"> 
                <!-- general form elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="title"> Edit Profile</h4>
                        @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (session()->has('message'))
                            <div class="alert alert-info alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ session('message') }}
                            </div>
                        @endif
                        @if (session()->has('error_message'))
                            <div class="alert alert-danger alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ session('error_message') }}
                            </div>
                        @endif
                    </div>
                    <!-- /.box-header --> 
                    <form name="profile-edit" id="profile-edit" action="{{ route('profile.profile.update', $user->id) }}" method="post" enctype="multipart/form-data" data-parsley-validate>
                        @csrf
                        @method('patch')
                        <div class="panel-body">
                            <input type="hidden" name="id" value="{{$user->id}}">
                            <div class="col-lg-12 text-center">
                                <div class="profile-pic profile-fix1">
                                    <div class="profile-user">
                                        <img id="blah" alt="" src="{{ asset('front/images/profile.png')}}">
                                    </div>
                                </div>
                            </div>
                    
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" placeholder="Enter your name" value="{{$user->name}}"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="email" placeholder="Enter your email address" value="{{$user->email}}" />
                                        </div>
                                    </div>                     
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                             <button class="btn btn-success">
                                                <i class="fa fa-save"> Save </i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

<style>
.profile-pic .profile-user img {
    width: 227px;
    height: 227px;
    object-fit: cover;
    border-radius: 100px;
}
.profile-pic {
    width: 227px;
    height: 227px;
    margin: auto;
    position: relative;
    margin-bottom: 60px !important;
}
.profile-pic .profile-user input[type="file"] {
    position: absolute;
    opacity: 1;
    cursor: pointer;
    height: 49px;
    right: 0;
    bottom: 0px; 
    border-radius: 67px;
    width: 49px;
}
i.fa.fa-camera {
    position: absolute;
    right: 0;
    bottom: 0px;
    width: 49px;
    height: 49px;
    background-color: #FFF;
    border: none;
    color: #006FFF;
    font-size: 25px;
    border-radius: 60px;
    display: flex;
    align-items: center;
    justify-content: center;
	font-weight: 900;
    cursor: pointer;
}
.profile-pic .profile-user input[type="file"]:after {
    position: absolute;
    right: 0;
    bottom: 0px;
    width: 49px;
    height: 49px;
    background-color: #FFF;
    border: none;
    color: #006FFF;
    font-size: 25px;
    border-radius: 60px;
    display: flex;
    align-items: center;
    content: "\f030";
    font-family: 'Font Awesome\ 5 Free';
    justify-content: center;
	font-weight: 900;
}
.image-alert{ 
color: #e80000;
    font-size: 12px;
    margin-bottom: 15px;
}
.profile-fix1{ margin-bottom:20px;}
</style>
@endsection

@section('script')
<script type="text/javascript">
    jQuery(document).on('click','i.fa.fa-camera',function(){
        jQuery('#imgInp').click();
    })
  imgInp.onchange = evt => {
    const [file] = imgInp.files
    if (file) {
      blah.src = URL.createObjectURL(file)
    }
  }
</script>
@endsection