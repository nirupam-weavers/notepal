<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
    public function comment_list(Request $request)
    {
    	$comment_list=Comment::orderBy('order', 'asc')->get();
        
        return view('admin.comment.comment_list',compact('comment_list'));
    }
    public function comment_add(){
        return view('admin.comment.add_comment');
    }
    public function comment_create(Request $request){
        $this->validate($request,[
            'name' => ['required', 'string', 'max:255'],
        ]);
        
        $comment = new Comment;
        $comment->name = $request->name;
        $comment->value = $request->value;
        $comment->order = $request->order;
        $comment->description = $request->description;
        $comment->save();
        
        \Session::flash('message','Successfully added.'); 
        return redirect()->route('list.comment');
    }
    public function comment_edit($id)
    {
        $comment=Comment::findOrFail($id);        
        return view('admin.comment.edit_comment',compact('comment'));
    }
    public function update_comment(Request $request){
        $this->validate($request,[
            'comment_id' => 'required'
        ]);
        
        $comment=Comment::findOrFail($request->comment_id);
        $comment->name = $request->name;
        $comment->value = $request->value;
        $comment->order = $request->order;
        $comment->description = $request->description;
        $comment->save();
        
        \Session::flash('message','Successfully updated.'); 
        return redirect()->route('list.comment');
    }
}
