
@foreach($messages as $message)
@if($sender_id!=$message->sender_id)
<div class="chating-content-other-wraper">
    <div class="chating-contentimg-box">
    <div class="chat-per-img">
        @if(!empty($message->sender->user_image))
            <img src="{{ asset('storage/user/'.$message->sender->user_image)}}" class="{{$message->id}}receiver{{$message->sender->id}}">
        @else
            <img src="{{ asset('front/images/img3.png')}}">
        @endif
    </div>
    <div class="time-chat">
        <p>{{\Carbon\Carbon::parse($message->created_at)->format('h:i a')}}</p>
    </div>
    </div>
    <div class="chating-content-text">
    <p>{{$message->message}}</p>
    </div>
</div>
@else
<div class="chating-content-other-wraper chating-content-you">
    <div class="chating-content-text">
    <p>{{$message->message}}</p>
    </div>
    <div class="chating-contentimg-box">
    <div class="chat-per-img">
        @if(!empty($message->sender->user_image))
            <img src="{{ asset('storage/user/'.$message->sender->user_image)}}" class="{{$message->id}}sender{{$message->sender->id}}">
        @else
            <img src="{{ asset('front/images/img3.png')}}">
        @endif
    </div>
    <div class="time-chat">
        <p>{{\Carbon\Carbon::parse($message->created_at)->format('h:i a')}}</p>
    </div>
    </div>
</div>
@endif
@endforeach