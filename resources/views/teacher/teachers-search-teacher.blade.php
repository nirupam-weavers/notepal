@extends('teacher.layouts.app')

@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('teacher.layouts.sidebar')
      @include('popup.teacher-book')
      <div class="dashbord-content-common ash-bg dashbord-parent-children">
          <div class="dashbord-content-common-pad">
            <div class="dashbord-inner-right-padd parent-search-area-wrap">
            <div class="parent-search-area">
          <form method="post" action="{{route('teacher.teacher.search')}}" id="filter-person">
            @csrf
            <div class="row">
              <div class="col-lg-4">
                  <div class="form-group">
                    <input type="text" class="form-control" id="search" name="search" aria-describedby="" placeholder="Search here by name of the user" value="{{app('request')->input('search')}}">
                  </div>
              </div>
              <div class="col-lg-2">
                <button type="submit"   class="btn">Search Now</button>
              </div>
              <div class="col-lg-2">
                <a href="{{route('teacher.teacher.search')}}" class="btn">Clear</a>
              </div>
            </div>
          </form>
        </div>
            </div>
            <div class="parent-teacher-srch-reslt only-teacher-srch-reslt">
              <h2>Teachers</h2>
              <div class="parent-teacher-srch-reslt-wrap">
                <table class="table" id="teacher-book" data-userid="">
                  <thead>
                    <tr>
                      <th scope="col">Profile Image</th>
                      <th scope="col">Teacher's Name</th>
                      <th scope="col">Experience</th>
                      <th scope="col">Subject</th>
                      <th scope="col">Rating</th>
                      <th scope="col">Badges Earned</th>
                      <th scope="col">Action</th>
                      <th scope="col">Follow</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($users as $user)
                    @if($user->role == 'teacher')
                    <tr id="search-result">
                      @if(isset($user->user_image))
                      <td><div class="user-img"><a href="{{ url('public/profile/'.$user->id) }}"><img src="{{ asset('storage/user/'.$user->user_image) }}" alt=""></a></div></td>
                      @else
                      <td><div class="user-img"><a href="{{ url('public/profile/'.$user->id) }}"><img src="{{ asset('front/images/img3.png')}}" alt=""></a></div></td>
                      @endif
                      <td>{{$user->name}}</td>
                      <td>{{$user->experience}}+ years</td>
                      <td>
                        <ul class="list-show">
                          @foreach($user->subjects as $subject)
                          <li><span>{{$subject->subject}}</span></li>
                          @endforeach
                        </ul>
                      </td>
                      <td>
                        <ul class="rating-box">
                          <li>
                            <ul>
                              <li><i class="fas fa-star"></i></li>
                              <li><i class="fas fa-star"></i></li>
                              <li><i class="fas fa-star"></i></li>
                              <li><i class="fas fa-star"></i></li>
                              <li><i class="fas fa-star"></i></li>
                            </ul>
                          </li>
                          <li>4.9</li>
                        </ul>
                      </td>
                      <td>
                        <ul class="achiv-box">
                          <li><div class="achive-img"><img src="{{ asset('front/images/achv.png')}}" alt=""></div></li>
                          <li>Ninza 2021</li>
                        </ul>
                      </td>
                      <td><a href="javascript:void(0);" class="btn yellow-btn" data-bs-toggle="modal" data-bs-target="#teacherBooking" onclick="addUserId({{$user->id}});">Book A Slot</a></td>
                      <td>
                      <?php
                        if(\App\Models\Follower::where(['user_id' => auth()->user()->id, 'follower_id'=> $user->id])->exists()){?>
                        <div class="binacular follower" data-id="{{$user->id}}"><img id="followerimg" src="{{ asset('front/images/binaclr11.png')}}" alt=""></div>
                       <?php }
                        else{?>
                         <div class="binacular follower" data-id="{{$user->id}}"><img id="followerimg" src="{{ asset('front/images/binaclr.png')}}" alt=""></div>
                     <?php }
                      ?>                       
                      </td>
                    </tr>
                    @endif
                    @endforeach
                  </tbody>
                </table>
                <div class="loder-btn">
                  {{$users->links()}}
                </div>
              </div>
            </div>           

            <div class="seperator"></div>

          </div>
        </div>

@endsection

@section('scripts')
<script src="{{asset('js/teacher-book.js')}}"></script>
@endsection