<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\FcmController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\Teacher\LessonController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('save-token', [FcmController::class, 'saveToken']);
Route::post('send-message', [FcmController::class, 'sendMessage']);

Route::post('save-message', [ChatController::class, 'saveMessage']);
Route::post('get-messages', [ChatController::class, 'getMessages']);
Route::post('get-user', [ChatController::class, 'getUser']);
Route::post('search-users', [ChatController::class, 'searchUsers']);

Route::post('show-user', [HomeController::class,'showUser']);
Route::post('edit-booking', [BookingController::class, 'edit_booking']);

Route::post('save-download-count', [LessonController::class, 'save_download_count']);