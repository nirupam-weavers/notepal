<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use App\Models\Grade;
use App\Models\Subject;
use App\Models\Strand;
use App\Models\Expectation;
use App\Models\GradeSubject;
use App\Models\SubjectStrand;
use DB;
class AdminController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('teacher.home');
    }

    public function admin_dashboard()
    {
        $total_user = User::count();
        return view('admin.dashboard',compact('total_user'));
    }

    public function logout(Request $request) 
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('admin/login');
    }

    public function showGradeList()
    {
        $grades = Grade::orderBy('created_at')->get(); 
        
        return view('admin.grade.grade-list',['grades'=>$grades]);
    }

    public function gradeCreate()
    {
      return view('admin.grade.grade-add');
    }

    public function showGradeEdit($id)
    {
        $grades = Grade::find($id);
        return view('admin.grade.grade-edit',['grades'=>$grades]);
    }

    public function gradeStore(Request $request)
    {
        $data = $request->all();
        //dd($data);
        $rules = [
            'grade' => ['required', 'string', 'max:255'],
        ];
        $messages = array(
          'grade.required' => 'This field is required',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } 

        $grade = Grade::create([
        'grade' => $data['grade'],
       ]);
        \Session::flash('message','Successfully updated.');
       return redirect()->route('admin.grade.list');
    }

    public function gradeUpdate(Request $request)
    {
        $data = $request->all();
        //dd($data);
        $rules = [
            'grade' => ['required', 'string', 'max:255'],
        ];
        $messages = array(
          'grade.required' => 'This field is required',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } 
        $grade = Grade::find($request->id); 
        $grade->grade = $request->grade;
        $grade->save();
        \Session::flash('message','Successfully updated.');
        return redirect()->route('admin.grade.list');
    }

    public function deleteGrade($id)
    {
        $grade = Grade::findOrFail($id);
        $grade->delete();
        \Session::flash('message','Successfully deleted.');
        return redirect()->route('admin.grade.list');
    }

    public function subjectList()
    {
        $subjects = Subject::orderBy('created_at')->get(); 
        
        return view('admin.subject.subject-list',['subjects'=>$subjects]);
    }
    public function showSubjectList()
    {
        $subjects = Subject::has('grade_subjects')->get(); 
        
        return view('admin.subject.subject-list',['subjects'=>$subjects]);
    }

    public function SubjectCreate()
    {
        $grades = Grade::all();
        return view('admin.subject.subject-add',['grades'=>$grades]);
    }

    public function subjectEdit($id)
    {
        $subject = Subject::find($id);
        return view('admin.subject.subject-edit',compact('subject'));
    }

    public function subjectStore(Request $request)
    {
        $data = $request->all();
        //dd($data);
        $rules = [
            'subject' => ['required', 'string', 'max:255'],
        ];
        $messages = array(
          'subject.required' => 'Subject field is required',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
       
        $subject = Subject::updateOrCreate(
            ['subject' => $data['subject']],
            ['subject' => $data['subject']]
        );
        \Session::flash('message','Successfully updated.'); 
       return redirect()->route('admin.subject.list');
    }

    public function deleteSubject($id)
    {
        $subject = Subject::findOrFail($id);
        $subject->delete();
        DB::table('grade_subjects')->where('subject_id',$id)->delete();
        return redirect()->route('admin.subject.list');
    }

    public function subject_grade(Request $request)
    {
        $grades = Grade::all();
        $subjects = Subject::orderBy('created_at')->get(); 
        return view('admin.subject.assign-subject-grade',compact('grades','subjects'));
    }

    public function assign_subject_grade(Request $request)
    {
        $data = $request->all();
        //dd($data);
        $rules = [
            'grade_id' => ['required'],
            'subject_id' => ['required'],
        ];
        $messages = array(
          'grade_id.required' => 'Grade field is required',
          'subject_id.required' => 'Subject field is required',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        foreach ($data['grade_id'] as $key => $grade_id) {
            GradeSubject::updateOrCreate(
                ['subject_id' => $data['subject_id'],'grade_id' => $grade_id],
                ['subject_id' => $data['subject_id'],'grade_id' => $grade_id]
            );
        }
        \Session::flash('message','Successfully updated.'); 
       return redirect()->route('grade.subject.list');
    }
    public function subject_grade_list()
    {
        $gradeSubjects = GradeSubject::with('grade','subject')->orderBy('created_at')->get(); 
        
        return view('admin.subject.subject-grade-list',compact('gradeSubjects'));
    }
    public function subjectGradeDelete($id)
    {
        $subject = GradeSubject::findOrFail($id);
        $subject->delete();
        \Session::flash('message','Successfully deleted.'); 
        return redirect()->route('grade.subject.list');
    }

    public function subjectGradeEdit($id)
    {
        $gradeSubject = GradeSubject::findOrFail($id);
        $grades = Grade::all();
        $subjects = Subject::orderBy('created_at')->get();
        return view('admin.subject.subject-grade-edit',compact('subjects','grades','gradeSubject'));
    }

    public function subjectStore_bkp(Request $request)
    {
        $data = $request->all();
        //dd($data);
        $rules = [
            'subject' => ['required', 'string', 'max:255'],
        ];
        $messages = array(
          'subject.required' => 'This field is required',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } 
        $subject = Subject::create([
         'subject' => $data['subject'],
        ]);
        //dd($subject->grade_subjects());
        foreach ($data['grade_id'] as $key => $grade_id) {
            $subject->grade_subjects()->attach(['grade_id' => $grade_id]);
        }
       return redirect()->route('admin.subject.list');
    }

    public function subjectUpdate(Request $request)
    {
        $data = $request->all();
        //dd($data);
        $rules = [
            'subject' => ['required', 'string', 'max:255'],
        ];
        $messages = array(
          'subject.required' => 'Subject is required',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } 
        
        $subject = Subject::find($request->id); 
        $subject->subject = $request->subject;
        $subject->save();
        
        \Session::flash('message','Successfully updated.'); 
        return redirect()->route('admin.subject.list');
    }

    public function subjectUpdate_bkp(Request $request)
    {
        $data = $request->all();
        //dd($data);
        $rules = [
            'subject' => ['required', 'string', 'max:255'],
            'grade_id' => ['required'],
        ];
        $messages = array(
          'subject.required' => 'This field is required',
          'grade_id.required' => 'This field is required',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } 
        DB::table('grade_subjects')->where('subject_id',$request->id)->delete();
        $subject = Subject::find($request->id); 
        $subject->subject = $request->subject;
        $subject->save();
        
        foreach ($data['grade_id'] as $key => $grade_id) {
            $subject->grade_subjects()->attach(['grade_id' => $grade_id]);
        }
        return redirect()->route('admin.subject.list');
    }

    public function strandList()
    {
        $strands = Strand::orderBy('created_at')->get();       
        return view('admin.strand.strand-list',compact('strands'));
    }

    public function strandCreate()
    {
      return view('admin.strand.strand-add');
    }

    public function strandEdit($id)
    {
        $strand = Strand::findOrFail($id);
        return view('admin.strand.strand-edit',compact('strand'));
    }

    public function deleteStrand($id)
    {
        $strand = Strand::findOrFail($id);
        $strand->delete();
        \Session::flash('message','Successfully deleted.');
        return redirect()->route('admin.strand.list');
    }

    public function strandStore(Request $request)
    {
        $data = $request->all();
        $rules = [
            'strand' => ['required', 'string', 'max:255'],
        ];
        $messages = array(
          'strand.required' => 'Strand field is required',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } 
       
        $strand = Strand::create([
            'strand' => $data['strand']
           ]);
        \Session::flash('message','Successfully updated.');
        return redirect()->route('admin.strand.list');
    }

    public function strandUpdate(Request $request)
    {
        $data = $request->all();
        $rules = [
            'strand' => ['required', 'string', 'max:255'],
        ];
        $messages = array(
          'strand.required' => 'Strand field is required',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } 
        $strand = Strand::findOrFail($request->id);
        $strand->strand = $request->strand;
        $strand->save();
        \Session::flash('message','Successfully updated.');
        return redirect()->route('admin.strand.list');
    }

    public function subjectStrandCreate()
    {
        $subjects = Subject::all();
        $grades = Grade::all();
        $strands = Strand::all();
        return view('admin.strand.subject-strand-add',compact('subjects','grades','strands'));
    }
    public function subjectStrandEdit($id)
    {
        $strand = SubjectStrand::findOrFail($id);
        $subjects = Subject::all();
        $grades = Grade::all();
        $strands = Strand::all();
        return view('admin.strand.subject-strand-edit',compact('subjects','grades','strand','strands'));
    }
    public function subject_strand_store(Request $request)
    {
        $data = $request->all();
        $rules = [
            'grade_id' => ['required'],
            'subject_id' => ['required'],
            'strand_id' => ['required'],
        ];
        $messages = array(
          'grade_id.required' => 'Grade field is required',
          'subject_id.required' => 'Subject field is required',
          'strand_id.required' => 'Strand field is required',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } 
        SubjectStrand::updateOrCreate(
            ['grade_id' => $data['grade_id'],'subject_id' => $data['subject_id'],'strand_id' => $data['strand_id']],
            ['grade_id' => $data['grade_id'],'subject_id' => $data['subject_id'],'strand_id' => $data['strand_id']]
        );
        \Session::flash('message','Successfully updated.');
        return redirect()->route('subject.strand.list');
    }
    public function subject_strand_update(Request $request)
    {
        $data = $request->all();
        $rules = [
            'grade_id' => ['required'],
            'subject_id' => ['required'],
            'strand_id' => ['required'],
        ];
        $messages = array(
          'grade_id.required' => 'Grade field is required',
          'subject_id.required' => 'Subject field is required',
          'strand_id.required' => 'Strand field is required',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } 
        $strand = SubjectStrand::findOrFail($data['id']);
        $strand->grade_id = $data['grade_id'];
        $strand->subject_id = $data['subject_id'];
        $strand->strand_id = $data['strand_id'];
        $strand->save();
        \Session::flash('message','Successfully updated.');
        return redirect()->route('subject.strand.list');
    }

    public function subject_strand_list(Request $request)
    {
        $strands = SubjectStrand::orderBy('created_at')->get();       
        return view('admin.strand.subject-strand-list',compact('strands'));
    }
    public function deleteSubjectStrand($id)
    {
        $strand = SubjectStrand::findOrFail($id);
        $strand->delete();
        \Session::flash('message','Successfully deleted.');
        return redirect()->route('subject.strand.list');
    }
    
    public function showSpecificexpectationList()
    {
        $expectations = Expectation::has('strand_expectation')->orderBy('created_at')->get(); 
        
        return view('admin.specificexpectation.specificexpectation-list',compact('expectations'));
    }

    public function showSpecificexpectationCreate()
    {
        $strands = Strand::all();
        $grades = Grade::all();
         return view('admin.specificexpectation.specificexpectation-add',compact('strands','grades'));
    }

    public function showSpecificexpectationEdit($id)
    {
        $specificexpectations = Expectation::find($id);
        $grades = Grade::all();
        $strands = Strand::all();
        $subjects = Subject::all();
        $exp_strand = [];
        if($specificexpectations->strand_expectation()->count()){
            $exp_strand = $specificexpectations->strand_expectation()->pluck('strand_id')->all();
        }
        return view('admin.specificexpectation.specificexpectation-edit',compact('specificexpectations','grades','strands','subjects','exp_strand'));
    }

    public function specificexpectationStore(Request $request)
    {
        $data = $request->all();
        //dd($data);
        $rules = [
            'specificexpectation' => ['required', 'string', 'max:255'],
        ];
        $messages = array(
          'specificexpectation.required' => 'This field is required',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $expectation = new Expectation;
        $expectation->grade_id = $data['grade_id'];
        $expectation->subject_id = $data['subject_id'];
        $expectation->strand_id = $data['strand_id'];
        $expectation->expectation = $data['specificexpectation'];
        $expectation->save();
        $expectation->strand_expectation()->attach(['strand_id' => $data['strand_id']]);

        // foreach ($data['strand_id'] as $key => $strand_id) {
        //     $specificexpectation->strand_expectation()->attach(['strand_id' => $strand_id]);
        // }
       return redirect()->route('admin.specificexpectation.list');
    }
    
    public function specificexpectationUpdate(Request $request)
    {
        $data = $request->all();
        //dd($data);
        $rules = [
            'specificexpectation' => ['required', 'string', 'max:255'],
            'strand_id' => ['required'],
        ];
        $messages = array(
          'specificexpectation.required' => 'This field is required',
          'strand_id.required' => 'This field is required',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } 
        DB::table('strand_expectations')->where('expectation_id',$request->id)->delete();
        $expectation = Expectation::find($request->id); 
        $expectation->grade_id = $data['grade_id'];
        $expectation->subject_id = $data['subject_id'];
        $expectation->strand_id = $data['strand_id'];
        $expectation->expectation = $data['specificexpectation'];
        $expectation->save();
        
        $expectation->strand_expectation()->attach(['strand_id' => $data['strand_id']]);
        return redirect()->route('admin.specificexpectation.list');
    }

    public function deleteSpecificExpectation($id)
    {
        $expectation = Expectation::findOrFail($id);
        $expectation->delete();
        DB::table('strand_expectations')->where('expectation_id',$id)->delete();
        return redirect()->route('admin.specificexpectation.list');
    }

    public function grade_subjects(Request $request)
    {
        $data = [];
        if(!empty($request->grade_id)){            
            $data['subjects'] = Subject::wherehas("grade_subjects",function($query) use ($request){
                $query->where('grade_id',$request->grade_id);
            })
            ->get(["subject","id"]);
        }
        
        return response()->json($data);
    }

    public function subject_strands(Request $request)
    {
        $data['strands'] = Strand::wherehas('subject_strands',function($query) use ($request){
            $query->where("subject_id",$request->subject_id);
            $query->where("grade_id",$request->grade_id);
        })        
        ->get(["strand","id"]);
        return response()->json($data);
    }

}
