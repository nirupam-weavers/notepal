@foreach($lessons as $lesson)            
<div  class="student-independnt-practic-sngl-box" >
  <div class="student-independnt-practic-sngl-box-lt">
    <div class="roted-box">
      <h3>
        Grade : {{implode(',',$lesson->grade_lesson()->pluck('grade_id')->all())}}
      </h3>
    </div>
  </div>
  <div class="student-independnt-practic-sngl-box-rt">
  <ul class="top-box">
    <li><span>Subject:</span> 
      @if($lesson->subject)
      {{$lesson->subject->subject}}
      @endif
    </li>
    <li><span>Strand:</span> 
      @if($lesson->strand)
      {{$lesson->strand->strand}}
      @endif
    </li>
    <li><span>BY:</span> {{$lesson->user->name}} / {{ date('d-m-Y', strtotime($lesson->created_at))}}</li>
  </ul>
  <ul>
  @if($lesson->expectation_lesson)
    @foreach($lesson->expectation_lesson as $exp)
    <li><p> {{$exp->expectation}}  </p></li>
    @endforeach
  @endif
  </ul>
  <div class="student-independnt-btm">
    <div class="row align-items-center">
      <div class="col-lg-6">
        <ul class="dnld-sec">
          <li>
            <ul class="star-rating">
              <li><i class="fas fa-star"></i></li>
              <li><i class="fas fa-star"></i></li>
              <li><i class="fas fa-star"></i></li>
              <li><i class="fas fa-star"></i></li>
              <li><i class="fas fa-star"></i></li>
              <li>{{$lesson->avg_rating}}</li>
            </ul>
          </li>
          <li><span><i class="fas fa-download"></i></span> {{$lesson->download_count}}</li>
          <li><span><i class="far fa-eye"></i></span> {{$lesson->view_count}}</li>
        </ul>
      </div>
      <div class="col-lg-6">
        <ul class="indepnd-action-box">
          <li class="blue-box"><a href=""><i class="fas fa-share-alt"></i></a></li>
          <?php
          $favoriteClass = "white-box";
          if($lesson->userFavorites()){                        
            if ($lesson->userFavorites()->where(['document_type' => 'lesson','user_id' => auth()->user()->id])->exists()) {
              $favoriteClass = "yellow-box";
            }
          }
          ?>
          <li class="{{$favoriteClass}} favorite" data-id="{{$lesson->id}}" data-doctype="lesson"><i class="fas fa-star"></i></li>
          <li class="white-box"><a href="{{ url('teacher/single-lesson/'.$lesson->id) }}"><i class="fas fa-chevron-right"></i></a></li>
        </ul> 
      </div>
    </div>
  </div>
  </div>
</div>
@endforeach