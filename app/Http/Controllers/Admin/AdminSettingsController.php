<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Models\Settings;
use Illuminate\Support\Facades\Input;

class AdminSettingsController extends Controller
{
    public function saveSettings(Request $request){
		
		$requests = $request->all();
		$meta_key = $requests['key'];
		$meta_value = $requests['value'];
		Settings::saveSettings($meta_key,$meta_value);
		
		return response()->json(['response' => 'done']); 
		
	}
	
	public function settingsOption(){
		return \View::make('admin.settings', []);
	}
	
	public function saveOrUpdateSettings(Request $request){
		$settings = $request->settings;
		foreach($settings as $meta_key => $meta_value){
			Settings::saveSettings($meta_key,$meta_value);
		}
		\Session::flash('message','Successfully updated.');
		return Redirect::to('/admin/settings');
	}
	
}
