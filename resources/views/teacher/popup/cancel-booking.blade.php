<!-- Schedule a Booking Modal -->
<div class="modal fade custom-modal" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="cancelModal"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true"><img src="{{asset('front/images/cross.svg')}}" alt="" /> </span>
      </button>

      <div class="modal-body">       
        <form action="{{route('teacher.cancel.booking')}}" method="POST">
          @csrf
          <input type="hidden" name="id" id="cancel_booking_id">
          <div class="col-lg-12 text-center">
              <h2 class="color-blue modal-heading">Cancel a booking</h2>
          </div>

          <div class="row justify-content-center align-items-center">
              <div class="col-lg-11">
                  <div class="row">
                      <div class="col-lg-12 mt-4">
                          <h6 class="color-blue font-16 book-fix">Reason:</h6>
                          <div class="form-input">
                              <textarea class="form-control" name="reason" placeholder="Write a reason for cancelling..."></textarea>
                          </div>
                      </div>
                      <div class="col-lg-12 text-center">
                          <div class="form-input">
                              <input type="submit" value="Submit" class="btn">
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </form>       

      </div>

    </div>
  </div>
</div>