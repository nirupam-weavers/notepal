var firebaseConfig = {
  apiKey: "AIzaSyBDZsKsG3jmQF-08NPauSEmCLeooqdaXGE",
  authDomain: "notepal-chat.firebaseapp.com",
  databaseURL: "https://notepal-chat-default-rtdb.firebaseio.com",
  projectId: "notepal-chat",
  storageBucket: "notepal-chat.appspot.com",
  messagingSenderId: "823290701522",
  appId: "1:823290701522:web:c6b57b9d93e99f4a936f0b",
  measurementId: "G-HMCQJX3BXV"
};
  
firebase.initializeApp(firebaseConfig);
//const messaging = firebase.messaging();
// initialize database
const db = firebase.database();

let loggedIn_id = $('#loggedIn_id').val();
var sender_image = $('#user_image').val();
let loggedIn_role = $('#loggedIn_role').val();
let receiver_id = $('#receiver_id').val();


function initFirebaseMessagingRegistration(event,url=null) {
   event.preventDefault();
    messaging
    .requestPermission()
    .then(function () {
        return messaging.getToken()
    })
    .then(function(token) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: '/api/save-token',
            type: 'POST',
            data: {
                token: token,
                id: loggedIn_id,
            },
            dataType: 'JSON',
            success: function (response) {
                jQuery('#web-notification').modal('hide');
                window.location.href=url;
            },
            error: function (err) {
                console.log('User Chat Token Error'+ err);
            },
        });

    }).catch(function (err) {
        console.log('User Chat Token Error'+ err);
    });
}

//const fetchChat = db.ref("messages/");


// messaging.onMessage(function(payload) {
//     let res = JSON.parse(payload.notification.body);
//     //console.log(res.sender_image)
//     let html = `<div class="chating-content-other-wraper">
//         <div class="chating-contentimg-box">
//         <div class="chat-per-img">
//             <img src="${res.sender_image}">
//         </div>
//         <div class="time-chat">
//         <p>${res.sent_at}</p>
//         </div>
//         </div>
//         <div class="chating-content-text">
//         <p>${payload.notification.title}</p>
//         </div>
//         </div>`;
//         $('#chat-box').append(html);
//         fixScroll();
// });

function sendMessage(type,data){
    let sent_at = moment().format("hh:mm A");
    let sent = moment().format();
    var ts = Date.parse(sent);

    receiver_id = $('#receiver_id').val();
    let message = {
        type: type,
        data: data,
        sender_image:sender_image,
        sender_id:loggedIn_id,
        receiver_id:receiver_id,
        sent_at:sent_at
    }
    db.ref("chats/" + loggedIn_id).push().set({
        type: type,
        data: data,
        sender_image:sender_image,
        sender_id:loggedIn_id,
        receiver_id:receiver_id,
        sent_at:sent_at,
        sent:ts
    });
    // $.ajaxSetup({
    //     headers: {
    //         'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
    //     }
    // });
    // $.ajax({
    //     url: '/api/send-message',
    //     type: "POST",
    //     data: message,
    //     cache: false,
    //     success: function(res){
    //         //console.log(res)
    //     }
    // });

    let html = `<div class="chating-content-other-wraper chating-content-you">
        <div class="chating-content-text">
        <p>${message.data}</p>
        </div>
        <div class="chating-contentimg-box">
        <div class="chat-per-img">
            <img src="${message.sender_image}">
        </div>
        <div class="time-chat">
            <p>${message.sent_at}</p>
        </div>
        </div>
        </div>`;
        $('#chat-box').append(html);
        fixScroll();    
}

$(document).on('click', '.set-receiver', function () {
    let element = $(this);
    let id = element.attr('data-id');
    receiver_id = element.attr('data-id');
    //console.log(loggedIn_id)
    if (typeof id !== 'undefined' && id !== false) {

    } else {
        element = $(this).parents('.set-receiver');
        id = element.attr('data-id')
    }
    $('#receiver_id').val(id);
    $('.chat-box-body').show();  
    $('.chating-per-profile-dtls').show(); 
    $('#chat-box').html('');
    //const rootRef = db.ref().child('chats');
    const senderRef = db.ref('chats/' + loggedIn_id).orderByChild('receiver_id').equalTo(id);
    const receiverRef = db.ref('chats/' + id).orderByChild('receiver_id').equalTo(loggedIn_id);
    // const senderRef = rootRef.orderByValue('sender_id').equalTo(loggedIn_id).orderByValue('receiver_id').equalTo(loggedIn_id);
    // const receiverRef = rootRef.orderByChild('receiver_id').equalTo(loggedIn_id);
    var rows = [];
    senderRef.once('value').then((snapshot) => {
        let res = snapshot.val();
        $.each(res,function(index,value){
            if(res != null){        
               rows.push(value); 
            }
        })
        //showRows(rows);
        receiverRef.once('value').then((snapshot) => {
            let res = snapshot.val();
            $.each(res,function(index,value){
                if(res != null){        
                   rows.push(value); 
                }
            })
            rows.sort((a, b) => a.sent - b.sent);
            showRows(rows);
        });
    });

    fixScroll();
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/api/get-user',
        type: "POST",
        data: {
            id:id,
        },
        cache: false,
        success: function(res){
            //console.log(res)
            $('#name').html(res.name);      
            $('#user-mame').html(res.name);      
            $('#category').html(res.role);      
            $('#email').html(res.email);   
            $('#experience').html(''); 
            if(res.experience != null) { 
                $('#experience').show();  
                $('#experience').html('Experience: <span>'+res.experience+'+ Years </span>');
            }else{
                $('#experience').hide(); 
            }
            let usr_img = `<img src="/front/images/img3.png">`;    
            if(res.user_image != null) {
                usr_img = `<img src="/storage/user/${res.user_image}">`;  
            }
            $('#user-image').html(usr_img);    
            $('#subject').html(res.subject_str);   
        }
    });
    const fetchChat = db.ref('chats/' + id).orderByChild('receiver_id').equalTo(loggedIn_id);
    fetchChat.on("child_added", function (snapshot) {
      const messages = snapshot.val();

      let html = `<div class="chating-content-other-wraper">
            <div class="chating-contentimg-box">
            <div class="chat-per-img">
                <img src="${messages.sender_image}">
            </div>
            <div class="time-chat">
            <p>${messages.sent_at}</p>
            </div>
            </div>
            <div class="chating-content-text">
            <p>${messages.data}</p>
            </div>
            </div>`;
            $('#chat-box').append(html);
            fixScroll();
    });

    const isType = db.ref('messages/' + receiver_id).orderByChild('receiver_id').equalTo(loggedIn_id);
        isType.on("child_changed", function (snapshot) {
          const data = snapshot.val();
          if(data.isTyping > 0){
            $(`#typing`).html('is typing..');        
          }else{
            $(`#typing`).html('');
          }
     });
    
})

let typing = null;
clearInterval(typing);
typing = setInterval(function(){
    $(`#typing`).html('')
},2000)


function showRows(frows){
    $.each(frows,function(index,value){
        if(loggedIn_id != value.sender_id){        
            let html = `<div class="chating-content-other-wraper">
            <div class="chating-contentimg-box">
            <div class="chat-per-img">
                <img src="${value.sender_image}">
            </div>
            <div class="time-chat">
            <p>${value.sent_at}</p>
            </div>
            </div>
            <div class="chating-content-text">
            <p>${value.data}</p>
            </div>
            </div>`;     
            $('#chat-box').append(html);       
        }else{
            let html = `<div class="chating-content-other-wraper chating-content-you">
                <div class="chating-content-text">
                <p>${value.data}</p>
                </div>
                <div class="chating-contentimg-box">
                <div class="chat-per-img">
                        <img src="${value.sender_image}">
                </div>
                <div class="time-chat">
                    <p>${value.sent_at}</p>
                </div>
                </div>
            </div>`;
            $('#chat-box').append(html);
        }
        fixScroll();
    });
}

$('#send-message').on('click',function(){
    let input = $('#chat-input').val();
    if(input != ''){
        sendMessage('text',input);
    }
    $('#chat-input').val('');
})

let count = 0;
$('#chat-input').on('keypress',function(e){
    count++;
    receiver_id = $('#receiver_id').val();
    const ref = db.ref('messages/' + loggedIn_id).orderByChild('receiver_id').equalTo(receiver_id);
    ref.once("value")
      .then(function(snapshot) {
        let data = snapshot.val();
        var isExist = snapshot.exists();
        if(!isExist){
            db.ref("messages/" + loggedIn_id).push().set({
                isTyping: count,
                sender_id:loggedIn_id,
                receiver_id:receiver_id
            });
        }else{
        var newPostKey = Object.keys(data)[0];
        var postData = {
                isTyping: count,
                sender_id:loggedIn_id,
                receiver_id:receiver_id
            };
        // Write the new post's data simultaneously in the posts list and the user's post list.
          var updates = {};
          updates["messages/" + loggedIn_id +"/"+ newPostKey] = postData;
          db.ref().update(updates);  
        }

        if(e.keyCode == 13){
            e.preventDefault();  
            var newPostKey = Object.keys(data)[0];
            var postData = {
                isTyping: 0,
                sender_id:loggedIn_id,
                receiver_id:receiver_id
            };
            var updates = {};
            updates["messages/" + loggedIn_id +"/"+ newPostKey] = postData;
            db.ref().update(updates);

            let input = $('#chat-input').val();
            if(input != ''){
                sendMessage('text',input);
            }
            $('#chat-input').val('');
            $(`#typing`).html('');
        }
      });
      
})



const fixScroll = () => {
    chatWindow = document.querySelector(`.chating-content`);
    var xH = chatWindow.scrollHeight;
    chatWindow.scrollTo(0, xH);
}

//Search user

var  timerId;
var  searchBoxDom  =  document.getElementById('search');

 // This represents a very heavy method. Which takes a lot of time to execute
 function  makeAjaxCall() {
   var search = searchBoxDom.value;
     $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
         }
     });
     $.ajax({
         url: '/api/search-users',
         type: "POST",
         data: {
             search: search,
             role: loggedIn_role,
             loggedIn_id: loggedIn_id,
         },
         cache: false,
         success: function(res){
           $('#users').html(res);
         }
     });       
 }

 // Debounce function: Input as function which needs to be debounced and delay is the debounced time in milliseconds
 var  debounceFunction  =  function (func, delay) {
   // Cancels the setTimeout method execution
   clearTimeout(timerId)
   // Executes the func after delay time.
   timerId  =  setTimeout(func, delay)
 }

 // Event listener on the input box
 searchBoxDom.addEventListener('input', function () {
   // Debounces makeAjaxCall method
   debounceFunction(makeAjaxCall, 500)
 }); 