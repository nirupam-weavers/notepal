@extends('admin.layouts.admin-app')


@section('content')
<div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>  Change Password </h1>
        <ol class="breadcrumb">
        <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Change Password</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row"> 
            <!-- left column -->
            <div class="col-md-12"> 
                <!-- general form elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="title"> Change Password</h4>
                        @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (session()->has('message'))
                            <div class="alert alert-info alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ session('message') }}
                            </div>
                        @endif
                        @if (session()->has('error_message'))
                            <div class="alert alert-danger alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ session('error_message') }}
                            </div>
                        @endif
                    </div>
                    <!-- /.box-header --> 

                    <form action="{{ route('profile.password.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('patch')
                        <div class="panel-body">
                            <div class="form-group">
                                <label>Old Password</label>
                                <span class="text-danger">*</span>
                                <input type="password" class="form-control {{ $errors->has('old_password') ? 'is-invalid' : '' }}" name="old_password" id="old_password">
                                <!-- @if ($errors->has('old_password'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('old_password') }}</strong>
                                </span>
                                @endif -->
                            </div>
                            <div class="form-group">
                                <label>New Password</label>
                                <span class="text-danger">*</span>
                                <input type="password" class="form-control {{ $errors->has('new_password') ? 'is-invalid' : '' }}" name="new_password" id="new_password">
                                <!-- @if ($errors->has('new_password'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('new_password') }}</strong>
                                </span>
                                @endif -->
                            </div>
                            <div class="form-group">
                                <label>Confirm Password</label>
                                <span class="text-danger">*</span>
                                <input type="password" class="form-control {{ $errors->has('confirm_new_password') ? 'is-invalid' : '' }}" name="confirm_new_password" id="confirm_new_password">
                                <!-- @if ($errors->has('confirm_new_password'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('confirm_new_password') }}</strong>
                                </span>
                                @endif -->
                            </div>
                            <div class="form-group">
                                
                                <button class="btn btn-primary">
                                    <i class="fa fa-save"> Save </i>
                                </button>
                            </div>
                        </div>
                        <div class="panel-footer">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection