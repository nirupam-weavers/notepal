<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use stdClass;

class FcmController extends Controller
{
    public function saveToken(Request $request)
    {
        //auth()->user()->update(['fcm_token'=>$request->token]);
        $user = User::findOrFail($request->id);
        $user->fcm_token = $request->token;
        $user->save();
        return response()->json(['token saved successfully.']);
    }

    public function sendMessage(Request $request)
    {
        $user = User::findOrFail($request->receiver_id);
        $firebaseToken = $user->fcm_token;
          //dd($firebaseToken);
        $SERVER_API_KEY = 'AAAAv6_zJtI:APA91bGbSx4mnhKXvOLZ_PRT1qWVoFxTiZRlXVqIqZBbHUh6xx_yevz8yZeSGPSN2bv8rM8dOHeY4JQE0pp4M_OVNLxFuvRoFJpL34wIYH5ZqA9K7cGr3scnZ-v3vUwIruN1cehIzFqw';
        $body = [
        	"sender_image" => $request->sender_image,  
            "sent_at" => $request->sent_at
        ];
  
        $data = [
            "to" => $firebaseToken,
            "notification" => [
                "title" => $request->data,
                "body" => $body,  
                'sound'=>"default"
            ],
            "webpush" => [
		      "fcm_options"=> [
		        "link"=> "http://127.0.0.1:8000/teacher/chat"
		      ]
            ],
            'priority'=>'high'
        ];
        $dataString = json_encode($data);
    
        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];
    
        $ch = curl_init();
      
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
               
        $response = curl_exec($ch);
  //dd($response);
        return response()->json($response);
    }
}
