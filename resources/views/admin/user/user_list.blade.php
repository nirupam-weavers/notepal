@extends('admin.layouts.admin-app')

@section('content')
 <!-- Content Wrapper. Contains page content -->

<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> User <small>User List</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">User</a></li>
      <li class="active">User List</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">User List</h3>
            </div>
            @if (session()->has('message'))
              <div class="alert alert-info alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {{ session('message') }}
              </div>
            @endif
          <!-- /.box-header -->
          <div class="box-body">
            <table id="user_table" class="table table-bordered table-hover">
              <thead>
                <tr>
                   <th><b>ID</b></th>
                   <th><b>Name</b></th>
                   <th><b>Email</b></th>
                   <th><b>Role</b></th>
                </tr>
              </thead>
              <tbody>
               @if(!empty($user_list))
               @php $i=1; @endphp
                 @foreach($user_list as $user)                 
                  <tr>
                   <td>{{ $i }}</td>
                   <td>{{$user->name}}</td>
                   <td>{{$user->email}}</td>
                   <td>{{$user->role}}</td>
                 </tr>
                 @php $i++; @endphp
                 @endforeach
               @endif
             
              </tbody>
              
            </table>
          </div>
          <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
      <!-- /.col --> 
    </div>
    <!-- /.row --> 
  </section>
  <!-- /.content --> 
</div>
<!-- /.content-wrapper -->
@endsection

@section('script')
<script type="text/javascript">
  $('#user_table').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'excel', 'pdf'
            {
                extend: 'excel',
                title: 'Users'
            }
        ]
    } );
</script>
@endsection
