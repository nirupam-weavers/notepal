<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Expectation extends Model
{
    use HasFactory;
    protected $fillable = [
        'expectation',
     ];

    public function strand_expectation()
    {
        return $this->belongsToMany(Strand::class, 'strand_expectations','expectation_id', 'strand_id');
    }

    public function grade()
    {
        return $this->belongsTo(Grade::class);
    }
    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }
    public function strand()
    {
        return $this->belongsTo(Strand::class);
    }
}
