@extends('admin.layouts.admin-app')

@section('content')
 <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Contact Emails <small>Contact Email List</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Contact Emails</a></li>
      <li class="active">Contact Emails List</li>
    </ol>
  </section>
  
  <!-- Main content -->
  
 @if(Session::has('message'))
  <p class="alert alert-info">{{ Session::get('message') }}</p>
@endif
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <table id="contact_table" class="table table-bordered table-hover">
              <thead>
                <tr>
                   <th><b>ID</b></th>
                   <th><b>Name</b></th>
                   <th><b>Email</b></th>
                   <th><b>Subject</b></th>
                   <th><b>Message</b></th>
                  <th align="center">Action</th>
                </tr>
              </thead>
              <tbody>
               @if(!empty($contactEmails))
                    @php $i=1; @endphp
                    @foreach($contactEmails as $contactEmail)                 
                      <tr>
                        <td>{{ $i }}</td>
                        <td>{{$contactEmail->name}}</td>
                        <td>{{$contactEmail->email}}</td>
                        <td>{{$contactEmail->subject}}</td>
                        <td>{{$contactEmail->message}}</td>
                        <td>
                          <a class="btn btn-danger" href="{{route('contact-emails.destroy',$contactEmail->id)}}" onclick="return confirm('Are you sure you want to delete this email permanently?')">Delete</a>
                        </td>
                      </tr>
                      @php $i++; @endphp
                    @endforeach
               @endif
             
              </tbody>
              
            </table>
          </div>
          <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
      <!-- /.col --> 
    </div>
    <!-- /.row --> 
  </section>
  <!-- /.content --> 
</div>
<!-- /.content-wrapper -->

@endsection

@section('script')
<script type="text/javascript">
  $('#contact_table').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'excel', 'pdf'
            {
                extend: 'excel',
                title: 'Contacts'
            }
        ]
    } );
</script>
@endsection
