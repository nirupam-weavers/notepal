@extends('teacher.layouts.app')

@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('teacher.layouts.sidebar')
      <div class="dashbord-content-common ash-bg dashbord-parent-children">
          <div class="dashbord-content-common-pad">
            <div class="my-children-details">
                <div class="my-children-img-wraper-box">
                <div class="my-children-img-edit-box">
                  <div class="my-children-img">
                   @if(auth()->user()->user_image)
                    <img  src="{{ asset('storage/user/'.auth()->user()->user_image) }}" />
                    @else
                    <?php $name = auth()->user()->name;
                    echo $name[0] ?>
                    @endif
                  </div>
                  <div class="my-children-img-edit">
                    <span><i class="fas fa-pencil-alt"></i></span>
                  </div>
                </div>
              </div>
                <div class="my-children-text-box">
                  <div class="row align-items-center">
                    <div class="col-lg-6">
                      <ul class="children-name-box">
                        <li>Name: <span>{{auth()->user()->name}}</span></li>
                        <li>Current Grade:{{auth()->user()->grade}}</li>
                      </ul>
                    </div>
                    <div class="col-lg-6">
                      <ul class="ext-rating">
                        <li><i class="fas fa-star"></i></li>
                        <li><i class="fas fa-star"></i></li>
                        <li><i class="fas fa-star"></i></li>
                        <li><i class="fas fa-star"></i></li>
                        <li><i class="fas fa-star"></i></li>
                        <li>4.9</li>
                      </ul>
                    </div>
                  </div>
                  <div class="children-bio">
                    <div class="children-bio-head">
                      <h3>Bio:</h3>
                    </div>
                    <div class="children-bio-body">
                      <p>{{auth()->user()->bio}}</p>
                    </div>
                  </div>
                  <ul class="children-btns">
                    <li><span class="flow-img"><img src="{{ asset('front/images/images/binaclr.png')}}" alt=""></span> Followers (100)</li>
                    <li><a href="" class="btn yellow-btn">Edit <span><i class="fas fa-pencil-alt"></i></span></a></li>
                  </ul>
                </div>
              </div>
            
        </div>
      <div class=" ash-bg dashbord-parent-children">
      <div class="parent-children-badge">
                <h3>Badges :</h3>
                <div class="parent-children-badge-wraper">
                  <div class="parent-children-badge-box">
                    <div class="icon-badge">
                      <div class="icon-badge-wrap">
                        <img src="{{ asset('front/images/achv.png')}}" alt="">
                      </div>
                    </div>
                    <div class="name-badge">
                      <h4><span>Badge Name: </span>Beginner</h4>
                    </div>
                    <div class="descrip-badge">
                      <p><span>Badge Description: </span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore </p>
                    </div>
                    <div class="date-badge">
                      <h6><span>Badge Earned on:</span>20th July, 2021</h6>
                    </div>
                  </div>
                </div>
                <div class="parent-children-badge-wraper">
                  <div class="parent-children-badge-box">
                    <div class="icon-badge">
                      <div class="icon-badge-wrap">
                        <img src="{{ asset('front/images/achv.png')}}" alt="">
                      </div>
                    </div>
                    <div class="name-badge">
                      <h4><span>Badge Name: </span>Ninza</h4>
                    </div>
                    <div class="descrip-badge">
                      <p><span>Badge Description: </span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore </p>
                    </div>
                    <div class="date-badge">
                      <h6><span>Badge Earned on:</span>20th July, 2021</h6>
                    </div>
                  </div>
                </div>
                <div class="parent-children-badge-wraper in-active">
                  <div class="parent-children-badge-box">
                    <div class="icon-badge">
                      <div class="icon-badge-wrap">
                        <img src="{{ asset('front/images/achv.png')}}" alt="">
                      </div>
                    </div>
                    <div class="name-badge">
                      <h4><span>Badge Name: </span>Ninza Pro</h4>
                    </div>
                    <div class="descrip-badge">
                      <p><span>Badge Description: </span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore </p>
                    </div>
                    <div class="date-badge">
                    </div>
                  </div>
                </div>
                <div class="parent-children-badge-wraper in-active">
                  <div class="parent-children-badge-box">
                    <div class="icon-badge">
                      <div class="icon-badge-wrap">
                        <img src="{{ asset('front/images/achv.png')}}" alt="">
                      </div>
                    </div>
                    <div class="name-badge">
                      <h4><span>Badge Name: </span>Champion</h4>
                    </div>
                    <div class="descrip-badge">
                      <p><span>Badge Description: </span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore </p>
                    </div>
                    <div class="date-badge">
                    </div>
                  </div>
                </div>
              </div>
        </div>
      @endsection