@extends('student.layouts.app')

@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('student.layouts.sidebar')
      
      <div class="dashbord-content-common ash-bg dashbord-parent-children">
          <div class="dashbord-content-common-pad">
            <div class="my-children-header">
              <div class="my-children-text">
                <h2>My Teacher</h2>
              </div>
              
            </div>
            @foreach($users as $user)
              <div class="my-children-details">
                <div class="my-children-img">
                @if(isset($user->user_image))
                      <img  src="{{ asset('upload/images/avatar/teacher/'.$user->user_image) }}" />
                      @else
                      <?php $name = $user->name;
                      echo $name[0] ?>
                      @endif
                  </div>
                <div class="my-children-text-box">
                  <ul class="children-name-box">
                    <li>Name: <span>{{$user->name}}</span></li>
                    <!-- <li>Experience:{{$user->experience}} +Years</li> -->
                  </ul>
                  <div class="children-bio">
                    <div class="children-bio-head">
                      <h3>Bio:</h3>
                    </div>
                    <div class="children-bio-body">
                      <p>{{$user->bio}}</p>
                    </div>
                  </div>
                  <ul class="children-btns">
                    <li><a href="#" class="btn blue-btn">View Teacher</a></li>
                    
                  </ul>
                </div>
              </div>
              @endforeach
          </div>
        </div>
    
  </div>
</div>
@endsection
