@extends('teacher.layouts.app')

@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('teacher.layouts.sidebar')
      <div class="dashbord-content-common ash-bg dashbord-parent-children">
          <div class="dashbord-content-common-pad">
            <div class="dashbord-inner-right-padd parent-search-area-wrap">
              <div class="parent-search-area">
                <form method="post" action="{{route('teacher.units')}}">
                  @csrf
                  <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                          <input type="text" class="form-control" name="search" placeholder="Search here by unit name" value="{{app('request')->input('search')}}">
                        </div>
                    </div>
                    <div class="col-lg-2">
                      <button type="submit" class="btn">Search Now</button>
                    </div>
                    <div class="col-lg-2">
                      <a href="{{route('teacher.units')}}" class="btn">Clear</a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="parent-teacher-srch-reslt only-teacher-srch-reslt">
              <h2>Units</h2>
              <div class="parent-teacher-srch-reslt-wrap">
                <table class="table" id="teacher-book" data-userid="">
                  <thead>
                    <tr>
                      <th scope="col">Name</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($units as $unit)
                    <tr>
                      <td>{{$unit->unit->name}}</td>
                      <td class="arrow-btn"><a href="{{ url('teacher/unit/lessons/'.$unit->unit_id) }}"><i class="fas fa-chevron-right"></i></a></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                <div class="loder-btn">
                  {{$units->links()}}
                </div>
              </div>
            </div>           

            <div class="seperator"></div>
          </div>
      </div>
    </div>
  </div>
</div>
</div>

@endsection

@section('scripts')

@endsection