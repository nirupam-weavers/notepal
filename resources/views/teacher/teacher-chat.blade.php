@extends('teacher.layouts.app')

@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('teacher.layouts.sidebar')
      <?php
        $user_image = asset('front/images/img3.png');
        if(!empty(auth()->user()->user_image)){
            $user_image = asset('storage/user/'.auth()->user()->user_image);
        }
      ?>      
      <input type="hidden" value="{{$user_image}}" id="user_image">
      <input type="hidden" value="{{auth()->user()->role}}" id="loggedIn_role">
      <input type="hidden" value="" id="receiver_id">

      <div class="dashbord-content-common ash-bg dashbord-parent-children dashbord-parent-calender">
          <div class="dashbord-content-common-pad">
            <div class="chatbox-page">
              <div class="row">
                <div class="col-lg-3 dark-blue-bg">
                  <div class="chat-person-side">
                    <div class="box-heading">
                        <div class="form-group">
                          <input type="text" class="form-control" id="search" placeholder="Search">
                        </div>
                    </div>
                    <div class="chat-person-body">
                      <ul id="users">
                        @foreach($allusers as $alluser)
                        <li class="set-receiver" data-id="{{$alluser->id}}">
                          <div class="single-chet-person">
                            <div class="chat-per-dtls">
                              <div class="chat-prsn-img">
                                @if(empty($alluser->user_image))
                                <img src="{{asset('front/images/img3.png')}}">
                                @else
                                <img src="{{ asset('storage/user/'.$alluser->user_image) }}">
                                @endif
                              </div>
                              <div class="chat-pesn-nm">
                                <h3>{{$alluser->name}} <span>{{$alluser->role}}</span></h3>
                              </div>
                            </div>
                            <div class="chat-box-other">
                              <ul>
                                <li><a href="javascript:void(0);"><span><i class="fas fa-ellipsis-h"></i></span></a></li>
                                <li>5 Min</li>
                              </ul>
                            </div>
                          </div>
                        </li>
                       @endforeach
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-lg-5 chat-body-bg">
                  <div class="chat-box-body" style="display: none;">
                    <div class="single-chating-area">
                    <div class="chat-box-type-chat">
                        <div class="form-group">
                          <textarea class="form-control" id="chat-input" rows="3" placeholder="Type your message..."></textarea>
                        </div>
                        <button type="submit" class="btn" id="send-message"></button>
                    </div>
                    <div class="cating-box-header">
                      <p><span id="name"></span> <span id="typing"></span></p>
                    </div>
                    <div class="chating-content">
                      <div class="chating-content-other" id="chat-box">
                        <p>Loading..</p>
                      </div>
                    </div>
                  </div>
                  </div>
                </div>
                <div class="col-lg-4 white-bg">
                  <div class="chating-per-profile-dtls" style="display: none;">
                    <div class="chating-per-profile-dtls-header">
                      <p>Profile Details</p>
                    </div>
                    <div class="chating-profile-nm">
                      <div class="chating-per-profile-img" id="user-image">
                      </div>
                      <h6 id="user-mame"></h6>
                      <!-- <p>Cape Town, RSA</p> -->
                    </div>
                    <div class="chating-user-dtls">
                      <ul>
                        <li>Category: <span id="category"></span></li>
                        <li>Email Id: <span id="email"></span></li>
                        <li id="experience"></li>
                        <li>Subject: <span id="subject"></span></li>
                        <li>Language: <span></span></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection