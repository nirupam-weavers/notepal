<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;


class AjaxController extends Controller
{
   
    public function getDifferentDashboardCounts()
    {
        $totalNumberOfUsers = User::count();
        $memberCount = User::where('role_id',2)->count();
        $itemCount = Product::where('in_stock','>',0)->count();
        $orderCount = OrderItem::where('status',0)->count();
        $com_order_Count = OrderItem::where('status',1)->count();
        $cancel_order_count = OrderItem::where('status',2)->count();
       // dd($memberCount);
        return response()->json(['member_count' => $memberCount, 'item_count' => $itemCount, 'order_count' => $orderCount, 'com_order_count' => $com_order_Count, 'cancel_order_count' => $cancel_order_count], 200);
    }
    
    
}
