<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Student\StudentController;
use App\Http\Controllers\Student\StudentLessonController;
use App\Http\Controllers\Student\StudentAssessmentController;
use App\Http\Controllers\Student\StudentPracticeController;
use App\Http\Controllers\Auth\StudentLoginController;
use App\Http\Controllers\Auth\StudentRegisterController;
use App\Http\Controllers\Auth\StudentPasswordController;
use App\Http\Controllers\Teacher\TeacherRatingController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\Teacher\TeacherController;
use App\Http\Controllers\Student\CalendarController;
use App\Http\Controllers\Student\ChatController;
use App\Http\Controllers\Student\NotificationController;
use App\Http\Controllers\HomeController;

Route::get('/check/age', [StudentRegisterController::class, 'showCheckAge'])->name('student.check.age');
Route::get('/signup', [StudentRegisterController::class, 'showRegistrationForm'])->name('student.signup');
Route::get('/login', [StudentLoginController::class, 'showLoginForm'])->name('student.login');
Route::post('/login/post', [StudentLoginController::class, 'login'])->name('student.login.post');
Route::post('/registerage', [StudentRegisterController::class, 'registerage'])->name('student.registerage');
Route::post('/register', [StudentRegisterController::class, 'register'])->name('student.register');

Route::get('forgot-password', [StudentPasswordController::class, 'showLinkForm'])->name('student.password.form');
Route::post('email-password', [StudentPasswordController::class, 'passwordEmail'])->name('student.password.email'); 
Route::get('reset-password/{token}/{email}', [StudentPasswordController::class, 'resetForm'])->name('student.reset.password');
Route::post('update-password', [StudentPasswordController::class, 'passwordUpdate'])->name('student.password.update');

Route::group(['middleware' => ['role:student','auth']], function(){
	Route::get('/home', [StudentController::class, 'index'])->name('student.home');
	Route::get('/myteacher', [StudentController::class, 'showMyteacher'])->name('student.myteacher');
	Route::get('/updateprofile', [StudentController::class, 'showUpdateProfile'])->name('student.updateprofile');
	Route::post('/updateprofile/post', [StudentController::class, 'updateProfile'])->name('student.updateprofile.post');
	Route::post('/logout', [StudentController::class, 'logout'])->name('student.logout');

	//Student menu
	Route::any('lesson', [StudentLessonController::class, 'showLesson'])->name('student.lesson');
	Route::get('single-lesson/{id}', [StudentLessonController::class, 'showSingleLesson'])->name('student.single.lesson');
	Route::any('assessment', [StudentAssessmentController::class, 'showAssessment'])->name('student.assessment');
	Route::get('single-assessment/{id}', [StudentAssessmentController::class, 'showSingleAssessment'])->name('student.single.assessment');
	Route::any('independentpractice', [StudentPracticeController::class, 'showPractice'])->name('student.independentpractice');
	Route::get('single-independentpractice/{id}', [StudentPracticeController::class, 'showSinglePractice'])->name('student.single.independentpractice');
	Route::get('favorite', [StudentLessonController::class, 'showFavorite'])->name('student.favorite');
	Route::post('rating', [TeacherRatingController::class, 'rating'])->name('student.rating');

	//search
	Route::post('/show-user', [HomeController::class,'showUser']);
	Route::get('search-users', [StudentController::class, 'search_user'])->name('student.search.users');
	Route::any('search-users/teacher', [StudentController::class, 'showTeacherSearch'])->name('student.teacher.search');
	Route::any('search-users/student', [StudentController::class, 'showStudentSearch'])->name('student.student.search');
	Route::post('/slot-booking', [BookingController::class, 'slot_booking']);
	Route::post('/get-teacher-times', [TeacherController::class, 'get_availability']);

	//calendar
	Route::get('calendar', [CalendarController::class, 'index'])->name('student.calendar');
	Route::post('/accept-booking', [BookingController::class, 'accept_booking'])->name('student.accept.booking');
	Route::post('/cancel-booking', [BookingController::class, 'cancel_booking'])->name('student.cancel.booking');

	//Chat
	Route::get('chat', [ChatController::class, 'index'])->name('student.chat');
	Route::get('notification', [NotificationController::class, 'index'])->name('student.notification');
});