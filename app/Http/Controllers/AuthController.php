<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
use App\TokenStore\TokenCache;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AuthController extends Controller
{
  protected $redirectTologin = '/';
  protected $redirectToHome = '/';

  public function signin($role)
  {
    // Initialize the OAuth client
    $oauthClient = new \League\OAuth2\Client\Provider\GenericProvider([
      'clientId'                => config('azure.appId'),
      'clientSecret'            => config('azure.appSecret'),
      'redirectUri'             => config('azure.redirectUri'),
      'urlAuthorize'            => config('azure.authority').config('azure.authorizeEndpoint'),
      'urlAccessToken'          => config('azure.authority').config('azure.tokenEndpoint'),
      'urlResourceOwnerDetails' => '',
      'scopes'                  => config('azure.scopes')
    ]);

    $authUrl = $oauthClient->getAuthorizationUrl();
    // Save client state so we can validate in callback
    session(['oauthState' => $oauthClient->getState(), 'role' => $role]);

    // Redirect to AAD signin page
    return redirect()->away($authUrl);
  }

  public function callback(Request $request)
  {
    // Validate state
    $role = session('role');
    $expectedState = session('oauthState');
    $request->session()->forget('oauthState');
    $providedState = $request->query('state');

    switch ($role) {
        case 'parent':
        $this->redirectTologin = 'parent/login';
        $this->redirectToHome = 'parent/home';
            break;
        case 'teacher':
        $this->redirectTologin = 'teacher/login';
        $this->redirectToHome = 'teacher/home';
            break;
        case 'student':
        $this->redirectTologin = 'student/login';
        $this->redirectToHome = 'student/home';
            break;
    }

    if (!isset($expectedState)) {
      // If there is no expected state in the session,
      // do nothing and redirect to the home page.
      return redirect($this->redirectTologin);
    }

    if (!isset($providedState) || $expectedState != $providedState) {
      return redirect($this->redirectTologin)
        ->with('error', 'Invalid auth state')
        ->with('errorDetail', 'The provided auth state did not match the expected value');
    }

    // Authorization code should be in the "code" query param
    $authCode = $request->query('code');
    if (isset($authCode)) {
      // Initialize the OAuth client
      $oauthClient = new \League\OAuth2\Client\Provider\GenericProvider([
        'clientId'                => config('azure.appId'),
        'clientSecret'            => config('azure.appSecret'),
        'redirectUri'             => config('azure.redirectUri'),
        'urlAuthorize'            => config('azure.authority').config('azure.authorizeEndpoint'),
        'urlAccessToken'          => config('azure.authority').config('azure.tokenEndpoint'),
        'urlResourceOwnerDetails' => '',
        'scopes'                  => config('azure.scopes')
      ]);

      try {
		  // Make the token request
		  $accessToken = $oauthClient->getAccessToken('authorization_code', [
		    'code' => $authCode
		  ]);

		  $graph = new Graph();
		  $graph->setAccessToken($accessToken->getToken());

		  $user = $graph->createRequest('GET', '/me?$select=displayName,mail,mailboxSettings,userPrincipalName')
		    ->setReturnType(Model\User::class)
		    ->execute();
      $email = null !== $user->getMail() ? $user->getMail() : $user->getUserPrincipalName();

		  $tokenCache = new TokenCache();
		  $tokenCache->storeTokens($accessToken, $user);
      $finduser = User::where('email', $email)->first();

      if($finduser){ 
          Auth::login($finduser);
          switch ($finduser->role) {
              case 'parent':
                  return redirect()->intended('parent/home');
                  break;
              case 'teacher':
                  return redirect()->intended('teacher/home');
                  break;
              case 'student':
                  return redirect()->intended('student/home');
                  break;
          }
 
      }else{
          $newUser = User::create([
              'name' => $user->getDisplayName(),
              'email' => $email,
              'role'=> $role,
              'password' => encrypt('123456')
          ]);

          Auth::login($newUser);
		      return redirect($this->redirectToHome);
      }

	  }
      catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
        return redirect($this->redirectTologin)
          ->with('error', 'Error requesting access token')
          ->with('errorDetail', json_encode($e->getResponseBody()));
      }
    }

    return redirect($this->redirectTologin)
      ->with('error', $request->query('error'))
      ->with('errorDetail', $request->query('error_description'));
  }

  public function signout()
  {
	  $tokenCache = new TokenCache();
	  $tokenCache->clearTokens();
	  return redirect('/');
  }
}