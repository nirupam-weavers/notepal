@foreach($users as $user)
<li class="set-receiver" data-id="{{$user->id}}">
  <div class="single-chet-person">
    <div class="chat-per-dtls">
      <div class="chat-prsn-img">
        @if(empty($user->user_image))
        <img src="{{asset('front/images/img3.png')}}">
        @else
        <img src="{{ asset('storage/user/'.$user->user_image) }}">
        @endif
      </div>
      <div class="chat-pesn-nm">
        <h3>{{$user->name}} <span>{{$user->role}}</span></h3>
      </div>
    </div>
    <div class="chat-box-other">
      <ul>
        <li><a href="javascript:void(0);"><span><i class="fas fa-ellipsis-h"></i></span></a></li>
        <li>5 Min</li>
      </ul>
    </div>
  </div>
</li>
@endforeach