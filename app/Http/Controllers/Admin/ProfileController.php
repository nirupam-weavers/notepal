<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Redirect;

class ProfileController extends Controller
{
    public function __construct()
    {
       // $this->middleware('can:navigate');
    }
    public function profile($user_id = null)
    {
        $user = auth()->user();
        
        return view('admin.profile.profile', compact('user'));
    }

    public function changePassword()
    {
        $user = auth()->user();
        return view('admin.profile.change-password', compact('user'));
    }

    public function changePasswordStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' =>'required',
            'new_password' =>'required',
            'confirm_new_password' =>'required',
        ]);
        if($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $userDetails = auth()->user();
        if (Hash::check($request->old_password , $userDetails->password) == false) {
        	\Session::flash('error-message','Check Your Current Password'); 
        	return redirect()->back();
        }
        if($request->new_password == $request->confirm_new_password)
        {
        	$userDetails->password = Hash::make($request->new_password);
            $userDetails->save();

            \Session::flash('message','Password Changed successfully.'); 
        	return redirect()->back();
        }
        else{
        	\Session::flash('error-message','Check Your Confirm Password'); 
        	return redirect()->back();
        }
    }

    public function profileUpdate($user_id = null, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' =>'required|string',
            'last_name' =>'required|string',
            'phone' =>'required|string|digits:10|unique:users,phone,'.$user_id,
            'email' => 'required|string|max:255|regex:/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/|unique:users,email,'.$user_id
        ]);
        if($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $userDetails = auth()->user();

        User::where('id','=', $userDetails->id)->update(
            [
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'phone' => $request->phone,
                'email' => $request->email
            ]
        );
        
        $userDetails->save();

        return redirect()->back()->with('message', 'Successfully updated!');
    }
	
	
}
