<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Notification;
use App\Models\NotificationSetting;
use Validator;

class NewsletterController extends Controller
{
    public function index()
    {
               
        return view('admin.notification.send_newsletter');
    }

    

    public function newsletterStore(Request $request)
    {
        $rules = [
            'heading' => ['required', 'string', 'max:255'],
            'newsletter' => ['required'],
            
        ];
        $messages = array(
          'heading.required' => 'Heading is required',
          'newsletter.required' => 'News Letter is required',
          
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } 
        $subject = "Notepal  Newsletter";
        $users = NotificationSetting::where('newsletter_status',1)->get();
        foreach($users as $user){
                       
            $emails= $user->user->email;
            send_follower_mail($emails,$subject);
            }

        $notification = new Notification();
        $notification->sender_id =  auth()->id();
        $notification->receiver_id = 0;
        $notification->read = 0;
        $notification->notification_type = $request->heading;
        $notification->message = $request->newsletter;
        
       $notification->save();       
        
        \Session::flash('message','Newsletter Successfully send.');
        return redirect()->route('admin.newsletter');
    }

    
    public function breakingnewsShow()
    {
               
        return view('admin.notification.send_breakingnews');
    }


    
    public function breakingnewsStore(Request $request)
    {
        $rules = [
            'heading' => ['required', 'string', 'max:255'],
            'breakingnews' => ['required'],
            
        ];
        $messages = array(
          'heading.required' => 'Heading is required',
          'breakingnews.required' => 'Breakingnews is required',
          
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } 
        $subject = "Notepal Breaking News";
        $message=$request->breakingnews;
        
        $users = NotificationSetting::where('breaking_status',1)->get();
        foreach($users as $user){
                       
            $emails= $user->user->email;
            send_follower_news_mail($emails,$subject,$message);
            }
            
        $notification = new Notification();
        $notification->sender_id =  auth()->id();
        $notification->receiver_id = 0;
        $notification->read = 0;
        $notification->notification_type = $request->heading;
        $notification->message = $message;
        
       $notification->save();       
        
        \Session::flash('message','Newsletter Successfully send.');
        return redirect()->route('admin.newsletter');
    }
    
}
