var base_url = window.location.origin;
var showUser = base_url+"/"+"api/show-user";
var editbooking = base_url+"/"+"api/edit-booking";

$('#parent-slot-datepicker').datetimepicker({    
   inline:true,
   format:'Y-m-d',
   timepicker:false,
   //minDate:new Date(),
 });

 function addUserId(id){
  $("#parent-book").attr('data-userid',id);
  var booking_date = $('#edit_id').attr("data-date");
  var booking_time = $('#edit_id').attr("data-time");
  $( "#parent-slot-datepicker" ).datetimepicker({ setDate: new Date(2022,07,19) });
    $.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
       }
   });
   $.ajax({
       url: showUser,
       type: "POST",
       data: {
           user_id: id,
       },
       cache: false,
       success: function(result){
         $('#showuser').html(result);
       }
   });
 }

 function getBookingId(id){
  $("#parent-book").attr('data-userid',id);
  $.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
       }
   });
   $.ajax({
       url: getBooking,
       type: "POST",
       data: {
           booking_id: id,
       },
       cache: false,
       success: function(result){
         $('#showuser').html(result);
       }
   });
 }

 $('#parent-slot-datepicker').on('change', function(e){
  e.preventDefault();
   var date = this.value;
   var dt = new Date(date);
   // var day = wdays[dt.getDay()];
   // var month = months[dt.getMonth()];
   // var day_dt = dt.getDate();
   let day_dt = new Intl.DateTimeFormat('en', {timeZone: 'UTC', day: 'numeric' }).format(dt);
   let month = new Intl.DateTimeFormat('en', {timeZone: 'UTC', month: 'long' }).format(dt);
   let day = new Intl.DateTimeFormat('en', {timeZone: 'UTC', weekday: 'long' }).format(dt);
   $('#parent-show-dt').html(day+', '+month+' '+day_dt);
   var duration = $('#parent-duration').val();
   $('#parent-booking-time-wrp').html(common_split_time(duration,date));           
});

  $('#parent-duration').on('change', function(e){
    e.preventDefault();
    var duration = $(this).val();
    var date = $('#parent-slot-datepicker').val();
    $('#parent-booking-time-wrp').html(common_split_time(duration,date));
  });

  $('#parent-booking-time-wrp').on('click', '.booking-time-each-common', function (e) {
    e.preventDefault();
    console.log(1);
    var ele = $(this);  
    var time = ele.attr('data-time');
    var time_txt = ele.text();
    $('#parent-booking-time-wrp').find('.booking-time-confrm').remove();
    var t_row = '<div class="booking-time-confrm"><span> '+time_txt+'</span><button type="button" data-time="'+time+'" id="parent-confirm" class="booking-confirm">Confirm</button></div>' 
    ele.after(t_row);
  });

  $('.teacher-book').on('click', function (e) {
    e.preventDefault();
    $('.teacher-book').removeClass("active");
    $(this).addClass("active");
  });

  $('.parent-book').on('click', function (e) {
    e.preventDefault();
    $('.parent-book').removeClass("active");
    $(this).addClass("active");
  });

  $('#parent-booking-time-wrp').on('click', '#parent-confirm', function (e) {
    e.preventDefault();
    var booking_id =$('#edit_id').attr("data-id");
    var user_id = $('#edit_id').attr('data-userid');
    var mentor_id = $('#edit_id').attr('data-mentorid');
    var date = $('#parent-slot-datepicker').val();
    console.log(booking_id);
    var dt = new Date(date);
    let day_for = new Intl.DateTimeFormat('en', {timeZone: 'UTC', weekday: 'long' }).format(dt);
    var duration = $('#parent-duration').val();
    var purpose = $('#parent-purpose').val();
    var role = $('#parent-role').val();
    var start_time = $(this).attr('data-time');
    var flag = true;
    if(purpose == ""){
      flag = false;
      toastr.error('Purpose field is required.');
    }
    if(note == ""){
      flag = false;
      toastr.error('Note field is required.');
    }
    if(purpose.length > 20){
      flag = false;
      toastr.error("You can't use more than 20 characters in Purpose.");
    }
    if(date == ""){
      flag = false;
      toastr.error('Pleade select date.');
    }

    if(flag){
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: editbooking,
        type: "POST",
        data: {
            user_id:user_id,
            mentor_id:mentor_id,
            day_for:day_for,
            date_for:date,
            start_time:start_time,
            duration:duration,
            purpose:purpose,
            note:note,
            booking_id:booking_id,
            //subject:subject
        },
        cache: false,
        success: function(result){
          switch (result['msg']) {
            case 'success':
                toastr.success('Booked successfully.');
                window.location.reload();
                $('#parentBooking').modal('hide');
                break;
            case 'error':
                toastr.error('Fields are required.');
                break;
          }
        }
      });
    }
  });


  