<?php

//use App\Models\Marks;

use Illuminate\Support\Facades\Mail;

$a_plus = range(95,100);
$a = range(86,94);
$a_minus = range(80,85);
$b_plus = range(77,79);
$b = range(74,76);
$b_minus = range(70,73);
$c_plus = range(67,69);
$c = range(64,66);
$c_minus = range(60,63);
$d_plus = range(57,59);
$d = range(54,56);
$d_minus = range(50,53);
$r = range(0,49);
$percentage = array('A+' => $a_plus,'A' => $a,'A-' => $a_minus,
	'B+' => $b_plus,'B' => $b,'B-' => $b_minus,
	'C+' => $c_plus,'C' => $c,'C-' => $c_minus,
	'D+' => $d_plus,'D' => $d,'D-' => $d_minus,
	'R' => $r,
	'I' => ['unable'],
	'N/A' => ['NA']
);
define('MARKS', $percentage);

function send_follower_mail($emails,$subject){
	if($subject == "Lesson creation"){
		Mail::send('mail.teacher.lesson-created', [],function($message) use($emails,$subject){
			$message->to($emails);
			$message->subject($subject);
			 
		});
	}
	elseif($subject == "Booking creation"){
		Mail::send('mail.teacher.booking-created', [],function($message) use($emails,$subject){
			$message->to($emails);
			$message->subject($subject);
			 
		});
	}
	 
	
}


function send_follower_news_mail($emails,$subject,$message){
	if($subject == "Booking creation"){
		Mail::send('mail.teacher.booking-created', ['message' => $message],function($message) use($emails,$subject){
			$message->to($emails);
			$message->subject($subject);
			 
		});
	}
	elseif($subject == "Notepal Breaking News"){
		Mail::send('mail.teacher.breaking-created', ['message' => $message],function($message) use($emails,$subject){
			$message->to($emails);
			$message->subject($subject);
			 
		});
	}
	 
}