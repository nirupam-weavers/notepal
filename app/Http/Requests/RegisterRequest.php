<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use stdClass;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => ['required', 'string', 'email', 'unique:users', 'max:255','regex:/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/'],
            'password' => ['required_if:social_not_exist,1', 'string', 'min:6'],
        ];
    }

    /*protected function failedValidation(Validator $validator)
    {
        $reponse = new stdClass;
        $reponse->code = 422;
        $reponse->errors = $validator->errors();
        $reponse->isSuccess = false;
        throw new HttpResponseException(response()->json($reponse, 200));
    }*/
}
