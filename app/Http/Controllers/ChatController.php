<?php

namespace App\Http\Controllers;

use App\Http\Resources\ChatResource;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Chat;
use stdClass;

class ChatController extends Controller
{
    public function saveMessage(Request $request)
    {
        $response = new stdClass;
        $response->code = 200;
        try {
            $chat = Chat::create([
                'type' => $request->type,
                'sender_id' => $request->sender_id,
                'receiver_id' => $request->receiver_id,
                'message' => $request->data,
            ]);
            $response->result = new ChatResource($chat);
        } catch (\Throwable $th) {
            $response->code = 400;
            $response->error = $th->getMessage();
        }
        return response()->json($response, 200);
    }
    public function getMessages(Request $request)
    {
        $messages = Chat::with('sender','receiver')->where('sender_id',$request->sender_id)
        ->where('receiver_id',$request->receiver_id)
        ->orWhere('receiver_id',$request->sender_id)
        ->where('sender_id',$request->receiver_id)
        ->get();
        $sender_id=$request->sender_id;
        // $messages = ChatResource::collection($chats);
        // dd($messages);
        return view('components.chat-message',compact('messages','sender_id'))->render();
    }

    public function getUser(Request $request)
    {
        $user = User::findOrFail($request->id);
        $subject_str = '';
        if($user->subjects()){
          $subjects = $user->subjects()->pluck('subject')->toArray();
          $subject_str = implode(',', $subjects);
        }
        $user->subject_str = $subject_str;
        return $user;
    }

    public function searchUsers(Request $request)
    {
        $users = User::where('id','!=',$request->loggedIn_id);
        switch ($request->role) {
            case 'teacher':
                $users = $users->where('role', '!=' , 'admin');
                break;
            case 'parent':
                $users = $users->where('role', '!=' , 'admin')->where('role', '!=' , 'student');
                break;
            case 'student':
                $users = $users->where('role', '!=' , 'admin')->where('role', '!=' , 'parent');
                break;
        }

        if(!empty($request->search)){
            $search = $request->search;
            $users = $users->where('name', 'LIKE', "%".$search."%");            
        }        
        $users = $users->get();
        return view('components.search-users',compact('users'))->render();
    }
}
