<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            //$role = explode(':', $request->route()->computedMiddleware[2]);
            $role = $request->route()->getPrefix();
            if(isset($role)){                
                switch ($role) {
                    case '/teacher':
                        return route('teacher.login');
                        break;
                    case '/parent':
                        return route('parent.login');
                        break;
                    case '/student':
                        return route('student.login');
                        break;
                    case '/admin':
                        return route('admin.login');
                        break;
                }
            }
            //return route('login');
        }
    }
}
