<?php

namespace App\Http\Controllers\Parent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subject;
use App\Models\Booking;
use App\Models\User;

class ChatController extends Controller
{
    public function index()
    {
    	$user_id = auth()->user()->id;
    	$allusers = User::where('role','!=','admin')->where('role','!=','student')->where('id','!=',$user_id)->get();
    	return view('parent.parent-chat',compact('allusers'));
    }
}
