@extends('teacher.layouts.app')

@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('teacher.layouts.sidebar')

      <div class="dashbord-content-common ash-bg dashbord-parent-children">
        <div class="dashbord-content-common-pad">
          <div class="lession-complt-frm-page">
            <div class="tab-content">
              <div class="tab-pane active" id="tabs-1" role="tabpanel">
                <div class="lession-complt-frm-body">
                  <form action="{{route('lesson.update')}}" method="POST" enctype="multipart/form-data">
                    <div class="lession-complt-frm-top">
                      @csrf
                      <input type="hidden" value="{{$lesson->id}}" name="lesson_id">
                      <div class="row">
                        <div class="col-lg-3">
                          <div class="form-group">
                            <label for="">Grade <span>*</span></label>
                            <select class="form-control" id="grade-dropdown" name="grade">
                            @foreach(\App\Models\Grade::all() as $grade)
                              <option value="{{$grade->id}}" @if(($lesson->grade_lesson()->pluck('grade_id')->first()== $grade->id)) selected="selected" @endif>Grade {{$grade->grade}}</option>
                            @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col-lg-4">
                          <div class="form-group">
                            <label for="">Subject (s)<span>*</span></label>
                            <select class="form-control" id="subject-dropdown" name="subject">
                              @if($lesson->subject->subject)
                              <option value="{{$lesson->subject->id}}" > {{$lesson->subject->subject}}</option>
                              @else
                              <option value="" >Select Subject</option>
                              @endif
                            </select>
                          </div>
                        </div>
                        <div class="col-lg-5">
                          <div class="form-group">
                            <label for="">Strand (s)<span>*</span></label>
                            <select class="form-control" id="strand-dropdown" name="strand">
                              @if($lesson->strand->strand)
                              <option value="{{$lesson->strand->id}}"> {{$lesson->strand->strand}}</option>
                              @else
                              <option value="" >Select Strand</option>
                              @endif
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="specific-exp">
                      <h3>Specific Expectation(s) <span>*</span></h3>
                      <div class="box-text">
                        <select class="form-control select2" id="expectation-dropdown" name="expectation[]" multiple="multiple" data-placeholder="Select Expectation" required>
                          @foreach($lesson->expectation_lesson as $exp)
                          @if($exp->expectation)
                          <option selected value="{{$exp->id}}">{{$exp->expectation}} </option>
                          @endif
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="single-box-file-wraper">
                      <div class="single-box-file-wraper-head">
                        <h3>Lesson Plan <span>*</span></h3>
                        <p>Upload your lesson plan in any of the following format. Please note, your lesson plan should be divided into three sections- Minds On, Lesson, and Consolidation in order for it to be accepted. </p>
                      </div>
                      <input type="hidden" name="lesson" value="1" id="lesson">
                      @include('teacher.createlesson.copylessonplan')
                    </div>
                    <div class="single-box-file-wraper">
                      <div class="single-box-file-wraper-head">
                        <h3>Independent Practice <span>(Optional)</span></h3>
                        <p>Upload your Independent Practice in any of the following format. </p>
                      </div>
                      @include('teacher.createlesson.copyindependentpractice')
                    </div>
                    <div class="single-box-file-wraper">
                      <div class="single-box-file-wraper-head">
                        <h3>Assessment <span>(Optional)</span></h3>
                        <p>Upload your Assessment in any of the following format. </p>
                      </div>
                      @include('teacher.createlesson.copyassessment')
                    </div>
                    <div class="create-lsn-btn">
                      <ul>
                        <li><a href="" class="btn inactive-btn">Discard</a></li>
                        <li><button type="submit" class="btn yellow-btn">Create Lesson Plan</button></li>
                      </ul>
                    </div>
                  </form>
                </div>
              </div>

              <div class="tab-pane" id="tabs-2" role="tabpanel">
              @include('teacher.createunit.create-unit')
              </div>
            </div>
           
          </div>
        </div>
      </div>
    </div>

    @endsection
    @section('scripts')
<script type="text/javascript">
$(document).ready(function() {
    $('#grade-dropdown').on('change', function() {
      var grade_id = $('#grade-dropdown').val();
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
          }
      });
      $.ajax({
          url: "{{route('grade.subjects')}}",
          type: "POST",
          data: {
              grade_id: grade_id
          },
          cache: false,
          success: function(result){
              $('#subject-dropdown').html('<option value="">Select Subject</option>'); 
              $.each(result.subjects,function(key,value){
              $("#subject-dropdown").append('<option value="'+value.id+'">'+value.subject+'</option>');
              });
              $('#strand-dropdown').html('<option value="">Select Strand</option>'); 
          }
      });       
        
    });    

    $('#subject-dropdown').on('change', function() {
            var subject_id = this.value;
            var grade_id = $('#grade-dropdown').val();;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{route('subject.strands')}}",
                type: "POST",
                data: {
                    subject_id: subject_id,
                    grade_id: grade_id
                },
                cache: false,
                success: function(result){
                  $('#strand-dropdown').html('<option value="">Select Strand</option>'); 
                  $.each(result.strands,function(key,value){
                  $("#strand-dropdown").append('<option value="'+value.id+'">'+value.strand+'</option>');
                  });
                  $('#expectation-dropdown').html('<option value="">Select Expectation</option>'); 
                }
            });        
        
    });

    $('#strand-dropdown').on('change', function() {
            var strand_id = this.value;
            var grade_id = $('#grade-dropdown').val();
            var subject_id = $('#subject-dropdown').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{route('strand.expectation')}}",
                type: "POST",
                data: {
                    strand_id: strand_id,
                    grade_id : grade_id,
                    subject_id : subject_id
                },
                cache: false,
                success: function(result){
                  $('#expectation-dropdown').html('<option value="">Select Expectation</option>'); 
                  $.each(result.expectations,function(key,value){
                  $("#expectation-dropdown").append('<option value="'+value.id+'">'+value.expectation+'</option>');
                  }); 
                }
            });        
        
    });

});
</script>
    @endsection