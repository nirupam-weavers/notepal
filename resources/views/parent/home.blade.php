@extends('parent.layouts.app')

@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">      
      @include('parent.layouts.sidebar')
     
        <div class="dashbord-content-common ash-bg dashbord-parent-children">
          <div class="dashbord-content-common-pad">
            <div class="my-children-header">
              <div class="my-children-text">
                <h2>My Children</h2>
              </div>
              <div class="my-children-add">
                  <div class="form-group">
                    <select class="form-control" id="exampleFormControlSelect1">
                      <option>John Doe <span>( Current Grade: Grade 3 )</span></option>
                      <option>John Doe <span>( Current Grade: Grade 3 )</span></option>
                      <option>John Doe <span>( Current Grade: Grade 3 )</span></option>
                      <option>John Doe <span>( Current Grade: Grade 3 )</span></option>
                      <option>John Doe <span>( Current Grade: Grade 3 )</span></option>
                    </select>
                  </div>
                  <a href="" class="btn yellow-btn" data-bs-toggle="modal" data-bs-target="#addChild">Add Child</a>
              </div>
            </div>
            @foreach($studentdata as $student)
            <div class="my-children-details">
                <div class="my-children-img">
                @if($student->user_image)         
                  <img src="{{ asset('upload/images/avatar/student/'.$student->user_image)  }}" alt="">        
                  @else
                  <img src="{{ asset('front/images/img3.png') }}" alt="">
                  @endif
                </div>
                <div class="my-children-text-box">
                  <ul class="children-name-box">
                    <li>Name: <span>{{$student->name}}</span></li>
                    <li>Current Grade: {{$student->grade}}</li>
                  </ul>
                  <div class="children-bio">
                    <div class="children-bio-head">
                      <h3>Bio:</h3>
                    </div>
                    <div class="children-bio-body">
                      <p>{{$student->bio}}</p>
                    </div>
                  </div>
                  <ul class="children-btns">
                    <li><a href="{{ url('parent/childedit/'.$student->id) }}" class="btn">Edit <span><i class="fas fa-pencil-alt"></i></span></a></li>
                    <li><a href="" class="btn yellow-btn">Add Report Card</a></li>
                  </ul>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="parent-add-children-notice-modal">
  <!-- Modal -->
  <div class="modal" id="addChild" tabindex="-1" role="dialog" aria-labelledby="addChild" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="children-notice">
            <h2>Is your child already a registered user of NotePal?</h2>
            <ul>
              <li><a href="javascript:void(0);" class="btn yellow-btn" data-bs-toggle="modal" data-bs-target="#sendEmailForm">Yes</a></li>
              <li><a href="javascript:void(0);" class="btn" data-bs-toggle="modal" data-bs-target="#add-children-frm">No</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="parent-add-children-code-modal">
<!-- Modal -->
<div class="modal" id="sendEmailForm" tabindex="-1" role="dialog" aria-labelledby="sendEmailForm" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="children-valide-code">
          <form action="{{route('send.otp')}}" method="post">
           @csrf
              <div class="form-group">
                <label for="email">Child's E-mail Registered with NotePal</label>
                <input type="email" name="email" class="form-control" placeholder="john.doe@gmail.com">
              </div>
              <p>A verification code will be sent to this email</p>
              <button type="submit" class="btn">Submit Now</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="parent-add-children-code-modal">
<!-- Modal -->
<div class="modal" id="otpForm" tabindex="-1" role="dialog" aria-labelledby="otpForm" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="children-valide-code">
          <form method="post" action="{{route('otp.verify')}}">
            @csrf
              <div class="form-group">
                <label for="email">Child's E-mail Registered with NotePal</label>
                <input type="email" name="email" class="form-control" placeholder="john.doe@gmail.com">
              </div>
              <div class="valid-code-box">
                <p>Enter the email verification code here</p>
                <input type="text" name="otp" class="form-control" required="">
              </div>
              <button type="submit" class="btn">Submit Now</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<div class="parent-add-children-form-modal">
<!-- Modal -->
<div class="modal" id="add-children-frm" tabindex="-1" role="dialog" aria-labelledby="add-children-frm" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="children-form-add">
          <h2>Add Your Child</h2>
          <form action="{{route('add.child')}}" method="post">
          @csrf
              <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" class="form-control" placeholder="John Doe">
                @error('name')
                    <span class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
              <div class="form-group">
                <label for="email">E-mail id</label>
                <input type="text" name="email" class="form-control" placeholder="john.doe@gmail.com">
                @error('email')
                    <span class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
              <div class="form-group">
                <label for="grade">Grade</label>
                <select class="form-control" name="grade">
                  @for($i=1;$i<=10;$i++)
                  <option value="{{$i}}">Grade {{$i}}</option>
                  @endfor
                </select>
              </div>
              <div class="form-group">
                <label for="bio">Bio</label>
                <textarea class="form-control" cols="30" rows="30" name="bio"></textarea>
              </div>
              <div class="form-group">
              <label for="password">Password</label>
              <input id="password" type="password" class="form-control" name="password" required autocomplete="new-password">
                  @error('password')
                  <span class="" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
                  @enderror
              </div>
               <button type="submit" class="btn">Add Now</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="parent-add-children-sucess-modal">
<!-- Modal -->
<div class="modal" id="add-children-sucess" tabindex="-1" role="dialog" aria-labelledby="add-children-sucess" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="children-sucess-add">
          <div class="tick-img">
            <img src="{{asset('front/images/tick.png')}}" alt="">
          </div>
          <h3>Child Added Successfully</h3>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


@if(session()->has('error-message'))
<div class="modal" id="error-message" tabindex="-1" aria-labelledby="addChildFormLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      <div class="modal-body">
        <div class="alert alert-danger alert-dismissible">
            {{ session()->get('error-message') }}
        </div>
      </div>
    </div>
  </div>
</div>
@endif

@if(session()->has('otp-sent'))
<script type="text/javascript">
  $('#otpForm').modal('show');
</script>
@endif
@if(session()->has('error-message'))
<script type="text/javascript">
  $('#error-message').modal('show');
</script>
@endif
@if(session()->has('add-children-success'))
<script type="text/javascript">
  $('#add-children-sucess').modal('show');
</script>
@endif
@if (count($errors) > 0)
<script type="text/javascript">
    $('#add-children-frm').modal('show');
</script>
@endif
@endsection