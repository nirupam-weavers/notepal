<?php

namespace App\Http\Controllers;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Contracts\Mail\Mailer;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Child;

class ChildController extends Controller
{
    use SendsPasswordResetEmails;

    public function send_otp(Request $request, Mailer $mailer)
    {
        $this->validateEmail($request);
        $child = User::where(['email' => $request->email,'role' => 'student'])->first();

        if(!$child) {
                return redirect()->back()->with('error-message', 'Email does not match');
        }

        $child->otp = User::generateOtpToken();
        $child->save();
        
        $emailID = $request->email;
        $mailer->send('mail.send-otp', ['child' => $child], function($message) use($emailID){
            $message->to($emailID)
                    ->subject('Email verification.');
        });

        session()->flash('otp-sent', 'OTP sent successfully');
        return redirect()->route('parent.home');

    }

    public function otp_verify(Request $request)
    {
        $child = User::where(['otp' => $request->otp,'role' => 'student'])->first();

        if (!is_object($child)) {
            return redirect()->back()->with("error-message", "Otp dosen't match.");
        }

        $otp_expires_time = (strtotime($child->updated_at) + 180);
        if ($otp_expires_time <= time()) {
            return redirect()->back()->with("error-message", "OTP Expired");
        }

        $parent = auth()->user();
        $child->parent_id = $parent->id;
        $child->save();
        
        session()->flash('add-children-success', 'Child added successfully');
        return redirect()->route('parent.home');
    }

    public function add_child(RegisterRequest $request)
    {
        $data = $request->all();
        $parent = auth()->user();

        $student = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'role' => 'student',
            'password' => Hash::make($data['password']),
        ]);

        $student->parent_id = $parent->id;
        $student->grade = $data['grade']??'';
        $student->bio = $data['bio']??'';
        $student->save();

        session()->flash('add-children-success', 'Child added successfully');
        return redirect()->route('parent.home');
    }
}
