/*
Give the service worker access to Firebase Messaging.
Note that you can only use Firebase Messaging here, other Firebase libraries are not available in the service worker.
*/
importScripts('https://www.gstatic.com/firebasejs/7.23.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.23.0/firebase-messaging.js');
   
/*
Initialize the Firebase app in the service worker by passing in the messagingSenderId.
* New configuration for app@pulseservice.com
*/
firebase.initializeApp({
  apiKey: "AIzaSyBDZsKsG3jmQF-08NPauSEmCLeooqdaXGE",
  authDomain: "notepal-chat.firebaseapp.com",
  databaseURL: "https://notepal-chat-default-rtdb.firebaseio.com",
  projectId: "notepal-chat",
  storageBucket: "notepal-chat.appspot.com",
  messagingSenderId: "823290701522",
  appId: "1:823290701522:web:c6b57b9d93e99f4a936f0b",
  measurementId: "G-HMCQJX3BXV"
});
  
/*
Retrieve an instance of Firebase Messaging so that it can handle background messages.
*/
const messaging = firebase.messaging();
// messaging.setBackgroundMessageHandler(function(payload) {
//     console.log(
//         "[firebase-messaging-sw.js] Received background message ",
//         payload,
//     );
//     /* Customize notification here */
//     const notificationTitle = "Background Message Title";
//     const notificationOptions = {
//         body: "Background Message body.",
//         icon: "/itwonders-web-logo.png",
//     };
  
//     return self.registration.showNotification(
//         notificationTitle,
//         notificationOptions,
//     );
// });

messaging.onBackgroundMessage((payload) => {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  let res = JSON.parse(payload.notification.body);
    //console.log(res.sender_image)
    let html = `<div class="chating-content-other-wraper">
        <div class="chating-contentimg-box">
        <div class="chat-per-img">
            <img src="${res.sender_image}">
        </div>
        <div class="time-chat">
        <p>${res.sent_at}</p>
        </div>
        </div>
        <div class="chating-content-text">
        <p>${payload.notification.title}</p>
        </div>
        </div>`;
        $('#chat-box').append(html);
        
  // Customize notification here
  // const notificationTitle = 'Background Message Title';
  // const notificationOptions = {
  //   body: 'Background Message body.',
  //   icon: '/firebase-logo.png'
  // };

  // self.registration.showNotification(notificationTitle,
  //   notificationOptions);
});