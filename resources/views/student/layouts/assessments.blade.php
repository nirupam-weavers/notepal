@foreach($assessments as $assessment)               
<div   class="student-independnt-practic-sngl-box">
  <div class="roted-box">
    <h3> Grade : {{implode(',',$assessment->grade_lesson()->pluck('grade_id')->all())}} 
  </h3>
  </div>
  <ul class="top-box">
  <li><span>Subject:</span> {{$assessment->subject->subject}}
  </li>
    <li><span>Strand:</span> {{$assessment->strand->strand}}</li>
    <li><span>Date:</span>  {{ date('d-m-Y', strtotime($assessment->created_at))}}</li>
  </ul>
  <ul>
  @foreach($assessment->expectation_lesson as $exp)
  <li><p> {{$exp->expectation}}  </p></li>
  @endforeach
  </ul>
  <div class="student-independnt-btm">
    <div class="row align-items-center">
      <div class="col-lg-6">
        <ul class="dnld-sec">
          <li>
            <ul class="star-rating">
              <?php for($i=1;$i< $assessment->avg_rating;$i++){ ?>
              <li><i class="fas fa-star"></i></li>
               <?php } ?>
              <li>{{$assessment->avg_rating}}</li>
            </ul>
          </li>
          <li><span><i class="fas fa-download"></i></span> {{$assessment->download_count}}</li>
          <li><span><i class="far fa-eye"></i></span> {{$assessment->view_count}}</li>
        </ul>
      </div>
      <div class="col-lg-6">
        <ul class="indepnd-action-box">
          <li class="blue-box"><a href=""><i class="fas fa-share-alt"></i></a></li>
          <?php
        $favoriteClass = "white-box";
        if ($assessment->userFavorites()->where(['document_type' => 'assessment','user_id' => auth()->user()->id])->exists()) {
          $favoriteClass = "yellow-box";
        }
        ?>
        <li class="{{$favoriteClass}} favorite" data-id="{{$assessment->id}}" data-doctype="assessment"><i class="fas fa-star"></i></li>
          <li class="white-box"><a href="{{ url('student/single-assessment/'.$assessment->id) }}"><i class="fas fa-chevron-right"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
@endforeach