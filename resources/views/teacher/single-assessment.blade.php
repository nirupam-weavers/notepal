@extends('teacher.layouts.app')
@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('teacher.layouts.sidebar')
      <div class="dashbord-content-common ash-bg dashbord-parent-children">
          <div class="dashbord-content-common-pad">
            <div class="student-assessment-listing-page">
              <div class="student-assessment-listing-head">
                <div class="row">
                  <div class="col-lg-6">
                    <ul class="left-list">
                      <li>By:</li>
                      <li>
                        <ul>
                          <li><div class="user-name"><div class="user-iocn"><img src="{{ asset('front/images/img3.png')}}"></div>{{$assessment->user->name}}</div></li>
                          <li><div class="achiv-logo"><img src="{{ asset('front/images/achv.png')}}"></div></li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                  <div class="col-lg-6"> 
                    <ul class="right-list">
                      <li>Date:{{ date('d-m-Y', strtotime($assessment->created_at))}} </li>
                      <li>Share <span><i class="fas fa-share-alt"></i></span></li>
                      <li data-toggle="modal" data-target="#addrating">Rate <span><i class="fas fa-user"></i></span></li>
                      <li>Favourite <span><i class="far fa-star"></i></span></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="student-assessment-listing-body">
                <ul class="top-sec">
                  <li> Grade : {{implode(',',$assessment->grade_lesson()->pluck('grade_id')->all())}} </li>
                  <li>Subject: {{$assessment->subject->subject}}</li>
                  <li>Strand: {{$assessment->strand->strand}}</li>
                </ul>
                <div class="student-assessment-sngl-box">
                  <div class="student-assessment-sngl-box-head">
                    <h2>Specific Expectation(s)</h2>
                  </div>
                  <div class="student-assessment-sngl-box-body">
                  <ul>
                  @foreach($assessment->expectation_lesson as $exp)
                  <li><p> {{$exp->expectation}}  </p></li>
                  @endforeach
                  </ul>
                  </div>
                </div>
                <div class="student-assessment-sngl-box">
                  <div class="student-assessment-sngl-box-head">
                    <h2>Assessment</h2>
                  </div>
                  <div class="student-assessment-sngl-box-body">
                    <ul class="assigment-dwnload">
                    <?php $file =$assessment->documents->where('document_type','assessment')->pluck('document','file_type')->all(); ?>
                      @if(isset($file['msword'])) 
                      <li><span><img src="{{ asset('front/images/icon3.png')}}" alt=""></span> {{$file['msword']}}</li>
                      <li><a href="{{ Storage::url('document/'.$file['msword']) }}" class="download-sec" download onclick="downloadCount({{$assessment->id}});"><i class="fas fa-download"></i></a></li>
                      @endif
                      @if(isset($file['pdf'])) 
                      <li><span><img src="{{ asset('front/images/icon5.png')}}" alt=""></span> {{$file['pdf']}}</li>
                      <li><a href="{{ Storage::url('document/'.$file['pdf']) }}" class="download-sec" download onclick="downloadCount({{$assessment->id}});"><i class="fas fa-download"></i></a></li>
                      @endif
                      @if(isset($file['google_doc_link'])) 
                      <li><a href="{{$file['google_doc_link']}}" target="_blank"><span><img src="{{ asset('front/images/icon7.png')}}" alt=""></span></a></li>
                      @endif
                      @if(isset($file['google_form_link'])) 
                      <li><a href="{{$file['google_form_link']}}" target="_blank"><span><img src="{{ asset('front/images/icon4.png')}}" alt=""></span></a></li>
                      @endif
                      @if(isset($file['google_slide_link'])) 
                      <li><a href="{{$file['google_slide_link']}}" target="_blank"><span><img src="{{ asset('front/images/icon8.png')}}" alt=""></span></a></li>
                      @endif
                      @if(isset($file['video_link'])) 
                      <li data-toggle="modal" data-target="#videoModal"> <h5>Lesson Video Link</h5>
                      <div> <?php $video=str_replace('watch?v=', '/embed/', $file['video_link']);?>
                      <iframe class="embed-responsive-item" src="{{$video}}" allowfullscreen></iframe></div> </li>
                      @endif
                    </ul>
                  </div>
                </div>
                <div class="student-assessment-total-sec">
                  <div class="row">
                    <div class="col-lg-6">
                    <a href="{{ url('teacher/copy-lesson/'.$assessment->id) }}" class="btn yellow-btn">Copy Assessment</a>
                      <a href="{{ url('teacher/edit-lesson/'.$assessment->id) }}" class="btn yellow-btn"> Modify Assessment</a>
                      <a href="#" class="btn yellow-btn"> Related lesson plan</a>
                    </div>
                    <div class="col-lg-6">
                      <ul class="show-sec">
                        <li><span><i class="fas fa-download"></i></span> {{$assessment->download_count}}</li>
                      <li><span><i class="far fa-eye"></i></span> {{$assessment->view_count}}</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <br>
            <div class="rating-wrp">
              <h2>Reviewes & Ratings</h2>
              <div class="rating-wrp-inn">
                @if(!empty($review_group))
                <div class="tottal-rating">
                  <h3>{{$assessment->avg_rating}}</h3>
                   <div class="rating-box-show">
                      <div class="rating-show" style="width:{{$assessment->avg_rating*20}}%;"></div>
                      &nbsp;
                   </div>
                  <p>Based on {{$total_users}} Reviews</p>
                </div>
                <div class="tottal-rating-details">
                  <ul>
                    @foreach($review_group as $rg)
                    <li>
                      <div class="rating-box-show">
                          <div class="rating-show" style="width:{{$rg->rate*20}}%;"></div>
                          &nbsp;
                      </div>
                      <span class="star-no">{{$rg->total_counts}}</span>
                      <span class="rating-bar">
                        <div class="rating-5" style="width:{{($rg->total_counts / $total_users) * 100}}%;"></div>
                      </span>
                      <span class="star-earn">{{round(($rg->total_counts / $total_users) * 100)}}%</span>
                    </li>
                    @endforeach
                  </ul>
                </div>
                @endif
                <div class="add-rating" data-toggle="modal" data-target="#addrating"><a href="#" class="btn yellow-btn">Rate Now</a></div>
              </div>
            </div>
            <div class="review-list-wrp">
              <h2>Review <span>{{$total_users}}</span></h2>
              <div id="post-data">
                @include('teacher.layouts.reviews')
              </div>
              @if($reviews->lastPage() > 1)
              <a href="javascript:void(0);" class="see-more btn">Load More +</a>
              @endif
              <div class="ajax-load text-center" style="display:none; width: 50px; background: transparent; margin: 0px auto;">
                <img src="{{asset('front/images/blue-loader.gif')}}">
              </div>
            </div>
            <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <button type="button" class="close btn-round btn-primary" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                  <div class="embed-responsive embed-responsive-16by9">
                  @if(isset($file['video_link'])) 
                    <?php $video=str_replace('watch?v=', '/embed/', $file['video_link']);?>
                    <iframe class="embed-responsive-item" src="{{$video}}" allowfullscreen></iframe>
                    @endif
                  </div>
                </div>
              </div>
            </div>
           </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="modal fade" id="addrating" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><img src="{{asset('front/images/cross.svg')}}" alt=""/></span>
        </button>
      </div>
      <div class="modal-body">
        <div class="children-form-add">
          <h4>Rate and Give feedback</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut ex dolor. Suspendisse ornare laoreet malesuada. Vestibulum blandit, diam eu finibus dictum</p>
          <form action="{{ route('teacher.rating') }}" method="POST">
            @csrf
            <div class="rating">
              <input type="radio" name="rate" value="5" id="5"><label for="5">☆</label>
              <input type="radio" name="rate" value="4" id="4"><label for="4">☆</label>
              <input type="radio" name="rate" value="3" id="3"><label for="3">☆</label>
              <input type="radio" name="rate" value="2" id="2"><label for="2">☆</label>
              <input type="radio" name="rate" value="1" id="1"><label for="1">☆</label>
            </div>
            <div class="form-group">
              <input type="hidden" name="lesson_id" value="{{$assessment->id}}">
              <div class="feedback-input-wrp">
                <label><img src="{{asset('front/images/writing.png')}}" alt="" /> <span>Write your feedback here</span></label>
                <textarea name="feedback" class="form-control" placeholder="Write something here"></textarea>
              </div>
            </div>               
            <button type="submit" class="btn yellow-btn">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
  var page = 1;
  var lastPage = "{{$reviews->lastPage()}}";
  jQuery(document).on('click', ".see-more", function() {
      page++;
      loadMoreData(page);
      if (lastPage <= page) {
          jQuery(this).hide();
          return false;
      }
  });  

  function loadMoreData(page) {
      $.ajax({
          url: '?page=' + page,
          type: "get",
          beforeSend: function() {
              $('.ajax-load').show();
          }
      }).done(function(data) {
          if (data.html == " ") {
              $('.ajax-load').html("No more records found");
              return;
          }
          $('.ajax-load').hide();
          $("#post-data").append(data.html);

      }).fail(function(jqXHR, ajaxOptions, thrownError) {
          alert('server not responding...');
      });
  }

  function downloadCount(lesson_id){
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
          }
      });
      $.ajax({
          url: "{{url('api/save-download-count')}}",
          type: "POST",
          data: {
              lesson_id: lesson_id,
          },
          cache: false,
          success: function(result){
          }
      }); 
  }

</script>
@endsection