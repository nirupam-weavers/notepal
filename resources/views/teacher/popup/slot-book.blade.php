<!-- Schedule a Booking Modal -->
<div class="modal fade scheduleBooking" id="scheduleBooking" tabindex="-1" role="dialog" aria-labelledby="scheduleBooking"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true"><img src="{{asset('front/images/cross.svg')}}" alt="" /> </span>
      </button>

      <div class="modal-body">       

        <div class="row">
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 booking-search-pop">
            <div class="row filter-search-wrp">
              <div class="col-lg-4">
                  <div class="form-group">
                    <select class="form-control custom-input" id="filter" name="filter">
                      <option value="">Filter</option>
                      <option value="teacher">Teacher</option>
                      <option value="parent">Parent</option>
                      <option value="student">Student</option>
                    </select>
                  </div>
              </div>
              <div class="col-lg-8">
                  <div class="form-group">
                    <input type="text" class="form-control custom-input" id="search" name="search" aria-describedby="" placeholder="Search" value="{{app('request')->input('search')}}">
                  </div>
              </div>
            </div>
            <div id="teachers">
              @foreach($users as $user)
              <div class="teachers-each-search" data-id="{{$user->id}}" data-role="{{$user->role}}">
                <div class="teachers-each-img">
                @if(isset($user->user_image))
                      <img src="{{ asset('storage/user/'.$user->user_image) }}" alt="">
                      @else
                      <img src="{{ asset('front/images/img3.png')}}" alt="">
                      @endif
                 
                  <img src="{{asset('front/images/achv.png')}}" alt="" class="teacher-badge"/>
                </div>
                <div class="teachers-each-info">
                  <h3>Name: <span>{{$user->name}}</span></h3>
                  <h3>Category: <span>{{$user->role}}</span></h3>
                </div>
                <div class="teachers-each-del">
                  <img src="{{asset('front/images/cross.svg')}}" alt="" />
                </div>
              </div>
              @endforeach
            </div>
          </div>

          <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 booking-cal-pop">
            <h2>Schedule Booking <span>(Select a date and time)</span></h2> 

            <div class="booking-cal-inn">
              <input type="text" id="slot-datepicker"/>
            </div>           
            
            <div class="duration-wrp">
              <img src="{{asset('front/images/clock.svg')}}" alt="" />
              <select id="duration">
                <option value="00:15">15 min meeting</option>
                <option value="00:30">30 min meeting</option>
                <option value="00:45">45 min meeting</option>
                <option value="00:60">60 min meeting</option>
              </select>
            </div>  
            <div class="purpose">              
              <textarea id="purpose" cols="50" rows="5" placeholder="Purpose"></textarea>
            </div> 
            <div class="note">              
              <textarea id="note" cols="50" rows="5" placeholder="Note"></textarea>
            </div>                  
          </div>

          <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 booking-time-pop">
            <p id="show_dt"></p>
            <div class="booking-time-wrp" id="booking-time-wrp">
            </div>
          </div>
          
          
        </div>

      </div>

    </div>
  </div>
</div>