<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUnit extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
         'grade' => 'required',
         'subject' => 'required',
         'strand' => 'required',
         'expectation' => 'required',
         'unit_lesson_ms_file' => 'file|mimes:doc,docx|max:2048',
         'unit_practice_ms_file' => 'file|mimes:doc,docx|max:2048',
         'unit_ass_ms_file' => 'file|mimes:doc,docx|max:2048',
         'unit_lesson_pdf_file' => 'file|mimes:pdf|max:2048',
         'unit_practice_pdf_file' => 'file|mimes:pdf|max:2048',
         'unit_ass_pdf_file' => 'file|mimes:pdf|max:2048',
         'unit_lesson_google_doc_link' => 'max:1000',
         'unit_lesson_google_form_link' => 'max:1000',
         'unit_lesson_google_slide_link' => 'max:1000',
         'unit_lesson_video_link' => 'max:1000',
         'unit_practice_google_doc_link' => 'max:1000',
         'unit_practice_google_form_link' => 'max:1000',
         'unit_practice_google_slide_link' => 'max:1000',
         'unit_practice_video_link' => 'max:1000',
         'unit_ass_google_doc_link' => 'max:1000',
         'unit_ass_google_form_link' => 'max:1000',
         'unit_ass_google_slide_link' => 'max:1000',
         'unit_ass_video_link' => 'max:1000', 
         'unit_lesson' => 'required',           
        ];
    }

     /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'unit_lesson_ms_file.mimes' => 'Lesson File Type must be in doc,docx',
            'unit_practice_ms_file.mimes' => 'Practice File Type must be in doc,docx',
            'unit_ass_ms_file.mimes' => 'Assesment File Type must be in doc,docx',
        ];
    }
}
