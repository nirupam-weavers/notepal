@extends('student.layouts.app')

@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('student.layouts.sidebar')
      
      
        <div class="dashbord-content-common ash-bg dashbord-parent-children">
          <div class="dashbord-content-common-pad">
            <div class="dashbord-inner-right-padd parent-search-area-wrap">
              <div class="parent-search-area">
                <form>
                  <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                          <select class="form-control" id="exampleFormControlSelect1">
                            <option>Sort</option>
                            <option>sort 2</option>
                            <option>sort 3</option>
                            <option>sort 4</option>
                            <option>sort 5</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                          <select class="form-control" id="exampleFormControlSelect1">
                            <option>Filter</option>
                            <option>Filter 2</option>
                            <option>Filter 3</option>
                            <option>Filter 4</option>
                            <option>Filter 5</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="form-group">
                          <input type="text" class="form-control" id="" aria-describedby="" placeholder="Search here by name of the assessment /  related lesson">
                        </div>
                    </div>
                    <div class="col-lg-3">
                      <button type="submit" class="btn">Search Now</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="student-independnt-practic-sec">
              <h2>Assessments:</h2>
              <div class="student-independnt-practic-sngl-box">
                <div class="roted-box">
                  <h3>grade: 2-3</h3>
                </div>
                <ul class="top-box">
                  <li><span>Subject:</span> Gr 2 Math, Gr 3 Math</li>
                  <li><span>Strand:</span> Gr 2 Number /  Number Sense, Gr 3 Number /  Number Sense</li>
                  <li><span>BY:</span> JOHN DOE / 20-06-21</li>
                </ul>
                <p>Gr 2 81.1 read. represent. compose. and decompose whole numbers up to and including 200. using a variety of tools and strategies. and describe various ways they are used in everyday life</p>
                <div class="student-independnt-btm">
                  <div class="row align-items-center">
                    <div class="col-lg-6">
                      <ul class="dnld-sec">
                        <li><span><i class="fas fa-download"></i></span> 100</li>
                        <li><span><i class="far fa-eye"></i></span> 100</li>
                      </ul>
                    </div>
                    <div class="col-lg-6">
                      <ul class="indepnd-action-box">
                        <li class="blue-box"><a href=""><i class="fas fa-share-alt"></i></a></li>
                        <li class="yellow-box"><a href=""><i class="fas fa-star"></i></a></li>
                        <li class="white-box"><a href=""><i class="fas fa-chevron-right"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="student-independnt-practic-sngl-box">
                <div class="roted-box">
                  <h3>grade: 2-3</h3>
                </div>
                <ul class="top-box">
                  <li><span>Subject:</span> Gr 2 Math, Gr 3 Math</li>
                  <li><span>Strand:</span> Gr 2 Number /  Number Sense, Gr 3 Number /  Number Sense</li>
                  <li><span>BY:</span> JOHN DOE / 20-06-21</li>
                </ul>
                <p>Gr 2 81.1 read. represent. compose. and decompose whole numbers up to and including 200. using a variety of tools and strategies. and describe various ways they are used in everyday life</p>
                <div class="student-independnt-btm">
                  <div class="row align-items-center">
                    <div class="col-lg-6">
                      <ul class="dnld-sec">
                        <li><span><i class="fas fa-download"></i></span> 100</li>
                        <li><span><i class="far fa-eye"></i></span> 100</li>
                      </ul>
                    </div>
                    <div class="col-lg-6">
                      <ul class="indepnd-action-box">
                        <li class="blue-box"><a href=""><i class="fas fa-share-alt"></i></a></li>
                        <li class="blue-box"><a href=""><i class="far fa-star"></i></a></li>
                        <li class="white-box"><a href=""><i class="fas fa-chevron-right"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="student-independnt-practic-sngl-box">
                <div class="roted-box">
                  <h3>grade: 2-3</h3>
                </div>
                <ul class="top-box">
                  <li><span>Subject:</span> Gr 2 Math, Gr 3 Math</li>
                  <li><span>Strand:</span> Gr 2 Number /  Number Sense, Gr 3 Number /  Number Sense</li>
                  <li><span>BY:</span> JOHN DOE / 20-06-21</li>
                </ul>
                <p>Gr 2 81.1 read. represent. compose. and decompose whole numbers up to and including 200. using a variety of tools and strategies. and describe various ways they are used in everyday life</p>
                <div class="student-independnt-btm">
                  <div class="row align-items-center">
                    <div class="col-lg-6">
                      <ul class="dnld-sec">
                        <li><span><i class="fas fa-download"></i></span> 100</li>
                        <li><span><i class="far fa-eye"></i></span> 100</li>
                      </ul>
                    </div>
                    <div class="col-lg-6">
                      <ul class="indepnd-action-box">
                        <li class="blue-box"><a href=""><i class="fas fa-share-alt"></i></a></li>
                        <li class="yellow-box"><a href=""><i class="fas fa-star"></i></a></li>
                        <li class="white-box"><a href=""><i class="fas fa-chevron-right"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="student-independnt-practic-sngl-box">
                <div class="roted-box">
                  <h3>grade: 2-3</h3>
                </div>
                <ul class="top-box">
                  <li><span>Subject:</span> Gr 2 Math, Gr 3 Math</li>
                  <li><span>Strand:</span> Gr 2 Number /  Number Sense, Gr 3 Number /  Number Sense</li>
                  <li><span>BY:</span> JOHN DOE / 20-06-21</li>
                </ul>
                <p>Gr 2 81.1 read. represent. compose. and decompose whole numbers up to and including 200. using a variety of tools and strategies. and describe various ways they are used in everyday life</p>
                <div class="student-independnt-btm">
                  <div class="row align-items-center">
                    <div class="col-lg-6">
                      <ul class="dnld-sec">
                        <li><span><i class="fas fa-download"></i></span> 100</li>
                        <li><span><i class="far fa-eye"></i></span> 100</li>
                      </ul>
                    </div>
                    <div class="col-lg-6">
                      <ul class="indepnd-action-box">
                        <li class="blue-box"><a href=""><i class="fas fa-share-alt"></i></a></li>
                        <li class="blue-box"><a href=""><i class="far fa-star"></i></a></li>
                        <li class="white-box"><a href=""><i class="fas fa-chevron-right"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="student-independnt-practic-sngl-box">
                <div class="roted-box">
                  <h3>grade: 2-3</h3>
                </div>
                <ul class="top-box">
                  <li><span>Subject:</span> Gr 2 Math, Gr 3 Math</li>
                  <li><span>Strand:</span> Gr 2 Number /  Number Sense, Gr 3 Number /  Number Sense</li>
                  <li><span>BY:</span> JOHN DOE / 20-06-21</li>
                </ul>
                <p>Gr 2 81.1 read. represent. compose. and decompose whole numbers up to and including 200. using a variety of tools and strategies. and describe various ways they are used in everyday life</p>
                <div class="student-independnt-btm">
                  <div class="row align-items-center">
                    <div class="col-lg-6">
                      <ul class="dnld-sec">
                        <li><span><i class="fas fa-download"></i></span> 100</li>
                        <li><span><i class="far fa-eye"></i></span> 100</li>
                      </ul>
                    </div>
                    <div class="col-lg-6">
                      <ul class="indepnd-action-box">
                        <li class="blue-box"><a href=""><i class="fas fa-share-alt"></i></a></li>
                        <li class="blue-box"><a href=""><i class="far fa-star"></i></a></li>
                        <li class="white-box"><a href=""><i class="fas fa-chevron-right"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="lod-more-btn">
                <a href="" class="btn yellow-btn">Load More</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
