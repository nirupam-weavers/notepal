@extends('admin.layouts.admin-app')

@section('content')
 <!-- Content Wrapper. Contains page content -->

<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Grade Subject <small>Subject List</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Grade Subject</a></li>
      <li class="active">Grade Subject List</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Grade Subject List</h3>
            @if ($errors->any())
                  <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              @if (session()->has('message'))
                  <div class="alert alert-info alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      {{ session('message') }}
                  </div>
              @endif
              @if (session()->has('error_message'))
                  <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      {{ session('error_message') }}
                  </div>
              @endif
            <a href="{{route('assign.subject.grade')}}" class="btn btn-primary a-btn-slide-text" style="float:right"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <span><strong>Add</strong></span> </a> 
            </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="category_table" class="table table-bordered table-hover">
              <thead>
                <tr>
                   <th><b>Sl No</b></th>
                   <th><b>Grade</b></th>
                   <th><b>Subject</b></th>
                   <th align="center">Action</th>
                </tr>
              </thead>
              <tbody>
              <?php $i=1;?>
               @if(!empty($gradeSubjects))
                 @foreach($gradeSubjects as $value)                 
                  <tr>
                   <td>{{$i}}</td>
                   <td>Grade {{$value->grade->grade}}</td>
                   <td>{{$value->subject->subject}}</td>
                   <td><a class="btn btn-primary" href="{{ url('admin/subject/grade/edit/'.$value->id) }}">Edit</a>
                    <a class="btn btn-danger" href="{{ url('admin/subject/grade/delete/'.$value->id) }}" onclick="return confirm('Do you want to remove?')">Delete</a>
                   </td>
                 </tr>
                 <?php $i++;?>
                 @endforeach
               @endif
             
              </tbody>
              
            </table>
          </div>
          <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
      <!-- /.col --> 
    </div>
    <!-- /.row --> 
  </section>
  <!-- /.content --> 
</div>
<!-- /.content-wrapper -->
@endsection

 