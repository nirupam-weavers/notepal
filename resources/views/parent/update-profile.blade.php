
@extends('parent.layouts.app')

@section('content')
@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('parent.layouts.sidebar')

       
        <div class="dashbord-content-common ash-bg dashbord-parent-children">
          <div class="dashbord-content-common-pad">
            <div class="teacher-edit-profile">
              <div class="teacher-edit-profile-wraper">
                <div class="teacher-img">
                  <div class="teacher-img-only">
                  @if($user->user_image)         
                  <img id="blah" src="{{ asset('storage/user/'.$user->user_image)  }}" alt="">        
                  @else
                  <img id="blah" src="{{ asset('front/images/img3.png') }}" alt="">
                  @endif
                  </div>
                  <form method="POST" name="updateprofile" action="{{ route('parent.updateprofile.post') }}" enctype="multipart/form-data">
                     @csrf
                  <div class="edit-pic">
                    <span><i class="fas fa-pencil-alt"></i></span>
                    <input type="file" id="imgInp" name="myfile" accept="image/*">
                  </div>
                </div>
                <div class="teacher-edit-form">
                 
                    <div class="row">
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label for="">Enter your Name</label>
                          <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}" placeholder="Usser name">
                          @error('name')
                            <span class="" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                           @enderror
                        </div>
                      </div>
                     
                      <div class="col-lg-12">
                          <div class="form-group">
                            <label for="">Give a brief description about yourself</label>
                            <textarea class="form-control" id="bio" name="bio" rows="3">{{$user->bio}}</textarea>
                          </div>
                      </div>
                    </div>
                 
                </div>
              </div>
                    <div class="submit-sec">
                        <button type="submit" class="btn yellow-btn">Submit</button>
                    </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <script src="{{ asset('js/updateprofile.js')}}"></script>
@endsection