var days = [];
var wrapper = $('#calendar');
var calendar = {};
var times_arr = []
calendar.times_arr = times_arr;
var slot = 0;
const sorter = {
    1: "Monday",
    2: "Tuesday",
    3: "Wednesday",
    4: "Thursday",
    5: "Friday",
    6: "Saturday",
    7: "Sunday"
}

$(wrapper).on('click', '.day', function (e) {
    e.preventDefault();
    let i = $(this).data("day");
    $(this).addClass('select-day');
    if (jQuery.inArray(i, days) == -1) {
        slot = 0;
        days.push(i);
        let row;
        row = '<div class="slot-inn"><span>' + sorter[i] + '</span><select class="form-control start start_time">"' + times() + '"</select>To<select class="form-control end end_time">"' + times() + '"</select><button class="add_button" data-day="' + i + '"><i class="fa fa-plus"></i></button></div>';
        $("#day-" + i).append(row);
        s_row = '<div id="s_row' + i + '" class="slot-add-wrp"></div>';
        $("#day-" + i).after(s_row);
    }
});

$(wrapper).on('click', '.add_button', function (e) {
    e.preventDefault();
    var ele = $(this);
    var i = ele.attr("data-day");
    $('.copy_to_all').attr('data-day', i);    //day
    var prev_slot = $('#s_row' + i + ' .slot:last-child').find('button').attr("data-slot");
    if (typeof prev_slot !== 'undefined') {
        slot = parseInt(prev_slot) + 1;
    } else {
        slot = 0;
    }
    var start = ele.closest('.slot').find('.start').val();
    var end = ele.closest('.slot').find('.end').val();
    const time = {};
    time[i] = {
        "slot": slot,
        "start": start,
        "end": end
    }
    var flag = true;
    var overlap = false;
    if (start == "") {
        flag = false;
        toastr.error('Start time is required.');
    } else if (end == "") {
        flag = false;
        toastr.error('End time is required.');
    } else if (start >= end) {
        flag = false;
        toastr.error('Start time should be less than End time.');
    }
    calendar.times_arr.forEach(function (item) {
        if (typeof item[i] !== 'undefined') {
            if ((item[i].end > start) && (item[i].end <= end)) {
                flag = false;
                overlap = true;
            }
        }
    });
    if (overlap) {
        toastr.error('Slot is Overlapped.');
    }
    if (flag) {
        calendar.times_arr.push(time);
        var slot_row = "";
        slot_row = '<div class="slot"><div class="slot-inn"><span class="slot-add">' + tConv24(start) + ' to ' + tConv24(end) + '</span><button class="remove_button" data-slot="' + slot + '" data-day="' + i + '"><i class="fa fa-minus"></i></button></div></div>';
        slot++;
        $('#s_row' + i).append(slot_row);
    }
    //console.log(calendar.times_arr);
});

$('#calendar').on('click', '.remove_button', function (e) {
    e.preventDefault();
    var ele = $(this);
    var r_slot = ele.attr("data-slot");
    var s_day = ele.attr("data-day");
    $('.copy_to_all').attr('data-day', s_day);
    calendar.times_arr = calendar.times_arr.filter(function (obj) {
        if (typeof obj[s_day] !== 'undefined') {
            return obj[s_day].slot != r_slot;
        } else {
            return obj;
        }
    });
    //console.log(calendar.times_arr);       
    $(this).closest('.slot').remove();
});


$('#cal').on('click', '.copy_to_all', function (e) {
    var ele = $(this);
    var day = ele.attr("data-day");
    $('.copy_to_all').attr('data-copy', 1);
    var copy_row = "";
    calendar.times_arr.forEach(function (item) {
        if (typeof item[day] !== 'undefined') {
            copy_row += '<div class="slot">' + tConv24(item[day].start) + ' ' + tConv24(item[day].end) + '</div>';
        }
    });

    days.forEach(function (i) {
        $('#s_row' + i).html(copy_row);
    });
    //console.log(calendar.times_arr);
});
$('body').on('click', '.save-availibility', function () {
    var copy_to_all = $('.copy_to_all').attr('data-copy');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: "save-availibility",
        type: "POST",
        data: {
            times: JSON.stringify(calendar),
            days: days,
            copy_to_all: copy_to_all
        },
        success: function (response) {
            switch (response['msg']) {
                case 'success':
                    toastr.success('Availability updated.');
                    $('#manageAvailability').modal('hide');
                    break;
                case 'error':
                    toastr.error('Fields are required.');
                    break;
            }
        },
    });
});

