<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'NotePal') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('front/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('front/css/font-awesome-all.min.css')}}" rel="stylesheet">
    <link href="{{ asset('front/css/custom.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('front/css/owl.carousel.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('front/css/owl.theme.default.min.css')}}" rel="stylesheet" type="text/css"> 
    <link href="{{ asset('front/css/animate.min.css')}}" rel="stylesheet" type="text/css">
    @yield('styles')
    <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/
        html5shiv.js"></script>
     <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/
        respond.min.js"></script>
    <![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script src="{{ asset('front/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('front/js/font-awesome-all.min.js')}}"></script>
    <script src="{{ asset('front/js/custom.js')}}"></script>
    <script src="{{ asset('front/js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('front/js/wow.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.0/jquery.waypoints.min.js"></script>
    <script src="{{ asset('front/js/jquery.counterup.js')}}"></script>
</head>
<body>       
    @yield('content')
@if(session()->has('password-reset'))
<div class="modal" id="passwordreset" tabindex="-1" aria-labelledby="addChildFormLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      <div class="modal-body">
        <div class="alert alert-success alert-dismissible">
            {{ session()->get('password-reset') }}
        </div>
      </div>
    </div>
  </div>
</div>
@endif

@if(session()->has('password-reset'))
<script type="text/javascript">
  $('#passwordreset').modal('show');
</script>
@endif

    <script type="text/javascript">
      $("body").on('click', '.toggle-password', function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $("#pass_log_id");
        if (input.attr("type") === "password") {
          input.attr("type", "text");
        } else {
          input.attr("type", "password");
        }

      });
    </script>
    @yield('scripts')
    <script>
       new WOW().init();
    </script>
    <script>
        function preventBack() {
            window.history.forward(); 
        }
          
        setTimeout("preventBack()", 0);
          
        window.onunload = function () { null };
    </script>
</body>
</html>
