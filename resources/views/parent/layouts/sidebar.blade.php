<div class="dashbord-common-left-menu">
   
<div class="dashbord-left-menu main-menu">
    
  <div class="dashbord-left-menu-wraper">
    <span class="show-sidebar hide-sidebar"><i class="fas fa-chevron-left"></i></span> 
    <div class="logo">
    <a href="{{ route('parent.home') }}"> <img src="{{ asset('front/images/logo.png')}}"></a>
    </div>
    <div class="dashbord-menu-mail">
      <div class="dashbord-menu-list">
        <ul>
          <li class="active"><a href=""><span><i class="far fa-list-alt"></i></span> <span class="menu-name">My Children</span></a></li>
          <li>
            <a href="{{ route('parent.calendar') }}" data-title="calendar" class="activelink"><span><i class="far fa-calendar"></i></span> <span class="menu-name">Calendar</span></a>
          </li>
          <li class="wrap-menu">
            <a href="javascript:void(0);" data-title="search-users" class="activelink"><span><i class="fas fa-search"></i></span> <span class="menu-name">Search Users</span></a>
            <ul class="sub-menu">              
              <li>
                <a href="{{ route('parent.parent.search') }}" data-title="lesson" class="activelink"> <span class="menu-name">Parent</span></a>
              </li>
              <li>
                <a href="{{ route('parent.teacher.search') }}" data-title="lesson" class="activelink"> <span class="menu-name">Teacher</span></a>
              </li>
            </ul>
          </li>
          <li>
            <a href="{{ route('parent.chat') }}"><span><i class="far fa-comments"></i></span> <span class="menu-name">Chat</span></a>
          </li>
          <li>
            <a href="{{ route('parent.report.card') }}" data-title="report-card" class="activelink"><span><i class="far fa-sticky-note"></i></span> <span class="menu-name">Report Card</span></a>
          </li>
          <li>
            <a href="{{ route('parent.updateprofile') }}" data-title="updateprofile" class="activelink"><span><i class="far fa-edit"></i></span> <span class="menu-name">Update profile</span></a>
          </li>
          <li><a href="{{ route('parent.logout') }}" onclick="event.preventDefault();
                                                       document.getElementById('logout-form').submit();"><span><i class="fas fa-lock"></i></span> <span class="menu-name">Log Out</span></a></li>
          <form id="logout-form" action="{{ route('parent.logout') }}" method="POST" class="d-none">
            @csrf
          </form>
        </ul>
      </div>
      <div class="dashbord-last-btn">
        <a href="" class="btn">Find The Tutor</a>
      </div>
    </div>
  </div>
</div>

</div>

<div class="dashbors-right-content dashbors-right-content-parent">
        <div class="dashbord-right-top-head">
          <div class="dashbord-right-top-head-wraper">
            <p>Welcome to the parents portal!</p>
            <div class="dashbord-right-user">
              <ul>
                <li><a href="{{ route('parent.notification') }}"><span><i class="far fa-bell"></i></span> Notifications</a></li>
                <li><a href="{{ route('parent.updateprofile') }}"><div class="name-user">
                @if(auth()->user()->user_image)  
                  <img  class="name-user" src="{{ asset('storage/user/'.auth()->user()->user_image) }}" />       
                  @else
                   <?php $name=auth()->user()->name;
                   echo $name[0] ?>
                  @endif    
              </div></a></li>
              </ul>
            </div>
          </div>
        </div>

        <script>
          $(".show-sidebar").click(function(){
            $(".dashbord-common-left-menu").toggleClass("dashbord-left-menu-hide");
            $(".dashbors-right-content").toggleClass("dashbors-right-content-big");
            $(this).toggleClass("hide-sidebar");
          });
        </script>