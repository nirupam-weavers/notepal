<div class="parent-add-children-notice-modal">
  <!-- Modal -->
  <div class="modal fade" id="manageAvailability" tabindex="-1" role="dialog" aria-labelledby="manageAvailability" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><img src="{{asset('front/images/cross.svg')}}" alt=""></span>
          </button>
        <div class="modal-body">
          <div class="children-notice">
           
            <form>
                <div id="calendar">
                  <div class="day-wrp">
                      <div class="day" data-day="1"><button type="button">M</button></div>
                      <div class="day" data-day="2"><button type="button">T</button></div>
                      <div class="day" data-day="3"><button type="button">W</button></div>
                      <div class="day" data-day="4"><button type="button">T</button></div>
                      <div class="day" data-day="5"><button type="button">F</button></div>
                      <div class="day" data-day="6"><button type="button">S</button></div>
                      <div class="day" data-day="7"><button type="button">S</button></div>

                       <div id="cal"><button type="button" class="copy_to_all" data-day="1" data-copy="0"><i class="fa fa-clone" aria-hidden="true"></i> Copy To All</button></div>
                  </div>

                  <div id="day-1" class="slot"></div>
                  <div id="day-2" class="slot"></div>
                  <div id="day-3" class="slot"></div>
                  <div id="day-4" class="slot"></div>
                  <div id="day-5" class="slot"></div>
                  <div id="day-6" class="slot"></div>
                  <div id="day-7" class="slot"></div>
              </div>
              <div><button type="button" class="btn blue-btn save-availibility">Save</button></div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>