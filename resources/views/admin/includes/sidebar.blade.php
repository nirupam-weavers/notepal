<aside class="main-sidebar"> 
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar"> 
    
    <!-- Sidebar user panel -->
    <div class="user-panel">
			<div class="pull-left image">
        @php $user = auth()->user(); @endphp
          <img alt=""  src="{{ asset('front/images/logo.png')}}">
			</div>
			<div class="pull-left info">
			  <p>{{ $user->first_name.' '.$user->last_name }}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>

    <!-- search form -->
    
    <!-- /.search form --> 
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>       
        <li class="{{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['dashboard']))?' active menu-open':'' }}"> <a href="{{ route('admin.home') }}"> <i class="fa fa-dashboard" aria-hidden="true"></i> <span>Dashboard</span>  </a>
      </li> 
      <li class="header"> </li> 
      <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['user']))?' active menu-open':'' }}"> <a href="{{ route('admin.grade.list') }}"> <i class="fa fa-graduation-cap" aria-hidden="true"></i> <span>Grade</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
      </li>      
      <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['subject']))?' active menu-open':'' }}"> <a href="#"> <i class="fa fa-book" aria-hidden="true"></i> <span>Subject</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
        <ul class="treeview-menu">
          <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['subject']))?' active menu-open':'' }}" ><a href="{{route('admin.subject.create')}}"><i class="fa fa-plus" aria-hidden="true"></i> Add Subject</a></li>
          <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['subject']))?' active menu-open':'' }}"><a href="{{route('admin.subject.list')}}"><i class="fa fa-list-alt" aria-hidden="true"></i> Subject List</a></li>
          <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['subject']))?' active menu-open':'' }}" ><a href="{{route('assign.subject.grade')}}"><i class="fa fa-plus" aria-hidden="true"></i> Assign Grade Subject</a></li>
          <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['subject']))?' active menu-open':'' }}"><a href="{{route('grade.subject.list')}}"><i class="fa fa-list-alt" aria-hidden="true"></i> Grade Subject List</a></li>
        </ul>
      </li> 
      <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['strand']))?' active menu-open':'' }}"> <a href="{{ route('admin.strand.list') }}"> <i class="fa fa-cog" aria-hidden="true"></i> <span>Strand</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
        <ul class="treeview-menu">
          <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['strand']))?' active menu-open':'' }}" ><a href="{{route('admin.strand.create')}}"><i class="fa fa-plus" aria-hidden="true"></i> Add strand</a></li>
          <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['strand']))?' active menu-open':'' }}"><a href="{{route('admin.strand.list')}}"><i class="fa fa-list-alt" aria-hidden="true"></i> Strand List</a></li>

          <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['strand']))?' active menu-open':'' }}" ><a href="{{route('subject.strand.create')}}"><i class="fa fa-plus" aria-hidden="true"></i> Add Subject Strand</a></li>
          <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['strand']))?' active menu-open':'' }}"><a href="{{route('subject.strand.list')}}"><i class="fa fa-list-alt" aria-hidden="true"></i>Subject Strand List</a></li>
        </ul>
      </li> 
      <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['user']))?' active menu-open':'' }}"> <a href="{{ route('admin.specificexpectation.list') }}"> <i class="fa fa-eraser" aria-hidden="true"></i> <span>Specific Expectation</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
      </li>
      <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['comment']))?' active menu-open':'' }}"> <a href="#"> <i class="fa fa-list-alt" aria-hidden="true"></i> <span>Comments</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
        <ul class="treeview-menu">
          <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['comment']) && in_array(Request::segment(3),['add-comment']) )?' active menu-open':'' }}" ><a href="{{route('add.comment')}}"><i class="fa fa-plus" aria-hidden="true"></i> Add comment</a></li>
          <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['comment']) && in_array(Request::segment(3),['list-comment']) )?' active menu-open':'' }}"><a href="{{route('list.comment')}}"><i class="fa fa-list-alt" aria-hidden="true"></i> Comment List</a></li>
        </ul>
      </li>
      <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['category']))?' active menu-open':'' }}"> <a href="#"> <i class="fa fa-list-alt" aria-hidden="true"></i> <span>Categories</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
        <ul class="treeview-menu">
          <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['category']) && in_array(Request::segment(3),['add-category']) )?' active menu-open':'' }}" ><a href="{{route('add.category')}}"><i class="fa fa-plus" aria-hidden="true"></i> Add Category</a></li>
          <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['category']) && in_array(Request::segment(3),['list-category']) )?' active menu-open':'' }}"><a href="{{route('list.category')}}"><i class="fa fa-list-alt" aria-hidden="true"></i> Categories List</a></li>
        </ul>
      </li>
      <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['marks']))?' active menu-open':'' }}"> <a href="#"> <i class="fa fa-list-alt" aria-hidden="true"></i> <span>Marks</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
        <ul class="treeview-menu">
          <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['category']))?' active menu-open':'' }}" ><a href="{{route('admin.marks.create')}}"><i class="fa fa-plus" aria-hidden="true"></i> Add Marks</a></li>
          <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['category']))?' active menu-open':'' }}"><a href="{{route('admin.marks.list')}}"><i class="fa fa-list-alt" aria-hidden="true"></i> Marks List</a></li>
        </ul>
      </li>


      <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['marks']))?' active menu-open':'' }}"> <a href="#"> <i class="fa fa-list-alt" aria-hidden="true"></i> <span>Notification</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
        <ul class="treeview-menu">
          <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['category']))?' active menu-open':'' }}" ><a href="{{route('admin.newsletter')}}"><i class="fa fa-plus" aria-hidden="true"></i> Newsletter</a></li>
          <li class="treeview {{ (Request::segment(1)=='admin' && in_array(Request::segment(2),['category']))?' active menu-open':'' }}"><a href="{{route('admin.breakingnews')}}"><i class="fa fa-plus" aria-hidden="true"></i> Breaking News</a></li>
        </ul>
      </li>
      
      
    </ul>
  </section>
  <!-- /.sidebar --> 
</aside>
