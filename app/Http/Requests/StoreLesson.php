<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLesson extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
         'grade' => 'required',
         'subject' => 'required',
         'strand' => 'required',
         'expectation' => 'required',
         'lesson_ms_file' => 'file|mimes:doc,docx|max:2048',
         'practice_ms_file' => 'file|mimes:doc,docx|max:2048',
         'ass_ms_file' => 'file|mimes:doc,docx|max:2048',
         'lesson_pdf_file' => 'file|mimes:pdf|max:2048',
         'practice_pdf_file' => 'file|mimes:pdf|max:2048',
         'ass_pdf_file' => 'file|mimes:pdf|max:2048',
         'lesson_google_doc_link' => 'max:1000',
         'lesson_google_form_link' => 'max:1000',
         'lesson_google_slide_link' => 'max:1000',
         'lesson_video_link' => 'max:1000',
         'practice_google_doc_link' => 'max:1000',
         'practice_google_form_link' => 'max:1000',
         'practice_google_slide_link' => 'max:1000',
         'practice_video_link' => 'max:1000',
         'ass_google_doc_link' => 'max:1000',
         'ass_google_form_link' => 'max:1000',
         'ass_google_slide_link' => 'max:1000',
         'ass_video_link' => 'max:1000', 
         'lesson' => 'required',           
        ];
    }

     /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'lesson_ms_file.mimes' => 'Lesson File Type must be in doc,docx',
            'practice_ms_file.mimes' => 'Practice File Type must be in doc,docx',
            'ass_ms_file.mimes' => 'Assesment File Type must be in doc,docx',
        ];
    }
}
