<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      <div class="dashbord-common-left-menu dashbord-left-menu-student">
        <div class="dashbord-left-menu main-menu">
          <div class="nav_close" onclick="menu_close()">
            <i class="far fa-times-circle"></i>
          </div>
          <div class="dashbord-left-menu-wraper">
            <div class="logo">
            <a href="{{ route('student.home') }}"> <img src="{{ asset('front/images/logo.png')}}"></a>
            </div>
            <div class="dashbord-menu-mail">
              <div class="dashbord-menu-list">
                <ul>
                  <li class="">
                    <a href="{{ route('student.lesson') }}" data-title="lesson" class="activelink"><span><i class="far fa-lightbulb"></i></span> Lessons</a>
                  </li>
                  <li>
                    <a href="{{ route('student.independentpractice') }}" data-title="independentpractice" class="activelink"><span><i class="far fa-file-archive"></i></span> Independent Practice</a>
                  </li>
                  <li>
                    <a href="{{ route('student.assessment') }}" data-title="assessment" class="activelink"><span><i class="far fa-file-archive"></i></span> Assessments</a>  
                  </li>
                  <li>
                    <a href="{{ route('student.favorite') }}" data-title="favorite" class="activelink"><span><i class="far fa-star"></i></span> Favourites</a>
                  </li>
                  <li>
                    <a href="{{ route('student.calendar') }}" data-title="calendar" class="activelink"><span><i class="far fa-calendar"></i></span> Calendar  </a>
                  </li>
                  <!-- <li><a href=""><span><i class="far fa-clone"></i></span> Notes</a></li>
                  <li><a href=""><span><i class="far fa-sticky-note"></i></span> Report Card</a></li> -->
                  <li class="wrap-menu">
                    <a href="javascript:void(0);" data-title="search-users" class="activelink"><span><i class="fas fa-search"></i></span> Search Users</a>
                     <ul class="sub-menu">                       
                        <li><a href="{{ route('student.teacher.search') }}">Teacher</a></li>
                        <li><a href="{{ route('student.student.search') }}">Students</a></li>
                     </ul>
                  </li>
                  <li>
                    <a href="{{ route('student.myteacher') }}" data-title="myteacher" class="activelink"><span><i class="fas fa-user"></i></span>My Teacher</a>
                  </li>
                  <li><a href="{{ route('student.chat') }}"><span><i class="far fa-comments"></i></span> Chat</a></li>
                  <li>
                    <a href="{{ route('student.updateprofile') }}" data-title="updateprofile" class="activelink"><span><i class="far fa-edit"></i></span> Update profile</a>
                  </li>
                  <li><a href="{{ route('student.logout') }}" onclick="event.preventDefault();
                                                       document.getElementById('logout-form').submit();"><span><i class="fas fa-lock"></i></span> Log Out</a></li>
                  <form id="logout-form" action="{{ route('student.logout') }}" method="POST" class="d-none">
                    @csrf
                  </form>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div onclick="menu_open()" class="nav_btn">
          <i class="fas fa-bars"></i>
        </div>
      </div>
      <div class="dashbors-right-content dashbors-right-content-student">
        <div class="dashbord-right-top-head">
          <div class="dashbord-right-top-head-wraper">
            <p>Welcome to the student portal!</p>
            <div class="dashbord-right-user">
              <ul>
                <li><a href="{{ route('student.notification') }}"><span><i class="far fa-bell"></i></span> Notifications</a></li>
                <li><a href="{{ route('student.updateprofile') }}">
                    <div class="name-user">
                      @if(auth()->user()->user_image)
                      <img class="name-user" src="{{ asset('storage/user/'.auth()->user()->user_image) }}" />
                      @else
                      <?php $name = auth()->user()->name;
                      echo $name[0] ?>
                      @endif
                    </div>
                  </a></li>
              </ul>
            </div>
          </div>
        </div>