<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class ChatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $user_image = asset('front/images/img3.png');
        if(!empty($this->sender->user_image)){
            $user_image = asset('storage/user/'.$this->sender->user_image);
        }
        return [
            'id' => $this->id,
            'data' => $this->message,
            'type' => $this->type,
            'sender_image' => $user_image,
            'receiver_image' => $user_image,
            'sent' => Carbon::parse($this->created_at)->format('h:i a'),
        ];
    }
}
