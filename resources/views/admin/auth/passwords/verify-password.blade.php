@extends('admin.layouts.app')

@section('content')
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>Mentor</b>Peak</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">{{ __('Otp Verification') }}</p>
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         @foreach ($errors->all() as $error)
            <div>{{$error}}</div>
         @endforeach
    </div>
     @endif

    @if (session()->has('error-message'))
    <div class="alert alert-danger alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session('error-message') }}
    </div>
    @endif
    @if (session()->has('message'))
    <div class="alert alert-info alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session('message') }}
    </div>
    @endif
    
    <form method="POST" action="{{ route('admin.password.otp.verify') }}">
        @csrf
          <div class="form-input otp-verification">
          <input type="text" class="num" id="digit-1" name="otp[]" data-next="digit-2" maxlength="1">
          <span class="splitter">&ndash;</span>
          <input type="text" class='num' id="digit-2" name="otp[]" data-next="digit-3" data-previous="digit-1" maxlength="1"/>
          <span class="splitter">&ndash;</span>
          <input type="text" class='num' id="digit-3" name="otp[]" data-next="digit-4" data-previous="digit-2" maxlength="1"/>
          <span class="splitter">&ndash;</span>
          <input type="text" class='num' id="digit-4" name="otp[]" data-next="digit-5" data-previous="digit-3" maxlength="1"/>
          <span class="splitter">&ndash;</span>
          <input type="text" class='num' id="digit-5" name="otp[]" data-next="digit-6" data-previous="digit-4" maxlength="1"/>
          <span class="splitter">&ndash;</span>
          <input type="text" class='num' id="digit-6" name="otp[]" data-previous="digit-5" maxlength="1"/>
          </div>
      
      <div class="row">
        <div class="otp-timer"><span id="timer">00:59</span></div>  
        <!-- /.col -->
        <div class="col-xs-12 verify-otp-btn">
          <button type="submit" class="btn">Verify</button>
        </div>
        <!-- /.col -->
      </div>
    </form> 
    <!-- @if (Route::has('password.request')) 
    <a href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a><br>
    @endif -->
    <a href="{{ route('admin.password.request') }}">Resend Otp</a>
  </div>
  <!-- /.login-box-body -->
</div>






<!--content sction-->

<style>
/*.otp-verification input { background-color: #e6e6e6; color: #828282;} */
.otp-verification input {
    width: 60px;
    height: 60px;
    background-color: #e6e6e6;
    border: none;
    text-align: center;
    font-size: 18px;
    color: #828282;
    border-radius: 8px;
}
.otp-verification .splitter {
    padding: 0 15px;
    color: #006fff;
    font-size: 20px;
    margin-bottom: 0;
}
.otp-verification {
    display: flex;
    align-items: center;
    font-weight: 500;
    justify-content: center;
    margin-bottom: 30px;
}
.otp-timer span {
    font-size: 18px;
    font-weight: 500;
    text-align: center;
    margin-bottom: 0;
    margin-top: 15px;
}
.login-box {
    width: 610px;
    max-width: 100%;
    text-align: center;
}
.verify-otp-btn .btn {
    width: auto;
    padding: 15px 45px;
    margin: 0 auto 10px;
}
.btn {
    display: inline-block;
    vertical-align: top;
    background-color: #006fff;
    color: #fff ;
    padding: 10px 40px;
    font-weight: 600;
    border-radius: 7px;
    border: 1px solid #006fff;
}
</style>
<!--content sction-->

@endsection