<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Favorite;
use App\Models\User;
use App\Models\Lesson;
use Illuminate\Support\Facades\Auth;

class PublicController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
     
    public function showProfile($id)
    {   
        $lessons = Lesson::where('user_id',$id)->get();
        $user = User::find($id);
        $user_id = auth()->user()->id;
    	$teachers = User::where('role','teacher')->where('id','!=',$user_id)->get();
        if(auth()->user()->role == 'student'){
        return view('student.student-public-profile',compact('lessons','user','teachers'));
        }
        if(auth()->user()->role == 'parent'){
            return view('parent.parent-public-profile',compact('lessons','user','teachers'));
        }
        return view('teacher.teacher-public-profile',compact('lessons','user','teachers'));
    }
}
