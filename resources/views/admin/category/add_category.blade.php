@extends('admin.layouts.admin-app')

@section('content')

<div class="content-wrapper"> 
  <!-- Content Header -->
  <section class="content-header">
    <h1> Category <small>Entry Form</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Category</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row"> 
      <!-- left column -->
      <div class="col-md-12"> 
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add</h3>
            @if ($errors->any())
                  <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              @if (session()->has('message'))
                  <div class="alert alert-info alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      {{ session('message') }}
                  </div>
              @endif
              @if (session()->has('error_message'))
                  <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      {{ session('error_message') }}
                  </div>
              @endif
          </div>
          <!-- /.box-header --> 
          <!-- form start -->
          <form name="entry-form" method="post" action="{{route('create.category')}}">
            {{ csrf_field() }}
            <input type="hidden" name="category_id" value="" id="category_id">
            <div class="box-body">
              <div class="form-group">
                <label for="parent_id">Parent Category</label>
                  <select class="form-control" name="parent_id">
                    <option value="0">Select Category</option>
                    @if(!empty($category))
                    @foreach($category as $each_category)
                     <option value="{{$each_category->id}}" style="{{($each_category->parent_id == 0) ? 'font-weight: bold' : ''}}">{{$each_category->name}}</option>
                     @endforeach
                    @endif
                   </select>
              </div>
              <div class="form-group">
                <label for="name">Category Name<a class='lnkred'>*</a></label>
                <input type="text" class="form-control" name="name" value="" placeholder="Category Name *" id="category_name">
              </div>
              <div class="form-group">
                <label for="name">Category Comments<a class='lnkred'>*</a></label>
                <select class="form-control select2" multiple="multiple" data-placeholder="Category Comments" name="comments[]">
                    <option value="0">Select Comments</option>
                    @if(!empty($comments))
                    @foreach($comments as $comment)
                     <option value="{{$comment->id}}">{{$comment->name}}</option>
                     @endforeach
                    @endif
                </select>
              </div>
              <div class="form-group">
                <label for="status">Status<a class='lnkred'>*</a></label>
                </div>
              <div class="checkbox">
                  <input type="radio" name="status"  checked="checked" value="1">
                  Active
                  <input type="radio" name="status" value="0">
                  Inactive
              </div>
            </div>
            <!-- /.box-body -->
            
            <div class="box-footer">
              <input type="submit" class="btn btn-primary" value="Submit">
            </div>
          </form>
        </div>
        <!-- /.box --> 
        
      </div>
      <!--/.col --> 
    </div>
    <!-- /.row --> 
  </section>
  <!-- /.content --> 
</div>

@endsection