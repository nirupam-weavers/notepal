<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Grade;
use App\Models\Subject;
use App\Models\Strand;
use App\Models\Lesson;
use App\Models\Document;
use App\Models\Expectation;
use App\Models\Rating;
use Response;
use DB;

class AssessmentController extends Controller
{
    public function showAssessment(Request $request)
    {    
      $user = auth()->user();
      $assessments = Lesson::with('expectation_lesson','documents','subject','strand')
      ->withCount(['grade_lesson as grade' => function($query) {
             $query->select('grade_id');
            }])
      ->whereHas('documents', function($query){
          $query->where('document_type','assessment');
      })->where('user_id',$user->id);

      if(!empty($request->search)){    
          $keyword = $request->search; 
          $assessments = $assessments->where(function($query) use ($keyword){      
              $query->whereHas('subject', function($query) use ($keyword){
                  $query->where('subject', 'LIKE', "%".$keyword."%");
              });
              $query->orWhereHas('strand',function($query) use($keyword){
                  $query->where('strand', 'LIKE', "%".$keyword."%");
              });
              $query->orWhereHas('expectation_lesson',function($query) use($keyword){
                  $query->where('expectations.expectation', 'LIKE', "%".$keyword."%");
              });
          });
      }

      if(!empty($request->filter)){
            $filter = $request->filter;
            $grade = explode(' ',$filter);
            $assessments = $assessments->where(function($query) use ($filter,$grade){
                //$query->where('subject', 'LIKE', "%".$filter."%");
                $query->whereHas('subject', function($query) use ($filter){
                    $query->where('subject', 'LIKE', "%".$filter."%");
                });
                $query->orWhereHas('strand',function($query) use($filter){
                    $query->where('strand', 'LIKE', "%".$filter."%");
                });
                $query->orWhereHas('documents',function($query) use($filter){
                    $query->where('file_type', 'LIKE', "%".$filter."%");
                });
                $query->orWhereHas('rating',function($query) use($filter){
                    $query->where('rate', 'LIKE', "%".$filter."%");
                });
                if(isset($grade[1])){
                    $query->orWhereHas('grade_lesson',function($query) use($grade){
                        $query->where('grades.grade', 'LIKE', "%".$grade[1]."%");
                    });
                }
            });
        }
        if(!empty($request->sort)){
            $sort = $request->sort;
            switch ($sort) {
                case 'date':
                    $assessments = $assessments->orderBy('created_at', 'DESC');
                    break;
                case 'views':
                    $assessments = $assessments->orderBy('view_count', 'DESC');
                    break;
                case 'downloads':
                    $assessments = $assessments->orderBy('download_count', 'DESC');
                    break;
                case 'ratings':
                    $assessments = $assessments->whereHas('rating',function($query){
                        $query->orderBy('rate', 'DESC');
                    });
                    break;
            }
        }
        $assessments = $assessments->paginate(10);
        $subjects = Subject::all();
        $grades = Grade::all();
        $strands = Strand::all();

        if ($request->ajax()) {
        $view = view('teacher.layouts.assessments',compact('assessments'))->render();
            return response()->json(['html'=>$view]);
        }

      return view('teacher.assessment',compact('assessments','subjects','grades','strands'));  
    }

    public function showSingleAssessment($id, Request $request)
    {
      $assessment = Lesson::findOrFail($id);
      $assessment->increment( 'view_count', 1 );

      $review_group = DB::table('ratings')->select(DB::raw('count(id) as total_counts,avg(rate) as avg_rating,rate'))
        ->where('lesson_id',$id)
        ->groupBy('rate')
        ->orderBy('rate','desc')
        ->get();
        $total_users = Rating::where('lesson_id',$id)->count();
        $reviews = Rating::where('lesson_id',$id)->paginate(5);
        if ($request->ajax()) {
        $view = view('teacher.layouts.reviews',compact('reviews'))->render();
            return response()->json(['html'=>$view]);
        }
       
      return view('teacher.single-assessment',compact('assessment','review_group','total_users','reviews'));
    }

    
}
