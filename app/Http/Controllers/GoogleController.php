<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Exception;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Session;

class GoogleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToGoogle($role)
    {
        return Socialite::driver('google')->with(['state' => $role])->redirect();
    }
        
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleGoogleCallback(Request $request)
    {
        try {
            $role = $request->state;
            $user = Socialite::driver('google')->stateless()->user();
            $finduser = User::where('email', $user->email)->first();

            if($finduser){     
                if(empty($finduser->google_id)){
                    $finduser->google_id = $user->id;
                    $finduser->save();
                }  
                Auth::login($finduser);
                switch ($finduser->role) {
                    case 'parent':
                        return redirect()->intended('parent/home');
                        break;
                    case 'teacher':
                        return redirect()->intended('teacher/home');
                        break;
                    case 'student':
                        return redirect()->intended('student/home');
                        break;
                }
       
            }else{
                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'google_id'=> $user->id,
                    'role'=> $role,
                    'password' => encrypt('123456')
                ]);
      
                Auth::login($newUser);
      
                switch ($role) {
                    case 'parent':
                        return redirect()->intended('parent/home');
                        break;
                    case 'teacher':
                        return redirect()->intended('teacher/home');
                        break;
                    case 'student':
                        return redirect()->intended('student/home');
                        break;
                }
            }
      
        } catch (Exception $e) {
            dd($e);
        }
    }

}
