<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Teacher\TeacherController;
use App\Http\Controllers\Auth\TeacherLoginController;
use App\Http\Controllers\Auth\TeacherRegisterController;
use App\Http\Controllers\Auth\TeacherPasswordController;
use App\Http\Controllers\Teacher\LessonController;
use App\Http\Controllers\Teacher\AssessmentController;
use App\Http\Controllers\Teacher\PracticeController;
use App\Http\Controllers\Teacher\TeacherRatingController;
use App\Http\Controllers\Teacher\CalendarController;
use App\Http\Controllers\Teacher\ChatController;
use App\Http\Controllers\Teacher\NotificationController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NoteController;

Route::get('/login', [TeacherLoginController::class, 'showLoginForm'])->name('teacher.login');
Route::post('/login/post', [TeacherLoginController::class, 'login'])->name('teacher.login.post');
Route::get('/signup', [TeacherRegisterController::class, 'showRegistrationForm'])->name('teacher.signup');
Route::post('/register', [TeacherRegisterController::class, 'register'])->name('teacher.register');

Route::get('forgot-password', [TeacherPasswordController::class, 'showLinkForm'])->name('teacher.password.form');
Route::post('email-password', [TeacherPasswordController::class, 'passwordEmail'])->name('teacher.password.email'); 
Route::get('reset-password/{token}/{email}', [TeacherPasswordController::class, 'resetForm'])->name('teacher.reset.password');
Route::post('update-password', [TeacherPasswordController::class, 'passwordUpdate'])->name('teacher.password.update');

Route::group(['middleware' => ['role:teacher','auth']], function(){
	Route::get('/home',  [LessonController::class, 'showMyLesson'])->name('teacher.home');
	Route::get('/updateprofile', [TeacherController::class, 'showUpdateProfile'])->name('teacher.updateprofile');
	Route::post('/updateprofile/post', [TeacherController::class, 'updateProfile'])->name('teacher.updateprofile.post');
	Route::post('/logout', [TeacherController::class, 'logout'])->name('teacher.logout');
	//Unit
	Route::any('/units', [LessonController::class, 'units'])->name('teacher.units');
	Route::any('/unit/lessons/{id}', [LessonController::class, 'unitLessons'])->name('teacher.unit.lessons');
	Route::post('unit/store', [LessonController::class, 'unit_store'])->name('unit.store');
	//Lesson
	Route::get('lesson/create', [LessonController::class, 'showCreateLesson'])->name('lesson.create');
	Route::post('lesson/store', [LessonController::class, 'lesson_store'])->name('lesson.store');
	Route::post('grade/subjects', [LessonController::class, 'grade_subjects'])->name('grade.subjects');
	Route::post('subject/strands', [LessonController::class, 'subject_strands'])->name('subject.strands');
	Route::post('strand/expectation', [LessonController::class, 'strand_expectation'])->name('strand.expectation');
	Route::get('copy-lesson/{id}', [LessonController::class, 'showCopyLesson'])->name('lesson.copy');
	Route::get('edit-lesson/{id}', [LessonController::class, 'showEditLesson'])->name('lesson.edit');
	Route::post('lesson/update', [LessonController::class, 'lesson_update'])->name('lesson.update');

	//teacher menu
	Route::any('mylesson', [LessonController::class, 'showMyLesson'])->name('teacher.mylesson');
	Route::any('lesson', [LessonController::class, 'showLesson'])->name('teacher.lesson');
	Route::get('single-lesson/{id}', [LessonController::class, 'showSingleLesson'])->name('teacher.single.lesson');
	Route::any('assessment', [AssessmentController::class, 'showAssessment'])->name('teacher.assessment');
	Route::get('single-assessment/{id}', [AssessmentController::class, 'showSingleAssessment'])->name('teacher.single.assessment');
	Route::any('independentpractice', [PracticeController::class, 'showPractice'])->name('teacher.independentpractice');
	Route::get('single-independentpractice/{id}', [PracticeController::class, 'showSinglePractice'])->name('teacher.single.independentpractice');
	Route::get('favorite', [LessonController::class, 'showFavorite'])->name('teacher.favorite');

	//Calendar
	Route::get('calendar', [CalendarController::class, 'index'])->name('teacher.calendar');
	Route::get('profile', [LessonController::class, 'showProfile'])->name('teacher.profile');
	Route::get('myprofile', [LessonController::class, 'showMyProfile'])->name('teacher.myprofile');
	
	Route::post('searchuser', [TeacherController::class, 'getUserSearch'])->name('teacher.user.search.post');
	Route::post('/save-availibility', [TeacherController::class, 'saveAvailibility'])->name('teacher.save.availibility');
	Route::post('/find-teacher', [TeacherController::class, 'findTeacher'])->name('find.teacher');
	Route::post('/find-user', [HomeController::class,'find_user']);
	Route::post('/show-user', [HomeController::class,'showUser']);
	Route::post('/get-booking', [HomeController::class,'getBooking']);

	Route::post('/get-teacher-times', [TeacherController::class, 'get_availability'])->name('get.teacher.times');
	Route::post('/slot-booking', [BookingController::class, 'slot_booking'])->name('slot.booking');
	Route::post('/edit-booking', [BookingController::class, 'edit_booking'])->name('edit.booking');
	Route::post('/accept-booking', [BookingController::class, 'accept_booking'])->name('accept.booking');
	Route::post('/cancel-booking', [BookingController::class, 'cancel'])->name('cancel.booking');
	Route::post('/cancel-booking', [BookingController::class, 'cancel_booking'])->name('teacher.cancel.booking');
	Route::post('/selfcancel-booking', [BookingController::class, 'selfcancel_booking'])->name('teacher.selfcancel.booking');

	Route::post('rating', [TeacherRatingController::class, 'rating'])->name('teacher.rating');

	//Route::any('user-search', [TeacherController::class, 'showUserSearch'])->name('teacher.user.search');
	Route::get('search-users', [TeacherController::class, 'search_user'])->name('teacher.search.users');
	Route::any('search-users/teacher', [TeacherController::class, 'showTeacherSearch'])->name('teacher.teacher.search');
	Route::any('search-users/parent', [TeacherController::class, 'showParentSearch'])->name('teacher.parent.search');
	Route::any('search-users/student', [TeacherController::class, 'showStudentSearch'])->name('teacher.student.search');
	Route::get('followerlist', [TeacherController::class, 'followerlist'])->name('teacher.followerlist');
	//Chat
	Route::get('chat', [ChatController::class, 'index'])->name('teacher.chat');
	Route::get('notification', [NotificationController::class, 'index'])->name('teacher.notification');
	//Note
	Route::any('notes', [NoteController::class, 'index'])->name('teacher.notes');
	Route::get('note/create', [NoteController::class, 'create'])->name('teacher.note.create');
	Route::get('note/edit/{id}', [NoteController::class, 'edit'])->name('teacher.note.edit');
	Route::post('note/store', [NoteController::class, 'store'])->name('teacher.note.store');
	Route::post('note/update', [NoteController::class, 'update'])->name('teacher.note.update');
	Route::get('note/delete/{id}', [NoteController::class, 'destroy'])->name('teacher.note.delete');
});