@extends('teacher.layouts.app')

@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('teacher.layouts.sidebar')
      <div class="dashbord-content-common ash-bg dashbord-parent-children">
          <div class="dashbord-content-common-pad">
            <div class="dashbord-inner-right-padd parent-search-area-wrap">
              <div class="parent-search-area">
                <form method="post" action="{{route('teacher.notes')}}">
                  @csrf
                  <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                          <input type="text" class="form-control" name="search" placeholder="Search here by Title" value="{{app('request')->input('search')}}">
                        </div>
                    </div>
                    <div class="col-lg-2">
                      <button type="submit" class="btn">Search Now</button>
                    </div>
                    <div class="col-lg-2">
                      <a href="{{route('teacher.notes')}}" class="btn">Clear</a>
                    </div>
                    <div class="col-lg-2">
                      <a href="{{route('teacher.note.create')}}" class="btn">Add</a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="parent-teacher-srch-reslt only-teacher-srch-reslt">
              <h2>Notes</h2>
              <div class="parent-teacher-srch-reslt-wrap">
                <table class="table" id="teacher-book" data-userid="">
                  <thead>
                    <tr>
                      <th scope="col">Title</th>
                      <th scope="col">Description</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($notes as $note)
                    <tr>
                      <td>{{$note->title}}</td>
                      <td>{{$note->description}}</td>
                      <td><a href="{{route('teacher.note.edit',$note->id)}}" class="btn yellow-btn">Edit</a> <a href="{{route('teacher.note.delete',$note->id)}}" class="btn">Delete</a></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                <div class="loder-btn">
                  {{$notes->links()}}
                </div>
              </div>
            </div>           

            <div class="seperator"></div>
          </div>
      </div>
    </div>
  </div>
</div>
</div>

@endsection

@section('scripts')

@endsection