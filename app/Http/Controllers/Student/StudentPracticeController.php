<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Grade;
use App\Models\Subject;
use App\Models\Strand;
use App\Models\Lesson;
use App\Models\Document;
use App\Models\Expectation;
use Response;

class StudentPracticeController extends Controller
{
    public function showPractice(Request $request)
    {    
         

        $user = auth()->user();
        $practices = Lesson::with('expectation_lesson','documents','subject','strand')
        ->withCount(['grade_lesson as grade' => function($query) {
               $query->select('grade_id');
              }])
        ->whereHas('documents', function($query){
            $query->where('document_type','practice');
        })->whereHas('grade_lesson',function($query){
          $query->where('grade_id', auth()->user()->grade);
      });

        if(!empty($request->search)){    
            $keyword = $request->search; 
            $practices = $practices->where(function($query) use ($keyword){      
                $query->whereHas('subject', function($query) use ($keyword){
                    $query->where('subject', 'LIKE', "%".$keyword."%");
                });
                $query->orWhereHas('strand',function($query) use($keyword){
                    $query->where('strand', 'LIKE', "%".$keyword."%");
                });
                $query->orWhereHas('expectation_lesson',function($query) use($keyword){
                    $query->where('expectations.expectation', 'LIKE', "%".$keyword."%");
                });
            });
        }

        if(!empty($request->filter)){
            $filter = $request->filter;
            $grade = explode(' ',$filter);
            $lessons = $lessons->where(function($query) use ($filter,$grade){
                //$query->where('subject', 'LIKE', "%".$filter."%");
                $query->whereHas('subject', function($query) use ($filter){
                    $query->where('subject', 'LIKE', "%".$filter."%");
                });
                $query->orWhereHas('strand',function($query) use($filter){
                    $query->where('strand', 'LIKE', "%".$filter."%");
                });
                $query->orWhereHas('documents',function($query) use($filter){
                    $query->where('file_type', 'LIKE', "%".$filter."%");
                });
                $query->orWhereHas('rating',function($query) use($filter){
                    $query->where('rate', 'LIKE', "%".$filter."%");
                });
                if(isset($grade[1])){
                    $query->orWhereHas('grade_lesson',function($query) use($grade){
                        $query->where('grades.grade', 'LIKE', "%".$grade[1]."%");
                    });
                }
            });
        }
        if(!empty($request->sort)){
            $sort = $request->sort;
            switch ($sort) {
                case 'date':
                    $lessons = $lessons->orderBy('created_at', 'DESC');
                    break;
                case 'views':
                    $lessons = $lessons->orderBy('view_count', 'DESC');
                    break;
                case 'downloads':
                    $lessons = $lessons->orderBy('download_count', 'DESC');
                    break;
                case 'ratings':
                    $lessons = $lessons->whereHas('rating',function($query){
                        $query->orderBy('rate', 'DESC');
                    });
                    break;
            }
        }
        $practices = $practices->paginate(10);
        $subjects = Subject::all();
        $grades = Grade::all();
        $strands = Strand::all();


        if ($request->ajax()) {
          $view = view('student.layouts.practices',compact('practices'))->render();
          return response()->json(['html'=>$view]);
        }
      return view('student.independentpractice',compact('practices','subjects','grades','strands'));
        
        
    }

    public function showSinglePractice($id)
    {   
       
        $practices = Lesson::whereHas('documents', function($query){
            $query->where('document_type','practice');
        })->whereHas('grade_lesson',function($query){
            $query->where('grade_id', auth()->user()->grade);
        })->where('id',$id)->get();
      return view('student.single-independentpractice',compact('practices'));
    }

    
}
