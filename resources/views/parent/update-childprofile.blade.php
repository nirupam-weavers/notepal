
@extends('parent.layouts.app')

@section('content')
@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('parent.layouts.sidebar')

      <div class="dashbors-right-content dashbors-right-content-parent">
        <div class="dashbord-right-top-head">
          <div class="dashbord-right-top-head-wraper">
            <p>Welcome to the student portal!</p>
            <div class="dashbord-right-user">
              <ul>
                <li><a href=""><span><i class="far fa-bell"></i></span> Notifications</a></li>
                <li><a href=""><div class="name-user">
                  @if(auth()->user()->user_image)  
                  <img  class="name-user" src="{{ asset('storage/user/'.auth()->user()->user_image) }}" />       
                  @else
                   <?php $name=auth()->user()->name;
                   echo $name[0] ?>
                  @endif
                </div></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="dashbord-content-common ash-bg dashbord-parent-children">
          <div class="dashbord-content-common-pad">
            <div class="teacher-edit-profile">
              <div class="teacher-edit-profile-wraper">
                <div class="teacher-img">
                  <div class="teacher-img-only">
                  @if($user->user_image)         
                  <img id="blah" src="{{ asset('upload/images/avatar/student/'.$user->user_image)  }}" alt="">        
                  @else
                  <img id="blah" src="{{ asset('front/images/img3.png') }}" alt="">
                  @endif
                  
                  </div>
                  <form method="POST" name="updateprofile" action="{{ route('parent.updatechildprofile') }}" enctype="multipart/form-data">
                     @csrf
                  <div class="edit-pic">
                    <span><i class="fas fa-pencil-alt"></i></span>
                    <input type="file" id="imgInp" name="myfile" accept="image/*">
                  </div>
                </div>
                <div class="teacher-edit-form">
                 
                <div class="row">
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label for="">Enter your Name</label>
                          <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}" placeholder="User name">
                         <input type="hidden" name="id" value="{{$user->id}}"/>
                          @error('name')
                            <span class="" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                           @enderror
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label for="">Enter your Age</label>
                          <input type="text" class="form-control" id="age" name="age" value="{{$user->age}}" placeholder="User Age" required>
                          @error('age')
                            <span class="" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                           @enderror
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label for="">Enter your Grade</label>
                          <input type="text" class="form-control" id="grade" name="grade" value="{{$user->grade}}" placeholder="User Grade">
                          @error('grade')
                            <span class="" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                           @enderror
                        </div>
                      </div>
                      <div class="col-lg-12">
                          <div class="form-group">
                            <label for="">Give a brief description about yourself</label>
                            <textarea class="form-control" id="bio" name="bio" rows="3">{{$user->bio}}</textarea>
                          </div>
                      </div>
                    </div>
                 
                </div>
              </div>
                    <div class="submit-sec">
                        <button type="submit" class="btn yellow-btn">Submit</button>
                    </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <script src="{{ asset('js/updateprofile.js')}}"></script>
@endsection