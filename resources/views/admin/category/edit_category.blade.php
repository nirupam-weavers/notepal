@extends('admin.layouts.admin-app')

@section('content')

<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Category <small>Entry Form</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Category</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row"> 
      <!-- left column -->
      <div class="col-md-12"> 
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Edit</h3>
            @if ($errors->any())
                  <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              @if (session()->has('message'))
                  <div class="alert alert-info alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      {{ session('message') }}
                  </div>
              @endif
              @if (session()->has('error_message'))
                  <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      {{ session('error_message') }}
                  </div>
              @endif
          </div>
          <!-- /.box-header --> 
          <!-- form start -->
          <form name="entry-form" method="post" action="{{route('update.category')}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="category_id" value="{{$category->id}}" id="category_id">
            <div class="box-body">
              <div class="form-group">
                <label for="parent_id">Parent Category</label>
                  <select class="form-control" name="parent_id">
                    <option value="0">Select Parent Category</option>
                    @if(!empty($all_category))
                    @foreach($all_category as $each_category)
                     <option value="{{$each_category->id}}" @if($each_category->id==$category->parent_id) selected @endif style="{{($each_category->parent_id == 0) ? 'font-weight: bold' : ''}}">{{$each_category->name}}</option>
                     @endforeach
                    @endif
                   </select>
              </div>
              <div class="form-group">
                <label for="category_name">Category Name<a class='lnkred'>*</a></label>
                <input type="text" class="form-control" name="name" value="{{$category->name}}" id="category_name">
              </div>
              <div class="form-group">
                <label for="name">Category Comments<a class='lnkred'>*</a></label>
                <select class="form-control select2" multiple="multiple" data-placeholder="Category Comments" name="comments[]">
                    <option value="0">Select Comments</option>
                    @if(!empty($comments))
                     @foreach($comments as $comment)
                     <option value="{{$comment->id}}" @if(in_array($comment->id,$cat_comments)) selected='selected' @endif>{{$comment->name}}</option>
                     @endforeach
                    @endif
                </select>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Status<a class='lnkred'>*</a></label>
              </div>
              <div class="checkbox">
                  <input type="radio" name="status"  checked="checked" value="1" <?php if($category->status=="1"){echo" checked";}?> >
                  Active
                  <input type="radio" name="status" value="0" <?php if($category->status=="0"){echo" checked";}?>>
                  Inactive
              </div>
            </div>
            <!-- /.box-body -->
            
            <div class="box-footer">
              <input type="submit" class="btn btn-primary" value="Submit">
            </div>
          </form>
        </div>
        <!-- /.box --> 
        
      </div>
      <!--/.col (left) --> 
    </div>
    <!-- /.row --> 
  </section>
  <!-- /.content --> 
</div>
@endsection