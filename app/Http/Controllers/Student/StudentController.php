<?php

namespace App\Http\Controllers\Student;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Subject;
use Illuminate\Support\Facades\Validator;
class StudentController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = User::find(auth()->id()); 
        return view('student.update-profile',['user'=>$user]);
        //return view('student.home');
    }

    public function showMyteacher()
    {
        $users = User::where('role','teacher')->get(); 
        return view('student.myteacher',compact('users'));
    }
    public function showUpdateProfile()
    {
        $user = User::find(auth()->id()); 
        
        return view('student.update-profile',['user'=>$user]);
    }

    public function updateProfile(Request $request)
    {
        $data = $request->all();
        //dd($data);
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'age' => ['required'],
            'grade' => ['required'],
            'bio' => ['required'],
         ];
        $messages = array(
          'name.required' => 'This field is required',
          'age.required' => 'This field is required',
          'grade.required' => 'This field is required',
          'bio.required' => 'This field is required',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->route('student.updateprofile')->withInput()->withErrors($validator);
        }
        $user = User::find(auth()->id());
         
        if($file = $request->file('myfile')){
            $file = $request->file('myfile');
            $fileName = time().'_'. $file->getClientOriginalName();
            $file->storeAs('user', $fileName, 'public');
                $user->name = $request->name;      
                $user->user_image = $fileName;
                $user->age = $request->age;
                $user->grade = $request->grade;
                $user->bio =  $request->bio;
                $user->save();
        }
        else{
            $user->name = $request->name; 
            $user->age = $request->age;
            $user->grade = $request->grade;     
            $user->bio =  $request->bio;
            $user->save();
        }
            return redirect('student/updateprofile');
    }
    public function logout(Request $request) 
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('student/login');
    }
    public function search_user()
    {
      return view('student.search-user');
    }
    public function showTeacherSearch(Request $request)
    {
        $subjects = Subject::all();
        $user_id = auth()->user()->id;
    	$teachers = User::where('role','teacher')->where('id','!=',$user_id)->get();
        $users = User::with('expertise')
        ->where('role', '=' , 'teacher');

        if(!empty($request->search)){
            $search = $request->search;
            $users = $users->where('name', 'LIKE', "%".$search."%");
            
        }
       
        if(!empty($request->filter)){
            $filter = $request->filter;
            $users = $users->whereHas('subjects', function($query) use ($filter){
                $query->where('subject', 'LIKE', "%".$filter."%");
            });
        }
        if(!empty($request->sort)){
            $sort = $request->sort;
            $users = $users->orderBy('name', $sort);
        }
        
        $users = $users->paginate(5);
        
        return view('student.student-search-teacher',compact('users','subjects','teachers'));
       
    }
    public function showStudentSearch(Request $request)
    {
        $subjects = Subject::all();
        $user_id = auth()->user()->id;
    	$teachers = User::where('role','teacher')->where('id','!=',$user_id)->get();
        $users = User::with('expertise')
        ->where('role', '=' , 'Student')
        ->where('id', '!=' , $user_id);

        if(!empty($request->search)){
            $search = $request->search;
            $users = $users->where('name', 'LIKE', "%".$search."%");
            
        }
       
        if(!empty($request->filter)){
            $filter = $request->filter;
            $users = $users->whereHas('subjects', function($query) use ($filter){
                $query->where('subject', 'LIKE', "%".$filter."%");
            });
        }
        if(!empty($request->sort)){
            $sort = $request->sort;
            $users = $users->orderBy('name', $sort);
        }
        
        $users = $users->paginate(5);
        
        return view('student.student-search-student',compact('users','subjects','teachers'));
       
    }
}
