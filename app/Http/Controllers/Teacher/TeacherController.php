<?php

namespace App\Http\Controllers\Teacher;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Availability;
use App\Models\Expertise;
use App\Models\User;
use App\Models\Subject;
use App\Models\Strand;
use App\Models\Lesson;
use Illuminate\Support\Facades\Validator;
use Laravel\Ui\Presets\React;
use Carbon\Carbon;

class TeacherController extends Controller
{
    protected $slot_count = 0;
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('teacher.home');
    }

    public function showUpdateProfile()
    {
        $user = User::find(auth()->id()); 
        
        return view('teacher.update-profile',['user'=>$user]);
    }

    public function updateProfile(Request $request)
    {
        $data = $request->all();
        //dd($data);
        $rules = [
            'name' => ['required', 'string', 'max:255'],
        ];
        $messages = array(
          'name.required' => 'This field is required',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->route('teacher.updateprofile')->withInput()->withErrors($validator);
        }  
        $user_id = auth()->id(); 
        $user = User::find($user_id);
         
        if($file = $request->file('myfile')){
            $file = $request->file('myfile');
            $fileName = time().'_'. $file->getClientOriginalName();
            $file->storeAs('user', $fileName, 'public');
                $user->name = $request->name;      
                $user->user_image = $fileName;
                $user->experience =  $request->experience;
                $user->bio =  $request->bio;
                $user->save();
        }
        else{
            $user->name = $request->name; 
            $user->experience =  $request->experience;     
            $user->bio =  $request->bio;
            $user->save();
        }
        if(!empty($request->subject_ids)){          
          foreach ($request->subject_ids as $subject_id) {
            $expertise = Expertise::firstOrNew(['user_id' => $user_id,'subject_id' => $subject_id]);
            $expertise->user_id =  $user_id;
            $expertise->subject_id =  $subject_id;
            $expertise->save(); 
          }
        }        
        
        return redirect('teacher/updateprofile');
    }

    public function logout(Request $request) 
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('teacher/login');
    }
    public function search_user()
    {
      return view('teacher.search-user');
    }

    public function showUserSearch(Request $request)
    {
        $subjects = Subject::all();
        $user_id = auth()->user()->id;
    	$teachers = User::where('role','teacher')->where('id','!=',$user_id)->get();
        $users = User::with('expertise')
        ->where('role', '!=' , 'admin');

        if(!empty($request->search)){
            $search = $request->search;
            $users = $users->where('name', 'LIKE', "%".$search."%");
            
        }
       
        if(!empty($request->filter)){
            $filter = $request->filter;
            $users = $users->whereHas('subjects', function($query) use ($filter){
                $query->where('subject', 'LIKE', "%".$filter."%");
            });
        }
        if(!empty($request->sort)){
            $sort = $request->sort;
            $users = $users->orderBy('name', $sort);
        }
        
        $users = $users->get();
        
        return view('teacher.teachers-search-user',compact('users','subjects','teachers'));
       
    }
    public function showTeacherSearch(Request $request)
    {
        $subjects = Subject::all();
        $user_id = auth()->user()->id;
    	$teachers = User::where('role','teacher')->where('id','!=',$user_id)->get();
        $users = User::with('expertise')
        ->where('role', '=' , 'teacher')
        ->where('id', '!=' , $user_id);

        if(!empty($request->search)){
            $search = $request->search;
            $users = $users->where('name', 'LIKE', "%".$search."%");
            
        }
       
        if(!empty($request->filter)){
            $filter = $request->filter;
            $users = $users->whereHas('subjects', function($query) use ($filter){
                $query->where('subject', 'LIKE', "%".$filter."%");
            });
        }
        if(!empty($request->sort)){
            $sort = $request->sort;
            $users = $users->orderBy('name', $sort);
        }
        
        $users = $users->paginate(5);
        
        return view('teacher.teachers-search-teacher',compact('users','subjects','teachers'));
       
    }
    public function showStudentSearch(Request $request)
    {
        $subjects = Subject::all();
        $user_id = auth()->user()->id;
    	$teachers = User::where('role','teacher')->where('id','!=',$user_id)->get();
        $users = User::with('expertise')
        ->where('role', '=' , 'student');

        if(!empty($request->search)){
            $search = $request->search;
            $users = $users->where('name', 'LIKE', "%".$search."%");
            
        }
       
         
        if(!empty($request->sort)){
            $sort = $request->sort;
            $users = $users->orderBy('name', $sort);
        }
        
        $users = $users->paginate(5);
        
        return view('teacher.teachers-search-student',compact('users','subjects','teachers'));
       
    }

    public function showParentSearch(Request $request)
    {
        $subjects = Subject::all();
        $user_id = auth()->user()->id;
    	$teachers = User::where('role','teacher')->where('id','!=',$user_id)->get();
        $users = User::with('expertise')
        ->where('role', '=' , 'parent');

        if(!empty($request->search)){
            $search = $request->search;
            $users = $users->where('name', 'LIKE', "%".$search."%");
            
        }
       
        if(!empty($request->sort)){
            $sort = $request->sort;
            $users = $users->orderBy('name', $sort);
        }
        
        $users = $users->paginate(5);
        
        return view('teacher.teachers-search-parent',compact('users','subjects','teachers'));
       
    }

    public function getUserSearch(Request $request)
    {
       
        $data['users'] = User::where('name','like', '%'.$request->search.'%')
        ->get();
        return response()->json($data);
    
    }

    public function saveAvailibility(Request $request)
    {
        //dd($request->days); 
        $times = json_decode($request->times);
        $data = [];
        $a = false;
        if(!empty($times->times_arr)){
            $user = auth()->user();
            $days = ['1' => "Monday",'2' => "Tuesday",'3' => "Wednesday",'4' => "Thursday",'5' => "Friday",'6' => "Saturday",'7' => "Sunday"];
            if($request->copy_to_all == '1'){
                $req_days = $request->days;                
                $copy_arr = [];
                foreach ($times->times_arr as $key => $time) {
                    foreach ($days as $key => $day) {
                        if(isset($time->$key)){
                            $slot_id = $time->$key->slot;
                            $start_time = $time->$key->start;
                            $end_time = $time->$key->end;
                            $slots = ["slot" => $slot_id,"start" => $start_time,"end" => $end_time];
                            $copy_arr[] = $slots;
                        }
                    }
                }
                if(!empty($copy_arr)){ 
                    foreach ($req_days as $key => $day_i) {
                        foreach ($copy_arr as $key => $value) {
                            $a = Availability::updateOrCreate(
                                [
                                  'user_id' => $user->id,
                                  'day' => $days[$day_i],
                                  'slot_id' => $value['slot'],
                                ],
                                [
                                  'user_id' => $user->id,
                                  'day' => $days[$day_i],
                                  'slot_id' => $value['slot'],
                                  'start_time' => $value['start'],
                                  'end_time' => $value['end'],
                                ]
                            );
                        }
                    }                   
                }
            }else{                
                foreach ($times->times_arr as $key => $time) {
                    foreach ($days as $key => $day) {
                        if(isset($time->$key)){
                            $slot_id = $time->$key->slot;
                            $start_time = $time->$key->start;
                            $end_time = $time->$key->end;
                            $a = Availability::updateOrCreate(
                                [
                                  'user_id' => $user->id,
                                  'day' => $day,
                                  'slot_id' => $slot_id,
                                ],
                                [
                                  'user_id' => $user->id,
                                  'day' => $day,
                                  'slot_id' => $slot_id,
                                  'start_time' => $start_time,
                                  'end_time' => $end_time,
                                ]
                            );
                            
                        }                
                    }
                }
            }
            if($a){
                $data['msg'] = 'success';
            }

        }else{
            $data['msg'] = 'error';
        }

        return $data;
    }

    public function findTeacher(Request $request)
    {
        $data = [];
        $html = "";
        $user_id = auth()->user()->id;
        if(!empty($request->search)){ 
            $search = $request->search;            
            $teachers = User::where(function($q) use ($search){
                $q->where('name', 'LIKE', "%".$search."%");
                $q->orWhere('role', 'LIKE', "%".$search."%");
            })
            ->where('id','!=',$user_id)
            ->where('role','!=','admin')
            ->groupBy('users.id')
            ->get();
            $user = asset('front/images/img3.png');
            $cross = asset('front/images/cross.svg');
            foreach ($teachers as $key => $value) {
                // $html .= '<div data-id="'.$value->id.'" class="teacher">'.$value->name.'</div>';
                // $html .= '<div>'.$subjects.'</div>';

                $html .= "<div class='teachers-each-search' data-id=".$value->id." data-role=".$value->role."><div class='teachers-each-img'><img src=".$user."></div><div class='teachers-each-info'><h3>Name: <span>".$value->name."</span></h3><h3>Category: <span>".$value->role."</span></h3></div><div class='teachers-each-del'><img src=".$cross."></div></div>";
            }
        }
        
        return $html;
    }
     /**
   * Convert time into decimal time.
   *
   * @param string $time The time to convert
   *
   * @return integer The time as a decimal value.
   */
    public function time_to_decimal($time) {
        $timeArr = explode(':', $time);
        $decTime = ($timeArr[0]*60) + ($timeArr[1]);

        return $decTime;
    }

    /**
     * Convert decimal time into time in the format hh:mm:ss
     *
     * @param integer The time as a decimal value.
     *
     * @return string $time The converted time value.
     */
    public function decimal_to_time($decimal) {
        $hours = floor($decimal / 60);
        $minutes = floor($decimal % 60);
        $seconds = $decimal - (int)$decimal;
        $seconds = round($seconds * 60);

        return str_pad($hours, 2, "0", STR_PAD_LEFT) . ":" . str_pad($minutes, 2, "0", STR_PAD_LEFT);
    }

    public function get_availability(Request $request)
    {
      $user_id = $request->user_id;
      $day = $request->day;
      $day_date = $request->day_date;
      $session_length = $request->duration;
      $availability = [];

      $checkAvail = Availability::where('user_id', $user_id)->where('day', $day)->get()->toArray();
      if(!empty($checkAvail)){
        foreach ($checkAvail as $value) {
          $availability[$day][] = $value;
        }
      }else{
        $availability[$day] = '';
      }
      $user = auth()->user();
      $mentor = User::findOrfail($user_id);
      $buffer_time_before = 0;
      $buffer_time_after = 0;
      $buffer_time = $buffer_time_before + $buffer_time_after;
      $buffer_time = $this->time_to_decimal($session_length) + $buffer_time;
      $buffer_time = $this->decimal_to_time($buffer_time);
      if(!empty($availability)){ 
        $is_slot = 0;   
        $now = Carbon::now()->timestamp;    
        foreach ($availability as $day => $value) {          
          if(!empty($value)){
            foreach($value as $v){
              $s =  $v['start_time'];
              $e =  $v['end_time'];
              $start_time = Carbon::parse("$request->day_date $s")->format('Y-m-d H:i:s');
              $end_time = Carbon::parse("$request->day_date $e")->format('Y-m-d H:i:s');
              $data = $this->SplitTime($start_time, $end_time, $session_length,$buffer_time,$now);
            }          
          }          
        }
        if ($this->slot_count == 1) {?>
          <button type="button" data-time="" class="booking-time-each"></button> 
        <?php }
      }
    }

    public function SplitTime($StartTime, $EndTime, $session_length, $buffer_time,$now){
      $StartTime    = strtotime ($StartTime); //Get Timestamp
      $EndTime      = strtotime ($EndTime); //Get Timestamp
      $d = explode(':', $session_length);
      $d1 = explode(':', $buffer_time);
      
      $AddMins  = $d[0]*60*60 + $d[1]*60;      
      $AddBuffer  = $d1[0]*60*60 + $d1[1]*60;      
      while ($StartTime <= $EndTime) //Run loop
      {
          $end_time = $StartTime + $AddMins;
          if($end_time <= $EndTime && $end_time >= $now){
      ?>
          <button type="button" data-time="<?=date("H:i", $StartTime);?>" class="booking-time-each">
              <?=date("g:i a", $StartTime);?>
          </button> 
      <?php 
          $this->slot_count++;          
          }
          $StartTime += $AddBuffer; //Endtime check
      }
    }

    public function followerlist()
    {
        return view('teacher.followerlist');
    }

}
