@extends('student.layouts.app')

@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('student.layouts.sidebar')
      <div class="dashbord-content-common ash-bg dashbord-parent-children">
          <div class="dashbord-content-common-pad">
          @foreach($assessments as $assessment)
            <div class="student-assessment-listing-page">
              <div class="student-assessment-listing-head">
                <div class="row">
                  <div class="col-lg-6">
                    
                  </div>
                  <div class="col-lg-6"> 
                    <ul class="right-list">
                      <li>Date:{{ date('d-m-Y', strtotime($assessment->created_at))}} </li>
                      <li>Share <span><i class="fas fa-share-alt"></i></span></li>
                      <li>Rate <span><i class="fas fa-user" data-toggle="modal" data-target="#addrating"></i></span></li>
                      <li>Favourite <span><i class="far fa-star"></i></span></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="student-assessment-listing-body">
                <ul class="top-sec">
                  <li> Grade : {{implode(',',$assessment->grade_lesson()->pluck('grade_id')->all())}} </li>
                  <li>Subject: {{$assessment->subject->subject}}</li>
                  <li>Strand: {{$assessment->strand->strand}}</li>
                </ul>
                <div class="student-assessment-sngl-box">
                  <div class="student-assessment-sngl-box-head">
                    <h2>Specific Expectation(s)</h2>
                  </div>
                  <div class="student-assessment-sngl-box-body">
                  <ul>
                  @foreach($assessment->expectation_lesson as $exp)
                  <li><p> {{$exp->expectation}}  </p></li>
                  @endforeach
                  </ul> 
                  </div>
                </div>
                <div class="student-assessment-sngl-box">
                  <div class="student-assessment-sngl-box-head">
                    <h2>Assessment</h2>
                  </div>
                  <div class="student-assessment-sngl-box-body">
                    <ul class="assigment-dwnload">
                    <?php $file =$assessment->documents->where('document_type','assessment')->pluck('document','file_type')->all(); ?>
                      @if(isset($file['msword'])) 
                      <li><span><img src="{{ asset('front/images/icon3.png')}}" alt=""></span> {{$file['msword']}}</li>
                      <li><a href="{{url(storage_path('app/public/document/'.$file['msword']))}}" class="download-sec" download><i class="fas fa-download"></i></a></li>
                      @endif
                      @if(isset($file['pdf'])) 
                      <li><span><img src="{{ asset('front/images/icon5.png')}}" alt=""></span> {{$file['pdf']}}</li>
                      <li><a href="{{url(storage_path('app/public/document/'.$file['pdf']))}}" class="download-sec" download><i class="fas fa-download"></i></a></li>
                      @endif
                      @if(isset($file['google_doc_link'])) 
                      <li><a href="{{$file['google_doc_link']}}" target="_blank"><span><img src="{{ asset('front/images/icon7.png')}}" alt=""></span></a></li>
                      @endif
                      @if(isset($file['google_form_link'])) 
                      <li><a href="{{$file['google_form_link']}}" target="_blank"><span><img src="{{ asset('front/images/icon4.png')}}" alt=""></span></a></li>
                      @endif
                      @if(isset($file['google_slide_link'])) 
                      <li><a href="{{$file['google_slide_link']}}" target="_blank"><span><img src="{{ asset('front/images/icon8.png')}}" alt=""></span></a></li>
                      @endif
                     @if(isset($file['video_link'])) 
                      <li data-toggle="modal" data-target="#videoModal"> <h5>Assessment Video Link</h5>
                      <div> <?php $video=str_replace('watch?v=', '/embed/', $file['video_link']);?>
                      <iframe class="embed-responsive-item" src="{{$video}}" allowfullscreen></iframe></div> </li>
                      @endif
                    </ul>
                  </div>
                </div>
                <div class="student-assessment-total-sec">
                  <div class="row">
                    <div class="col-lg-6">
                     
                    </div>
                    <div class="col-lg-6">
                      <ul class="show-sec">
                        <li><span><i class="fas fa-download"></i></span> 100</li>
                        <li><span><i class="far fa-eye"></i></span> 100</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @endforeach
          </div>
        </div>

        <div class="modal fade" id="addrating" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="children-form-add">
            <h5>Add Your Rating</h5>
            <form action="{{ route('student.rating') }}" method="POST">
            @csrf
            <div class="rate">
              <input type="radio" id="star5" name="rate" value="5" />
              <label for="star5" title="text">5 stars</label>
              <input type="radio" id="star4" name="rate" value="4" />
              <label for="star4" title="text">4 stars</label>
              <input type="radio" id="star3" name="rate" value="3" />
              <label for="star3" title="text">3 stars</label>
              <input type="radio" id="star2" name="rate" value="2" />
              <label for="star2" title="text">2 stars</label>
              <input type="radio" id="star1" name="rate" value="1" />
              <label for="star1" title="text">1 star</label>
            </div>
                <div class="form-group">
                 
                @foreach($assessments as $assessment)
                  <input type="hidden" name="lesson_id" value="{{$assessment->id}}">
                @endforeach
                  <input type="text" name="feedback" class="form-control" >
                </div>               
                 <button type="submit" class="btn"  >Add Rate</button>
          </div>
        </div>
      </div>
    </div>
  </div> 
      @endsection