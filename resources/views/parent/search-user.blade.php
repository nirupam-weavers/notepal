@extends('parent.layouts.app')

@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('parent.layouts.sidebar')
      <div class="dashbord-content-common ash-bg dashbord-parent-children">
        <div class="dashbord-content-common-pad">
          <div class="dashbord-inner-right-padd ">
            <div class="parent-search-area">
                <div class="row">
                  <div class="col-lg-4">
                    <a href="{{route('parent.teacher.search')}}" class="btn">Search Teacher</a>
                  </div>
                  <div class="col-lg-4">
                    <a href="{{route('parent.parent.search')}}" class="btn">Search Parent</a>
                  </div>
                </div>
            </div>
          </div>
          

        </div>
      </div>

      @endsection