<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <link rel="stylesheet" href="{{asset('admin/dist/css/bootstrap.min.css')}}">
      <!-- Theme style -->
      <link rel="stylesheet" href="{{asset('admin/dist/css/AdminLTE.min.css')}}">
      <!-- iCheck -->
      <link rel="stylesheet" href="{{asset('admin/plugins/iCheck/square/blue.css')}}">
      <!-- Google Font -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
      <link rel="icon" type="image/favicon" href="{{ asset('front/images/favicon.png') }}">
      <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('front/images/favicon.png') }}">
</head>
            
@yield('content')

<!-- jQuery 3 -->
<script src="{{asset('admin/dist/js/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('admin/dist/js/bootstrap.min.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('admin/plugins/iCheck/icheck.min.js')}}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
<script type="text/javascript">
  $('.num').keyup(function (event) {
    var current = $(this).val();
    if (current.length >= 1) {
      event.preventDefault();
      $(this).nextAll('input').first().focus();
    }
  });  
</script>
</body>
</html>
