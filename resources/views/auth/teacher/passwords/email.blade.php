@extends('layouts.app')

@section('content')
<header class="main-header">
    <div class="container-fluid header-row">
      <div class="logo">
        <a href="#"><img src="images/logo.png" alt=""></a>
      </div>
      <div class="login-btn">
        <a href="{{route('teacher.signup')}}" class="btn">Sign Up</a>
      </div>
    </div>
</header>

<div class="parents-signup-page">
  <div class="container-fluid">
    <div class="row full-hight">
      <div class="col-lg-6">
        <div class="parent-signup-frm">
            <h3>Forgot Password?</h3>
            <p>Please enter your mail id to reset the password</p>
            @if (Session::has('message'))
                 <div class="alert alert-success" role="alert">
                    {{ Session::get('message') }}
                </div>
            @endif
            @if (Session::has('error'))
                 <div class="alert alert-danger" role="alert">
                    {{ Session::get('error') }}
                </div>
            @endif
            <form method="POST" action="{{ route('teacher.password.email') }}">
             @csrf
                <div class="form-group">
                  <label for="exampleInputEmail1">E-mail id</label>
                  <input type="email" name="email" value="{{old('email')}}" class="form-control" id="" aria-describedby="" placeholder="john.doe@gmail.com">
                  @if ($errors->has('email'))
                      <span class="text-danger">{{ $errors->first('email') }}</span>
                  @endif
                </div>
                  <div class="login-btn">
                    <!-- <a href="" class="btn">{{ __('Send Password Reset Link') }}</a> -->
                    <button type="submit" class="btn">
                        {{ __('Send Password Reset Link') }}
                    </button>
                  </div>
            </form>
        </div>
      </div>
      <div class="col-lg-6 prple-bg reletive-div">
        <div class="parent-signup-text">
          <h3>Platform for tutors</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, </p>
          <p>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
        </div>
        <div class="overlap-img">
            <img src="images/img6.png" alt="">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
