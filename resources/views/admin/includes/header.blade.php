<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- <link rel="icon" href="{{ asset('front/images/favicon.jpg') }}" sizes="64x64" />
  <link rel="icon" href="{{ asset('front/images/favicon70.jpg') }}" sizes="70x70" />
  <link rel="icon" href="{{ asset('front/images/favicon150.jpg') }}" sizes="150x150" />
  <link rel="icon" href="{{ asset('front/images/favicon310.jpg') }}" sizes="310x310" />
  <link rel="apple-touch-icon" href="{{ asset('front/images/favicon.jpg') }}" />
  <meta name="msapplication-TileImage" content="{{ asset('front/images/favicon.jpg') }}" /> -->
  <link rel="stylesheet" href="{{asset('admin/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('admin/plugins/datatables/dataTables.bootstrap.css')}}">
  <link rel="stylesheet" href="{{asset('admin/plugins/select2/select2.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('admin/dist/css/AdminLTE.min.css')}}">
  <!-- Bootstrap 3.3.6 -->
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{asset('admin/dist/css/skins/_all-skins.min.css')}}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('admin/plugins/iCheck/flat/blue.css')}}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{asset('admin/plugins/morris/morris.css')}}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{asset('admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{asset('admin/plugins/datepicker/datepicker3.css')}}">
  <!-- <link rel="stylesheet" href="{{asset('admin/plugins/datepicker/bootstrap-datepicker.min.css')}}"> -->
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('admin/plugins/daterangepicker/daterangepicker.css')}}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
  <link rel="icon" type="image/favicon" href="{{ asset('front/images/favicon.png') }}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('front/images/favicon.png') }}">
  <link rel="stylesheet" href="{{asset('admin/dist/css/jquery.dataTables.min.css')}}">
  <link rel="stylesheet" href="{{asset('admin/dist/css/buttons.dataTables.min.css')}}">
  <style>
    .dropdown-toggle:after {display: inline-block;margin-left: 0.255em;vertical-align: 0.255em;content: "";border-top: 0.3em solid;border-right: 0.3em solid transparent;border-bottom: 0;border-left: 0.3em solid transparent;}
    .navbar-nav>.user-menu>.dropdown-menu>li.user-header>img{margin: auto;}
    .user-panel>.image>img {height: 38px;width: 35px;object-fit: cover;border-radius: 50%;}
  </style>
  @yield('style')
</head>
<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    
    <!-- Logo -->
    <a href="{{ route('admin.home') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>{{ config('app.name', 'NotePal') }}</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="{{ asset('front/images/logo.png') }}" alt="{{ config('app.name') }}" style="width: 150px;"> </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
         
          <!-- Control Sidebar Toggle Button -->
         
          <li class="dropdown user user-menu">
             
             @php $user = auth()->user(); @endphp
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
             
                <img  class="user-image img-circle elevation-2" alt="User Image" src="{{ asset('front/images/logo.png')}}" >
             
              <span class="hidden-xs">{{ $user->name}}</span>
            </a>
            <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                 
                  
                    <img class="img-circle elevation-2 thumbnail" alt="User Image" src="{{ asset('front/images/profile.png')}}" height="50px" width="50px">
                  <p>
                      {{ $user->name}}
                      <small>Member Since: {{ Carbon\Carbon::parse(Auth::user()->created_at)->format('F Y') }}</small>
                  </p>
                </li>
                <!-- Menu Body -->
                <li class="user-body">
                  <div class="row">
                      <div class="col-xs-12 text-center" style="width:100%">
                        <a href="{{ route('profile.password') }}"  class="btn btn-default btn-flat">
                          Change Password
                        </a>
                      </div>
                  </div>
                  <!-- /.row -->
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                      <a href="{{ route('profile.profile') }}" class="btn btn-default btn-flat">Profile</a>
                  </div>
                  <div class="pull-right">
                     <a  class="btn btn-default btn-flat"href="{{ route('admin.logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" id="log-out">Sign out</a>
                    <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                  </div>
                </li>
            </ul>
        </li>
      </div>
    </nav>
  </header>