@extends('teacher.layouts.app')
@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('teacher.layouts.sidebar')
      <div class="dashbord-content-common ash-bg">
            <div class="dashbord-content-common-pad">
              <div class="followers-wrp">
                <div class="row">
                  <div class="col-xl-5 col-5 col-md-5 col-sm-5">
                    <div class="followers-top">
                      <h2>My Followers</h2>
                    </div>
                    <div class="followers-list">
                      <ul>
                        <?php $followers = \App\Models\Follower::where('follower_id' , auth()->user()->id)->get() ;?>
                      @foreach($followers as $follower)
                        <li>
                        @if($follower->user->user_image)
                          <span class="user-img"><img src="{{ asset('storage/user/'.$follower->user->user_image)}}" alt="" /> </span>
                          @else
                          <span class="user-img"><img src="{{ asset('front/images/img3.png')}}" alt="" /> </span>
                          @endif
                          <span class="user-name">{{$follower->user->name}}</span>
                          <a href="javascript:void(0);" class="btn red-btn follower" data-id="{{$follower->user_id}}">Remove</a>
                        </li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                  <div class="col-xl-5 col-5 col-md-5 col-sm-5">
                    <div class="following-top">
                      <h2>I am Following</h2>
                    </div>
                    <div class="followers-list">
                      <ul>
                      <?php $followings = \App\Models\Follower::where('user_id' , auth()->user()->id)->pluck('follower_id') ;?>
                      @foreach($followings as $following)
                      <?php $followername=\App\Models\User::where('id' , $following)->first('name','user_image');?>
                        <li>
                          @if($followername->user_image)
                          <span class="user-img"><img src="{{ asset('storage/user/'.$followername->user_image)}}" alt="" /> </span>
                          @else
                          <span class="user-img"><img src="{{ asset('front/images/img3.png')}}" alt="" /> </span>
                          @endif
                         
                          <span class="user-name">{{$followername->name}}</span>
                          <a href="javascript:void(0);" class="btn yellow-btn follower" data-id="{{$following}}">Unfollow</a>
                        </li>
                      @endforeach
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
@endsection
