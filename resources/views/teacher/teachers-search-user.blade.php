@extends('teacher.layouts.app')

@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('teacher.layouts.sidebar')
      @include('teacher.popup.slot-book')
      @include('popup.parent-book')
      <div class="dashbord-content-common ash-bg dashbord-parent-children">
          <div class="dashbord-content-common-pad">
            <div class="dashbord-inner-right-padd parent-search-area-wrap">
            <div class="parent-search-area">
          <form method="post" action="{{route('teacher.user.search')}}" id="filter-person">
            @csrf
            <div class="row">
              <div class="col-lg-2">
                  <div class="form-group">
                    <select class="form-control" id="sort" name="sort">
                      <option value="">Sort</option>
                      <option value="ASC" @if(app('request')->input('sort') == "ASC") selected @endif>Name Ascending</option>
                      <option value="DESC" @if(app('request')->input('sort') == "DESC") selected @endif>Name Descending</option>
                    </select>
                  </div>
              </div>
              <div class="col-lg-2">
                  <div class="form-group">
                    <select class="form-control" id="filter" name="filter">
                      <option value="">Filter</option>
                      @foreach($subjects as $subject)
                      <option value="{{$subject->subject}}" @if(app('request')->input('filter') == $subject->subject) selected @endif>{{$subject->subject}}</option>
                      @endforeach
                    </select>
                  </div>
              </div>
              <div class="col-lg-4">
                  <div class="form-group">
                    <input type="text" class="form-control" id="search" name="search" aria-describedby="" placeholder="Search here by name of the user" value="{{app('request')->input('search')}}">
                  </div>
              </div>
              <div class="col-lg-2">
                <button type="submit"   class="btn">Search Now</button>
              </div>
              <div class="col-lg-2">
                <a href="{{route('teacher.user.search')}}" class="btn">Clear</a>
              </div>
            </div>
          </form>
        </div>
            </div>
            <div class="parent-teacher-srch-reslt only-teacher-srch-reslt">
              <h2>Teachers</h2>
              <div class="parent-teacher-srch-reslt-wrap">
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Profile Image</th>
                      <th scope="col">Teacher's Name</th>
                      <th scope="col">Experience</th>
                      <th scope="col">Subject</th>
                      <th scope="col">Rating</th>
                      <th scope="col">Badges Earned</th>
                      <th scope="col">Action</th>
                      <th scope="col">Follow</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($users as $user)
                    @if($user->role == 'teacher')
                    <tr id="search-result">
                      @if(isset($user->user_image))
                      <td><div class="user-img"><a href="{{ url('public/profile/'.$user->id) }}"><img src="{{ asset('upload/images/avatar/teacher/'.$user->user_image) }}" alt=""></a></div></td>
                      @else
                      <td><div class="user-img"><a href="{{ url('public/profile/'.$user->id) }}"><img src="{{ asset('front/images/img3.png')}}" alt=""></a></div></td>
                      @endif
                      <td>{{$user->name}}</td>
                      <td>{{$user->experience}}+ years</td>
                      <td>
                        <ul class="list-show">
                          @foreach($user->subjects as $subject)
                          <li><span>{{$subject->subject}}</span></li>
                          @endforeach
                        </ul>
                      </td>
                      <td>
                        <ul class="rating-box">
                          <li>
                            <ul>
                              <li><i class="fas fa-star"></i></li>
                              <li><i class="fas fa-star"></i></li>
                              <li><i class="fas fa-star"></i></li>
                              <li><i class="fas fa-star"></i></li>
                              <li><i class="fas fa-star"></i></li>
                            </ul>
                          </li>
                          <li>4.9</li>
                        </ul>
                      </td>
                      <td>
                        <ul class="achiv-box">
                          <li><div class="achive-img"><img src="{{ asset('front/images/achv.png')}}" alt=""></div></li>
                          <li>Ninza 2021</li>
                        </ul>
                      </td>
                      <td><a href="javascript:void(0);" class="btn yellow-btn teacher-book" data-bs-toggle="modal" data-bs-target="#scheduleBooking" data-id="{{$user->id}}">Book A Slot</a></td>
                      <td><div class="binacular"><img src="{{ asset('front/images/binaclr.png')}}" alt=""></div></td>
                    </tr>
                    @endif
                    @endforeach
                  </tbody>
                </table>
                <div class="loder-btn">
                  <a href="" class="btn yellow-text-btn">Load More Teachers <span><i class="fas fa-plus"></i></span></a>
                </div>
              </div>
            </div>

            <div class="seperator"></div>

            <div class="teacher-table-two">
              <div class="row">
                <div class="col-lg-5">
                  <div class="parent-teacher-srch-reslt only-teacher-srch-reslt">
                    <h2>Parents</h2>
                    <div class="parent-teacher-srch-reslt-wrap">
                      <table class="table" id="parent-book" data-userid="">
                        <thead>
                          <tr>
                            <th scope="col">Profile Image</th>
                            <th scope="col">Parents's Name</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                         @if($user->role == 'parent')
                          <tr>
                          @if(isset($user->user_image))
                          <td><div class="user-img"><img src="{{ asset('storage/user/'.$user->user_image) }}" alt=""></div></td>
                          @else
                          <td><div class="user-img"><img src="{{ asset('front/images/img3.png')}}" alt=""></div></td>
                          @endif
                            <td>{{$user->name}}</td>
                            <td><a href="javascript:void(0);" class="btn yellow-btn parent-book" data-bs-toggle="modal" data-bs-target="#parentBooking" onclick="addUserId({{$user->id}});">Book A Slot</a></td>
                          </tr>
                        @endif
                        @endforeach
                        </tbody>
                      </table>
                      <div class="loder-btn">
                        <a href="javascript:void(0);" class="btn yellow-text-btn">Load More Parents <span><i class="fas fa-plus"></i></span></a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-7">
                  <div class="parent-teacher-srch-reslt only-teacher-srch-reslt">
                    <h2>Students</h2>
                    <div class="parent-teacher-srch-reslt-wrap">
                      <table class="table">
                        <thead>
                       <tr>
                            <th scope="col">Profile Image</th>
                            <th scope="col">Students's Name</th>
                            <th scope="col">Grade</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                         @if($user->role == 'student')
                          <tr>
                          @if(isset($user->user_image))
                          <td><div class="user-img"><a href="#"><img src="{{ asset('upload/images/avatar/student/'.$user->user_image) }}" alt=""></a></div></td>
                          @else
                          <td><div class="user-img"><img src="{{ asset('front/images/img3.png')}}" alt=""></div></td>
                          @endif
                            <td>{{$user->name}}</td>
                            <td>Grade: {{$user->grade}}, Section: A</td>
                            <td><a href="javascript:void(0);" class="btn yellow-btn">Book A Slot</a></td>
                          </tr>
                          @endif
                        @endforeach
                        </tbody>
                      </table>
                      <div class="loder-btn">
                        <a href="javascript:void(0);" class="btn yellow-text-btn">Load More Students <span><i class="fas fa-plus"></i></span></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="seperator"></div>

          </div>
        </div>

@endsection

@section('scripts')
<script src="{{asset('js/parent-book.js')}}"></script>
<script src="{{asset('js/times.js')}}"></script>
    <script type="text/javascript">
     
      


        $('#subject-dropdown').on('change', function() {
          var subject_id = this.value;
          $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
          });
          $.ajax({
            url: "{{route('subject.strands')}}",
            type: "POST",
            data: {
              subject_id: subject_id
            },
            cache: false,
            success: function(result) {
              $('#strand-dropdown').html('<option value="">Select Strand</option>');
              $.each(result.strands, function(key, value) {
                $("#strand-dropdown").append('<option value="' + value.id + '">' + value.strand + '</option>');
              });
              $('#expectation-dropdown').html('<option value="">Select Expectation</option>');
            }
          });

        });

         
    </script>
    @endsection