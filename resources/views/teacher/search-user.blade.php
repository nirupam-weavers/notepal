@extends('teacher.layouts.app')

@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('teacher.layouts.sidebar')
      <div class="dashbord-content-common ash-bg dashbord-parent-children">
        <div class="dashbord-content-common-pad">
          <div class="dashbord-inner-right-padd ">
            <div class="parent-search-area">
              <form method="post" action="{{route('teacher.parent.search')}}" id="filter-person">
                @csrf
                <div class="row">
                  <div class="col-lg-4">
                    <a href="{{route('teacher.teacher.search')}}" class="btn">Search Teacher</a>
                  </div>
                  <div class="col-lg-4">
                    <a href="{{route('teacher.parent.search')}}" class="btn">Search Parent</a>
                  </div>
                  <div class="col-lg-4">
                    <a href="{{route('teacher.student.search')}}" class="btn">Search Student</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
          

        </div>
      </div>

      @endsection