<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use DB;
use Illuminate\Contracts\Mail\Mailer;

class UserController extends Controller
{
    public function user_list(Request $request)
    {
    	$user_list = User::where('role', '!=', 'admin')->orderBy('updated_at', 'desc')->get();
        
        return view('admin.user.user_list',compact('user_list'));
    }
    public function user_add()
    {
    	$user = DB::table('users')->get();
        return view('admin.user.add_user',compact('user'));
    }   
    public function user_edit($id)
    {
        $user = User::findOrFail($id);
        return view('admin.user.edit_user',compact('user'));
    }
    public function update_user(Request $request)
    {
        $inputs = $request->all();
        $this->validate($request,[
            'first_name' => 'required|string|max:255',
            'last_name' => 'nullable|string|max:255',
            // 'email' => 'required|string|email|max:255|unique:users,email,' . $id,
            'user_image' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
            'is_verified' => 'nullable',
        ]);
        
        $user = User::where('id', $inputs['user_id'])->first();
        $user->first_name = $inputs['first_name'];
        $user->last_name = $inputs['last_name'];
        $user->zip_code = $inputs['zip_code'];
        $user->is_verified = $inputs['is_verified'];
        if ($request->hasFile('user_image')) 
        {
            $image = $request->file('user_image');
            $imageName = time().'.'.$image->extension();
            //$image->storeAs('categories', $imageName);
            $image->move(public_path('user'), $imageName);
            $user->user_image = $imageName;             
        }
        $user->save();

        
        \Session::flash('message','Successfully updated.'); 
        return redirect()->back();
    }
       
}
