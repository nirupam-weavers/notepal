<div class="single-box-file-box">
  <ul>
    <li data-toggle="modal" data-target="#ass-ms-file">
      <div class="single-box-file-area select-box">
        <div class="box-icon">
          <img src="{{ asset('front/images/icon3.png')}}" alt="">
        </div>
        <div class="icon-text">
          <h6>Microsoft Doc</h6>
          <span id="ass_ms_file"></span>
        </div>
      </div>
    </li>
    <li data-toggle="modal" data-target="#ass-google-doc">
      <div class="single-box-file-area">
        <div class="box-icon">
          <img src="{{ asset('front/images/icon7.png')}}" alt="">
        </div>
        <div class="icon-text">
          <h6>Google Doc</h6>
          <span id="ass_google_doc_link"></span>
        </div>
      </div>
    </li>
    <li data-toggle="modal" data-target="#ass-google-form">
      <div class="single-box-file-area">
        <div class="box-icon">
          <img src="{{ asset('front/images/icon4.png')}}" alt="">
        </div>
        <div class="icon-text">
          <h6>Google Form</h6>
          <span id="ass_google_form_link"></span>
        </div>
      </div>
    </li>
    <li data-toggle="modal" data-target="#ass-google-slide">
      <div class="single-box-file-area">
        <div class="box-icon">
          <img src="{{ asset('front/images/icon8.png')}}" alt="">
        </div>
        <div class="icon-text">
          <h6>Google Slide</h6>
          <span id="ass_google_slide_link"></span>
        </div>
      </div>
    </li>
    <li data-toggle="modal" data-target="#ass-pdf">
      <div class="single-box-file-area">
        <div class="box-icon">
          <img src="{{ asset('front/images/icon5.png')}}" alt="">
        </div>
        <div class="icon-text">
          <h6>PDF File</h6>
          <span id="ass_pdf_file"></span>
        </div>
      </div>
    </li>
    <li data-toggle="modal" data-target="#ass-video">
      <div class="single-box-file-area">
        <div class="box-icon">
          <img src="{{ asset('front/images/icon6.png')}}" alt="">
        </div>
        <div class="icon-text">
          <h6>Video File</h6>
          <span id="ass_video_link"></span>
        </div>
      </div>
    </li>
  </ul>
</div>

<div class="parent-add-children-form-modal">
  <!-- Modal -->
  <div class="modal fade" id="ass-ms-file" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><img src="{{asset('front/images/cross.svg')}}" alt="" /></span>
          </button>
        </div>
        <div class="modal-body">
          <div class="children-form-add">
            <h2>Add Your File</h2>
            <div class="form-group">
              <div class="custom-input-file">
                <input type="file" onchange="myChangeFunction(this)" name="ass_ms_file" accept=".doc, .docx" class="file">
                <input type="text" class="file-name" readonly="readonly">
                <input type="button" class="btn yellow-btn fileUpload" value="Browse">
              </div>
            </div>
            <button type="button" class="btn" data-dismiss="modal">Add Now</button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--ass-google-doc -->
  <div class="modal fade" id="ass-google-doc" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><img src="{{asset('front/images/cross.svg')}}" alt="" /></span>
          </button>
        </div>
        <div class="modal-body">
          <div class="children-form-add">
            <h2>Add Your Google doc Link</h2>
            <div class="form-group">
              <input type="url" onchange="myChangeFunction(this)" name="ass_google_doc_link" class="form-control">
            </div>
            <button type="button" class="btn" data-dismiss="modal">Add Now</button>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!--ass-google-form -->
  <div class="modal fade" id="ass-google-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><img src="{{asset('front/images/cross.svg')}}" alt="" /></span>
          </button>
        </div>
        <div class="modal-body">
          <div class="children-form-add">
            <h2>Add Your Google form link</h2>
            <div class="form-group">
              <input type="url" onchange="myChangeFunction(this)" name="ass_google_form_link" class="form-control">
            </div>
            <button type="button" class="btn" data-dismiss="modal">Add Now</button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- ass-google-slide -->
  <div class="modal fade" id="ass-google-slide" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><img src="{{asset('front/images/cross.svg')}}" alt="" /></span>
          </button>
        </div>
        <div class="modal-body">
          <div class="children-form-add">
            <h2>Add Your Google Slide Link</h2>
            <div class="form-group">
              <input type="url" onchange="myChangeFunction(this)" name="ass_google_slide_link" class="form-control">
            </div>
            <button type="button" class="btn" data-dismiss="modal">Add Now</button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- ass-pdf -->
  <div class="modal fade" id="ass-pdf" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><img src="{{asset('front/images/cross.svg')}}" alt=""/></span>
          </button>
        </div>
        <div class="modal-body">
          <div class="children-form-add">
            <h2>Add Your PDF File</h2>
            <div class="form-group">
              <div class="custom-input-file">
                <input type="file" onchange="myChangeFunction(this)" name="ass_pdf_file" class="file" accept=".pdf">
                <input type="text" class="file-name" readonly="readonly">
                <input type="button" class="btn yellow-btn fileUpload" value="Browse">
              </div>
            </div>
            <button type="button" class="btn" data-dismiss="modal">Add Now</button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- ass-video -->
  <div class="modal fade" id="ass-video" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><img src="{{asset('front/images/cross.svg')}}" alt="" /></span>
          </button>
        </div>
        <div class="modal-body">
          <div class="children-form-add">
            <h2>Add Your Video Link</h2>
            <div class="form-group">
              <input type="url" onchange="myChangeFunction(this)" name="ass_video_link" class="form-control">
            </div>
            <button type="button" class="btn" data-dismiss="modal">Add Now</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>