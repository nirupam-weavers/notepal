<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Rating;
use App\Models\Lesson;
use Illuminate\Support\Facades\Validator;
 
use Response;

class TeacherRatingController extends Controller
{
    public function rating(Request $request)
    {

        $rules = [
            'rate' => ['required'],
        ];
        $messages = array(
          'rate.required' => 'Rating is required',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } 
        $lesson_id = $request->lesson_id;
        $lesson = Lesson::find($lesson_id);
        if(is_object($lesson)){
            //dd(auth()->user()->id,$lesson->user_id);
            if(auth()->user()->id == $lesson->user_id){
             return redirect()->back()->withInput()->withErrors("You can't give rating for own lesson"); 
            }
        }
        $chk_rating = Rating::where('lesson_id', $lesson_id)->where('user_id', auth()->user()->id)->first();
        if(is_object($chk_rating)){
            return redirect()->back()->withInput()->withErrors("You alredy give the rating");
        }

        $rating = new Rating;
        $rating->user_id = auth()->user()->id;
        $rating->rate = $request->rate;
        $rating->lesson_id = $request->lesson_id;
        $rating->feedback = $request->feedback;
        $rating->save();

        if ($lesson->avg_rating == 0) {
                $lesson->avg_rating = $request->rate;
        }else{
            $lesson->avg_rating = ($lesson->avg_rating + $request->rate) / 2;
        }
        $lesson->save();        
       
        return redirect()->back();
       }

    

    
}
