<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'date_for' =>'required',
          'start_time' =>'required',
          'duration' =>'required',
        ];
    }

    public function messages()
    {
        return [
            'duration.required' => 'Session Length is required.',
            'time.required' => 'Time is required.',
            'date.required' => 'Date is required.',
        ];
    }
}
