<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Booking;
use App\Models\Follower;
use App\Models\Notification;
use App\Http\Requests\BookingRequest;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class BookingController extends Controller
{
    public function slot_booking(BookingRequest $request)
    {
        $user = auth()->user();
        $mentor_id = $request->user_id;
        $day_for = $request->day_for;
        $date_for = $request->date_for;
        $start_time = $request->start_time;
        $length = $request->duration;       

        $l = explode(':', $length);
        $l1 = $l[0];
        $l2 = $l[1];
        $session_length = $l1;
        if ($l2 == '15') {
          $session_length += 0.25;
        }  elseif ($l2 == '30') {
          $session_length += 0.5;
        }
        elseif ($l2 == '45') {
         $session_length += 0.75;
        }elseif ($l2 == '60') {
         $session_length += 1.0;
        }
        $end_time = Carbon::parse($start_time)
            ->addMinutes($session_length*60)
            ->format('H:i:s');
        $book = new Booking;
        $book->mentor_id = $mentor_id;
        $book->user_id = $user->id;
        $book->purpose = $request->purpose;
        $book->note = $request->note;
        $book->session_length = $session_length;
        $book->status = '';
        $book->day_for = $day_for;
        $book->date_for = $date_for;
        $book->start_time = $start_time;
        $book->end_time = $end_time;
        $book->save();
        $book->booking_id = Crypt::encryptString($book->id);
        $book->save(); 

        $sendmail = User::find($mentor_id);
       
            $email = $sendmail->email;
            $subject = "Booking created";
            send_follower_mail($email,$subject);

        $notification = new Notification;
        $notification->sender_id = $user->id;
        $notification->receiver_id = $mentor_id;
        $notification->notification_type = "Booking creation";
        $notification->object_id = $book->id;
        $notification->message = $subject;
        $notification->read = 0;
        $notification->save();

        $data['msg'] = 'success';
        
        return $data;
    }

    public function accept_booking(Request $request)
    {
        $booking_id = $request->id;
        $booking = Booking::findOrfail($booking_id);
        $booking->status = 'Accepted';
        $booking->user_status = 'Accepted';
        $booking->save();

        Session::flash('success','Booking is successfully accepted'); 
        return redirect()->back();
    }

    public function selfcancel_booking(Request $request)
    {
        $booking_id = $request->id;
        $booking = Booking::findOrfail($booking_id);
        $booking->date_for = date('Y-m-d');
        $booking->status = 'Cancelled';
        $booking->user_status = 'Cancelled';
        $booking->cancel_reason = $request->reason;
        $booking->save();

        Session::flash('success','Booking is successfully cancelled'); 
        return redirect()->back();
    }

    public function cancel_booking(Request $request)
    {
        $booking_id = $request->id;
        $booking = Booking::findOrfail($booking_id);
        $booking->date_for = date('Y-m-d');
        $booking->status = 'Cancelled';
        $booking->user_status = 'Rejected';
        $booking->cancel_reason = $request->reason;
        $booking->save();

        Session::flash('success','Booking is successfully cancelled'); 
        return redirect()->back();
    }


    public function edit_booking(BookingRequest $request)
    {
        $data= [];
        $user_id = $request->user_id;
        $mentor_id = $request->mentor_id;
        $day_for = $request->day_for;
        $date_for = $request->date_for;
        $start_time = $request->start_time;
        $length = $request->duration; 
        
        if(strlen($request->purpose) > 20){
            return $data; 
        }
        if(strlen($request->note) > 20){
            return $data; 
        }

        $l = explode(':', $length);
        $l1 = $l[0];
        $l2 = $l[1];
        $session_length = $l1;
        if ($l2 == '15') {
          $session_length += 0.25;
        }  elseif ($l2 == '30') {
          $session_length += 0.5;
        }
        elseif ($l2 == '45') {
         $session_length += 0.75;
        }elseif ($l2 == '60') {
         $session_length += 1.0;
        }
        $end_time = Carbon::parse($start_time)
            ->addMinutes($session_length*60)
            ->format('H:i:s');
        $book = Booking::find($request->booking_id);
        //$book->mentor_id = $mentor_id;
        $book->user_id = $user_id;
        $book->purpose = $request->purpose;
        $book->note = $request->note;
        $book->session_length = $session_length;
        $book->status = '';
        $book->day_for = $day_for;
        $book->date_for = $date_for;
        $book->start_time = $start_time;
        $book->end_time = $end_time;
        $book->save();
        $book->booking_id = Crypt::encryptString($book->id);
        $book->save(); 

        $sendmail = User::find($mentor_id);
       
            $email = $sendmail->email;
            $subject = "Booking created";
            send_follower_mail($email,$subject);

        $notification = new Notification;
        $notification->sender_id = $user_id;
        $notification->receiver_id = $mentor_id;
        $notification->notification_type = "Booking creation";
        $notification->object_id = $book->id;
        $notification->message = "Booking edited ";
        $notification->read = 0;
        $notification->save();

        $data['msg'] = 'success';
        
        return $data;
    }
}
