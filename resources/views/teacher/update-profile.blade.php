
@extends('teacher.layouts.app')

@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('teacher.layouts.sidebar')
 
        <div class="dashbord-content-common ash-bg dashbord-parent-children">
          <div class="dashbord-content-common-pad">
            <div class="teacher-edit-profile">
              <div class="teacher-edit-profile-wraper">
                <div class="teacher-img">
                  <div class="teacher-img-only">
                  @if($user->user_image)         
                  <img id="blah" src="{{ asset('storage/user/'.$user->user_image)  }}" alt="">        
                  @else
                  <img id="blah" src="{{ asset('front/images/img3.png') }}" alt="">
                  @endif
                  </div>
                  <form method="POST" name="updateprofile" action="{{ route('teacher.updateprofile.post') }}" enctype="multipart/form-data">
                     @csrf
                  <div class="edit-pic">
                    <span><i class="fas fa-pencil-alt"></i></span>
                    <input type="file" id="imgInp" name="myfile" accept="image/*">
                  </div>
                </div>
                <div class="teacher-edit-form">
                 
                    <div class="row">
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label for="">Enter your Name</label>
                          <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}" placeholder="User name">
                           @error('name')
                            <span class="" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                           @enderror
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="">Enter your mail id</label>
                          <input type="email" class="form-control" id="" aria-describedby=""  value="{{$user->email}}">
                          @error('email')
                            <span class="" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                           @enderror
                        </div>
                      </div>
                      <div class="col-lg-6">
                          <div class="form-group">
                            <label for="">Enter your total experience (in years)</label>
                            <select class="form-control" id="" name="experience">
                              <option value="{{$user->experience}}" @if(isset($user->experience)) selected="selected" @endif > {{$user->experience}} +Years</option>
                              <option value="8">8+ Years</option>
                              <option value="7">7+ Years</option>
                              <option value="6">6+ Years</option>
                              <option value="5">5+ Years</option>
                              <option value="4">4+ Years</option>
                              <option value="3">3+ Years</option>
                              <option value="2">2+ Years</option>
                              <option value="1">1+ Years</option>
                              
                            </select>
                          </div>
                          @error('experience')
                            <span class="" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                           @enderror
                      </div>
                      <div class="col-lg-6">
                          <div class="form-group">
                            <label for="">Enter your teaching subject</label>
                            <?php
                            $sel_subjects = [];
                            if($user->expertises){
                              $sel_subjects = $user->expertises->pluck('subject_id')->toArray();
                            }
                            ?>
                            <select class="form-control select2" id="" name="subject_ids[]" multiple="multiple" data-placeholder="Select Subject">
                            @foreach(\App\Models\Subject::all() as $subject)
                            <option value="{{$subject->id}}" @if(in_array($subject->id,$sel_subjects)) selected='selected' @endif> {{$subject->subject}}</option>
                            @endforeach
                            </select>
                          </div>
                      </div>
                      <div class="col-lg-12">
                          <div class="form-group">
                            <label for="">Give a brief description about yourself</label>
                            <textarea class="form-control" id="bio" name="bio" rows="3">{{$user->bio}}</textarea>
                          </div>
                      </div>
                    </div>
                 
                </div>
              </div>
                    <div class="submit-sec">
                        <button type="submit" class="btn yellow-btn">Submit</button>
                    </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <script src="{{ asset('js/updateprofile.js')}}"></script>
@endsection