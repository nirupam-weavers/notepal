@extends('admin.layouts.admin-app')

@section('content')
 <!-- Content Wrapper. Contains page content -->

<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Comment <small>Comment List</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Comment</a></li>
      <li class="active">Comment List</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Comment List</h3>
            <a href="{{route('add.comment')}}" class="btn btn-primary a-btn-slide-text" style="float:right"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <span><strong>Add</strong></span> </a> 
            </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="comment_table" class="table table-bordered table-hover">
              <thead>
                <tr>
                   <th><b>ID</b></th>
                   <th><b>Comment Name</b></th>
                   <th><b>Comment Value</b></th>
                   <th><b>Comment Order</b></th>
                   <th><b>Description</b></th>
                  <th align="center">Action</th>
                </tr>
              </thead>
              <tbody>
               @if(!empty($comment_list))
               @php $i=1; @endphp
                 @foreach($comment_list as $comment)                 
                  <tr>
                   <td>{{ $i }}</td>
                   <td>{{$comment->name}}</td>
                   <td>{{$comment->value}}</td>
                   <td>{{$comment->order}}</td>
                   <td>{{$comment->description}}</td>                   
                   <td><a href="{{route('edit.comment',$comment->id)}}">Edit</a></td>
                 </tr>
                 @php $i++; @endphp
                 @endforeach
               @endif
             
              </tbody>
              
            </table>
          </div>
          <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
      <!-- /.col --> 
    </div>
    <!-- /.row --> 
  </section>
  <!-- /.content --> 
</div>
<!-- /.content-wrapper -->
@endsection

@section('script')
<script type="text/javascript">
  $('#comment_table').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'excel', 'pdf'
            {
                extend: 'excel',
                title: 'Categories'
            }
        ]
    } );
</script>
@endsection