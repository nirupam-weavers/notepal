@extends('layouts.app')

@section('content')
<header class="main-header">
    <div class="container-fluid header-row">
      <div class="logo">
        <a href="#"><img src="images/logo.png" alt=""></a>
      </div>
      <div class="login-btn">
        <a href="{{ route('student.login') }}" class="btn">Sign In</a>
      </div>
    </div>
</header>

<div class="parents-signup-page student-sigh-up-page">
  <div class="container-fluid">
    <div class="row full-hight">
      <div class="col-lg-6">
        <div class="parent-signup-frm">
            <h3>Sign Up</h3>
            <form method="POST" action="{{ route('student.registerage') }}" id="check-age">
             @csrf
                <div class="form-group">
                  <label for="exampleInputEmail1">Enter your birth date</label>
                  <div class="date-brth-value">
                      <div class="form-group">
                          <select class="form-control" name="day" id="day">
                          <option value="">Select Day</option>
                          @for($i=1;$i<=31;$i++)
                            <option value="{{$i}}">{{$i}}</option>
                          @endfor
                          </select>
                          @error('day')
                            <span role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                        <div class="form-group">
                          <select class="form-control" name="month" id="month">
                          <option value="">
                       Select Month
                        </option>
                        <option value="Jan">Jan</option>
                        <option value="Feb">Feb</option>
                        <option value="Mar">Mar</option>
                        <option value="Apr">Apr</option>
                        <option value="May">May</option>
                        <option value="Jun">Jun</option>
                        <option value="Jul">Jul</option>
                        <option value="Aug">Aug</option>
                        <option value="Sep">Sep</option>
                        <option value="Oct">Oct</option>
                        <option value="Nov">Nov</option>
                        <option value="Dec">Dec</option>
                          </select>
                          @error('month')
                            <span role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror 
                        </div>
                        <div class="form-group">
                          <select class="form-control" name="year" id="year">
                          <option value="">
                         Select Year
                          </option>
                          <?php $year = date("Y");?>
                          @for ($i = 1990; $i <= $year; $i++)
                              <option value="{{ $i }}">{{ $i }}</option>
                          @endfor  
                         </select>
                         @error('year')
                            <span role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                  </div>
                </div>
                <div class="login-btn">
                    <button type="button" class="btn blue-btn" id="continue">
                        {{ __('Continue') }}
                    </button>
                  </div>
                
            </form>
        </div>
      </div>
      <div class="col-lg-6 blue-bg reletive-div">
        <div class="parent-signup-text">
          <h3>Platform for students</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, </p>
          <p>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
        </div>
        <div class="overlap-img">
            <img src="{{ asset('front/images/student-signup-img.png') }}" alt="">
        </div>
      </div>
    </div>
  </div>
</div>

<div class="parent-add-children-notice-modal">
  <!-- Modal -->
  <div class="modal" id="checkAge" tabindex="-1" role="dialog" aria-labelledby="checkAge" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="children-notice">
            <h2>Looks like you are a student of under 13 years of age. Please ask your parent to create an account. Click Yes to go to parent sign up screen.</h2>
            <ul>
              <li><a href="{{route('parent.signup')}}" class="btn yellow-btn">Yes</a></li>
              <li><button type="button" class="btn" data-bs-dismiss="modal">No</button></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
  $('#continue').click(function(){
    var flag = true;
    var day = $('#day').val();
    var month = $('#month').val();
    var year = $('#year').val();
    if(day == '' || month == '' || year == ''){
      alert("Date feild is required");
    }else{      
      var birthDateString = day +'/'+ month +'/'+ year;
      if(getAge(birthDateString) < 13) {
          $('#checkAge').modal('show');
      }else{
        $('#check-age').submit();
      }
    }
  });

  function getAge(birthDateString) {
    var today = new Date();
    var birthDate = new Date(birthDateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}

 
</script>
@endsection
