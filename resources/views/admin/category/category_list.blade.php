@extends('admin.layouts.admin-app')

@section('content')
 <!-- Content Wrapper. Contains page content -->

<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Category <small>Category List</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Category</a></li>
      <li class="active">Category List</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Category List</h3>
            <a href="{{route('add.category')}}" class="btn btn-primary a-btn-slide-text" style="float:right"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <span><strong>Add</strong></span> </a> 
            </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="category_table" class="table table-bordered table-hover">
              <thead>
                <tr>
                   <th><b>ID</b></th>
                   <th><b>Category</b></th>
                   <th><b>Parent Category</b></th>
                   <th><b>Child Category</b></th>
                   <th><b>Category Comments</b></th>
                   <th><b>Status</b></th>
                  <th align="center">Action</th>
                </tr>
              </thead>
              <tbody>
               @if(!empty($category_list))
               @php $i=1; @endphp
                 @foreach($category_list as $category)                 
                  <tr>
                   <td>{{ $i }}</td>
                   <td>{{$category->name}}</td>
                   <td>
                     @if($category->parent){{$category->parent->name}}@endif
                   </td>
                   <td>
                     @if($category->get_children)
                     {{implode(',',$category->get_children->pluck('name')->toArray())}}
                     @endif
                   </td>
                   <td>
                    @if($category->category_comments())
                    {{implode(',',$category->category_comments->pluck('name')->all())}}
                    @endif
                   </td>
                    @if($category->status==1)
                    <td><span class="label label-success">Active</span></td>
                    @else
                    <td><span class="label label-danger">Inactive</span></td>
                    @endif
                   <td><a href="{{route('edit.category',$category->id)}}">Edit</a></td>
                 </tr>
                 @php $i++; @endphp
                 @endforeach
               @endif
             
              </tbody>
              
            </table>
          </div>
          <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
      <!-- /.col --> 
    </div>
    <!-- /.row --> 
  </section>
  <!-- /.content --> 
</div>
<!-- /.content-wrapper -->
@endsection

@section('script')
<script type="text/javascript">
  $('#category_table').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'excel', 'pdf'
            {
                extend: 'excel',
                title: 'Categories'
            }
        ]
    } );
</script>
@endsection