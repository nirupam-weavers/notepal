<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use Carbon\Carbon;

class StudentRegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showCheckAge()
    {
        return view('auth.student.register-age');
    }

    public function showRegistrationForm()
    {
        return view('auth.student.register');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function register(Request $request)
    {
        $data = $request->all();
        //dd($data);
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'unique:users', 'max:255','regex:/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/'],
            'password' => ['required', 'string', 'min:6'],
            'check' => ['required'],
        ];
        $messages = array(
          'name.required' => 'This field is required',
          'email.required' => 'This field is required',
          'password.required' => 'This field is required',
          'check.required' => 'Check this box',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } 

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'role' => 'student',
            'password' => Hash::make($data['password']),
            'age' => $request->age,
        ]);

        $this->guard()->login($user);

        return redirect()->route('student.login');
    }

    public function registerage(Request $request)
    {
        $rules = [
            'day' => ['required'],
            'month' => ['required'],
            'year' => ['required'],            
        ];
        $messages = array(
          'age.required' => 'This field is required',
          'day.required' => 'This field is required',
          'month.required' => 'This field is required',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->route('student.signup')->withInput()->withErrors($validator);
        } 
        $day = $request->day;
        $month = $request->month;
        $year = $request->year;
        $dob = $day."-".$month."-".$year;
        $age = Carbon::parse($dob)->age ;
        
        if($age < 13){
            return redirect()->route('parent.signup');  
        }
        else{
            return redirect()->route('student.signup');    
        }
        
    }
}
