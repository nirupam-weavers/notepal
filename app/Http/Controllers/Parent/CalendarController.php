<?php

namespace App\Http\Controllers\Parent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subject;
use App\Models\Booking;
use App\Models\User;

class CalendarController extends Controller
{
    public function index()
    {
    	$user_id = auth()->user()->id;
    	$parents = User::where('role','parent')->where('id','!=',$user_id)->get();
    	$subjects = Subject::all();
    	//$bookings = Booking::where('mentor_id',$user_id)->orWhere('user_id',$user_id)->orderBy('created_at','DESC')->get();
    	$pendding_statuses = Booking::where('user_id',$user_id)->orderBy('created_at','DESC')->get();
		$bookings = Booking::where('mentor_id',$user_id)->orderBy('created_at','DESC')->get();
		return view('parent.calendar',compact('subjects','parents','bookings','pendding_statuses'));
    }
}
