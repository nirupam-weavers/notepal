<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Favorite;
use App\Models\Follower;
use App\Models\User;
use App\Models\Booking;
use App\Models\NotificationSetting;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function admin_dashboard()
    {
        $total_user = User::count();
        return view('admin.dashboard',compact('total_user'));
    }

    public function logout(Request $request) 
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('admin/login');
    }

    public function toggleFavorite(Request $request)
    {
        $rtrn=0;
        $user_id = Auth::user()->id;
        $exists = Favorite::where('lesson_id',$request->lesson_id)
        ->where('document_type',$request->document_type)
        ->where('user_id',$user_id)
        ->first();
        if ($exists === null)
        {
            $fav = new Favorite;
            $fav->lesson_id= $request->lesson_id;
            $fav->document_type= $request->document_type;
            $fav->user_id =$user_id;
            $fav->save();
            $rtrn=1;
        }
        else
        {
            $exists->delete();
        }       
        
        echo $rtrn;
    }

    public function toggleFollower(Request $request)
    {
        $rtrn=0;
        $user_id = Auth::user()->id;
        $exists = Follower::where('user_id',$user_id )
        ->where('follower_id',$request->follower_id)
        ->first();
        if ($exists === null)
        {
            $fav = new Follower();
            $fav->user_id =  $user_id;
            $fav->follower_id= $request->follower_id;
            $fav->save();
            $rtrn=1;
           
         } else
            {
                $exists->delete();
            }     
        
        echo $rtrn;
    }

    public function find_user(Request $request)
    {
        $html = "";
        $user_id = auth()->user()->id;
        if(!empty($request->search)){ 
            $search = $request->search;
            $usrrole = $request->usrrole; 
            $users = User::where(function($q) use ($search){
                $q->where('name', 'LIKE', "%".$search."%");
                $q->orWhere('role', 'LIKE', "%".$search."%");
            });
            $users = $users->where('id','!=',$user_id)->where('role','!=','admin');
            switch($usrrole){
                case 'parent':
                    $users = $users->where('role','!=','student');
                    break;
                case 'student':
                    $users = $users->where('role','!=','parent');
                    break;
            }        
            $users = $users->get();
            $user = asset('front/images/img3.png');
            $cross = asset('front/images/cross.svg');
            foreach ($users as $value) {
                $html .= "<div class='teachers-each-search' data-id=".$value->id."><div class='teachers-each-img'><img src=".$user."></div><div class='teachers-each-info'><h3>Name: <span>".$value->name."</span></h3><h3>Category: <span>".$value->role."</span></h3></div><div class='teachers-each-del'><img src=".$cross."></div></div>";
            }
        }
        
        return $html;
    }

    public function showUser(Request $request)
    {
        $html = "";
        $user = User::findOrFail($request->user_id);            
        if(!empty($user->user_image)){
            $user_img = asset('storage/users/'.$user->user_image);
        }else{
            $user_img = asset('front/images/img3.png');            
        }
        $user_badge = asset('front/images/badge.png');
        $html = "<div class='teachers-each-img'><img src=".$user_img."><img src=".$user_badge." class='teacher-badge'></div><div class='teachers-each-info'><h3>Name:<span>".$user->name."</span></h3><h3>Category:<span>".$user->role."</span></h3></div>";        
        
        return $html;
    }

    public function getBooking(Request $request)
    {
        $html = "";
      
        $booking = Booking::findOrFail($request->booking_id);            
        if(!empty($booking->user->user_image)){
            $user_img = asset('storage/users/'.$booking->user->user_image);
        }else{
            $user_img = asset('front/images/img3.png');            
        }
        $user_badge = asset('front/images/badge.png');
        $html = "<div class='teachers-each-img'><img src=".$user_img."><img src=".$user_badge." class='teacher-badge'></div><div class='teachers-each-info'><h3>Name:<span>".$user->name."</span></h3><h3>Category:<span>".$user->role."</span></h3></div>";        
        
        return $html;
    }


    public function toggleBooking(Request $request)
    {
        $rtrn=0;
        $user_id = Auth::user()->id;
        $exists = NotificationSetting::where('user_id',$user_id )
        ->first();
        if ($exists === null)
        {
            $notificationsetting = new NotificationSetting();
            $notificationsetting->user_id =  $user_id;
            $notificationsetting->booking_status= $request->booking_status;
            $notificationsetting->save();
            $rtrn=1;
           
         } else
            {
            $exists->booking_status= $request->booking_status;
            $exists->save();
            $rtrn=2;
            }     
        
        echo $rtrn;
    }

    public function toggleLesson(Request $request)
    {
        $rtrn=0;
        $user_id = Auth::user()->id;
        $exists = NotificationSetting::where('user_id',$user_id )
        ->first();
        if ($exists === null)
        {
            $notificationsetting = new NotificationSetting();
            $notificationsetting->user_id =  $user_id;
            $notificationsetting->lesson_status= $request->lesson_status;
            $notificationsetting->save();
            $rtrn=1;
           
         } else
            {
            $exists->lesson_status= $request->lesson_status;
            $exists->save();
            $rtrn=2;
            }     
        
        echo $rtrn;
    }
    public function toggleNewsletter(Request $request)
    {
        $rtrn=0;
        $user_id = Auth::user()->id;
        $exists = NotificationSetting::where('user_id',$user_id )
        ->first();
        if ($exists === null)
        {
            $notificationsetting = new NotificationSetting();
            $notificationsetting->user_id =  $user_id;
            $notificationsetting->newsletter_status= $request->newsletter_status;
            $notificationsetting->save();
            $rtrn=1;
           
         } else
            {
            $exists->newsletter_status= $request->newsletter_status;
            $exists->save();
            $rtrn=2;
            }     
        
        echo $rtrn;
    }
    public function toggleBreakingnews(Request $request)
    {
        $rtrn=0;
        $user_id = Auth::user()->id;
        $exists = NotificationSetting::where('user_id',$user_id )
        ->first();
        if ($exists === null)
        {
            $notificationsetting = new NotificationSetting();
            $notificationsetting->user_id =  $user_id;
            $notificationsetting->breaking_status= $request->breakingnews_status;
            $notificationsetting->save();
            $rtrn=1;
           
         } else
            {
            $exists->breaking_status= $request->breakingnews_status;
            $exists->save();
            $rtrn=2;
            }     
        
        echo $rtrn;
    }
}
