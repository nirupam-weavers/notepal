const wdays = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
var base_url = window.location.origin;
var studentbook = base_url+"/"+"student/slot-booking";
var showUser = base_url+"/"+"student/show-user";

$('#student-slot-datepicker').datetimepicker({    
   inline:true,
   format:'Y-m-d',
   timepicker:false,
   minDate:new Date(),
 });

 function addUserId(id){
  $("#student-book").attr('data-userid',id);
  $.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
       }
   });
   $.ajax({
       url: showUser,
       type: "POST",
       data: {
           user_id: id,
       },
       cache: false,
       success: function(result){
         $('#showuser').html(result);
       }
   });
 }

  $('#student-slot-datepicker').on('change', function(e){
    e.preventDefault();
     var date = this.value;
     var dt = new Date(date);
     // var day = wdays[dt.getDay()];
     // var month = months[dt.getMonth()];
     // var day_dt = dt.getDate();
     let day_dt = new Intl.DateTimeFormat('en', {timeZone: 'UTC', day: 'numeric' }).format(dt);
     let month = new Intl.DateTimeFormat('en', {timeZone: 'UTC', month: 'long' }).format(dt);
     let day = new Intl.DateTimeFormat('en', {timeZone: 'UTC', weekday: 'long' }).format(dt);
     $('#student-show-dt').html(day+', '+month+' '+day_dt);
     var duration = $('#student-duration').val();
     $('#student-booking-time-wrp').html(split_time(duration,date));           
  });

  $('#student-duration').on('change', function(e){
    e.preventDefault();
    var duration = $(this).val();
    var date = $('#student-slot-datepicker').val();
    $('#student-booking-time-wrp').html(split_time(duration,date));
  });

  $('.booking-time-wrp').on('click', '.booking-time-each', function (e) {
    e.preventDefault();
    var ele = $(this);  
    var time = ele.attr('data-time');
    var time_txt = ele.text();
    $('#student-booking-time-wrp').find('.booking-time-confrm').remove();
    var t_row = '<div class="booking-time-confrm"><span> '+time_txt+'</span><button type="button" data-time="'+time+'" id="student-confirm" class="booking-confirm">Confirm</button></div>' 
    ele.after(t_row);
  });

  $('.teacher-book').on('click', function (e) {
    e.preventDefault();
    $('.teacher-book').removeClass("active");
    $(this).addClass("active");
  });

  $('.student-book').on('click', function (e) {
    e.preventDefault();
    $('.student-book').removeClass("active");
    $(this).addClass("active");
  });

  $('.booking-time-wrp').on('click', '#student-confirm', function (e) {
    e.preventDefault();
    var user_id = $('#student-book').attr('data-userid');
    var date = $('#student-slot-datepicker').val();
    var dt = new Date(date);
    //var day_for = wdays[dt.getDay()];
    let day_for = new Intl.DateTimeFormat('en', {timeZone: 'UTC', weekday: 'long' }).format(dt);
    var duration = $('#student-duration').val();
    var purpose = $('#student-purpose').val();
    var note = $('#student-note').val();
    //var subject = $('#student-subject').val();
    var start_time = $(this).attr('data-time');
    var flag = true;
    if(purpose == ""){
      flag = false;
      toastr.error('Purpose field is required');
    }
    if(note == ""){
      flag = false;
      toastr.error('Note field is required.');
    }
    if(date == ""){
      flag = false;
      toastr.error('Pleade select date.');
    }

    if(flag){
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: studentbook,
        type: "POST",
        data: {
            user_id:user_id,
            day_for:day_for,
            date_for:date,
            start_time:start_time,
            duration:duration,
            purpose:purpose,
            note:note,
            //subject:subject
        },
        cache: false,
        success: function(result){
          switch (result['msg']) {
            case 'success':
                toastr.success('Booked successfully.');
                $('#studentBooking').modal('hide');
                break;
            case 'error':
                toastr.error('Fields are required.');
                break;
          }
        }
      });
    }
  });