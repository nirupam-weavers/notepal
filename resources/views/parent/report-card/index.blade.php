@extends('parent.layouts.app')

@section('content')
@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('parent.layouts.sidebar')

       
        <div class="dashbord-content-common ash-bg dashbord-parent-children">
          <div class="dashbord-content-common-pad">
            <div class="my-children-header">
              <div class="my-children-text">
                <h2>Report Card</h2>
              </div>
              <div class="my-children-add">
              <form action="{{route('parent.report.card')}}" method="POST" id="marksForm">
              @csrf
                  <div class="form-group">
                      <select class="form-control" name="user_id" id="selectChild">
                        <option value="">Select Child</option>
                        @foreach($children as $child)
                        <option value="{{$child->id}}" @if(app('request')->input('user_id')==$child->id) selected @endif>{{$child->name}} <span>( Current Grade: Grade {{$child->grade}} )</span></option>
                        @endforeach
                      </select>
                  </div>
              </form>
                  <a href="{{route('parent.report.card.add')}}" class="btn">Add Report Card</a>
              </div>
            </div>
            <div class="fix-section"  >
              <div class="my-children-view-area parent-report-card-view">
                <ul class="nav nav-tabs" role="tablist" id="myHeader">
                  <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab" id="marksView">Marks View</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab" id="graphView">Graph View</a>
                  </li>
                  <a href="{{route('parent.report.card.edit')}}" class="btn">Edit Report Card</a>
                </ul><!-- Tab panes -->
                
                <?php
                //$nse = array(0 => 'NA',1 => 'N',2 => 'S',3 => 'G',4 => 'E');
                ?>
                <div class="tab-content">
                  <div class="tab-pane active" id="tabs-1" role="tabpanel">
                    <table class="table">
                      <thead id="myTableHeader">
                        <tr>
                          <th scope="col"></th>
                          @foreach($year as $yr)
                          <?php $yr_arr = explode('-',$yr);?>
                          <th scope="col" colspan="3">
                            <ul class="table-season">
                              <li>Year <span>- {{$yr_arr[0]}}-{{$yr_arr[0]+1}}</span>* </li>
                              <li>Grade <span>- {{$yr_arr[1]}}</span>* </li>
                            </ul>
                          </th>
                          @endforeach
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td scope="row" colspan="10" class="vew-head"><h2>Learning Skill <span>*</span></h2></td>
                        </tr>
                        <tr>
                          <th>Subject Name:</th>
                          @for($j=1;$j<=$cols;$j++)
                          <th>Progress Report</th>
                          <th>Term 1</th>
                          <th>Term 2</th>
                          @endfor
                        </tr>
                        @foreach($marks as $skill => $mark)
                        <?php $m = explode(',',$mark);?>
                        <tr>
                          <td style="background-color: #e5ffe5;" class="td-pad">{{$skill}}</td>
                          @for($x=0;$x < count($m);$x++)
                          <?php $mg = explode('|',$m[$x]);?>
                          <td style="background-color: #fffae5;" class="td-pad">{{$nse[$mg[0]]}}</td>
                          @endfor
                        </tr>
                        @endforeach
                        <tr>
                          <td scope="row" colspan="10" class="vew-head"><h2>Academic Achievement <span>*</span></h2></td>
                        </tr>
                        <tr>
                          <td scope="row" colspan="10" class="vew-head-two"><h3>Part One: Language</h3></td>
                        </tr>
                        @foreach($lang_marks as $skill => $mark)
                        <?php $m = explode(',',$mark);?>
                        <tr>
                          <td style="background-color: #e5ffe5;" class="td-pad">{{$skill}}</td>
                          @for($x=0;$x < count($m);$x++)
                          <?php 
                          $mg = explode('|',$m[$x]);
                          $letter='';
                          $chk_m = $mg[0];
                          $grade = $mg[1];
                          $key = array_keys(array_filter(MARKS,function ($ar) use ($chk_m) {return in_array($chk_m, $ar);}));
                          if(isset($key[0])){
                            $letter=$key[0];
                          }
                          if($grade == 7 || $grade == 8){
                            $letter=$chk_m;
                          }
                          ?>
                          <td style="background-color: #fffae5;" class="td-pad">{{$letter}}</td>
                          @endfor
                        </tr>
                        @endforeach
                        <tr>
                          <td scope="row" colspan="10" class="vew-head-two"><h3>Part Two: French</h3></td>
                        </tr>
                        @foreach($frenches as $skill => $mark)
                        <?php $m = explode(',',$mark);?>
                        <tr>
                          <td style="background-color: #e5ffe5;" class="td-pad">{{$skill}}</td>
                          @for($x=0;$x < count($m);$x++)
                          <?php
                          $mg = explode('|',$m[$x]);
                          $letter='';
                          $chk_m = $mg[0];
                          $grade = $mg[1];
                          $key = array_keys(array_filter(MARKS,function ($ar) use ($chk_m) {return in_array($chk_m, $ar);}));
                          if(isset($key[0])){
                            $letter=$key[0];
                          }
                          if($grade == 7 || $grade == 8){
                            $letter=$chk_m;
                          }
                          ?>
                          <td style="background-color: #fffae5;"class="td-pad">{{$letter}}</td>
                          @endfor
                        </tr>
                        @endforeach
                        <tr>
                          <td scope="row" colspan="10" class="vew-head-two"><h3>Part Three: Other Subjects</h3></td>
                        </tr>
                        @foreach($others as $skill => $mark)
                        <?php $m = explode(',',$mark);?>
                        <tr>
                          <td style="background-color: #e5ffe5;" class="td-pad">{{$skill}}</td>
                          @for($x=0;$x < count($m);$x++)
                          <?php
                          $mg = explode('|',$m[$x]);
                          $letter='';
                          $chk_m = $mg[0];
                          $grade = $mg[1];
                          $key = array_keys(array_filter(MARKS,function ($ar) use ($chk_m) {return in_array($chk_m, $ar);}));
                          if(isset($key[0])){
                            $letter=$key[0];
                          }
                          if($grade == 7 || $grade == 8){
                            $letter=$chk_m;
                          }
                          elseif(($skill== "History" || $skill == "Geography") && ($grade != 7 || $grade != 8)){
                              $letter ='NA';
                          }
                          ?>
                          <td style="background-color: #fffae5;" class="td-pad">{{$letter}}</td>
                          @endfor
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                  <div class="tab-pane" id="tabs-2" role="tabpanel">
                    <div class="report-graph-view">
                      <table class="table">
                      <thead>
                        <tr>
                          <th scope="col"></th>
                          @foreach($year as $yr)
                          <?php $yr_arr = explode('-',$yr);?>
                          <th scope="col" colspan="3">
                            <ul class="table-season">
                              <li>Year <span>- {{$yr_arr[0]}}-{{$yr_arr[0]+1}}</span>* </li>
                              <li>Grade <span>- {{$yr_arr[1]}}</span>* </li>
                            </ul>
                          </th>
                          @endforeach
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td scope="row" colspan="10" class="vew-head"><h2>Learning Skill <span>* &nbsp</span><span> &nbsp &nbsp N = 1, S = 2, G = 3, E = 4</span></h2></td>
                        </tr>
                        <tr class="fix-header">
                          <th>Subject Name:</th>
                          @for($j=1;$j<=$cols;$j++)
                          <th>Progress Report</th>
                          <th>Term 1</th>
                          <th>Term 2</th>
                          @endfor
                        </tr>
                        <?php $i=0; ?>
                        @foreach($marks as $key=> $mark)
                        <tr>
                          <td style="background-color: #e5ffe5;" class="td-pad">{{$key}}</td>
                          <td colspan="9" style="background-color: #fff;"><canvas id="areaChart{{$i}}" height="80"></canvas></td>
                        </tr>  
                        <?php $i++; ?>                      
                        @endforeach
                        <tr>
                          <td scope="row" colspan="10" class="vew-head"><h2>Academic Achievement <span>*</span></h2></td>
                        </tr>
                        <tr>
                          <td scope="row" colspan="10" class="vew-head-two"><h3>Language</h3></td>
                        </tr>
                        @foreach($lang_marks as $key=> $mark)
                        <tr>
                          <td style="background-color: #e5ffe5;" class="td-pad">{{$key}}</td>
                          <td colspan="9" style="background-color: #fff;"><canvas id="areaChart{{$i}}" height="80"></canvas></td>
                        </tr>  
                        <?php $i++; ?>                       
                        @endforeach
                        <tr>
                          <td scope="row" colspan="10" class="vew-head-two"><h3>French</h3></td>
                        </tr>
                        @foreach($frenches as $key=> $mark)
                        <tr>
                          <td style="background-color: #e5ffe5;" class="td-pad">{{$key}}</td>
                          <td colspan="9" style="background-color: #fff;"><canvas id="areaChart{{$i}}" height="80"></canvas></td>
                        </tr>  
                        <?php $i++; ?>                       
                        @endforeach
                        <tr>
                          <td scope="row" colspan="10" class="vew-head-two"><h3>Other Subjects</h3></td>
                        </tr>
                        @foreach($others as $key=> $mark)
                        <tr>
                          <td style="background-color: #e5ffe5;" class="td-pad">{{$key}}</td>
                          <td colspan="9" style="background-color: #fff;"><canvas id="areaChart{{$i}}" height="80"></canvas></td>
                        </tr>  
                        <?php $i++; ?>                       
                        @endforeach
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
  function showGraph(id,value) {
    let marks = value.split(",");
    const newmarks = marks.map((val) => val.split("|")[0]);
    //console.log(newmarks);
    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $('#areaChart'+id).get(0).getContext('2d')
    // This will get the first returned node in the jQuery collection.
    var areaChart       = new Chart(areaChartCanvas)

    var areaChartData = {
      labels  : newmarks,
      datasets: [
        {
          label               : 'Marks',
          fillColor           : '#7fdefb',
          strokeColor         : 'rgba(210, 214, 222, 1)',
          pointColor          : 'rgba(210, 214, 222, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : newmarks
        }
      ]
    }

    var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale               : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : false,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - Whether the line is curved between points
      bezierCurve             : true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension      : 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot                : false,
      //Number - Radius of each point dot in pixels
      pointDotRadius          : 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth     : 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius : 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke           : true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth      : 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill             : true,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio     : true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive              : true
    }

    //Create the line chart
    areaChart.Line(areaChartData, areaChartOptions)

  }
  var marks = {!! json_encode($marks) !!};
  var lang_marks = {!! json_encode($lang_marks) !!};
  var frenches = {!! json_encode($frenches) !!};
  var others = {!! json_encode($others) !!};

  $('#selectChild').on('change', function(e){
    e.preventDefault();
    var eid = $(this).val();
    $('#editid').val(eid);
    $("#marksForm").submit();    
  });
  $('#report_card').on('click', function(e){
    e.preventDefault();
    $("#marksForm").submit();    
  });

  $('#graphView').on('click', function(e){
    e.preventDefault();
    setTimeout(() => {
      var i = 0;
      $.each(marks, function(k,v){
         // console.log(k + ": " + v);
          showGraph(i,v);
          i++;
      });
      $.each(lang_marks, function(k,v){
          showGraph(i,v);
          i++;
      });
      $.each(frenches, function(k,v){
          showGraph(i,v);
          i++;
      });
      $.each(others, function(k,v){
          showGraph(i,v);
          i++;
      });
    },100);
    
  });

</script>

<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;
var tableHeader = document.getElementById("myTableHeader");
var tableSticky = tableHeader.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky-section");
    tableHeader.classList.add("table-sticky-section");
  } else {
    header.classList.remove("sticky-section");
    tableHeader.classList.remove("table-sticky-section");
  }
}

// window.onscroll = function() {myFunction()};

// var header2 = document.getElementById("myTableHeader");
// var sticky3 = header.offsetTop;

// function myFunction() {
//   if (window.pageYOffset > sticky2) {
//     header2.classList.add("table-sticky-section");
//   } else {
//     header2.classList.remove("table-sticky-section");
//   }
// }
</script>

@endsection