<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\GoogleController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PublicController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('redirect/google/{role}', [GoogleController::class, 'redirectToGoogle']);
Route::get('auth/google/callback', [GoogleController::class, 'handleGoogleCallback']);
Route::get('microsoft/signin/{role}', [AuthController::class, 'signin']);
Route::get('/callback', [AuthController::class, 'callback']);
Route::get('public/profile/{id}', [PublicController::class, 'showProfile'])->name('public.profile');

Route::prefix('teacher')
    ->group(base_path('routes/teacher.php'));
Route::prefix('parent')
    ->group(base_path('routes/parent.php'));
Route::prefix('student')
    ->group(base_path('routes/student.php'));
Route::prefix('admin')
    ->group(base_path('routes/admin.php'));


Route::group(['middleware' => 'auth'], function(){
	Route::post('toggle/favorite', [HomeController::class,'toggleFavorite'])->name('toggle.favorite');
    Route::post('toggle/follower', [HomeController::class,'toggleFollower'])->name('toggle.follower');
    Route::post('toggle/booking_status', [HomeController::class,'toggleBooking'])->name('toggle.booking_status');	
    Route::post('toggle/lesson_status', [HomeController::class,'toggleLesson'])->name('toggle.lesson_status');	
    Route::post('toggle/newsletter_status', [HomeController::class,'toggleNewsletter'])->name('toggle.newsletter_status');	
    Route::post('toggle/breakingnews_status', [HomeController::class,'toggleBreakingnews'])->name('toggle.breakingnews_status');	
	Route::post('/find-user', [HomeController::class,'find_user']);	
});

