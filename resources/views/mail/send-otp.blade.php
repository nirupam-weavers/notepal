<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- So that mobile will display zoomed in -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- enable media queries for windows phone 8 -->
<meta name="format-detection" content="telephone=no">
<!-- disable auto telephone linking in iOS -->
<title>Email Verification OTP</title>
</head>

<body style="margin:0; padding:0;" bgcolor="#fff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="600" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#fff">
  <tr>
    <td height="20"></td>  
  </tr>    
  <tr>
	<td style="text-align: center;">
      <a href="#" target="_blank" style="text-decoration: none; border: 0;"><span style=" vertical-align: middle;font-size: 25px;
    color: #5a5454;
    font-weight: 800;">NotePal</span></a>
    </td>
  </tr>
  <tr>
    <td height="20"></td>  
  </tr>    
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="50">&nbsp;</td>
            <td width="500">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="border-bottom: 4px solid #c6a546;">
                 <tbody>  
                <tr>
                  <td width="30">&nbsp;</td>
                  <td width="460">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td>
                       <h1 style="font-family:Arial, Helvetica, sans-serif; font-size:24px; line-height:1; color:#4d4d4d; text-align: center;">Email Verification OTP</h1>
                        <p style="font-family:Arial, Helvetica, sans-serif; font-size:16px; line-height:1.6; color:#4d4d4d;">Hello <b>{{$child->name}},</b></p>	
                        <p style="font-family:Arial, Helvetica, sans-serif; font-size:16px; line-height:1.6; color:#4d4d4d;">Your <b>OTP</b> is : <b>{{$child->otp}}</b><br/></p>	
                        <p style="font-family:Arial, Helvetica, sans-serif; font-size:16px; line-height:1.6; color:#4d4d4d;">See you back on <b>NotePal !!!</b></p>
                      </td>
                    </tr>
                    <tr>
                      <td height="30">&nbsp;</td>
                    </tr>
                    </table>  
                  </td>
                  <td width="30">&nbsp;</td>
                </tr>
               </tbody>   
              </table>
            </td>
            <td width="50">&nbsp;</td>
          </tr>
          
        </table>
     </td>
    </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="35">&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50">&nbsp;</td>
                <td width="500">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td>
                          <p style="font-family:Arial, Helvetica, sans-serif; font-size:13px; line-height:1.3; color:#fff; text-align: center;" >This email has been sent from NotePal Team </p>
                          
                      </td>
                    </tr>
                    </table>
                  </td>
                <td width="50">&nbsp;</td>
              </tr>
            </table>
        </td>
        </tr>
        <tr>
          <td height="35">&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
