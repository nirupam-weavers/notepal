@extends('admin.layouts.admin-app')

@section('content')

<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Strand <small>Entry Form</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Strand</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row"> 
      <!-- left column -->
      <div class="col-md-12"> 
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add</h3>
            @if ($errors->any())
                  <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              @if (session()->has('message'))
                  <div class="alert alert-info alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      {{ session('message') }}
                  </div>
              @endif
              @if (session()->has('error_message'))
                  <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      {{ session('error_message') }}
                  </div>
              @endif
          </div>
          <!-- /.box-header --> 
          <!-- form start -->
          <form name="entry-form" method="post" action="{{route('admin.strand.store')}} " enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="box-body"> 
              <div class="form-group">
                <label for="parent_id">Select Grade</label>
                  <select  class="form-control" name="grade_id" data-placeholder="Subject Grade"  required>
                    <option value="0">Select Grade</option>
                    @if(!empty($grades))
                    @foreach($grades as $grade)
                    <option value="{{$grade->id}}" >Grade {{$grade->grade}}</option>
                    @endforeach
                    @endif
                  </select>
              </div>             
              <div class="form-group">
                <label for="parent_id">Select Subject</label>
                  <select  class="form-control" name="subject_id" data-placeholder="Subject Subject"  required>
                    <option value="0">Select Subject</option>
                    @if(!empty($subjects))
                    @foreach($subjects as $subject)
                    <option value="{{$subject->id}}" >{{$subject->subject}}</option>
                    @endforeach
                    @endif
                  </select>
              </div>
              <div class="form-group">
                <label for="name">Strand Name<a class='lnkred'>*</a></label>
                <input type="text" class="form-control" name="strand" value="" placeholder="strand Name *" id="strand_name">
              </div>
            </div>
            <!-- /.box-body -->
            
            <div class="box-footer">
              <input type="submit" class="btn btn-primary" value="Submit">
            </div>
          </form>
        </div>
        <!-- /.box --> 
        
      </div>
      <!--/.col (left) --> 
    </div>
    <!-- /.row --> 
  </section>
  <!-- /.content --> 
</div>

@endsection