<div class="lession-complt-frm-body">
<form action="{{route('unit.store')}}" method="POST" enctype="multipart/form-data">
  @csrf
  <div class="lession-complt-frm-top">
    <div class="row">
      <div class="col-lg-3">
          <div class="form-group">
            <label for="">Select Unit<span>*</span></label>
            <select class="form-control select2" id="unit-select" name="unit_id">
              <option value="">Select Unit</option>
              <option value="create-new-unit">Create new Unit</option>
              @foreach($units as $unit)
              <option value="{{$unit->id}}">{{$unit->name}}</option>
              @endforeach
            </select>
          </div>
      </div>
      <div class="col-lg-4">
        <div class="form-group">          
          <h3>OR</h3>
        </div>
      </div>
      <div class="col-lg-5">
        <div class="form-group" id="unit-new" style="display: none;">
            <label for="">Enter Unit Name<span>*</span></label>
              <input class="form-control" type="text" name="unit">
          </div>
      </div>
    </div>
  </div>
  <div class="lession-complt-frm-top">
    <h3>Add lesson to unit</h3>
    <div class="row">
      <div class="col-lg-3">
          <div class="form-group">
            <label for="">Grade <span>*</span></label>
            <select class="form-control" id="unit-grade-dropdown" name="grade">
              <option value="">Select Grade</option>
              @foreach(\App\Models\Grade::all() as $grade)
              <option value="{{$grade->id}}" @if(old('grade') !== null && ($grade->grade == old('grade'))) selected="selected" @endif>Grade {{$grade->grade}}</option>
              @endforeach
            </select>
          </div>
      </div>
      <div class="col-lg-4">
          <div class="form-group">
            <label for="">Subject <span>*</span></label>
            <select class="form-control" id="unit-subject-dropdown" name="subject">
              <option value="">Select Subject</option>
            </select>
          </div>
      </div>
      <div class="col-lg-5">
          <div class="form-group">
            <label for="">Strand <span>*</span></label>
            <select class="form-control" id="unit-strand-dropdown" name="strand">
              <option value="">Select Strand</option>
            </select>
          </div>
      </div>
    </div>
  </div>
  <div class="specific-exp">
    <h3>Specific Expectation(s) <span>*</span></h3>
    <div class="box-text">
      <select class="form-control select2" id="unit-expectation-dropdown" name="expectation[]" multiple="multiple" data-placeholder="Select Expectation" required>
      </select>
    </div>
  </div>
  <div class="single-box-file-wraper">
    <div class="single-box-file-wraper-head">
    <h3>Lesson Plan <span>*</span></h3>
    <p>Upload your lesson plan in any of the following format. Please note, your lesson plan should be divided into three sections- Minds On, Lesson, and Consolidation in order for it to be accepted. </p>
    </div> 
    <input type="hidden" name="unit_lesson" value="" id="unitlesson">
    @include('teacher.createunit.lessonplan')                      
  </div>
  <div class="single-box-file-wraper">
    <div class="single-box-file-wraper-head">
    <h3>Independent Practice <span>(Optional)</span></h3>
    <p>Upload your Independent Practice in any of the following format. </p>
    </div>
    @include('teacher.createunit.independentpractice')
  </div>
  <div class="single-box-file-wraper">
    <div class="single-box-file-wraper-head">
    <h3>Assessment <span>(Optional)</span></h3>
    <p>Upload your Assessment in any of the following format. </p>
    </div>
    @include('teacher.createunit.assessment')
  </div>
  <div class="create-lsn-btn">
    <ul>
      <li><a href="{{route('lesson.create')}}" class="btn inactive-btn">Discard</a></li>
      <li><button type="submit" class="btn yellow-btn">Create Lesson Plan</button></li>
    </ul>
  </div>
</form>
</div>