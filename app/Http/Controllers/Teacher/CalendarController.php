<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subject;
use App\Models\Booking;
use App\Models\User;

class CalendarController extends Controller
{
    public function index()
    {
    	$user_id = auth()->user()->id;
    	$users = User::where('role','!=','admin')->where('id','!=',$user_id)->get();
    	$subjects = Subject::all();
    	$pendding_statuses = Booking::where('user_id',$user_id)->orderBy('created_at','DESC')->get();
		$bookings = Booking::where('mentor_id',$user_id)->orderBy('created_at','DESC')->get();
    	return view('teacher.calendar',compact('subjects','users','bookings','pendding_statuses'));
    }
}
