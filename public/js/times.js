
function tConv24(time24) {
    var ts = time24;
    var H = +ts.substr(0, 2);
    var h = (H % 12) || 12;
    h = (h < 10) ? ("0" + h) : h;  // leading 0 at the left for 1 digit hours
    var ampm = H < 12 ? " am" : " pm";
    ts = h + ts.substr(2, 3) + ampm;
    return ts;
};

function parseTime(s) {
    var c = s.split(':');
    return parseInt(c[0]) * 60 + parseInt(c[1]);
}

function convertHours(mins) {
    var hour = Math.floor(mins / 60);
    var mins = mins % 60;
    var converted = pad(hour, 2) + ':' + pad(mins, 2);
    return converted;
}

function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

function times() {
    var start_time = parseTime("10:00"),
        end_time = parseTime("20:59"),
        interval = 30;
    var i, formatted_time;
    var time_slots = new Array();
    for (var i = start_time; i <= end_time; i = i + interval) {
        formatted_time = convertHours(i);
        time_slots.push(formatted_time);
    }
    var options = '<option value="">Select Time</option>';
    time_slots.forEach(i => {
        options += '<option value="' + i + '">' + tConv24(i) + '</option>';
    });
    return options;
}

const isToday = (myDate) => {
    let dt = myDate.split('-');
    let d = new Date();
    let ye = new Intl.DateTimeFormat('en', {timeZone: 'UTC', year: 'numeric' }).format(d);
    let mo = new Intl.DateTimeFormat('en', {timeZone: 'UTC', month: 'numeric' }).format(d);
    let da = new Intl.DateTimeFormat('en', {timeZone: 'UTC', day: 'numeric' }).format(d);
    // console.log(`${da}-${mo}-${ye}`);
    // console.log(dt);
    return dt[2] == parseInt(da) && dt[1] == parseInt(mo) && dt[0] == parseInt(ye) ;
}

function split_time(duration,myDate) {
    var fields = duration.split(':');  
    var start_time = parseTime("10:00"),
        end_time = parseTime("20:59"),
        interval = parseInt(fields[1]);  
    const today = isToday(myDate);
    if(today){
        let hr = new Intl.DateTimeFormat('en', {timeZone: 'UTC', hour: 'numeric' }).format(new Date());
        let min = new Intl.DateTimeFormat('en', {timeZone: 'UTC', minute: 'numeric' }).format(new Date());
        start_time = parseTime(`${hr}:${min}`);
    }
    //console.log(start_time);

    var i, formatted_time;
    var time_slots = new Array();
    for (var i = start_time; i <= end_time; i = i + interval) {
        formatted_time = convertHours(i);
        time_slots.push(formatted_time);
    }
    var options = '';
    time_slots.forEach(i => {
        options += '<button type="button" data-time="' + i + '" class="booking-time-each">' + tConv24(i) + '</button>';
    });
    return options;
}

function common_split_time(duration,myDate) {
    var fields = duration.split(':');  
    var start_time = parseTime("10:00"),
        end_time = parseTime("20:59"),
        interval = parseInt(fields[1]);  
    const today = isToday(myDate);
    if(today){
        let hr = new Intl.DateTimeFormat('en', {timeZone: 'UTC', hour: 'numeric' }).format(new Date());
        let min = new Intl.DateTimeFormat('en', {timeZone: 'UTC', minute: 'numeric' }).format(new Date());
        start_time = parseTime(`${hr}:${min}`);
    }
    //console.log(start_time);

    var i, formatted_time;
    var time_slots = new Array();
    for (var i = start_time; i <= end_time; i = i + interval) {
        formatted_time = convertHours(i);
        time_slots.push(formatted_time);
    }
    var options = '';
    time_slots.forEach(i => {
        options += '<button type="button" data-time="' + i + '" class="booking-time-each-common">' + tConv24(i) + '</button>';
    });
    return options;
}