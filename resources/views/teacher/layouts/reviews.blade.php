@foreach($reviews as $review)            
<div class="review-each">
  <div class="review-each-top">
    <div class="review-top-user">
      <div class="review-top-user-lt">{{$review->user->name[0]}}</div>
      <div class="review-top-user-rt">
        <h3>{{$review->user->name}} <span>{{$review->user->role}}</span></h3>
      </div>
    </div>
    <div class="review-top-date">
      <img src="{{asset('front/images/cal-icon.png')}}" alt="" /> <span>{{date("d-m-Y", strtotime($review->created_at))}}</span>
    </div>
  </div>
  <p>{{$review->feedback}}</p>
</div>
@endforeach