@extends('student.layouts.app')

@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('student.layouts.sidebar')
      <div class="dashbord-content-common ash-bg dashbord-parent-children">
          <div class="dashbord-content-common-pad">
            <div class="my-children-details"> 
                <div class="my-children-img-wraper-box public-profile-img">
                <div class="my-children-img-edit-box">
                  <div class="my-children-img">
                   @if($user->user_image)
                    <img  src="{{ asset('storage/user/'.$user->user_image) }}" />
                    @else
                    <img src="{{ asset('front/images/img3.png')}}" alt="">
                    @endif

                    <div class="public-profile-badge">
                    <img src="{{ asset('front/images/badge.png')}}" alt="">
                  </div>
                  </div>
                  <!-- <div class="my-children-img-edit">
                    <span><i class="fas fa-pencil-alt"></i></span>
                  </div> -->
                </div>
              </div>
                <div class="my-children-text-box public-profile-info">
                  <div class="row align-items-center">
                    <div class="col-lg-6">
                      <ul class="children-name-box">
                        <li>Name: <span>{{$user->name}}</span></li>
                        <li>Current Grade:{{$user->grade}}</li>
                      </ul>
                    </div>
                    <div class="col-lg-6">
                      <ul class="ext-rating">
                        <li><i class="fas fa-star"></i></li>
                        <li><i class="fas fa-star"></i></li>
                        <li><i class="fas fa-star"></i></li>
                        <li><i class="fas fa-star"></i></li>
                        <li><i class="fas fa-star"></i></li>
                        <li>4.9</li>
                      </ul>
                    </div>
                  </div>
                  <div class="children-bio">
                    <div class="children-bio-head">
                      <h3>Bio:</h3>
                    </div>
                    <div class="children-bio-body">
                      <p>{{$user->bio}}</p>
                    </div>
                  </div>
                  <ul class="children-btns">
                  <li><span class="flow-img"><img src="{{ asset('front/images/binaclr.png')}}" alt=""></span> Followers (100)</li>
                    <li><a href="" class="btn yellow-btn">Book Slot </a></li>
                  </ul>
                </div>
              </div>
            
        </div>
      @if($user->role == 'teacher' )
      <div class=" ash-bg dashbord-parent-children">
          <div class="dashbord-content-common-pad">
            <div class="student-independnt-practic-sec">
              <h2>Lessons:</h2>
              @foreach($lessons as $lesson)
              @foreach($lesson->documents as $document)
              @if($document->document_type == "lesson")
              <div class="student-independnt-practic-sngl-box">
                <div class="roted-box">
                  <h3>Grade: {{implode(',',$lesson->grade_lesson()->pluck('grade_id')->all())}}</h3>
                </div>
                <ul class="top-box">
                  <li><span>Subject:</span> {{$lesson->subject->subject}}</li>
                  <li><span>Strand:</span> {{$lesson->strand->strand}}</li>
                  <li><span>BY:</span> {{$lesson->user->name}} / {{ date('d-m-Y', strtotime($lesson->created_at))}}</li>
                </ul>
                <ul>
              @foreach($lesson->expectation_lesson as $exp)
              <li><p> {{$exp->expectation}}  </p></li>
              @endforeach
              </ul>
                <div class="student-independnt-btm">
                  <div class="row align-items-center">
                    <div class="col-lg-6">
                      <ul class="dnld-sec">
                        <li>
                          <ul class="star-rating">
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li>4.9</li>
                          </ul>
                        </li>
                        <li><span><i class="fas fa-download"></i></span> 100</li>
                        <li><span><i class="far fa-eye"></i></span> 100</li>
                      </ul>
                    </div>
                    <div class="col-lg-6">
                      <ul class="indepnd-action-box">
                        <li class="blue-box"><a href=""><i class="fas fa-share-alt"></i></a></li>
                        <li class="yellow-box"><a href=""><i class="fas fa-star"></i></a></li>
                        <li class="white-box"><a href="{{ url('teacher/single-lesson/'.$lesson->id) }}"><i class="fas fa-chevron-right"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              @endif
              @endforeach
              @endforeach
            </div>
            <div class="student-independnt-practic-sec">
              <h2>Independent Practice:</h2>
              @foreach($lessons as $lesson)
              @foreach($lesson->documents as $document)
              @if($document->document_type == "practice")
              <div class="student-independnt-practic-sngl-box">
                <div class="roted-box">
                  <h3>grade: {{implode(',',$lesson->grade_lesson()->pluck('grade_id')->all())}}</h3>
                </div>
                <ul class="top-box">
                  <li><span>Subject:</span> {{$lesson->subject->subject}}</li>
                  <li><span>Strand:</span> {{$lesson->strand->strand}}</li>
                  <li><span>BY:</span> {{auth()->user()->name}} / {{ date('d-m-Y', strtotime($lesson->created_at))}}</li>
                </ul>
                <ul>
              @foreach($lesson->expectation_lesson as $exp)
              <li><p> {{$exp->expectation}}  </p></li>
              @endforeach
              </ul>
                <div class="student-independnt-btm">
                  <div class="row align-items-center">
                    <div class="col-lg-6">
                      <ul class="dnld-sec">
                        <li>
                          <ul class="star-rating">
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li>4.9</li>
                          </ul>
                        </li>
                        <li><span><i class="fas fa-download"></i></span> 100</li>
                        <li><span><i class="far fa-eye"></i></span> 100</li>
                      </ul>
                    </div>
                    <div class="col-lg-6">
                      <ul class="indepnd-action-box">
                        <li class="blue-box"><a href=""><i class="fas fa-share-alt"></i></a></li>
                        <li class="yellow-box"><a href=""><i class="fas fa-star"></i></a></li>
                        <li class="white-box"><a href="{{ url('teacher/single-independentpractice/'.$lesson->id) }}"><i class="fas fa-chevron-right"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              @endif
              @endforeach
              @endforeach
            </div>
            <div class="student-independnt-practic-sec">
              <h2>Assessments:</h2>
              @foreach($lessons as $lesson)
              @foreach($lesson->documents as $document)
              @if($document->document_type == "assessment")
              <div class="student-independnt-practic-sngl-box">
                <div class="roted-box">
                  <h3>grade: {{implode(',',$lesson->grade_lesson()->pluck('grade_id')->all())}}</h3>
                </div>
                <ul class="top-box">
                  <li><span>Subject:</span> {{$lesson->subject->subject}}</li>
                  <li><span>Strand:</span> {{$lesson->strand->strand}}</li>
                  <li><span>BY:</span> {{auth()->user()->name}} / {{ date('d-m-Y', strtotime($lesson->created_at))}}</li>
                </ul>
                <ul>
              @foreach($lesson->expectation_lesson as $exp)
              <li><p> {{$exp->expectation}}  </p></li>
              @endforeach
              </ul>
                <div class="student-independnt-btm">
                  <div class="row align-items-center">
                    <div class="col-lg-6">
                      <ul class="dnld-sec">
                        <li>
                          <ul class="star-rating">
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li>4.9</li>
                          </ul>
                        </li>
                        <li><span><i class="fas fa-download"></i></span> 100</li>
                        <li><span><i class="far fa-eye"></i></span> 100</li>
                      </ul>
                    </div>
                    <div class="col-lg-6">
                      <ul class="indepnd-action-box">
                        <li class="blue-box"><a href=""><i class="fas fa-share-alt"></i></a></li>
                        <li class="yellow-box"><a href=""><i class="fas fa-star"></i></a></li>
                        <li class="white-box"><a href="{{ url('teacher/single-assessment/'.$lesson->id) }}"><i class="fas fa-chevron-right"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              @endif
              @endforeach
              @endforeach
            </div>
          </div>
        </div>
      @endif
      @endsection