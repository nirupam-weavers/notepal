@extends('teacher.layouts.app')
@section('content')
<div class="dashbord-common">
  <div class="container-fluid">
    <div class="dashbord-common-wraper">
      @include('teacher.layouts.sidebar') 
        <div class="dashbord-content-common ash-bg dashbord-parent-children">
          <div class="dashbord-content-common-pad">
            <div class="teacher-edit-profile">
              <form method="POST" action="{{ route('teacher.note.update') }}">
              @csrf
              <input type="hidden" name="id" value="{{$note->id}}">
                <div class="teacher-edit-profile-wraper">
                  <div class="teacher-edit-form">                 
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="form-group">
                            <label for="">Title</label>
                            <input type="text" class="form-control" value="{{$note->title}}" name="title" placeholder="Title">
                          </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                              <label for="">Description</label>
                              <textarea class="form-control" name="description" rows="3">{{$note->description}}</textarea>
                            </div>
                        </div>
                      </div>                 
                  </div>
                </div>
                <div class="submit-sec">
                    <button type="submit" class="btn yellow-btn">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection