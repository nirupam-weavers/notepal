<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class TeacherRegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        return view('auth.teacher.register');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function register(Request $request)
    {
        $data = $request->all();
        //dd($data);
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'unique:users', 'max:255','regex:/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/'],
            'password' => ['required', 'string', 'min:6'],
            'check' => ['required'],
        ];
        $messages = array(
          'name.required' => 'This field is required',
          'email.required' => 'This field is required',
          'password.required' => 'This field is required',
          'check.required' => 'Check this box',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->route('teacher.signup')->withInput()->withErrors($validator);
        }   

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'role' => 'teacher',
            'password' => Hash::make($data['password']),
        ]);

        $this->guard()->login($user);

        return redirect()->route('teacher.home');
    }
}
