@extends('admin.layouts.admin-app')


@section('content')
    <!-- Content Wrapper. Contains page content -->

<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Dashboard <small>Control panel</small> </h1>
  </section>
  
  <!-- Main content -->
  <section class="content"> 
    
    <div class="form-group">
        <h3 class="text-center">Welcome {{ Auth::user()->first_name.' '.Auth::user()->last_name }}</h3>
    </div>

    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><a href="{{route('list.user')}}"><i class="fa fa-users" aria-hidden="true"></i></a></span>
            <div class="info-box-content">
                <a href="{{route('list.user')}}"><span class="info-box-text"><strong>Total Members </strong></span>
              <span class="info-box-number" id="total-member"></span></a>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>
    </div>  
     
    
  </section>
  <!-- /.content --> 
</div>

@endsection

@section('script')
<script src="{{ asset('admin/dist/js/jquery.uploadfile.js')}}"></script>

<script>
 jQuery(document).ready(function(){
    jQuery.ajax({
        'url' : "{{ route('admin.ajax.get-dashboard-counts') }}",
        'method' : 'GET',
        success: function(response){
          if(response.member_count){
              jQuery('#total-member').html(response.member_count);
          }          
        }
    })
  });
</script>

@endsection


