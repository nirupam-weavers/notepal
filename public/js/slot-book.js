const wdays = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
var base_url = window.location.origin;
var teachertimes = base_url+"/"+"teacher/get-teacher-times";
var finduser = base_url+"/"+"teacher/find-user";
var findteacher = base_url+"/"+"teacher/find-teacher";
var slotbooking = base_url+"/"+"teacher/slot-booking";
var logic = function( currentDateTime ){
    this.setOptions({
       //minTime:'11:00',
       maxTime:'22:10',
     });
 };

 $('#slot-datepicker').datetimepicker({    
   inline:true,
   format:'Y-m-d',
   timepicker:false,
   //allowTimes:['12:00','13:00','15:00','17:00','17:05','17:20','19:00','20:00'],
//    minTime:'11:00',
//    maxTime:'22:00',
//    step: 30,
   // onGenerate:logic,
   // onChangeDateTime:logic,
   // onShow:logic,
   minDate:new Date(),
 });

 $('#filter').on('change', function() {
     $('#booking-time-wrp').html('');
     var search = this.value;
     $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
         }
     });
     $.ajax({
         url: findteacher,
         type: "POST",
         data: {
             search: search
         },
         cache: false,
         success: function(result){
           $('#teachers').html(result);
         }
     });        
   
 });

 var  timerId;
 var  searchBoxDom  =  document.getElementById('search');

 // This represents a very heavy method. Which takes a lot of time to execute
 function  makeAjaxCall() {
   var search = searchBoxDom.value;
     $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
         }
     });
     $.ajax({
         url: finduser,
         type: "POST",
         data: {
             search: search,
             usrrole: 'teacher',
         },
         cache: false,
         success: function(result){
           $('#teachers').html(result);
         }
     });       
 }

 // Debounce function: Input as function which needs to be debounced and delay is the debounced time in milliseconds
 var  debounceFunction  =  function (func, delay) {
   // Cancels the setTimeout method execution
   clearTimeout(timerId)
   // Executes the func after delay time.
   timerId  =  setTimeout(func, delay)
 }

 // Event listener on the input box
 searchBoxDom.addEventListener('input', function () {
   // Debounces makeAjaxCall method
   debounceFunction(makeAjaxCall, 500)
 });  

  $('#teachers').on('click', '.teachers-each-del', function (e) {
    e.preventDefault();
    $(this).closest('.teachers-each-search').remove();
  });  

  $('#teachers').on('click', '.teachers-each-search', function (e) {
    e.preventDefault();
    $('#booking-time-wrp').html('');
    var ele = $(this);   
    $('#teachers').find('.teachers-each-search').removeClass('teacher-active');
    ele.addClass('teacher-active');
    var user_id = ele.attr('data-id');
    var role = ele.attr('data-role');
    var date = $('#slot-datepicker').val();
    if(date !== ""){
      var dt = new Date(date);
      // var day = wdays[dt.getDay()];
      // var month = months[dt.getMonth()];
      // var day_dt = dt.getDate();
     let day_dt = new Intl.DateTimeFormat('en', {timeZone: 'UTC', day: 'numeric' }).format(dt);
     let month = new Intl.DateTimeFormat('en', {timeZone: 'UTC', month: 'long' }).format(dt);
     let day = new Intl.DateTimeFormat('en', {timeZone: 'UTC', weekday: 'long' }).format(dt);

      $('#show_dt').html(day+', '+month+' '+day_dt);
      var duration = $('#duration').val();
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: teachertimes,
        type: "POST",
        data: {
            user_id: user_id,
            day:day,
            day_date:date,
            duration:duration
        },
        cache: false,
        success: function(result){
          if(role == 'teacher'){
            $('#booking-time-wrp').html(result);
          }else{
            $('#booking-time-wrp').html(split_time(duration,date));            
          }
        }
      }); 
    }
  });

  $('#slot-datepicker').on('change', function(e){
    e.preventDefault();
    $('#booking-time-wrp').html('');
     var date = this.value;
     //var day = moment(date).tz("America/Los_Angeles");
     let dt = new Date(date);
     //let dt = d.toUTCString();
     let day_dt = new Intl.DateTimeFormat('en', {timeZone: 'UTC', day: 'numeric' }).format(dt);
     let month = new Intl.DateTimeFormat('en', {timeZone: 'UTC', month: 'long' }).format(dt);
     let day = new Intl.DateTimeFormat('en', {timeZone: 'UTC', weekday: 'long' }).format(dt);

      //console.log(ye,mo,da);

     // var day = wdays[dt.getDay()];
     // var month = months[dt.getMonth()];
     // var day_dt = dt.getDate();
     $('#show_dt').html(day+', '+month+' '+day_dt);
     var duration = $('#duration').val();
     var user_id = $('#teachers').find('.teacher-active').attr('data-id');
     var role = $('#teachers').find('.teacher-active').attr('data-role');
     if(typeof user_id === "undefined"){
      toastr.error('Pleade select teacher.');
     }else{
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: teachertimes,
        type: "POST",
        data: {
            user_id: user_id,
            day:day,
            day_date:date,
            duration:duration
        },
        cache: false,
        success: function(result){
          //$('#booking-time-wrp').html(result);
          if(role == 'teacher'){
            $('#booking-time-wrp').html(result);
          }else{
            $('#booking-time-wrp').html(split_time(duration,date));            
          }
        }
      });
     }       
  });

  $('#duration').on('change', function(e){
    e.preventDefault();
    var user_id = $('#teachers').find('.teacher-active').attr('data-id');
    var role = $('#teachers').find('.teacher-active').attr('data-role');
    var date = $('#slot-datepicker').val();
    if(typeof user_id === "undefined"){
      toastr.error('Pleade select teacher.');
     }else if(date == ""){
      toastr.error('Pleade select date.');
     }
      else{
        var dt = new Date(date);
        // var day = wdays[dt.getDay()];
        // var month = months[dt.getMonth()];
        // var day_dt = dt.getDate();
        let day_dt = new Intl.DateTimeFormat('en', {timeZone: 'UTC', day: 'numeric' }).format(dt);
        let month = new Intl.DateTimeFormat('en', {timeZone: 'UTC', month: 'long' }).format(dt);
        let day = new Intl.DateTimeFormat('en', {timeZone: 'UTC', weekday: 'long' }).format(dt);

        $('#show_dt').html(day+', '+month+' '+day_dt);
        var duration = $('#duration').val();
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url: teachertimes,
          type: "POST",
          data: {
              user_id: user_id,
              day:day,
              day_date:date,
              duration:duration
          },
          cache: false,
          success: function(result){
            if(role == 'teacher'){
              $('#booking-time-wrp').html(result);
            }else{
              $('#booking-time-wrp').html(split_time(duration,date));            
            }
          }
        }); 
      }
  });

  $('.booking-time-wrp').on('click', '.booking-time-each', function (e) {
    e.preventDefault();
    var ele = $(this);  
    var time = ele.attr('data-time');
    var time_txt = ele.text();
    $('.booking-time-wrp').find('.booking-time-confrm').remove();
    var t_row = '<div class="booking-time-confrm"><span> '+time_txt+'</span><button type="button" data-time="'+time+'" id="confirm" class="booking-confirm" data-dismiss="modal">Confirm</button></div>' 
    ele.after(t_row);
  });

  $('.booking-time-wrp').on('click', '#confirm', function (e) {
    e.preventDefault();
    var user_id = $('#teachers').find('.teacher-active').attr('data-id');
    var date = $('#slot-datepicker').val();
    var dt = new Date(date);
    //var day_for = wdays[dt.getDay()];
    let day_for = new Intl.DateTimeFormat('en', {timeZone: 'UTC', weekday: 'long' }).format(dt);
    var duration = $('#duration').val();
    var purpose = $('#purpose').val();
    var note = $('#note').val();
    //var subject = $('#subject').val();
    var start_time = $(this).attr('data-time');
    var flag = true;
    if(purpose == ""){
      flag = false;
      toastr.error('Purpose field is required.');
    }
    if(note == ""){
      flag = false;
      toastr.error('Note field is required.');
    }
    if(date == ""){
      flag = false;
      toastr.error('Pleade select date.');
    }

    if(flag){
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: slotbooking,
        type: "POST",
        data: {
            user_id:user_id,
            day_for:day_for,
            date_for:date,
            start_time:start_time,
            duration:duration,
            purpose:purpose,
            note:note,
            //subject:subject
        },
        cache: false,
        success: function(result){
          switch (result['msg']) {
            case 'success':
                $('#scheduleBooking').modal('hide');
                toastr.success('Booked successfully.');
                break;
            case 'error':
                toastr.error('Fields are required.');
                break;
          }
        }
      });
    }
  });