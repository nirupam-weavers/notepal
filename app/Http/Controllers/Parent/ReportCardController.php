<?php

namespace App\Http\Controllers\Parent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Grade;
use App\Models\Category;
use App\Models\Report;
use App\Models\Comment;
use App\Models\Marks;
use Illuminate\Support\Facades\Validator;

class ReportCardController extends Controller
{
    public function index(Request $request)
    {
        //dd($request->all());
        $learnings = [];
        $langs = [];
        $mrg_marks = [];
        $mrg_lang_marks = [];
        $lang_marks = [];
        $marks = [];$cnt1=0;$cnt2=0;$cnt3=0;$cnt4=0;
        $year = [];
        $frenches = [];
        $others = [];
        if(!empty($request->user_id)){ 
            $learning_skills = $this->get_mark_array('Learning Skill',$request->user_id);
            $marks = $learning_skills['lang_marks'];
            $cnt1 = $learning_skills['cnt'];
            $year = $learning_skills['year'];
            
            $languages = $this->get_mark_array('LANGUAGE',$request->user_id);
            $lang_marks = $languages['lang_marks'];
            $cnt2 = $languages['cnt'];
            //$year = $languages['year'];

            $french = $this->get_mark_array('FRENCH',$request->user_id);
            $frenches = $french['lang_marks'];
            $cnt3 = $french['cnt'];
            //$year = $french['year'];

            $other_sub = $this->get_mark_array('OTHER SUBJECT (S)',$request->user_id);
            $others = $other_sub['lang_marks'];
            $cnt4 = $other_sub['cnt'];
            //$year = $other_sub['year'];            
        }
        if($cnt1 >= $cnt2){
          $cols = $cnt1;
        }else{
          $cols = $cnt2;
        }
        $user=$request->user_id;
        $children = auth()->user()->children;
        $nse = Comment::pluck('name','value');
        //dd($marks);
        
        return view('parent.report-card.index',compact('children','marks','lang_marks','frenches','others','cols','year','user','nse'));
    }
    public function get_mark_array($sec,$user_id)
    {
        $langs = [];
        $mrg_lang_marks = [];
        $lang_marks = [];
        $marks = [];$cnt=0;
        $year = [];
        $data = [];
        $languages = Category::where('name','LIKE','%'.$sec.'%')->first();

        if(!empty($languages->get_children)){            
            foreach ($languages->get_children as $academic) {
               foreach ($academic->reports()->where('user_id',$user_id)->get() as $key => $value) {
                //if(!empty($value->progress_report)){
                    $langs[$value->year.'-'.$value->grade][$value->category->name][1] = (int)$value->progress_report.'|'.$value->grade;
                //}
                //if(!empty($value->term_1)){
                    $langs[$value->year.'-'.$value->grade][$value->category->name][2] = (int)$value->term_1.'|'.$value->grade;
                //}
                //if(!empty($value->term_2)){
                    $langs[$value->year.'-'.$value->grade][$value->category->name][3] = (int)$value->term_2.'|'.$value->grade;
                //}
                if(isset($langs[$value->year.'-'.$value->grade][$value->category->name]) && count($langs)>1 && $langs[$value->year.'-'.$value->grade][$value->category->name] != null){
                    ksort($langs[$value->year.'-'.$value->grade][$value->category->name]);
                }
               }
            }
        }

        if(count($langs)>0){
            foreach ($langs as $year_grade => $achievement) {
                $year[] = $year_grade;
                $grade = explode('-', $year_grade)[1];
                $zero = 0;
                foreach ($achievement as $key => $val) {
                    if(!isset($val[1])){
                        $val[1] = $zero.'|'.$grade;
                    }
                    if(!isset($val[2])){
                        $val[2] = $zero.'|'.$grade;
                    }
                    if(!isset($val[3])){
                        $val[3] = $zero.'|'.$grade;
                    }
                    ksort($val);
                    $mrg_lang_marks[$key][] = $val;
                } 
                $cnt++;           
            }
        }
        if(!empty($mrg_lang_marks)){
            foreach ($mrg_lang_marks as $sub => $mrg_mark) {
                $mark = array_merge_recursive(...$mrg_mark);
                $m = implode(',',$mark);
                $lang_marks[$sub] = $m;
            }
        }
        $data['lang_marks'] = $lang_marks;
        $data['year'] = $year;
        $data['cnt'] = $cnt;
       
        return $data;
    }
    public function add_report_card()
    {
        $grades = Grade::all();
        $marks = Marks::all();
        $learning_skills = Category::find(1);
        $academics = Category::find(2);
        $children = auth()->user()->children;
        return view('parent.report-card.add',compact('grades','marks','learning_skills','academics','children'));
    }
    public function edit_report_card()
    {
        $grades = Grade::all();
        $children = auth()->user()->children;
        $marks = Marks::all();
        return view('parent.report-card.edit',compact('grades','children','marks'));
    }
    public function update_report_card(Request $request)
    {
        $rules = [
            'user_id' => ['required'],
            'year' => ['required'],
            'grade' => ['required'],
            'type' => ['required'],
         ];
        $messages = array(
          'user_id.required' => 'Select a student',
          'year.required' => 'Year field is required',
          'grade.required' => 'Grade field is required',
          'type.required' => 'Report field is required',
        );
        $validator = Validator::make( $request->all(), $rules, $messages );

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $report = Report::where([
            'user_id' => $request->user_id,
            'year' => $request->year,
            'grade' => $request->grade,
        ]);
        switch ($request->type) {
            case 'progress_report':
                $report = $report->where('progress_report','!=',0)->pluck('progress_report','category_id');
                break;
            case 'term_1':
                $report = $report->where('term_1','!=',0)->pluck('term_1','category_id');
                break;
            case 'term_2':
                $report = $report->where('term_2','!=',0)->pluck('term_2','category_id');
                break;
        }
        //dd($report[13]);

        $grades = Grade::all();
        $marks = Marks::all();
        $learning_skills = Category::find(1);
        $academics = Category::find(2);
        $children = auth()->user()->children;
        return view('parent.report-card.update',compact('grades','marks','learning_skills','academics','children','report'));
    }
    // public function report_post(Request $request)
    // {
    //     if(!empty($request->comment)){
    //         $rules = [
    //             'user_id' => ['required'],
    //             'year' => ['required'],
    //             'grade' => ['required'],
    //             'type' => ['required'],
    //          ];
    //         $messages = array(
    //           'user_id.required' => 'Select a student',
    //           'year.required' => 'Year field is required',
    //           'grade.required' => 'Grade field is required',
    //           'type.required' => 'Report field is required',
    //         );
    //         $validator = Validator::make( $request->all(), $rules, $messages );
    
    //         if($validator->fails()) {
    //             return redirect()->back()->withInput()->withErrors($validator);
    //         }
    //         foreach($request->comment as $category_id => $marks){
    //             $report = new Report;
    //             $report->user_id = $request->user_id;
    //             $report->year = $request->year;
    //             $report->grade = $request->grade;
    //             $report->category_id = $category_id;
    //             switch ($request->type) {
    //                 case 'progress_report':
    //                     $report->progress_report = (int)$marks;
    //                 break;
    //                 case 'term_1':
    //                     $report->term_1 = (int)$marks;
    //                 break;
    //                 case 'term_2':
    //                     $report->term_2 = (int)$marks;
    //                 break;
    //             }
    //             $report->save();
    //         }
    //     }

    //     return redirect()->back()->with('success', 'Success! Report Card Created');
    // }

    public function report_update(Request $request)
    {
        if(!empty($request->comment)){
            $rules = [
                'user_id' => ['required'],
                'year' => ['required'],
                'grade' => ['required'],
                'type' => ['required'],
             ];
            $messages = array(
              'user_id.required' => 'Select a student',
              'year.required' => 'Year field is required',
              'grade.required' => 'Grade field is required',
              'type.required' => 'Report field is required',
            );
            $validator = Validator::make( $request->all(), $rules, $messages );
    
            if($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }
            foreach($request->comment as $category_id => $marks){
                $report = Report::where([
                    'user_id' => $request->user_id,
                    'year' => $request->year,
                    'grade' => $request->grade,
                    'category_id' => $category_id,
                ])->first();
                
                if(!empty($report)){                    
                    switch ($request->type) {
                        case 'progress_report':
                            $report->progress_report = (int)$marks;
                        break;
                        case 'term_1':
                            $report->term_1 = (int)$marks;
                        break;
                        case 'term_2':
                            $report->term_2 = (int)$marks;
                        break;
                    }
                }else{
                    $report = new Report;
                    $report->user_id = $request->user_id;
                    $report->year = $request->year;
                    $report->grade = $request->grade;
                    $report->category_id = $category_id;
                    switch ($request->type) {
                        case 'progress_report':
                            $report->progress_report = (int)$marks;
                        break;
                        case 'term_1':
                            $report->term_1 = (int)$marks;
                        break;
                        case 'term_2':
                            $report->term_2 = (int)$marks;
                        break;
                    }
                }
                
                $report->save();
            }
        }
       //return $this->index($request);
        return redirect()->route('parent.report.card',['user_id' => $request->user_id])->with('success', 'Success! Report Card Updated');
    }
}
