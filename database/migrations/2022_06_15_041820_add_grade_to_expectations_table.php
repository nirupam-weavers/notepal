<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGradeToExpectationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expectations', function (Blueprint $table) {
            $table->integer('grade_id');
            $table->integer('subject_id');
            $table->integer('strand_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expectations', function (Blueprint $table) {
            $table->dropColumn('grade_id');
            $table->dropColumn('subject_id');
            $table->dropColumn('strand_id');
        });
    }
}
