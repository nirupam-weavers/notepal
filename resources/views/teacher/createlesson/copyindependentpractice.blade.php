<div class="single-box-file-box">
  <ul>
    <li>
      <div class="single-box-file-area select-box">
        <div class="box-icon" data-toggle="modal" data-target="#practice-ms-file">
          <img src="{{ asset('front/images/icon3.png')}}" alt="">
        </div>
        <div class="icon-text">
          <h6>Microsoft Doc</h6>
          @foreach($lesson->documents as $document)
          @if($document->document_type == 'practice')
          @if($document->file_type == 'msword')
          <input type="hidden" value="{{$document->document}}" name="practice_ms_file" class="form-control">
          <span>{{$document->document}}</span>
          @endif
          @endif
          @endforeach
          <span id="practice_ms_file"></span>
        </div>
      </div>
    </li>
    <li>
      <div class="single-box-file-area">
        <div class="box-icon" data-toggle="modal" data-target="#practice-google-doc">
          <img src="{{ asset('front/images/icon7.png')}}" alt="">
        </div>
        <div class="icon-text">
          <h6>Google Doc</h6>
          @foreach($lesson->documents as $document)
          @if($document->document_type == 'practice')
          @if($document->file_type == 'google_doc_link')
          <input type="hidden" value="{{$document->document}}" name="practice_google_doc_link" class="form-control">
          <span>{{$document->document}}</span>
          @endif
          @endif
          @endforeach
          <span id="practice_google_doc_link"></span>
        </div>
      </div>
    </li>
    <li>
      <div class="single-box-file-area">
        <div class="box-icon" data-toggle="modal" data-target="#practice-google-form">
          <img src="{{ asset('front/images/icon4.png')}}" alt="">
        </div>
        <div class="icon-text">
          <h6>Google Form</h6>
          @foreach($lesson->documents as $document)
          @if($document->document_type == 'practice')
          @if($document->file_type == 'google_form_link')
          <input type="hidden" value="{{$document->document}}" name="practice_google_form_link" class="form-control">
          <span>{{$document->document}}</span>
          @endif
          @endif
          @endforeach
          <span id="practice_google_form_link"></span>
        </div>
      </div>
    </li>
    <li>
      <div class="single-box-file-area">
        <div class="box-icon" data-toggle="modal" data-target="#practice-google-slide">
          <img src="{{ asset('front/images/icon8.png')}}" alt="">
        </div>
        <div class="icon-text">
          <h6>Google Slide</h6>
          @foreach($lesson->documents as $document)
          @if($document->document_type == 'practice')
          @if($document->file_type == 'google_slide_link')
          <input type="hidden" value="{{$document->document}}" name="practice_google_slide_link" class="form-control">
          <span>{{$document->document}}</span>
          @endif
          @endif
          @endforeach
          <span id="practice_google_slide_link"></span>
        </div>
      </div>
    </li>
    <li>
      <div class="single-box-file-area">
        <div class="box-icon" data-toggle="modal" data-target="#practice-pdf">
          <img src="{{ asset('front/images/icon5.png')}}" alt="">
        </div>
        <div class="icon-text">
          <h6>PDF File</h6>
          @foreach($lesson->documents as $document)
          @if($document->document_type == 'practice')
          @if($document->file_type == 'pdf')
          <input type="hidden" value="{{$document->document}}" name="practice_pdf" class="form-control">
          <span>{{$document->document}}</span>
          @endif
          @endif
          @endforeach
          <span id="practice_pdf_file"></span>
        </div>
      </div>
    </li>
    <li>
      <div class="single-box-file-area">
        <div class="box-icon" data-toggle="modal" data-target="#practice-video">
          <img src="{{ asset('front/images/icon6.png')}}" alt="">
        </div>
        <div class="icon-text">
          <h6>Video File</h6>
          @foreach($lesson->documents as $document)
          @if($document->document_type == 'practice')
          @if($document->file_type == 'video_link')
          <input type="hidden" value="{{$document->document}}" name="practice_video_link" class="form-control">
          <span>{{$document->document}}</span>
          @endif
          @endif
          @endforeach
          <span id="practice_video_link"></span>
        </div>
      </div>
    </li>
  </ul>
</div>

<div class="parent-add-children-form-modal">
  <!-- Modal -->
  <div class="modal fade" id="practice-ms-file" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="children-form-add">
            <h2>Add Your File</h2>
            <div class="form-group">
              <input type="file" onchange="myChangeFunction(this)" name="practice_ms_file" class="form-control" id="status" accept=".doc, .docx">
            </div>
            <button type="button" class="btn" data-dismiss="modal">Add Now</button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--practice-google-doc -->
  <div class="modal fade" id="practice-google-doc" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="children-form-add">
            <h2>Add Your Google doc Link</h2>
            <div class="form-group">
              <input type="url" onchange="myChangeFunction(this)" name="practice_google_doc_link" class="form-control" id="status">
            </div>
            <button type="button" class="btn" data-dismiss="modal">Add Now</button>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!--practice-google-form -->
  <div class="modal fade" id="practice-google-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="children-form-add">
            <h2>Add Your Google form link</h2>
            <div class="form-group">
              <input type="url" onchange="myChangeFunction(this)" name="practice_google_form_link" class="form-control" id="status">
            </div>
            <button type="button" class="btn" data-dismiss="modal">Add Now</button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- practice-google-slide -->
  <div class="modal fade" id="practice-google-slide" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="children-form-add">
            <h2>Add Your Google Slide Link</h2>
            <div class="form-group">
              <input type="url" onchange="myChangeFunction(this)" name="practice_google_slide_link" class="form-control" id="status">
            </div>
            <button type="button" class="btn" data-dismiss="modal">Add Now</button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- practice-pdf -->
  <div class="modal fade" id="practice-pdf" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="children-form-add">
            <h2>Add Your PDF File</h2>
            <div class="form-group">
              <input type="file" onchange="myChangeFunction(this)" name="practice_pdf_file" class="form-control" id="status" accept=".pdf">
            </div>
            <button type="button" class="btn" data-dismiss="modal">Add Now</button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- practice-video -->
  <div class="modal fade" id="practice-video" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="children-form-add">
            <h2>Add Your Video Link</h2>
            <div class="form-group">
              <input type="url" onchange="myChangeFunction(this)" name="practice_video_link" class="form-control" id="status">
            </div>
            <button type="button" class="btn" data-dismiss="modal">Add Now</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>