<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Note;

class NoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user_id = Auth::id();
        $notes = Note::where('user_id',$user_id);

        if(!empty($request->search)){
            $keyword = $request->search;
            $notes = $notes->where('title', 'LIKE', "%".$keyword."%");
        }

        $notes = $notes->paginate(10);
        return view('teacher.note.list',compact('notes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('teacher.note.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'description' => 'required|max:555',
        ]);
        $user_id = Auth::id();
        $note = new Note;
        $note->user_id = $user_id;
        $note->title = $request->title;
        $note->description = $request->description;
        $note->save();

        return redirect()->route('teacher.notes')->with('success', 'Note saved successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $note = Note::findOrFail($id);
        return view('teacher.note.edit',compact('note'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'description' => 'required|max:555',
        ]);

        $note = Note::findOrFail($request->id);
        $note->title = $request->title;
        $note->description = $request->description;
        $note->save();

        return redirect()->route('teacher.notes')->with('success', 'Note updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $note = Note::findOrFail($id);
        $note->delete();

        return redirect()->route('teacher.notes')->with('success', 'Note deleted successfully!');
    }
}
