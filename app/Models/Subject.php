<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use HasFactory;

     /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'subject',
     ];

    public function grade_subjects()
    {
        return $this->belongsToMany(Grade::class, 'grade_subjects','subject_id', 'grade_id');
    }
}
