function menu_open() {
    jQuery(".main-menu").css({"transform":"translateX(0)"}) 
}
function menu_close() { 
    jQuery(".main-menu").css({"transform":"translateX(-350px)"})
}

jQuery(window).scroll(function(){
  if (jQuery(window).scrollTop() >= 100) {
    jQuery('header').addClass('fixed');
   }
   else {
    jQuery('header').removeClass('fixed');
   }
});

jQuery(document).ready(function(){
    jQuery('.testim-sldr').owlCarousel({
    loop:true,
    autoplay:true,
    margin:30,
    nav:true,
    navText: ["<img src='images/lt-arw.svg'>","<img src='images/rt-arw.svg'>"],
    dots:false,
    responsive:{
        0:{
            items:1,
            dots:true,
            nav:false
        },
        600:{
            items:1,
            dots:true,
            nav:false
        },
        1000:{
            items:1
        }
    }
});
});

jQuery(document).ready(function($) {
    jQuery('.counter').counterUp({
        delay: 10,
        time: 500
    });
});


