@extends('admin.layouts.admin-app')

@section('content')
 <!-- Content Wrapper. Contains page content -->

<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Strand <small>Strand List</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Strand</a></li>
      <li class="active">Strand List</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Strand List</h3>
            <a href="{{route('admin.strand.create')}}" class="btn btn-primary a-btn-slide-text" style="float:right"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <span><strong>Add</strong></span> </a> 
            </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="category_table" class="table table-bordered table-hover">
              <thead>
                <tr>
                   <th><b>Grade</b></th>
                   <th><b>Subject</b></th>
                   <th><b>strand</b></th>
                   <th align="center">Action</th>
                </tr>
              </thead>
              <tbody>
               @if(!empty($strands))
               @php $i=1; @endphp
                 @foreach($strands as $strand)                 
                  <tr>
                   <td> 
                    @if($strand->subject->grade_subjects())
                      @php $i=0; $item = $strand->subject->grade_subjects()->count(); @endphp
                      @foreach($strand->subject->grade_subjects()->get() as $key => $value)  
                      @php $i++; @endphp
                      Grade {{$value->grade}}@if($i < $item) {{','}} @endif
                      @endforeach
                    @endif
                   </td>
                    <td>{{$strand->subject->subject}}</td>
                   <td>{{$strand->strand}}</td>
                  <td><a class="btn btn-primary" href="{{ url('admin/strand-edit/'.$strand->id) }}">Edit</a>
                    <a class="btn btn-danger" href="{{ url('admin/strand-delete/'.$strand->id) }}" onclick="return confirm('Do you want to remove?')">Delete</a>
                  </td>
                 </tr>
                 @php $i++; @endphp
                 @endforeach
               @endif
             
              </tbody>
              
            </table>
          </div>
          <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
      <!-- /.col --> 
    </div>
    <!-- /.row --> 
  </section>
  <!-- /.content --> 
</div>
<!-- /.content-wrapper -->
@endsection

 