<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subject;
use App\Models\Booking;
use App\Models\User;
use App\Models\Notification;
use App\Models\NotificationSetting;

class NotificationController extends Controller
{
    public function index()
    {
    	$user_id = auth()->user()->id;
    	$notifications = Notification::where('receiver_id',$user_id)
                        ->where('read',0)
                        ->get();
        $notificationsstatus = NotificationSetting::where('user_id',$user_id)
        ->first(); 
    	return view('student.student-notification',compact('notifications','notificationsstatus'));
    }
}
