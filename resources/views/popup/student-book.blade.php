<!-- Schedule a Booking Modal -->
<div class="modal fade scheduleBooking" id="studentBooking" tabindex="-1" role="dialog" aria-labelledby="studentBooking"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
        <span aria-hidden="true"><img src="{{asset('front/images/cross.svg')}}" alt="" /> </span>
      </button>

      <div class="modal-body">       

        <div class="row">
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 booking-search-pop"> 
              <div class="teachers-each-search book-teachers-each" id="showuser">
              </div>     
              <div class="book-teacher-btm">
                  <div class="duration-wrp">
                    <img src="{{asset('front/images/clock.svg')}}" alt="" />
                    <select id="student-duration">
                      <option value="00:15">15 min meeting</option>
                      <option value="00:30">30 min meeting</option>
                      <option value="00:45">45 min meeting</option>
                      <option value="00:60">60 min meeting</option>
                    </select>
                  </div> 

                  <div class="purpose">              
                    <textarea id="student-purpose" cols="50" rows="5" placeholder="Purpose"></textarea>
                  </div>
                  <div class="note">              
                    <textarea id="student-note" cols="50" rows="5" placeholder="Note"></textarea>
                  </div> 
                </div>  
          </div>
          <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 booking-cal-pop">
            <h2>Schedule Booking <span>(Select a date and time)</span></h2> 

            <div class="booking-cal-inn">
              <input type="text" id="student-slot-datepicker"/>
            </div>                    
          </div>
          <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 booking-time-pop">
            <p id="student-show-dt"></p>
            <div class="booking-time-wrp" id="student-booking-time-wrp">
            </div>
          </div>        
          
        </div>

      </div>

    </div>
  </div>
</div>