@extends('layouts.app')

@section('content')
<header class="main-header">
    <div class="container-fluid header-row">
      <div class="logo">
        <a href="#"><img src="{{ asset('front/images/logo.png') }}" alt=""></a>
      </div>
      <div class="login-btn">
      <a href="{{route('teacher.signup')}}" class="btn">Sign Up</a>
      </div>
    </div>
</header>

<div class="parents-signup-page teacher-signup-page">
  <div class="container-fluid">
    <div class="row full-hight">
      <div class="col-lg-6">
        <div class="parent-signup-frm">
            <h3>Sign In</h3>
            <form method="POST" name="login" action="{{ route('teacher.login.post') }}">
            @csrf
                <div class="form-group">
                  <label for="email">E-mail</label>
                  <input type="email" name="email" value="{{ old('email') }}" class="form-control" id="" aria-describedby="" placeholder="john.doe@gmail.com">
                  @error('email')
                      <span role="alert" class="text-danger">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                </div>
                  <div class="form-group">
                    <label for="password">Password</label>
                    <input id="pass_log_id" type="password" name="password" class="form-control" id="" placeholder="**************">
                    <div class="eye-icon">
                      <i class="far fa-eye toggle-password"></i>
                    </div>
                    @error('password')
                        <span role="alert" class="text-danger">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  @if (Route::has('parent.password.form'))
                        <a class="" href="{{ route('teacher.password.form') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                  @endif
                  <div class="login-btn">
                    <button type="submit" class="btn yellow-btn">
                        {{ __('Sign In') }}
                    </button>
                  </div>
            </form>
            <div class="anothe-signup">
              <p>Or, Continue with</p>
            </div>
            <div class="media-signup">
              <ul>
                <li><a href="{{ url('redirect/google/teacher') }}" class="google-signup">
                  <span><img src="{{ asset('front/images/ggl.png')}}" alt=""></span>
                  Google
                </a></li>
                 <li><a href="{{ url('microsoft/signin/teacher') }}" class="micro-signup">
                   <span><img src="{{ asset('front/images/micro.png')}}" alt=""></span>
                   Microsoft
                 </a></li>
              </ul>
            </div>
        </div>
      </div>
      <div class="col-lg-6 yellow-bg reletive-div">
        <div class="parent-signup-text">
          <h3>Platform for teachers</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, </p>
          <p>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
        </div>
        <div class="overlap-img">
            <img src="{{ asset('front/images/teacher-login.png')}}" alt="">
        </div>
      </div>
    </div>
  </div>
</div>
<script src="{{ asset('js/login.js')}}"></script>
@endsection
